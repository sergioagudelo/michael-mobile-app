package com.lendingpoint.customerportal;

import androidx.test.espresso.ViewInteraction;
import androidx.test.rule.ActivityTestRule;

import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import tools.fastlane.screengrab.Screengrab;
import tools.fastlane.screengrab.UiAutomatorScreenshotStrategy;
import tools.fastlane.screengrab.locale.LocaleTestRule;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withContentDescription;
import static org.hamcrest.Matchers.allOf;

@RunWith(JUnit4.class)
public class MainActivityTest {
    @ClassRule
    public static final LocaleTestRule localeTestRule = new LocaleTestRule();

    @Rule
    public ActivityTestRule<SplashActivity> mActivityTestRule = new ActivityTestRule<>(SplashActivity.class);

    @BeforeClass
    public static void beforeAll() {
        Screengrab.setDefaultScreenshotStrategy(new UiAutomatorScreenshotStrategy());
    }

    @Test
    public void testTakeScreenshot() {
//        EspressoViewFinder.INSTANCE.waitForDisplayed(withContentDescription(is("ForgotPasswordBtn")));

        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Login
        ViewInteraction ForgotPasswordBtn = onView(
                allOf(withContentDescription("ForgotPasswordBtn"), isDisplayed()));

        ForgotPasswordBtn.check(matches(isDisplayed()));

        Screengrab.screenshot("00Login");

        ForgotPasswordBtn.perform(click());

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        // ForgotPassword
        ViewInteraction ForgotPasswordFormSubmitBtn = onView(
                allOf(withContentDescription("ForgotPasswordFormSubmitBtn"), isDisplayed()));

        ForgotPasswordFormSubmitBtn.check(matches(isDisplayed()));

        Screengrab.screenshot("01ForgotPassword");

        ForgotPasswordFormSubmitBtn.perform(click());

        try {
            Thread.sleep(2500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        // ForgotPassword Confirm Modal
        ViewInteraction ForgotPasswordConfirmBtn = onView(
                allOf(withContentDescription("ForgotPasswordConfirmBtn"), isDisplayed()));

        ForgotPasswordConfirmBtn.perform(click());

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        // ResetPassword
        ViewInteraction ResetPasswordFormSubmitBtn = onView(
                allOf(withContentDescription("ResetPasswordFormSubmitBtn"), isDisplayed()));

        ResetPasswordFormSubmitBtn.check(matches(isDisplayed()));

        Screengrab.screenshot("02ResetPassword");

        ResetPasswordFormSubmitBtn.perform(click());

        try {
            Thread.sleep(2500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        // Back in Login
        ViewInteraction LoginFormSubmitBtn = onView(
                allOf(withContentDescription("LoginFormSubmitBtn"), isDisplayed()));

        LoginFormSubmitBtn.check(matches(isDisplayed()));

        LoginFormSubmitBtn.perform(click());

        try {
            Thread.sleep(2500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        // SecureAccount
        ViewInteraction SecureAccountBtn = onView(
                allOf(withContentDescription("SecureAccountBtn"), isDisplayed()));

        SecureAccountBtn.check(matches(isDisplayed()));

        SecureAccountBtn.perform(click());

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        // VerificationCode
        ViewInteraction VerificationCodeForm = onView(
                allOf(withContentDescription("VerificationCodeForm"), isDisplayed()));

        VerificationCodeForm.check(matches(isDisplayed()));

//        onView(allOf(withContentDescription("VerificationCodeInput_0"), isAssignableFrom(EditText.class))).perform(click(), replaceText("1"));
//        onView(allOf(withContentDescription("VerificationCodeInput_1"), isAssignableFrom(ReactEditText.class))).perform(click(), replaceText("2"));
//        onView(allOf(withContentDescription("VerificationCodeInput_2"), isAssignableFrom(ReactEditText.class))).perform(click(), replaceText("3"));
//        onView(allOf(withContentDescription("VerificationCodeInput_3"), isAssignableFrom(ReactEditText.class))).perform(click(), replaceText("4"), ViewActions.closeSoftKeyboard());
//
//        try {
//            Thread.sleep(1000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

        ViewInteraction VerificationCodeFormSubmitBtn = onView(
                allOf(withContentDescription("VerificationCodeFormSubmitBtn"), isDisplayed()));

        VerificationCodeFormSubmitBtn.check(matches(isDisplayed()));

        VerificationCodeFormSubmitBtn.perform(click());

        try {
            Thread.sleep(2500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        // Loans
        ViewInteraction LoanDetailsBtn = onView(
                allOf(withContentDescription("LoanDetailsBtn_LAI-00009779"), isDisplayed()));

        LoanDetailsBtn.check(matches(isDisplayed()));

        Screengrab.screenshot("04Loans");

        LoanDetailsBtn.perform(click());

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        // LoanDetails
        ViewInteraction LoanDetailsScreen = onView(
                allOf(withContentDescription("LoanDetailsScreen"), isDisplayed()));

        LoanDetailsScreen.check(matches(isDisplayed()));

        Screengrab.screenshot("05LoanDetails");
    }
}
