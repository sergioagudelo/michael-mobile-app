fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew cask install fastlane`

# Available Actions
## Android
### android test
```
fastlane android test
```
Runs all the tests
### android build_for_screengrab
```
fastlane android build_for_screengrab
```
Build debug and test APK for screenshots
### android screenshots
```
fastlane android screenshots
```
Generate new localized screenshots
### android verify
```
fastlane android verify
```
Verify next release
### android build
```
fastlane android build
```
Build an android release
### android post_deploy
```
fastlane android post_deploy
```
Generates release notes for slack and create the next tag
### android internal
```
fastlane android internal
```
Build a new internal version (for internal testing purposes)
### android beta
```
fastlane android beta
```
Build a new private beta version (with private beta testers)
### android production
```
fastlane android production
```
Build a new production version

----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
