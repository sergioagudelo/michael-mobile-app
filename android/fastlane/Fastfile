# This file contains the fastlane.tools configuration
# You can find the documentation at https://docs.fastlane.tools
#
# For a list of all available actions, check out
#
#     https://docs.fastlane.tools/actions
#
# For a list of all available plugins, check out
#
#     https://docs.fastlane.tools/plugins/available-plugins
#

# Uncomment the line if you want fastlane to automatically update itself
# update_fastlane

default_platform(:android)

platform :android do
  desc "Runs all the tests"
  lane :test do
    gradle(task: "test")
  end

  desc "Build debug and test APK for screenshots"
  lane :build_for_screengrab do
    gradle(
      task: 'clean'
    )
    gradle(
      task: 'assemble',
      build_type: 'Debug',
    )
    gradle(
      task: 'assemble',
      build_type: 'AndroidTest',
    )
  end

  desc "Generate new localized screenshots"
  lane :screenshots do
    # Prepare builds for Automatic UI Tests
    build_for_screengrab
    # Run UI Tests and take screenshots
    capture_android_screenshots
    # Upload new screenshots to google play
    supply
  end

  desc "Verify next release"
  lane :verify do |options|
    # Git status has to be clean
    ensure_git_status_clean
    # if the build is run on local machine
    if !is_ci? then
      # it sends slack message about the deploy was triggered
      # slack(message: "#{title} deploy was triggered on local machine")
      UI.message("#{options[:title]} deploy was triggered on local machine")
      # Clean project
      gradle(task: "clean")
      # yarn lint && yarn jest && yarn flow
      # check_code_quality
    else
      # We don't need to do this on CI because repo is fresh
      # slack(message: "#{options[:title]} deploy was triggered on CircleCI")
      UI.message("#{options[:title]} deploy was triggered on CircleCI")
    end

    # Check if there is any change since last version
    is_releaseable = analyze_commits(
      match: options[:tag_prefix],
      releases: {
        fix: 'patch',
        feat: 'minor',
        'BREAKING CHANGE': 'major'
      }
    )

    unless is_releaseable
      # slack(message: "Skip deploying #{options[:title]}. No changes since last one!")
      UI.important("Skip deploying #{options[:title]}. No changes since last one!")
    end

    is_releaseable
  end

  desc "Build an android release"
  lane :build do |options|
    next_version = lane_context[SharedValues::RELEASE_NEXT_VERSION]
    ensure_git_status_clean
    # Increment version in Android project
    version_code = gradle(task: "-q incrementVersionCode", properties: { 'version_number' => next_version })
    # Build app
    gradle(task: "bundle", build_type: 'Release')
    # Return new version code
    version_code.lines.last.to_i
  end

  desc "Generates release notes for slack and create the next tag"
  lane :post_deploy do |options|
    flavor = options[:flavor]
    title = options[:title]
    next_version = lane_context[SharedValues::RELEASE_NEXT_VERSION]
    build_number = options[:version_code]

    # Get release notes since last version for slack
    notes = conventional_changelog(title: title, format: 'slack')
    git_commit(path: ['./gradle.properties'], message: 'chore: android version bump')
    # Create tag to recognize future "last version" (the current version)
    add_git_tag(tag: "android/#{flavor}/#{next_version}/#{build_number}")
    push_git_tags

    # slack(message: notes)
    UI.important(notes)
  end

  desc "Build a new internal version (for internal testing purposes)"
  lane :internal do |options|
    flavor = 'internal'
    tag_prefix = 'android/internal*'
    title = 'Android Internal'
    app_flavor = 'Dev'
    track = 'internal'
    package_name = 'com.lendingpoint.customerportal'

    # Verify next release
    next unless verify(title: title, tag_prefix: tag_prefix)
    # Build android app
    version_code = build(flavor: app_flavor)
    # And deploy it to the store
    supply(track: track, package_name: package_name, skip_upload_apk: true)
    # Notify slack and create the tag
    post_deploy(flavor: flavor, title: title, version_code: version_code)
  end

  desc "Build a new private beta version (with private beta testers)"
  lane :beta do |options|
    flavor = 'beta'
    tag_prefix = 'android/beta*'
    title = 'Android Beta'
    app_flavor = 'Staging'
    track = 'alpha'
    package_name = 'com.lendingpoint.customerportal'

    # Verify next release
    next unless verify(title: title, tag_prefix: tag_prefix)
    # Build android app
    version_code = build(flavor: app_flavor)
    # And deploy it to the store
    supply(track: track, package_name: package_name, skip_upload_apk: true)
    # Notify slack and create the tag
    post_deploy(flavor: flavor, title: title, version_code: version_code)
  end

  desc "Build a new production version"
  lane :production do |options|
    flavor = 'production'
    tag_prefix = 'android/production*'
    title = 'Android Release'
    app_flavor = 'Production'
    track = 'internal'
    package_name = 'com.lendingpoint.customerportal'

    # Verify next release
    next unless verify(title: title, tag_prefix: tag_prefix)
    # Build android app
    version_code = build(flavor: app_flavor)
    # upload to Google Play
    supply(track: track, package_name: package_name, skip_upload_apk: true)
    # Notify slack and create the tag
    post_deploy(flavor: flavor, title: title, version_code: version_code)
  end

end
