fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew cask install fastlane`

# Available Actions
## iOS
### ios screenshots
```
fastlane ios screenshots
```
Generate new localized screenshots
### ios verify
```
fastlane ios verify
```
Verify next release
### ios build
```
fastlane ios build
```
Build a new version of iOS App
### ios post_deploy
```
fastlane ios post_deploy
```
Generates release notes and create the next tag
### ios release
```
fastlane ios release
```

### ios qa
```
fastlane ios qa
```
Build a new qa version (for internal testing purposes)
### ios staging
```
fastlane ios staging
```
Build a new private beta version (with private beta testers)
### ios uat
```
fastlane ios uat
```
Build a new private uat version (with private uat testers)
### ios production
```
fastlane ios production
```
Build a new production version

----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
