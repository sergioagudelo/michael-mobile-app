//
//  CustomerPortalUITests.swift
//  CustomerPortalUITests
//
//  Created by Johan Quiroga on 10/4/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

import XCTest

class CustomerPortalUITests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // UI tests must launch the application that they test.
        let app = XCUIApplication()
        setupSnapshot(app)
        app.launch()

        snapshot("00Login")
      
        let ForgotPasswordBtn = app.otherElements["ForgotPasswordBtn"]
        
        ForgotPasswordBtn.waitForExistence(timeout: 1)
      
        ForgotPasswordBtn.tap()
      
        
        let ForgotPasswordFormSubmitBtn = app.otherElements["ForgotPasswordFormSubmitBtn"]
             
        ForgotPasswordFormSubmitBtn.waitForExistence(timeout: 1)
      
        snapshot("01ForgotPassword")
      
        ForgotPasswordFormSubmitBtn.tap()

      
        let ForgotPasswordConfirmBtn = app.otherElements["ForgotPasswordConfirmBtn"]
        
        ForgotPasswordConfirmBtn.waitForExistence(timeout: 5)
      
        ForgotPasswordConfirmBtn.tap()
      
        let ResetPasswordFormSubmitBtn = app.otherElements["ResetPasswordFormSubmitBtn"]
        
        ResetPasswordFormSubmitBtn.waitForExistence(timeout: 1)
      
        snapshot("02ResetPassword")
      
        ResetPasswordFormSubmitBtn.tap()
    }

    func testLaunchPerformance() {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, *) {
            // This measures how long it takes to launch your application.
            measure(metrics: [XCTOSSignpostMetric.applicationLaunch]) {
                XCUIApplication().launch()
            }
        }
    }
}
