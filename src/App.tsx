// ? React-Native + Typescript @see https://facebook.github.io/react-native/docs/typescript#what-does-react-native-typescript-look-like
import React, { useEffect } from 'react';
import { StatusBar, StyleSheet } from 'react-native';
import * as Progress from 'react-native-progress';
import { PersistGate } from 'redux-persist/integration/react';
import { Provider } from 'react-redux';
import { mapping } from '@eva-design/eva';
import { EvaIconsPack } from '@ui-kitten/eva-icons';
import {
  ApplicationProvider,
  IconRegistry,
  Layout,
} from 'react-native-ui-kitten';
import SplashScreen from 'react-native-splash-screen';
import NetInfo from '@react-native-community/netinfo';
import messaging from '@react-native-firebase/messaging';
import AppNavigator from './navigation';
import NavigationService from './navigation/NavigationService';

import { store, persistor } from './store';

import { theme, mapping as customMapping } from './theme';
import {
  updateNetInfo,
  shouldShowForceUpdate,
  logNavigationStateChangeInAnalytics,
} from './utils';
import { updateNavigationHistory } from './modules/shared/store/actions/actions';
import { getActiveRouteName } from './modules/shared/helpers';
import { NetWorkIndicator } from './modules/shared/containers';
import { AddNotification } from './modules/notifications/store/actions/actions';
import config from './config';

const styles = StyleSheet.create({
  loading: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

type AppProps = {
  isHeadless: boolean;
};

const App = ({ isHeadless }: AppProps) => {
  useEffect(() => {
    SplashScreen.hide();

    //listen to network changes
    const unsubscribeConnectionInfoListener = NetInfo.addEventListener(
      updateNetInfo
    );

    //listen notification on foreground
    const unsubscribeForegroundNotificationListener = messaging().onMessage(
      async (remoteMessage) => {
        if (
          remoteMessage &&
          remoteMessage.data &&
          remoteMessage.data.newNotification
        ) {
          const parsedData = JSON.parse(remoteMessage.data.newNotification);
          store.dispatch(AddNotification(parsedData));
        }
      }
    );

    //check force update
    shouldShowForceUpdate();

    return () => {
      if (unsubscribeConnectionInfoListener) {
        unsubscribeConnectionInfoListener();
      }

      if (unsubscribeForegroundNotificationListener) {
        unsubscribeForegroundNotificationListener();
      }
    };
  }, []);

  //Push notifications. Don't show app on background notification
  if (isHeadless) {
    return null;
  }

  return (
    <>
      <StatusBar animated barStyle='dark-content' />
      <Provider store={store}>
        <IconRegistry icons={EvaIconsPack} />
        <ApplicationProvider
          mapping={mapping}
          theme={theme}
          customMapping={customMapping}
        >
          <PersistGate
            loading={
              <Layout style={styles.loading}>
                <Progress.CircleSnail
                  color={[
                    theme['color-primary-500'],
                    theme['color-warning-500'],
                    theme['color-info-500'],
                  ]}
                />
              </Layout>
            }
            persistor={persistor}
          >
            <NetWorkIndicator />
            <AppNavigator
              ref={(navigatorRef) => {
                NavigationService.setTopLevelNavigator(navigatorRef);
              }}
              onNavigationStateChange={(prevState, currentState): void => {
                const currentRouteName = getActiveRouteName(currentState);
                const previousRouteName = getActiveRouteName(prevState);

                if (currentRouteName !== previousRouteName) {
                  store.dispatch(
                    updateNavigationHistory({
                      currentRouteName,
                      previousRouteName,
                    })
                  );

                  if (__DEV__ && !config.disableAnalytics) {
                    logNavigationStateChangeInAnalytics(
                      currentRouteName,
                      previousRouteName
                    );
                  }
                }
              }}
            />
          </PersistGate>
        </ApplicationProvider>
      </Provider>
    </>
  );
};

export default App;
