import {store} from '../store';
import {setAppLocalVersion} from '../modules/shared/store/actions/actions';
import {checkAvailableUpdate} from '../modules/shared/utils';
import {DeviceInfo} from '../modules/shared/services';
import NavigationService from '../navigation/NavigationService';

export const shouldShowForceUpdate = async () => {
  const version = DeviceInfo.getAppLocalVersion();
  const os = DeviceInfo.getOsName();
  store.dispatch(setAppLocalVersion(version));

  const appUpdateInfo = await checkAvailableUpdate(version[os]);
  if (appUpdateInfo.shouldAppUpdate) {
    NavigationService.navigate("ForceUpdate", {
      isSoftUpdate: appUpdateInfo.isSoftUpdate,
    });
  }
};
