import { AxiosError } from 'axios';
import * as Sentry from '../modules/shared/services/Sentry';
import { ApiResponse } from '../services/utils';

/**
 * when we have 400 try to get the errors.detail for using
 * for messages and modals
 * @param error any
 */
const getDetailError = (error: any) => {
  const defaultErrMsg = "Something didn't go right but let's try again.";
  try {
    const response = JSON.parse(error.response.request._response);
    return response.errors.detail ? response.errors.detail : defaultErrMsg;
  } catch (error) {
    return defaultErrMsg;
  }
};

const getResponseError = (error: any) => {
  try {
    return JSON.parse(error.response.request._response);
  } catch (error) {
    return { error: 'no response found' };
  }
};

export const handleError = (error: AxiosError): ApiResponse => {
  // TODO MASK SENSITIVE INFO
  if (error) {
    Sentry.configureScope(scope => {
      scope.setTag('errorType', 'api');
    });
    Sentry.configureScope(scope => {
      scope.setExtra('details', getDetailError(error));
      scope.setExtra('headers', JSON.stringify(error.config.headers));
      scope.setExtra('url', JSON.stringify(error.config.url));
      scope.setExtra('method', JSON.stringify(error.config.method));
      scope.setExtra('message', error.message);
      scope.setExtra('response', getResponseError(error));
    });

    Sentry.captureException(error);
  } else {
    Sentry.configureScope(scope => {
      scope.setTag('errorType', 'unknown');
    });
    Sentry.captureMessage(`NO ERROR FOUND`);
  }

  return {
    success: false,
    status: error.response && error.response.status ? error.response.status : 0,
    details: getDetailError(error),
    error,
  };
};
