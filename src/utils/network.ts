import {store} from '../store';
import {getNetInfo} from '../modules/shared/store/actions/actions';
import { NetInfoState } from "@react-native-community/netinfo";

//triggered when connection information changes
export const updateNetInfo = ({
  isConnected,
  isInternetReachable,
}: NetInfoState) => {
  store.dispatch(
    getNetInfo({
      isConnected,
      isInternetReachable: !!isInternetReachable,
    })
  );
};
