import analytics from "@react-native-firebase/analytics";

//logs route state change to firebase analytics
export const logNavigationStateChangeInAnalytics = async (
  currentRouteName: string,
  previousRouteName: string
) => {
  try {
    await analytics().logEvent("navigation_state_change", {
      currentRouteName,
      previousRouteName,
    });
  } catch (error) {
    if (__DEV__) {
      console.log(error);
    }
  }
};
