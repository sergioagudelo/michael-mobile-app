import React, { useState, useEffect } from 'react';
import { handleHttpHookError, handleHttpMissingError, handleHttpMissingPayload } from '../helpers';
import { HTTPServicesResponse, HTTPHooksResponse } from '../utils';

type useHTTPProps = {
  httpFunction: (payload?:any) => Promise<HTTPServicesResponse>;
  functionPayload?:any
  callOnComponentMount? :boolean;
};

export default function useHTTP  ({ httpFunction, functionPayload=null, callOnComponentMount=false }: useHTTPProps): HTTPHooksResponse {
  const [wasCalled, setWasCalled] = useState(false);

  const [isLoading, setIsLoading] = useState(false);
  const [errorMessage, setErrorMessage] = useState<string | null>(null);
  const [payload, setPayload] = useState<any>(null);
  const [handleSubmit, setHandleSubmit] = useState<any>(null);

  useEffect(() => {
    let isCancelled = false;
    const callService = async (externalFunctionPayload = functionPayload) => {
      setWasCalled(true);
      setErrorMessage(null)
      try {
        setIsLoading(true)
        const response = externalFunctionPayload ? await httpFunction(externalFunctionPayload) : await httpFunction()
        setIsLoading(false)
        if (response) {
          const {
            success,
            payload: _payload,
            errorMessage: _errorMessage,
          } = response;

          if (success) {
            if (_payload) {
              setPayload(_payload)
              return { success, payload: _payload }
            }
            setErrorMessage(handleHttpMissingPayload({ ...response, httpFunction}))
            return
          }
          setErrorMessage(_errorMessage ?? handleHttpMissingError(response))
          return { success:false, errorMessage: _errorMessage };
        }
      } catch (error) {
        setIsLoading(false);
        return handleHttpHookError(error)
      }
    }

    if ( callOnComponentMount && !isCancelled && !wasCalled) {
      callService();
    }
    if (!handleSubmit) {
      setHandleSubmit(() => callService);
    }

    return () => {
      isCancelled = true;
    };
  }, [setIsLoading, isLoading, setPayload, payload, setErrorMessage, errorMessage, httpFunction, wasCalled, functionPayload, callOnComponentMount, handleSubmit]);

  return { isLoading, errorMessage, payload, handleSubmit };
};

