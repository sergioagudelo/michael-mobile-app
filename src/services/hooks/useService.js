import React, { useState, useEffect } from 'react';

/**
 *

type useHTTPProps = {
  httpFunction: (responsePayload?:any) => Promise<HTTPServicesResponse>;
  httpFunctionPayload?:any
};
export type HTTPServicesResponse<responsePayload = any> = {
  success: boolean;
  errorMessage?: string | null;
  responsePayload?: responsePayload | null;

  export type HTTPHooksResponse<responsePayload = any> = {
  isLoading: boolean;
  errorMessage?: string | null;
  responsePayload?: responsePayload | null;
};
export type HTTPServicesResponse<responsePayload = any> = {
  success: boolean;
  errorMessage?: string | null;
  responsePayload?: responsePayload | null;
};

};
 */
export const getDetailError = error => {
  const defaultErrMsg = 'Hubo un error, por favor intente de nuevo';
  try {
    const response = JSON.parse(error.response.request._response);
    return response.errors.detail ? response.errors.detail : defaultErrMsg;
  } catch (error) {
    return defaultErrMsg;
  }
};

export default function useService({
  httpFunction,
  httpFunctionPayload = null,
  callOnComponentMount = false,
}) {
  const [wasCalled, setWasCalled] = useState(false);

  const [isLoading, setIsLoading] = useState(false);
  const [errorMessage, setErrorMessage] = useState(null);
  const [responsePayload, setResponsePayload] = useState(null);
  const [handleSubmit, setHandleSubmit] = useState(null);

  useEffect(() => {
    let isCancelled = false;
    const callService = async (handleSubmitPayload = httpFunctionPayload) => {
      setWasCalled(true);
      try {
        setIsLoading(true);
        const response = handleSubmitPayload
          ? await httpFunction(handleSubmitPayload)
          : await httpFunction();
        setIsLoading(false);
        if (response) {
          const { success, payload: _responsePayload, error: _errorMessage } = response;

          if (success) {
            if (_responsePayload) {
              setResponsePayload(_responsePayload);
              return;
            }
            // setErrorMessage(handleHttpMissingPayload(response))
            setErrorMessage(getDetailError(response));

            return;
          }
          setErrorMessage(_errorMessage ?? 'error');
        }
      } catch (error) {
        if (__DEV__) {
          console.log(error);
        }
        setIsLoading(false);

        return { success: false, errorMessage: getDetailError(error) };
      }
    };

    if (callOnComponentMount && !wasCalled && !isCancelled) {
      callService();
    }
    if (!handleSubmit) {
      setHandleSubmit(() => callService);
    }

    return () => {
      isCancelled = true;
    };
  }, [
    setIsLoading,
    isLoading,
    setResponsePayload,
    responsePayload,
    setErrorMessage,
    errorMessage,
    httpFunction,
    wasCalled,
    httpFunctionPayload,
    handleSubmit,
    callOnComponentMount,
  ]);

  return [isLoading, errorMessage, responsePayload, handleSubmit];
}
