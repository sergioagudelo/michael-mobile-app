import React from 'react';
import shared from '../../modules/shared';

import { useHTTP } from '../hooks';
import { HTTPServicesResponse } from '../utils';

const { Loading } = shared.components;

export type HTTPErrorMessageComponentProps = { errorMessage: string };
export type HTTPPayloadMessageComponentProps = { payload: any };

export type HTTPProps = {
  onSubmit: () => Promise<HTTPServicesResponse>;
  asyncFunction: () => Promise<HTTPServicesResponse>;
  functionPayload?: any;
  LoadingComponent?: () => React.ReactElement<any>;
  ErrorMessageComponent?: ({
    errorMessage,
  }: HTTPErrorMessageComponentProps) => React.ReactElement<any>;
  handlePayload?: ({ payload }: HTTPPayloadMessageComponentProps) => React.ReactElement<any> | null;
};

export default function withHTTP(props: HTTPProps) {
  const {
    asyncFunction,
    functionPayload,
    LoadingComponent,
    ErrorMessageComponent,
    handlePayload,
    onSubmit,
  } = props;

  const { isLoading, errorMessage, payload } = useHTTP({
    asyncFunction,
    functionPayload,
    onSubmit,
  });

  // TODO default components

  if (isLoading) {
    return LoadingComponent ? <LoadingComponent /> : <Loading isLoading />;
  }
  if (errorMessage) {
    return ErrorMessageComponent ? <ErrorMessageComponent errorMessage={errorMessage} /> : null;
  }
  if (payload) {
    return handlePayload ? handlePayload({ payload }) : null;
  }
  return null;
}
