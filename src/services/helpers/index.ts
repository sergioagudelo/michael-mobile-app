// Higher-order functions
// error handlers
import { AxiosError } from 'axios';
import { HTTPServicesResponse } from '../utils';
import { httpErrorLogger } from '../middlewares';
// export const globalErrorHandler = (error: any):ApiResponse =>{
//   console.log(error)
//   return {
//     success: false,
//     status:0,
//     details:null,
//     response:null,
//     error:null
// }
// }

export const callServiceWithErrorHandler = async (service: any, errorHandler: any) => {
  try {
    const response = await service();
    return response;
  } catch (error) {
    return errorHandler(error);
  }
};

export const getDetailError = (error: any) => {
  const defaultErrMsg = "Something didn't go right but let's try again.";
  try {
    const response = JSON.parse(error.response.request._response);
    return response.errors.detail ? response.errors.detail : defaultErrMsg;
  } catch (error) {
    return defaultErrMsg;
  }
};

export const handleHttpMissingPayload = (response: any): string => {
  httpErrorLogger('HTTP', 'MissingPayload', response);
  return 'Missing Payload';
};

export const handleHttpMissingError = (response: any): string => {
  httpErrorLogger('HTTP', 'MissingError', response);
  return 'Missing Error Message';
};

export const handleHttpHookError = (error: AxiosError): HTTPServicesResponse<null> => {
  httpErrorLogger('HTTP', 'hook', error);
  return { success: false, errorMessage: getDetailError(error) };
};
