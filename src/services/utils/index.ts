import { AxiosError } from 'axios';
/**
 * all service consumers will only care about payload and error message.
 * Error handling, loggin, and http info will be handled inside each service.
 */

export type HTTPHooksResponse<payload = any> = {
  isLoading: boolean;
  errorMessage?: string | null;
  payload?: payload | null;
  handleSubmit: (args?: any) => Promise<any>;
};
export type HTTPServicesResponse<payload = any> = {
  success: boolean;
  errorMessage?: string | null;
  payload?: payload | null;
};

// ApiResponse = ServiceGlobal Response

export type ApiResponse<T = any | undefined> = {
  success: boolean;
  status?: number;
  details?: string;
  response?: T;
  error?: AxiosError;
};
