/**
 * This axios instance is created for iodev api-manager related services
 */

import axios from 'axios';

import config from '../../../../config';
import NavigationService from '../../../../navigation/NavigationService';
// FIXME nested type

import { httpErrorLogger } from '../../../middlewares';

import * as constants from '../constants';
import { ManagerTokens } from '../utils';

const { paths, NAME, errorNames } = constants;
const {
  api: {
    host,
    apiManager: { appToken },
    paths: { manager: managerPath },
  },
} = config;

let authTokens: ManagerTokens;

const manager = axios.create({
  baseURL: host + managerPath,
  headers: {
    'Content-Type': 'application/json',
  },
});

const clearAuthTokens = () => {
  authTokens = null;
};

export const setManagerTokens = (data: any) => {
  authTokens = {
    authorization: data.accessToken,
  };
};

/**
 * API REQUEST INTERCEPTOR
 */
manager.interceptors.request.use(
  async request => {
    if (authTokens) {
      request.headers.Authorization = `Bearer ${authTokens.authorization}`;
      request.headers['Application-Token'] = appToken;
    }

    return request;
  },
  error => Promise.reject(error),
);

/**
 * API RESPONSE INTERCEPTOR
 */
manager.interceptors.response.use(
  response => {
    if (response.status === 204) {
      return { ...response, data: 'Successful update' };
    }
    return response;
  },
  error => {
    const {
      response: { status },
    } = error;

    if (status === 401) {
      httpErrorLogger(NAME, errorNames.unauthorized, error);
      clearAuthTokens();
      NavigationService.navigate('Auth');
    } else if (status === 403 || status === 404 || status >= 500) {
      // general error
      httpErrorLogger(NAME, errorNames.general, error);
      NavigationService.navigate('TryAgain', { isLogged: !!authTokens });
    }
    return Promise.reject(error);
  },
);

export default manager;
