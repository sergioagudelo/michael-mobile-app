import axios from 'axios';
import moment from 'moment';
import config from '../../../../config';
import NavigationService from '../../../../navigation/NavigationService';
// FIXME nested type
import { LoginResponse } from '../../../../modules/auth/utils';
import AsyncStorage from '@react-native-community/async-storage';

import { httpErrorLogger } from '../../../middlewares';

import * as constants from '../constants';
import * as utils from '../utils';
import { handleError } from '../../../../utils/services';

const { paths, NAME, errorNames } = constants;
const {
  api: {
    host,
    apiManager: { appToken },
    sdocs,
    paths: { consumer: consumerPath },
  },
} = config;

let authTokens: utils.AuthTokens | null = null;
const managerAuth: utils.ManagerTokens | null = null;
export const AUTH_TOKEN_ASYNC_STORAGE_KEY = 'auth.token';

const consumer = axios.create({
  baseURL: host + consumerPath,
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
  },
});

export const clearAuthTokens = () => {
  authTokens = null;
};

export const setAuthTokens = (data: LoginResponse) => {
  authTokens = {
    accessToken: data.access_token,
    refreshToken: data.refresh_token,
    expiresIn: moment()
      .add(data.expires_in, 's')
      .subtract(60, 's')
      .toDate(),
  };
  AsyncStorage.setItem(AUTH_TOKEN_ASYNC_STORAGE_KEY, JSON.stringify(authTokens));
};
export const addAuthTokens = (data: any) => {
  authTokens = {
    ...authTokens,
    ...data,
  };
  AsyncStorage.setItem(AUTH_TOKEN_ASYNC_STORAGE_KEY, JSON.stringify(authTokens));
};

export const refreshToken = async (): Promise<any> => {
  return new Promise(resolve => {
    if (!authTokens) {
      return resolve(false);
    }

    axios
      .post(`${host}${consumerPath}${paths.refreshToken}`, { token: authTokens.refreshToken })
      .then(response => {
        const managerAuthorization = authTokens ? authTokens.managerAuthorization : null;
        setAuthTokens(response.data);
        if (managerAuthorization) {
          addAuthTokens({ managerAuthorization });
        }
        resolve(true);
      })
      .catch(error => {
        httpErrorLogger(NAME, errorNames.refreshToken, error);
        clearAuthTokens();
        resolve(false);
      });
  });
};

/** REQUEST INTERCEPTOR
 */
consumer.interceptors.request.use(
  async request => {
    if (authTokens || Object.keys(authTokens ?? {}).length === 0) {
      try {
        const authTokenString = await AsyncStorage.getItem(AUTH_TOKEN_ASYNC_STORAGE_KEY);
        if (authTokenString) {
          authTokens = JSON.parse(authTokenString);
        }
      } catch (e) {
        authTokens = null;
      }
    }

    if (authTokens) {
      if (moment().isAfter(authTokens.expiresIn)) {
        await refreshToken();
      }
      request.headers.Authorization = `Bearer ${authTokens.accessToken}`;

      // add sdocs credentials
      if (
        request.url &&
        request.url.startsWith(paths.applications) &&
        request.url.search(new RegExp(paths.offersCatalog, 'g')) < 0
      ) {
        request.headers.Authorization = authTokens.managerAuthorization;
        request.headers['Application-Token'] = sdocs.appToken;
      }

      // add manager auth credentials
      if (
        authTokens.managerAuthorization &&
        request.url === paths.applications + paths.offersCatalog
      ) {
        request.headers.Authorization = `Bearer ${authTokens.managerAuthorization}`;
        request.headers['Application-Token'] = appToken;
      }

      // console.log(request.headers);
    }

    return request;
  },
  error => {
    return Promise.reject(error);
  },
);

/**
 * API RESPONSE INTERCEPTOR
 */
consumer.interceptors.response.use(
  response => {
    if (response.status === 204) {
      return { ...response, data: 'Successful update' };
    }
    return response;
  },
  error => {
    const {
      response: { status },
    } = error;

    if (status === 401) {
      httpErrorLogger(NAME, errorNames.unauthorized, error);
      clearAuthTokens();
      NavigationService.navigate('Auth');
    } else if (status === 403 || status === 404 || status >= 500) {
      // general error
      httpErrorLogger(NAME, errorNames.general, error);
      NavigationService.navigate('TryAgain', { isLogged: !!authTokens });
    }

    //Log server errors in sentry
    if (status >= 500) {
      handleError(error);
    }

    return Promise.reject(error);
  },
);

export default consumer;
