import { AxiosError } from 'axios';
import { setAuthTokens, clearAuthTokens, addAuthTokens } from '../api/consumer';
import { setManagerTokens } from '../api/manager';
import { NAME, errorNames } from '../constants';
import { httpErrorLogger } from '../../../middlewares';
import { ApiResponse, HTTPServicesResponse } from '../../../utils';
import { getDetailError } from '../../../helpers';

export { setAuthTokens, clearAuthTokens, setManagerTokens, addAuthTokens };

// TODO REPLACE WITH HTTPServicesResponse
export const handleSubModuleError = (error: AxiosError): HTTPServicesResponse => {
  httpErrorLogger(NAME, errorNames.unexpected, error);
  return { success: false, errorMessage: getDetailError(error) };
};
