export const NAME = 'LENDINGPOINT';

export const paths = {
  user: '/me',
  documents: '/me/documents',
  login: '/login?grant_type=password',
  refreshToken: '/login?grant_type=refresh_token',
  loans: '/me/loans',
  mails: '/me/mails',
  resetPassword: '/reset-password',
  paymentMethods: '/me/payment-methods',
  loanApplications: '/me/loan-applications',
  apiManagerLogin: '/oauth/token',
  offersCatalog: '/offers/catalog',
  applications: '/applications',
};

// errors handled by api
export const errorNames = {
  unauthorized: 'unauthorized',
  general: 'general',
  refreshToken: 'refreshToken',
  // for errors handled by module services
  unexpected: 'subModule',
};
