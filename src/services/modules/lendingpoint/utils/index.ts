export type AuthTokens = {
  managerAuthorization?: string;
  accessToken: string;
  refreshToken: string;
  expiresIn: Date;
} | null;

export type ManagerTokens = {
  authorization: string;
} | null;
