import { AxiosError } from 'axios';

export const httpErrorLogger = (apiName: string, apiErrorCode: string, error: AxiosError) => {
  if (__DEV__) {
    console.log(`httpErrorLogger: API-ERROR-${apiName}-${apiErrorCode}`, error);
  }

  return null;
  // TODO SENTRY
};
