import env from 'react-native-config';

const config = {
  environment: env.ENVIRONMENT_NAME,
  disablePush: !!env.DISABLE_PUSH,
  disableForceUpdate: !!env.DISABLE_FORCE_UPDATE,
  disableAndroidBiometrics: !!env.DISABLE_ANDROID_BIOMETRICS,
  disableAnalytics: !!env.DISABLE_ANALYTICS,
  sentry: {
    dsn: env.SENTRY_DSN,
  },
  api: {
    host: env.API_HOST,
    paths: {
      consumer: '/consumer/v1',
      manager: '/api-manager',
    },
    apiManager: {
      managerAuth: {
        clientId: env.CLIENT_ID,
        clientSecret: env.CLIENT_SECRET,
        password: env.MANAGER_PASSWORD,
        username: env.MANAGER_USERNAME,
      },
      appToken: env.MANAGER_APP_TOKEN,
    },
    sdocs: {
      authKey: env.SDOCS_AUTH_KEY,
      appToken: env.SDOCS_APP_TOKEN,
    },
    appVersions: {
      bearerToken: env.APP_VERSION_BEARER_TOKEN,
    },
    // apiKey: env.API_KEY,
  },
  lpApps: {
    host: env.LP_APPS_HOST,
  },
  testflight: {
    host: env.TESTFLIGHT_HOST,
    token: env.TESTFLIGHT_TOKEN,
  },
  applyUrl: 'https://www.lendingpoint.com/apply/#/Start',
  contactUrl: 'https://www.lendingpoint.com/contact-us/',
  contactPhone: '+18889690959',
  tcUrl: 'https://www.lendingpoint.com/terms-of-use/',
  electronicCommunicationsUrl:
    'https://www.lendingpoint.com/consent-electronic-communications/',
  login: {
    initialValues: {
      username: env.DEBUG_USERNAME,
      password: env.DEBUG_PASSWORD,
    },
  },
};

const API_HOST = config.api.host;

export { API_HOST };

export default config;
