import Reactotron from 'reactotron-react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { reactotronRedux } from 'reactotron-redux';

const reactotron = Reactotron.setAsyncStorageHandler(AsyncStorage) // AsyncStorage would either come from `react-native` or `@react-native-community/async-storage` depending on where you get it from
  .configure({ name: 'Customer Portal App' }) // controls connection & communication settings
  .useReactNative({
    networking: {
      // optionally, you can turn it off with false.
      ignoreUrls: /symbolicate/,
    },
  }) // add all built-in react native plugins
  .use(reactotronRedux())
  .connect(); // let's connect!

Reactotron.clear();

console.tron = Reactotron;

function log(...args) {
  Reactotron.display({
    name: 'LOG',
    preview: args[0].toString(),
    value: args,
  });
}

function info(...args) {
  Reactotron.display({
    name: 'INFO',
    preview: args[0].toString(),
    value: args,
  });
}

function warn(...args) {
  Reactotron.display({
    name: 'WARN',
    preview: args[0].toString(),
    value: args,
    important: true,
  });
}

function error(...args) {
  Reactotron.display({
    name: 'ERROR',
    preview: args[0].toString(),
    value: args,
    important: true,
  });
}

console.info = info;
console.log = log;
console.warn = warn;
console.error = error;

export default reactotron;
