import { light as lightTheme } from '@eva-design/eva';

export default {
  ...lightTheme,
  'color-basic-100': '#FFFFFF',
  'color-basic-200': '#F7F8F9',
  'color-basic-300': '#EEF0F3',
  'color-basic-400': '#E6E9EE',
  'color-basic-500': '#DEE2E8',
  'color-basic-600': '#C1C6CE',
  'color-basic-700': '#A3ABB4',
  'color-basic-800': '#868F9B',
  'color-basic-900': '#697381',
  'color-basic-1000': '#4B5867',
  'color-basic-1100': '#2E3C4D',
  'color-basic-control-transparent-100': 'rgba(255, 255, 255, 0.08)',
  'color-basic-control-transparent-200': 'rgba(255, 255, 255, 0.16)',
  'color-basic-control-transparent-300': 'rgba(255, 255, 255, 0.24)',
  'color-basic-control-transparent-400': 'rgba(255, 255, 255, 0.32)',
  'color-basic-control-transparent-500': 'rgba(255, 255, 255, 0.40)',
  'color-basic-control-transparent-600': 'rgba(255, 255, 255, 0.48)',
  'color-basic-transparent-100': 'rgba(134, 143, 155, 0.08)',
  'color-basic-transparent-200': 'rgba(134, 143, 155, 0.16)',
  'color-basic-transparent-300': 'rgba(134, 143, 155, 0.24)',
  'color-basic-transparent-400': 'rgba(134, 143, 155, 0.32)',
  'color-basic-transparent-500': 'rgba(134, 143, 155, 0.40)',
  'color-basic-transparent-600': 'rgba(134, 143, 155, 0.48)',
  'color-primary-100': '#8BD9FA',
  'color-primary-200': '#6EC3EF',
  'color-primary-300': '#4CADE4',
  'color-primary-400': '#1398D9',
  'color-primary-500': '#0E84C5',
  'color-primary-600': '#0770B2',
  'color-primary-700': '#005D9F',
  'color-primary-800': '#004B8D',
  'color-primary-900': '#003267',
  'color-primary-transparent-100': 'rgba(14, 132, 197, 0.08)',
  'color-primary-transparent-200': 'rgba(14, 132, 197, 0.16)',
  'color-primary-transparent-300': 'rgba(14, 132, 197, 0.24)',
  'color-primary-transparent-400': 'rgba(14, 132, 197, 0.32)',
  'color-primary-transparent-500': 'rgba(14, 132, 197, 0.40)',
  'color-primary-transparent-600': 'rgba(14, 132, 197, 0.48)',
  'color-secondary-100': '#FEEDD3',
  'color-secondary-200': '#FDD6A8',
  'color-secondary-300': '#F9B87C',
  'color-secondary-400': '#F39B5A',
  'color-secondary-500': '#EB6E27',
  'color-secondary-600': '#E8450C',
  'color-secondary-700': '#A93713',
  'color-secondary-800': '#88220C',
  'color-secondary-900': '#701307',
  'color-tertiary-100': '#EDFCEA',
  'color-tertiary-200': '#D9FAD6',
  'color-tertiary-300': '#BEF2BF',
  'color-tertiary-400': '#A7E5AE',
  'color-tertiary-500': '#88D498',
  'color-tertiary-600': '#76C699',
  'color-tertiary-700': '#64B99A',
  'color-tertiary-800': '#52AB9A',
  'color-tertiary-900': '#409D9B',
  'color-success-100': '#EDFFF3',
  'color-success-200': '#D9FAD6',
  'color-success-300': '#BEF2BF',
  'color-success-400': '#A7E5AE',
  'color-success-500': '#88D498',
  'color-success-600': '#76C699',
  'color-success-700': '#64B99A',
  'color-success-800': '#52AB9A',
  'color-success-900': '#409D9B',
  'color-success-transparent-100': 'rgba(136, 212, 152, 0)',
  'color-success-transparent-200': 'rgba(136, 212, 152, 0.16)',
  'color-success-transparent-300': 'rgba(136, 212, 152, 0.24)',
  'color-success-transparent-400': 'rgba(136, 212, 152, 0.32)',
  'color-success-transparent-500': 'rgba(136, 212, 152, 0.40)',
  'color-success-transparent-600': 'rgba(136, 212, 152, 0.48)',
  'color-info-100': '#8BD9FA',
  'color-info-200': '#6EC3EF',
  'color-info-300': '#4CADE4',
  'color-info-400': '#1398D9',
  'color-info-500': '#0E84C5',
  'color-info-600': '#0770B2',
  'color-info-700': '#005D9F',
  'color-info-800': '#004B8D',
  'color-info-900': '#003267',
  'color-info-transparent-100': 'rgba(14, 132, 197, 0.05)',
  'color-info-transparent-200': 'rgba(14, 132, 197, 0.16)',
  'color-info-transparent-300': 'rgba(14, 132, 197, 0.24)',
  'color-info-transparent-400': 'rgba(14, 132, 197, 0.32)',
  'color-info-transparent-500': 'rgba(14, 132, 197, 0.40)',
  'color-info-transparent-600': 'rgba(14, 132, 197, 0.48)',
  'color-warning-100': '#FEEDD3',
  'color-warning-200': '#FDD6A8',
  'color-warning-300': '#F9B87C',
  'color-warning-400': '#F39B5A',
  'color-warning-500': '#EB6E27',
  'color-warning-600': '#E8450C',
  'color-warning-700': '#A93713',
  'color-warning-800': '#88220C',
  'color-warning-900': '#701307',
  'color-warning-transparent-100': 'rgba(235, 110, 39, 0)',
  'color-warning-transparent-200': 'rgba(235, 110, 39, 0.16)',
  'color-warning-transparent-300': 'rgba(235, 110, 39, 0.24)',
  'color-warning-transparent-400': 'rgba(235, 110, 39, 0.32)',
  'color-warning-transparent-500': 'rgba(235, 110, 39, 0.40)',
  'color-warning-transparent-600': 'rgba(235, 110, 39, 0.48)',
  'color-danger-100': '#FCD6CA',
  'color-danger-200': '#FAA697',
  'color-danger-300': '#F06A61',
  'color-danger-400': '#E23A3F',
  'color-danger-500': '#D0021B',
  'color-danger-600': '#B20127',
  'color-danger-700': '#95012E',
  'color-danger-800': '#780030',
  'color-danger-900': '#630031',
  'color-danger-transparent-100': 'rgba(208, 2, 27, 0)',
  'color-danger-transparent-200': 'rgba(208, 2, 27, 0.16)',
  'color-danger-transparent-300': 'rgba(208, 2, 27, 0.24)',
  'color-danger-transparent-400': 'rgba(208, 2, 27, 0.32)',
  'color-danger-transparent-500': 'rgba(208, 2, 27, 0.40)',
  'color-danger-transparent-600': 'rgba(208, 2, 27, 0.48)',
  'color-basic-default': '$color-basic-1000',
  'color-basic-active': '$color-basic-600',
  'color-basic-focus': '$color-basic-600',
  'color-basic-disabled': '$color-basic-transparent-600',
  'color-primary-default': '$color-primary-700',
  'color-primary-active': '$color-primary-600',
  'color-primary-focus': '$color-primary-500',
  'color-primary-disabled': '$color-basic-transparent-600',
  'color-success-default': '$color-success-500',
  'color-success-active': '$color-success-700',
  'color-success-focus': '$color-success-500',
  'color-success-disabled': '$color-basic-transparent-600',
  'color-info-default': '$color-info-500',
  'color-info-active': '$color-info-600',
  'color-info-focus': '$color-info-700',
  'color-info-disabled': '$color-basic-transparent-600',
  'color-warning-default': '$color-warning-600',
  'color-warning-active': '$color-warning-700',
  'color-warning-focus': '$color-warning-700',
  'color-warning-disabled': '$color-basic-transparent-600',
  'color-danger-default': '$color-danger-500',
  'color-danger-active': '$color-danger-400',
  'color-danger-focus': '$color-danger-600',
  'color-danger-disabled': '$color-basic-transparent-600',
  'background-basic-color-1': '$color-basic-100',
  'background-basic-color-2': '$color-basic-200',
  'background-basic-color-3': '$color-basic-300',
  'background-basic-color-4': '$color-basic-400',
  'background-alternative-color-1': '$color-basic-800',
  'background-alternative-color-2': '$color-basic-900',
  'background-alternative-color-3': '$color-basic-1000',
  'background-alternative-color-4': '$color-basic-1100',
  'background-primary-color-1': '$color-primary-500',
  'background-primary-color-2': '$color-primary-600',
  'background-primary-color-3': '$color-primary-700',
  'background-primary-color-4': '$color-primary-900',
  'border-basic-color-1': '$color-basic-500',
  'border-basic-color-2': '$color-basic-600',
  'border-basic-color-3': '$color-basic-700',
  'border-basic-color-4': '$color-basic-800',
  'border-basic-color-5': '$color-basic-900',
  'border-alternative-color-1': '$color-basic-500',
  'border-alternative-color-2': '$color-basic-400',
  'border-alternative-color-3': '$color-basic-300',
  'border-alternative-color-4': '$color-basic-200',
  'border-alternative-color-5': '$color-basic-100',
  'border-primary-color-1': '$color-primary-500',
  'border-primary-color-2': '$color-primary-600',
  'border-primary-color-3': '$color-primary-700',
  'border-primary-color-4': '$color-primary-800',
  'border-primary-color-5': '$color-primary-900',
  'border-success-color-1': '$color-success-500',
  'border-success-color-2': '$color-success-600',
  'border-success-color-3': '$color-success-700',
  'border-success-color-4': '$color-success-800',
  'border-success-color-5': '$color-success-900',
  'border-info-color-1': '$color-info-300',
  'border-info-color-2': '$color-info-400',
  'border-info-color-3': '$color-info-500',
  'border-info-color-4': '$color-info-600',
  'border-info-color-5': '$color-info-700',
  'border-warning-color-1': '$color-warning-500',
  'border-warning-color-2': '$color-warning-600',
  'border-warning-color-3': '$color-warning-400',
  'border-warning-color-4': '$color-warning-300',
  'border-warning-color-5': '$color-warning-200',
  'border-danger-color-1': '$color-danger-500',
  'border-danger-color-2': '$color-danger-600',
  'border-danger-color-3': '$color-danger-400',
  'border-danger-color-4': '$color-danger-300',
  'border-danger-color-5': '$color-danger-200',
  'text-basic-color': '$color-basic-1000',
  'text-alternate-color': '$color-basic-100',
  'text-gray-color': '$color-basic-600',
  'text-control-color': '$color-basic-900',
  'text-disabled-color': '$color-basic-500',
  'text-hint-color': '$color-basic-700',
  'text-primary-color': '$color-primary-default',
  'text-primary-active-color': '$color-primary-active',
  'text-primary-disabled-color': '$color-primary-disabled',
  'text-success-color': '$color-success-default',
  'text-success-active-color': '$color-success-active',
  'text-success-disabled-color': '$color-success-disabled',
  'text-info-color': '$color-info-default',
  'text-info-active-color': '$color-info-active',
  'text-info-disabled-color': '$color-info-disabled',
  'text-warning-color': '$color-warning-default',
  'text-warning-active-color': '$color-warning-active',
  'text-warning-disabled-color': '$color-warning-disabled',
  'text-danger-color': '$color-danger-default',
  'text-danger-active-color': '$color-danger-active',
  'text-danger-disabled-color': '$color-danger-disabled',
  'text-accent-color': '$color-primary-700',
  'icon-basic-color': '$color-basic-900',
  'icon-active-color': '$color-primary-600',
  'icon-control-color': '$color-basic-900',
  'icon-disabled-color': '$color-basic-500',
  'icon-hint-color': '$color-basic-700',
  'outline-color': '$color-basic-400',
};
