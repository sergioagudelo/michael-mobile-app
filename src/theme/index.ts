import theme from './lp_theme';
import mapping from './lp_mapping.json';

export { theme, mapping };
