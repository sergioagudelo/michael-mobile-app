import { createAppContainer, createSwitchNavigator, NavigationActions } from 'react-navigation';
import shared from '../modules/shared';

// Navigators
import AuthStack from './Auth';
import AppStack from './App';

// Screens

import AuthLoading from '../modules/auth/screens/AuthLoading';
import { getActiveRouteName } from '../modules/shared/helpers';

const { TryAgain, ForceUpdate } = shared.screens;
const SwitchNavigator = createSwitchNavigator(
  {
    AuthLoading,
    ForceUpdate,
    TryAgain,
    Auth: AuthStack,
    App: AppStack,
  },
  {
    initialRouteName: 'AuthLoading',
  },
);

// FORCE UPDATE INTERCEPTOR
const { getStateForAction: defaultGetStateForAction } = SwitchNavigator.router;
SwitchNavigator.router.getStateForAction = (action, state) => {
  if (state && action.type === NavigationActions.NAVIGATE) {
    const name = getActiveRouteName(state);
    if (name === 'ForceUpdate') {
      // Returning null from getStateForAction means that the action
      // has been handled/blocked, but there is not a new state
      if (action.params && action.params.overrideBlocking) {
        return defaultGetStateForAction(action, state);
      }

      return null;
    }
  }
  return defaultGetStateForAction(action, state);
};

export default createAppContainer(SwitchNavigator);
