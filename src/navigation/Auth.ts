import { createStackNavigator } from 'react-navigation-stack';

// Screens
import Login from '../modules/auth/screens/Login';
import ForgotPassword from '../modules/auth/screens/ForgotPassword';
import ResetPassword from '../modules/auth/screens/ResetPassword';
import Welcome from '../modules/auth/screens/Welcome';

const AuthStack = createStackNavigator(
  {
    Login: {
      screen: Login,
    },
    ForgotPassword: {
      screen: ForgotPassword,
    },
    ResetPassword: {
      screen: ResetPassword,
    },
    Welcome: {
      screen: Welcome,
    },
  },
  { headerMode: 'none' },
);

export default AuthStack;
