import { createBottomTabNavigator } from 'react-navigation-tabs';

// Screens
// import { Offers } from '../screens';
import { HelpCenter } from '../modules/support/screens';
import { Loans } from '../modules/loans/screens';
import { Profile } from '../modules/profile/screens';
import { Notifications } from '../modules/notifications/screens';
import { FinancialLife } from '../modules/news/screens';

// Modules
import shared from '../modules/shared';

// Components
const { TabBar } = shared.components;

const DashboardBottomTab = createBottomTabNavigator(
  {
    // Offers: {
    //   screen: Offers,
    // },
    Loans: {
      screen: Loans,
    },
    Notifications: {
      screen: Notifications,
    },
    Profile: {
      screen: Profile,
    },
    FinancialLife: {
      screen: FinancialLife,
    },
    HelpCenter: {
      screen: HelpCenter,
    },
  },
  {
    initialRouteName: 'Loans',
    tabBarComponent: TabBar,
    lazy: true,
    resetOnBlur: true
  },
);

export default DashboardBottomTab;
