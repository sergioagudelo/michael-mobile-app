import React from 'react';
import { createStackNavigator, StackViewTransitionConfigs } from 'react-navigation-stack';
import { Icon } from 'react-native-ui-kitten';

// Navigators
import Dashboard from './Dashboard';

// Screens
import { LoanDetails, PaymentsHistory } from '../modules/loans/screens';
import {
  ProfileInfo,
  ProfileDocumentsDetails,
  ProfileSecurityDetails,
} from '../modules/profile/screens';
import {
  MakePayment,
  PaymentReceipt,
  PaymentMethods,
  NewBankAccount,
  NewDebitCard,
  ExtraPaymentCheckout as ExtraPayment,
  AutopaySetup,
} from '../modules/payments/screens';
// import { VirtualCard, NewLoanOffers } from '../screens';
import { NotificationDetails } from '../modules/notifications/screens';
import { NewsDetails } from '../modules/news/screens';
import { HelpCenter, ChatSupport, CreditScore } from '../modules/support/screens';
import {
  RefinanceOffers,
  PersonalInfoConsent,
  Sdocs,
  WhatsNext,
  Declined,
} from '../modules/refinance/screens';
// Components
import shared from '../modules/shared';

const { HeaderBar, HeaderImg } = shared.components;

const chatSupportStyles = {
  headerTitle: {
    tintColor: '#FFF',
  },
  headerBack: {
    margin: 10,
  },
};

/**
 * routes that need modals
 */
const IOS_MODAL_ROUTES = ['NewsDetails', 'ChatSupport', 'NotificationDetails'];

/**
 * dash board stack navigator
 */
const AppStack = createStackNavigator(
  {
    Dashboard: {
      screen: Dashboard,
    },
    LoanDetails: {
      screen: LoanDetails,
    },
    AutopaySetup: {
      screen: AutopaySetup,
    },
    PaymentsHistory: {
      screen: PaymentsHistory,
    },
    MakePayment: {
      screen: MakePayment,
    },
    ExtraPayment: {
      screen: ExtraPayment,
    },
    // VirtualCard: {
    //   screen: VirtualCard,
    // },
    PaymentReceipt: {
      screen: PaymentReceipt,
      navigationOptions: {
        gesturesEnabled: false,
        headerBackImage: Object.assign(() => null),
      },
    },
    PaymentMethods: {
      screen: PaymentMethods,
    },
    ProfileInfo: {
      screen: ProfileInfo,
    },
    ProfileDocumentsDetails: {
      screen: ProfileDocumentsDetails,
    },
    ProfileSecurityDetails: {
      screen: ProfileSecurityDetails,
    },
    NewBankAccount: {
      screen: NewBankAccount,
    },
    NewDebitCard: {
      screen: NewDebitCard,
    },
    NewsDetails: {
      screen: NewsDetails,
      navigationOptions: { header: null },
    },
    CreditScore: {
      screen: CreditScore,
    },
    // NewLoanOffers: {
    //   screen: NewLoanOffers,
    // },
    HelpCenter: {
      screen: HelpCenter,
    },
    RefinanceOffers: {
      screen: RefinanceOffers,
    },
    PersonalInfoConsent: {
      screen: PersonalInfoConsent,
    },
    Sdocs: {
      screen: Sdocs,
    },
    WhatsNext: {
      screen: WhatsNext,
    },
    Declined: {
      screen: Declined,
    },
    ChatSupport: {
      screen: ChatSupport,
      navigationOptions: {
        gesturesEnabled: false,
        headerStyle: {
          elevation: 0,
          shadowOpacity: 0,
          borderBottomColor: 'transparent',
          borderBottomWidth: 0,
          backgroundColor: '#003267',
        },
        headerTitle: <HeaderBar imageStyle={chatSupportStyles.headerTitle} />,
        headerBackImage: Object.assign(
          () => (
            <Icon
              name="close"
              width={30}
              height={30}
              fill="#FFF"
              style={chatSupportStyles.headerBack}
            />
          ),
          { displayName: 'HeaderImg' },
        ),
      },
    },
    NotificationDetails: {
      screen: NotificationDetails,
      navigationOptions: {
        gesturesEnabled: false,
        headerBackImage: Object.assign(
          () => <Icon name="close" width={30} height={30} style={chatSupportStyles.headerBack} />,
          { displayName: 'HeaderImg' },
        ),
      },
    },
  },
  {
    headerMode: 'float',
    defaultNavigationOptions: () => ({
      headerTitle: <HeaderBar />,
      headerBackImage: Object.assign(() => <HeaderImg />, { displayName: 'HeaderImg' }),
      headerBackTitle: null,
      headerStyle: {
        elevation: 0,
        shadowOpacity: 0,
        borderBottomColor: 'transparent',
        borderBottomWidth: 0,
      },
    }),
    headerLayoutPreset: 'center',
    transitionConfig: (transitionProps, prevTransitionProps?) => {
      const isModal = IOS_MODAL_ROUTES.some(
        screenName =>
          screenName === transitionProps.scene.route.routeName ||
          (prevTransitionProps && screenName === prevTransitionProps.scene.route.routeName),
      );

      return StackViewTransitionConfigs.defaultTransitionConfig(
        transitionProps,
        prevTransitionProps,
        isModal,
      );
    },
  },
);

export default AppStack;
