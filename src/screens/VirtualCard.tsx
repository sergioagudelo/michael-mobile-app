import React from 'react';
import { SafeAreaView, ScrollView, View } from 'react-native';
import { Text, ThemedComponentProps, withStyles } from 'react-native-ui-kitten';
import * as Animatable from 'react-native-animatable';

// Types
import { testData as sharedTestData } from '../modules/shared/utils';
import { testData as loansTestData } from '../modules/loans/utils';

// Components
import shared from '../modules/shared';
import loans from '../modules/loans';

const { HeaderView, Background } = shared.components;
const { VirtualCardInfo, VirtualCardActivate, VirtualCardBilling } = loans.components;

/**
 * render credit card info & credit card status & billing info
 */
const VirtualCard = ({ themedStyle }: ThemedComponentProps) => {
  /**
   * return React.ReactElement
   */
  return (
    <SafeAreaView style={themedStyle.screen} {...shared.helpers.setTestID('VirtualCardScreen')}>
      <ScrollView contentContainerStyle={themedStyle.body}>
        <HeaderView
          title={`Congrats ${sharedTestData.userInfo.firstName}!`}
          subTitle="YOUR VIRTUAL CARD IS READY"
        />
        <View>
          <Background
            minHeight={250}
            colors={['#4CADE4', '#003267']}
            wrapperStyle={themedStyle.wrapper}
          />
        </View>

        <View style={themedStyle.content}>
          <Text
            category="h2"
            appearance="alternative"
            style={[themedStyle.item, themedStyle.centerText]}
          >
            Enjoy our best customers rewards! you deserve it.
          </Text>

          <Animatable.View animation="bounceInUp" duration={800} useNativeDriver>
            <VirtualCardInfo data={loansTestData.virtualCard} userInfo={sharedTestData.userInfo} />
          </Animatable.View>

          <Animatable.View
            style={themedStyle.item}
            animation="bounceInLeft"
            duration={800}
            delay={400}
            useNativeDriver
          >
            <VirtualCardActivate data={loansTestData.virtualCard} />
          </Animatable.View>

          <Animatable.View
            style={themedStyle.item}
            animation="bounceInRight"
            duration={800}
            delay={600}
            useNativeDriver
          >
            <VirtualCardBilling
              data={loansTestData.virtualCard}
              userInfo={sharedTestData.userInfo}
            />
          </Animatable.View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default withStyles(VirtualCard, () => ({
  centerText: {
    textAlign: 'center',
  },
  screen: {
    flex: 1,
  },
  body: {
    flexGrow: 1,
  },
  wrapper: {
    marginTop: 20,
  },
  content: {
    paddingTop: 40,
  },
  item: {
    marginHorizontal: 30,
  },
}));
