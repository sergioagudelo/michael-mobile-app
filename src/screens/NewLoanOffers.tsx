import React from 'react';
import { SafeAreaView, ScrollView, View } from 'react-native';
import { withStyles, Text, ThemedComponentProps } from 'react-native-ui-kitten';
import { withNavigation } from 'react-navigation';
import { NavigationStackScreenProps } from 'react-navigation-stack';

// Modules
import shared from '../modules/shared';
import loans from '../modules/loans';

// Components
const { Background } = shared.components;
const { NewLoanOfferCard } = loans.components;

type NewLoanOffersComponentProps = NavigationStackScreenProps & ThemedComponentProps;

const NewLoanOffersComponent = ({ navigation, themedStyle }: NewLoanOffersComponentProps) => {
  return (
    <SafeAreaView style={themedStyle.screen} {...shared.helpers.setTestID('NewLoanOffersScreen')}>
      <ScrollView contentContainerStyle={themedStyle.body}>
        <Text category="h1" status="accent" style={themedStyle.centerText}>
          Congrats john!
        </Text>
        <View>
          <Background
            minHeight={270}
            colors={['#4CADE4', '#003267']}
            wrapperStyle={themedStyle.wrapper}
          />
        </View>

        <View style={[themedStyle.content]}>
          <Text appearance="alternative" category="h2" style={themedStyle.centerText}>
            Enjoy our best customers rewards! you deserve it.
          </Text>
          <Text
            appearance="alternative"
            category="c1"
            style={[themedStyle.centerText, themedStyle.uppercase, themedStyle.subtitle]}
          >
            Choose the option that works best for you:
          </Text>
          {loans.utils.testData.newLoanOffers.map(offer => (
            <View key={offer.id} style={themedStyle.item}>
              <NewLoanOfferCard
                offer={offer}
                onPress={() => navigation.navigate('LoanAgreement', { loan: offer })}
              />
            </View>
          ))}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const NewLoanOffersScreen = withStyles(withNavigation(NewLoanOffersComponent), () => ({
  screen: {
    flex: 1,
  },
  body: {
    flexGrow: 1,
  },
  centerText: {
    textAlign: 'center',
  },
  uppercase: {
    textTransform: 'uppercase',
  },
  wrapper: {
    marginTop: 20,
  },
  content: {
    paddingTop: 40,
    paddingHorizontal: 30,
  },
  subtitle: {
    paddingVertical: 20,
  },
  item: {
    paddingVertical: 15,
  },
}));

export default NewLoanOffersScreen;
