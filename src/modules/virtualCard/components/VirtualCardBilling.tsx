import React from 'react';
import { View } from 'react-native';
import { ThemedComponentProps, withStyles, Text } from 'react-native-ui-kitten';

// Types
import { User } from '../../profile/utils';
import { VirtualCardType } from '../utils';

// Components
import shared from '../../shared';
import { Styles } from '../helpers';

const { Format, Dashed } = shared.components;

/**
 * component type
 */
type VirtualCardBillingProps = ThemedComponentProps & {
  userInfo: User;
  data: VirtualCardType;
};

/**
 * render card status/valid/limit
 */
const VirtualCardBillingElement = ({
  themedStyle,
  data,
  userInfo,
}: VirtualCardBillingProps): React.ReactElement => {
  /**
   * return React.ReactElement
   */
  return (
    <View style={[themedStyle.flex, themedStyle.billing]}>
      <View style={[themedStyle.flexRow, themedStyle.row]}>
        <View style={[themedStyle.flex]}>
          <Text category="h2" status="default">
            Billing details
          </Text>
        </View>
      </View>
      <Dashed />
      <View style={[themedStyle.flexRow, themedStyle.row]}>
        <View style={[themedStyle.flex, themedStyle.card]}>
          <Text category="metatext">CARD NUMBER</Text>
          <Text category="label" status="primary">
            {data.card}
          </Text>
        </View>
        <View style={[themedStyle.flex, themedStyle.csv]}>
          <Text category="metatext">CVC</Text>
          <Text category="label" status="primary">
            {data.csv}
          </Text>
        </View>
        <View style={[themedStyle.flex, themedStyle.expires]}>
          <Text category="metatext">EXPIRES</Text>
          <Format type="date" format="MM/YY" category="label" status="primary">
            {data.valid}
          </Format>
        </View>
      </View>
      <Dashed />
      <View style={[themedStyle.flexRow, themedStyle.row]}>
        <View style={[themedStyle.flex]}>
          <Text category="metatext">BILLING ADDRESS</Text>
          <Text category="label" status="primary">
            {userInfo.addressLine1}
          </Text>
        </View>
      </View>
      <Dashed />
      <View style={[themedStyle.flexRow, themedStyle.row]}>
        <View style={[themedStyle.flex]}>
          <Text category="metatext">ZIP</Text>
          <Text category="label" status="primary">
            {userInfo.zipCode}
          </Text>
        </View>
        <View style={[themedStyle.flexRow]}>
          <View style={[themedStyle.flex]}>
            <Text category="metatext">PHONE</Text>
            <Text category="label" status="primary">
              {userInfo.phoneNumber}
            </Text>
          </View>
        </View>
      </View>
      <Dashed />
    </View>
  );
};

/**
 * create styles using theme
 * @see https://akveo.github.io/react-native-ui-kitten/docs/components/withstyles/overview#withstyles
 */
const VirtualCardBilling = withStyles(VirtualCardBillingElement, () => ({
  ...Styles,
  billing: {
    marginBottom: 30,
  },
  row: {
    paddingVertical: 12,
  },
  card: {
    flex: 0.5,
  },
  csv: {
    flex: 0.2,
  },
  expires: {
    flex: 0.3,
  },
}));

export default VirtualCardBilling;
