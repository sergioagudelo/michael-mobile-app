import React from 'react';
import { ThemedComponentProps, withStyles, Text } from 'react-native-ui-kitten';

// Types
import { User } from '../../profile/utils';

import { VirtualCardType } from '../utils';

// Modules
import shared from '../../shared';

// Components
const { VirtualCardLayout } = shared.components;

/**
 * component type
 */
type VirtualCardInfoProps = {
  data: VirtualCardType;
  userInfo: User;
} & ThemedComponentProps;

/**
 * render card with number and userInfo
 */
const VirtualCardInfo = ({
  themedStyle,
  data,
  userInfo,
}: VirtualCardInfoProps): React.ReactElement => {
  /**
   * return React.ReactElement
   */
  return (
    <VirtualCardLayout
      dismissible={false}
      renderContent={() => (
        <Text
          category="h2"
          appearance="alternative"
          style={[
            themedStyle.uppercase,
            themedStyle.centerText,
            themedStyle.text,
            themedStyle.cardNumber,
          ]}
        >
          {data.card}
        </Text>
      )}
      renderFooter={() => (
        <>
          <Text
            category="h2"
            appearance="alternative"
            style={[themedStyle.uppercase, themedStyle.text, themedStyle.cardName]}
          >
            {`${userInfo.firstName} ${userInfo.lastName}`}
          </Text>
          <Text
            category="h2"
            appearance="alternative"
            style={[themedStyle.uppercase, themedStyle.text, themedStyle.cardExpDate]}
          >
            {shared.helpers.format.formatDate(data.valid, 'MM/YY')}
          </Text>
        </>
      )}
    />
  );
};

export default withStyles(VirtualCardInfo, () => ({
  centerText: {
    textAlign: 'center',
  },
  uppercase: {
    textTransform: 'uppercase',
  },
  row: {
    flexDirection: 'row',
  },
  text: {
    fontFamily: 'OCRAExtended',
    textShadowColor: '#092335',
    textShadowOffset: { width: 0, height: 0 },
    textShadowRadius: 10,
    letterSpacing: -0.1,
  },
  cardNumber: {
    fontSize: 23,
  },
  cardName: {
    fontSize: 20,
  },
  cardExpDate: {
    fontSize: 20,
  },
}));
