import { Platform, Linking, Animated } from 'react-native';
import InAppBrowser from 'react-native-inappbrowser-reborn';
import { set, get } from 'lodash';
import { FormikState } from 'formik';
import RNFetchBlob from 'rn-fetch-blob';

// Constants
import { NavigationState } from 'react-navigation';
import { APP_TEMP_DIRECTORY, routesBlacklist } from '../constants';

import { theme } from '../../../theme';
import Ico from './Ico';
import * as hooks from './hooks';
import * as format from './format';
import * as deviceInfo from './deviceInfo';

const getPlatformTestId = (id: string) =>
  Platform.OS === 'ios'
    ? { testID: id, accessible: false }
    : { accessible: true, accessibilityLabel: id };

/**
 * Adds a testID to the views on Android and iOS in their specific ways. On Android,
 * this will result in a ContentDescription on Debug builds (and no changes on live builds).
 */
const setTestID = (id: string) => (__DEV__ ? getPlatformTestId(id) : {});

const SHADOW_COLOR = theme['color-basic-600'];
const SHADOW_OPACITY = 0.35;

const shadow = (elevation: number | Animated.Value = 0) => {
  if (elevation instanceof Animated.Value) {
    const inputRange = [0, 1, 2, 3, 8, 24];

    return {
      shadowColor: SHADOW_COLOR,
      shadowOffset: {
        width: new Animated.Value(0),
        height: elevation.interpolate({
          inputRange,
          outputRange: [0, 0.5, 0.75, 2, 7, 23],
        }),
      },
      shadowOpacity: new Animated.Value(SHADOW_OPACITY),
      shadowRadius: elevation.interpolate({
        inputRange,
        outputRange: [0, 0.75, 1.5, 3, 8, 24],
      }),
    };
  }
  if (elevation === 0) {
    return {};
  }

  let height;
  let radius;
  switch (elevation) {
    case 1:
      height = 0.5;
      radius = 0.75;
      break;
    case 2:
      height = 0.75;
      radius = 1.5;
      break;
    default:
      height = elevation - 1;
      radius = elevation;
  }

  return {
    shadowColor: SHADOW_COLOR,
    shadowOffset: {
      width: 0,
      height,
    },
    shadowOpacity: SHADOW_OPACITY,
    shadowRadius: radius,
  };
};

const openUrl = async (url: string) => {
  try {
    if (await InAppBrowser.isAvailable()) {
      return InAppBrowser.open(url, {
        // iOS Properties
        dismissButtonStyle: 'done',
        // preferredBarTintColor: Colors.bluePrimary,
        // preferredControlTintColor: Colors.whitePrimary,
        readerMode: false,
        animated: true,
        // Android Properties
        showTitle: false,
        // toolbarColor: Colors.bluePrimary,
        // secondaryToolbarColor: Colors.whitePrimary,
        enableUrlBarHiding: true,
        enableDefaultShare: true,
        forceCloseOnRedirection: true,
      });
    }
    if (await Linking.canOpenURL(url)) {
      return Linking.openURL(url);
    }
    return null;
  } catch (error) {
    console.error(error);
    return null;
  }
};

type ActionType = { [key: string]: { [key: string]: string } };

const createActionTypes = (base: string, types: string[]) => {
  const res: ActionType = {};
  types.forEach(type => {
    set(res, type, `${base}/${type.replace('.', '/')}`);
  });
  return res;
};

const callNumber = (phone: string) => {
  let phoneNumber = phone;
  if (Platform.OS !== 'android') {
    phoneNumber = `telprompt:${phone}`;
  } else {
    phoneNumber = `tel:${phone}`;
  }

  Linking.canOpenURL(phoneNumber)
    .then(supported => {
      if (supported) {
        return Linking.openURL(phoneNumber);
      }
    })
    .catch(err => null);
};

function getInputStatus<V>(inputKey: keyof V | string, formik: Partial<FormikState<V>>) {
  if (!get(formik.touched, inputKey)) {
    return undefined;
  }

  if (get(formik.touched, inputKey) && get(formik.errors, inputKey)) {
    return 'danger';
  }
  return 'success';
}

const saveFile = async (filename: string, data: string) => {
  const path = `${APP_TEMP_DIRECTORY}/${filename}`;

  try {
    await RNFetchBlob.fs.writeFile(path, data, 'base64');
    return path;
  } catch (err) {
    throw new Error('Failed saving file');
  }
};

const getActiveRouteName = navigationState => {
  if (!navigationState) {
    return null;
  }
  const route = navigationState.routes[navigationState.index];
  // dive into nested navigators
  if (route.routes) {
    return getActiveRouteName(route);
  }
  return route.routeName;
};

export const isRouteBlacklisted = (route: string) =>
  routesBlacklist.some(blacklisted => route === blacklisted);

export {
  getActiveRouteName,
  setTestID,
  shadow,
  openUrl,
  createActionTypes,
  Ico,
  callNumber,
  hooks,
  format,
  getInputStatus,
  saveFile,
  deviceInfo,
};
