import { useState, useEffect } from 'react';
import { Dimensions, ScaledSize } from 'react-native';

/**
 * @see https://www.reactnativeschool.com/building-a-dimensions-hook-in-react-native
 * @param {"screen"|"window"} [dimension="window"]
 */
export const useScreenDimensions = (dimension: 'screen' | 'window' = 'window') => {
  const [screenData, setScreenData] = useState(Dimensions.get(dimension));

  useEffect(() => {
    const onChange = (result: { window: ScaledSize; screen: ScaledSize }) => {
      setScreenData(result[dimension]);
    };

    Dimensions.addEventListener('change', onChange);

    return () => Dimensions.removeEventListener('change', onChange);
  });

  return {
    ...screenData,
    isLandscape: screenData.width > screenData.height,
  };
};
