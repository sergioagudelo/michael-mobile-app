import { ActionType } from 'typesafe-actions';
import { getAppLocalVersion } from '../services/DeviceInfo';

// TODO DELETE THIS

export const dispatchAppVersion = (store, action: ActionType, payload = getAppLocalVersion()) => {
  store.dispatch(action(payload));
};
