import { Dimensions, ViewStyle } from 'react-native';

const window = Dimensions.get('window');

/**
 * return object with borders for debuging views
 * @param color string
 */
const addBorders = (color: string): object => {
  return {
    borderTopColor: color,
    borderBottomColor: color,
    borderLeftColor: color,
    borderRightColor: color,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderLeftWidth: 1,
    borderRightWidth: 1,
  };
};

/**
 * hide view using view port
 */
const hideView = (): ViewStyle => {
  return {
    top: window.height,
    bottom: -window.height,
  };
};

export default {
  addBorders,
  hideView,
};
