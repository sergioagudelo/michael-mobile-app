import React from 'react';
import { Icon, IconProps } from 'react-native-ui-kitten';
import { ViewStyle } from 'react-native';

/**
 * default icon with/height/fill
 * @see react-native-eva-icons/icons/
 */
const icp: ViewStyle & IconProps = {
  width: 17,
  height: 17,
  fill: '#DEE2E8',
};

/**
 * define icons we need for the app
 * @see react-native-eva-icons/icons/
 */
const Ico = ({ name, ...props }: IconProps) => <Icon name={name} {...icp} {...props} />;

export default Ico;
