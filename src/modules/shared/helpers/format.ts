import moment from 'moment';
import {
  MaskService,
  TextInputMaskOptionProp,
  TextInputMaskTypeProp,
} from 'react-native-masked-text';
import { Omit } from 'utility-types';
import { PaymentMethodType } from '../../payments/utils';
import { LoanDetailsType } from '../../loans/utils';

export const formatCardNumber = (cardNumber: string) =>
  cardNumber.slice(-8).replace('XXXX', '••••');

export const formatBankAccountNumber = (accountingNumber: string) =>
  `••••${accountingNumber ? accountingNumber.slice(-4) : ''}`; //

export const getLoanAutopayPaymentMethod = (loan: LoanDetailsType) =>
  loan.isAch
    ? {
        number: formatBankAccountNumber(loan.withdrawalAccountNumber??'xxxxxxxxx'),
        name: loan.withdrawalBankName,
      }
    : {
      number: formatCardNumber(loan.debitCardNumber??'xxxxxxxxx'),
        name: 'Debit card',
      };

export const formatPaymentMethodNumber = (method: PaymentMethodType|undefined) =>{
  if (!method) {
    return '••••'
  }
  return method.isACH
    ? formatBankAccountNumber(method.accountingNumber)
    : formatCardNumber(method.cardNumber);
}
// TODO ADD SUPPORT FOR +1 COUNTRY CODE
// (321) 654-9870
export const formatPhone = (phoneNumber: string): string => {
  const match = phoneNumber.match(/^(\d{3})(\d{3})(\d{4})$/);
  return match?`(${match[1]}) ${match[2]}-${match[3]}`:'';
};

/**
 * format string as number ex: 2000 = 2,000
 * @param value string the value for format
 * @param fractions 0 to 20
 * @param style string decimal | currency | percent
 * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/NumberFormat
 */
export const formatNumber = (value: string, fractions = 0, style = 'currency'): string => {
  if (value === '' || value === undefined) {
    return value;
  }
  if (!(style in { decimal: true, currency: true, percent: true })) {
    return value.toString();
  }
  const safeValue = value.replace ? Number(value.replace(/[^0-9-.]+/, '')) : value;
  return new Intl.NumberFormat('en-US', {
    style,
    currency: 'USD',
    minimumFractionDigits: fractions,
  }).format(safeValue as number);
};

/**
 *
 * @param value string any valid date representation
 * @param format string any valid moment format
 * @see https://momentjs.com/docs/#/displaying/
 */
export const formatDate = (value: string | number | Date, format = 'MM/DD/YYYY'): string => {
  return moment(value).format(format);
};

export const formatMoney = (value: number, config?: Intl.NumberFormatOptions) => {
  const options: Intl.NumberFormatOptions = {
    style: 'currency',
    currency: 'USD',
    ...config,
  };

  // ! if its a whole, dollar amount, leave off the .00
  if (value % 100 === 0) options.minimumFractionDigits = 2;
  const formatter = new Intl.NumberFormat('en-US', options);

  return formatter.format(value);
};

export const formatPercent = (value: number, options?: Omit<Intl.NumberFormatOptions, 'style'>) =>
  new Intl.NumberFormat('en-US', {
    style: 'percent',
    minimumFractionDigits: 2,
    ...options,
  }).format(value / 100);

export const formatCustom = (
  value: number | string,
  mask: TextInputMaskTypeProp,
  options: TextInputMaskOptionProp,
) => MaskService.toMask(mask, `${value}`, options);
