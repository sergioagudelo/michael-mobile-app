//type of response given by ouath token api
export type ApiManagerOAuthTokenResponse = {
  uuid: string;
  accessToken: string;
  refreshToken: string;
  tokenType: string;
  expires: Number;
};

//type of response give by app version api
export type AppVersionResponse = {
  id: string;
  data: {
    latestVersionIOS: string;
    latestVersionAndroid: string;
    versionType: string;
  };
  type: string;
};

//type of response given by remote version method @fetchRemoteAppVersionNumber
export type AppVersion = {
  version: string;
  type: string;
};
