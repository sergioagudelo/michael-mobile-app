import { getSystemName } from "react-native-device-info";
import {
  ApiManagerOAuthTokenResponse,
  AppVersionResponse,
  AppVersion,
} from "./types";

export const ANDROID_SYSTEM_NAME = "Android";

export const checkIfAllInformationForOAuthTokenProvided = (config: any) =>
  config.api.appVersions.grantType &&
  config.api.appVersions.username &&
  config.api.appVersions.password &&
  config.api.appVersions.clientId &&
  config.api.appVersions.clientSecret;

export const getAppVersionOAuthTokenInfo = (config: any) =>
  checkIfAllInformationForOAuthTokenProvided(config) && {
    grantType: config.api.appVersions.grantType,
    username: config.api.appVersions.username,
    password: config.api.appVersions.password,
    clientId: config.api.appVersions.clientId,
    clientSecret: config.api.appVersions.clientSecret,
  };

export const getAccessToken = (oAuthResponse: ApiManagerOAuthTokenResponse) =>
  oAuthResponse.accessToken;

export const getAppVersion = (appVersionResponse: AppVersionResponse): AppVersion => {
  const versionCode =
    getSystemName() === ANDROID_SYSTEM_NAME
      ? appVersionResponse.data.latestVersionAndroid
      : appVersionResponse.data.latestVersionIOS;
  return {
    version: versionCode,
    type: appVersionResponse.data.versionType,
  };
};
