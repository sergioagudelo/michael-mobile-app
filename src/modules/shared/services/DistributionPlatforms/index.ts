import Axios from 'axios';
import { handleError } from '../../../../utils/services';
import config from '../../../../config';
import { HTTPServicesResponse } from '../../../../services/utils';
import {
  ApiManagerOAuthTokenResponse,
  AppVersionResponse,
  AppVersion,
} from './types';
import {
  getAppVersionOAuthTokenInfo,
  getAccessToken,
  getAppVersion,
} from './helpers';

const apiManager = Axios.create({
  baseURL: config.api.host,
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
  },
});

export const fetchRemoteAppVersionNumber = async (): Promise<
  HTTPServicesResponse<AppVersion>
> => {
  if (!config.disableForceUpdate) {
    return { success: false };
  }

  try {
    const accessToken = config.api.appVersions.bearerToken;

    if (!accessToken) {
      return { success: false };
    }

    const appVersions = await apiManager.get<AppVersionResponse>(
      '/utilities/v1/mobile/versions',
      {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      }
    );

    return { success: true, payload: getAppVersion(appVersions.data) };
  } catch (e) {
    console.log(e);
    return handleError(e);
  }
};
