import * as Sentry from '@sentry/react-native';
import config from '../../../config';

Sentry.init({
  dsn: config.sentry.dsn,
  debug: __DEV__,
  environment: config.environment,
});

export const { captureException, captureMessage, configureScope } = Sentry;
