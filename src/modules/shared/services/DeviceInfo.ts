import {
  getVersion,
  getBaseOs,
  getBrand,
  getCarrier,
  getDeviceId,
  getFingerprint,
  getUserAgent,
  getSystemName,
  getSystemVersion,
  getReadableVersion,
} from 'react-native-device-info';
import RNFetchBlob from 'rn-fetch-blob';

type LocalVersionType = {
  [key: string]: string,
}

export const getOsName = (): any => getSystemName();
export const getAppLocalVersion = () : LocalVersionType => ({ [getSystemName()]: getVersion() });
export const getDeviceInfoForPush = async () => {
  const baseOs = await getBaseOs() ?? null ;
  const carrier = await getCarrier() ?? null;
  const fingerPrint = await getFingerprint() ?? null;
  const userAgent = await getUserAgent() ?? null;
  return RNFetchBlob.base64.encode(JSON.stringify({
    baseOs,
    brand: getBrand(),
    carrier,
    deviceId: getDeviceId(),
    fingerPrint,
    userAgent,
    systemName: getSystemName(),
    systemVersion: getSystemVersion(),
    readableVersion: getReadableVersion(),
  }))
  };
