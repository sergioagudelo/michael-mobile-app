import messaging from '@react-native-firebase/messaging';
import { handleError } from '../../../../utils/services';

export const requestUserPermission = async () => {
  try {
    const settings = await messaging().requestPermission();
      return settings ?? null;
  } catch (e) {
    handleError(e);
  }
};

// export const checkUserPermission = async () => {
//   try {
//     const authStatus = await messaging().hasPermission();
//     return authStatus ?? null;
//   } catch (e) {
//     console.log('err');
//     // handleError(e);
//   }
// };

export const registerAppWithFCM = async () => {
  try {
    await messaging().registerDeviceForRemoteMessages();
  } catch (e) {
    handleError(e);
  }
};
export const getFCMToken = async () => {
  try {
    const token = await messaging().getToken();
    return token??null
  } catch (e) {
    handleError(e);
  }
};


