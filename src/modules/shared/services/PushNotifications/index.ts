import * as cloudMessaging from './cloudMessaging';

export { cloudMessaging };
export const registerForPush = async () => {
  try {
    await cloudMessaging.requestUserPermission();
    const token = await cloudMessaging.getFCMToken();
    console.log('TOKEN', token);
    return token ?? 'ERROR-GETTING-TOKEN'
  } catch (e) {
    return null
  }
};
