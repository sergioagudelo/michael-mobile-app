import { createSelector } from 'reselect';
import { SharedState } from '../reducers';
import { NavigationHistoryEntry } from '../../utils';

const historySelector = (state: SharedState) => state.navigationHistory;

export const getNavigationHistory = createSelector(
  historySelector,
  (item: NavigationHistoryEntry) => item,
);
