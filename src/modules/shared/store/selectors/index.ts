import { RootState } from 'typesafe-actions';

import { NAME } from '../../constants';

import * as fromDeviceInfo from './deviceInfo';

import * as navigationHistory from './navigationHistory';

export const getIsNetConnected = (state: RootState) =>
  fromDeviceInfo.getIsNetConnected(state[NAME].device);

export const getBiometricAuthPermission = (state: RootState) =>
  fromDeviceInfo.getBiometricAuthPermission(state[NAME].device);

export const getIsBiometricAuthSupported = (state: RootState) =>
  fromDeviceInfo.getIsBiometricAuthSupported(state[NAME].device);
export const getIsBioAuthPromptRejected = (state: RootState) =>
  fromDeviceInfo.getIsBioAuthPromptRejected(state[NAME].device);

export const getModalShown = (state: RootState) => fromDeviceInfo.getModalShown(state[NAME].device);

export const getAppLocalVersion = (state: RootState) =>
  fromDeviceInfo.getAppLocalVersion(state[NAME].device);

export const getNavigationHistory = (state: RootState) =>
  navigationHistory.getNavigationHistory(state[NAME]);
