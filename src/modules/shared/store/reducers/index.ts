import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';
import deviceInfoReducer from './deviceInfo';
import navigationHistoryReducer from './navigationHistory';

const sharedReducer = combineReducers({
  device: deviceInfoReducer,
  navigationHistory: navigationHistoryReducer,
});

export default persistReducer({key: 'shared', storage: AsyncStorage, blacklist: ['navigationHistory'], whitelist: ['device']}, sharedReducer);
export type SharedState = ReturnType<typeof sharedReducer>;
