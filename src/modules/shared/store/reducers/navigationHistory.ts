import { createReducer } from 'typesafe-actions';

import { actions } from '../actions';
import { NavigationHistoryEntry } from '../../utils';

const navigationHistoryReducer = createReducer({} as NavigationHistoryEntry).handleAction(
  actions.updateNavigationHistory,
  (state, action) => action.payload,
);

export default navigationHistoryReducer;
