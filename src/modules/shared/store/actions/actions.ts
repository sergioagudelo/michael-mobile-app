import { createAction } from 'typesafe-actions';
import types from './types';
import { NavigationHistoryEntry } from '../../utils';

const getNetInfo = createAction(
  types.NET_INFO.GET,
  action => (data: { isConnected: boolean; isInternetReachable: boolean }) => action({ ...data }),
);

const updateNavigationHistory = createAction(
  types.NAVIGATION_HISTORY.SET,
  action => (payload: NavigationHistoryEntry) => action({ ...payload }),
);

const setBiometricAuthPermission = createAction(
  types.BIOMETRIC_AUTH.SET,
  action => (bool: boolean) => action(bool),
);

const isBiometricAuthSupported = createAction(
  types.BIOMETRIC_AUTH.IS_SUPPORTED,
  action => (bool: boolean) => action(bool),
);

const setModalShown = createAction(types.MODAL_SHOWN.SET, action => (bool: boolean) =>
  action(bool),
);
const setBioAuthPromptRejected = createAction(
  types.BIOMETRIC_AUTH_PROMPT.SET,
  action => (bool: boolean) => action(bool),
);

const setAppLocalVersion = createAction(
  types.APP.SET_LOCAL_VERSION,
  action => (payload: Record<string, any>) => action(payload),
);
// const getAppRemoteVersion = createAction(
//   types.APP.GET_REMOTE_VERSION,
//   action => (payload: Record<string, any>) => action(payload),
// );

export {
  getNetInfo,
  updateNavigationHistory,
  setBiometricAuthPermission,
  setModalShown,
  setBioAuthPromptRejected,
  isBiometricAuthSupported,
  setAppLocalVersion,
  // getAppRemoteVersion,
};
