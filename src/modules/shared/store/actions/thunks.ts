import { Dispatch } from "redux";

import * as actions from "./actions";
import * as services from "../../services";

const { DeviceInfo, DistributionPlatforms } = services;

export const getLocalAppVersionNumber = () => (dispatch: Dispatch) => {
  dispatch(actions.setAppLocalVersion(DeviceInfo.getAppLocalVersion()));
};

// TODO UPDATE APP VERSION
export const updateAppVersion = () => null; // console.log('>>> UPDATING');
const checkIsBiggerVersion = (_local: string, _remote: string) =>
  parseFloat(_local.slice(2)) < parseFloat(_remote.slice(2));

type AppUpdateInfo = {
  shouldAppUpdate: boolean;
  isSoftUpdate?: boolean;
};

export const checkAvailableUpdate = async (
  dispatch: Dispatch
): Promise<AppUpdateInfo> => {
  const version = DeviceInfo.getAppLocalVersion();
  dispatch(actions.setAppLocalVersion(version));
  const remote = await DistributionPlatforms.fetchRemoteAppVersionNumber();
  if (remote.success && remote.payload && remote.payload.version) {
    const os = DeviceInfo.getOsName();

    if (checkIsBiggerVersion(version[os], remote.payload.version)) {
      return {
        shouldAppUpdate: true,
        isSoftUpdate: remote.payload.type === 'soft'
      };
    }
  }
  return {
    shouldAppUpdate: false
  };
};
