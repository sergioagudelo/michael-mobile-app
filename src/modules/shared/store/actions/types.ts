import { NAME } from '../../constants';

import * as helpers from '../../helpers';

export default helpers.createActionTypes(NAME, [
  'NET_INFO.GET',
  'NAVIGATION_HISTORY.SET',
  'BIOMETRIC_AUTH.SET',
  'BIOMETRIC_AUTH.IS_SUPPORTED',
  'MODAL_SHOWN.SET',
  'BIOMETRIC_AUTH_PROMPT.SET',
  'APP.SET_LOCAL_VERSION',
  'APP.GET_REMOTE_VERSION',
]);
