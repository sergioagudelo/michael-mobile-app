import { connect } from 'react-redux';
import { RootState } from 'typesafe-actions';

// Components
import NetWorkIndicator from '../components/NetworkIndicator';

// Store
import * as store from '../store';

const mapStateToProps = (state: RootState) => ({
  isNetConnected: store.selectors.getIsNetConnected(state),
});

export default connect(mapStateToProps)(NetWorkIndicator);
