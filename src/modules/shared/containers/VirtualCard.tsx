import React, { useState } from 'react';
import * as Animatable from 'react-native-animatable';

// Modules
// import shared from '../../shared';

// Components
import { VirtualCard, VirtualCardComingSoonModal } from '../components';
// const { Loading } = shared.components;

const VirtualCardContainer = () => {
  const [isModalVisible, setIsModalVisible] = useState(false);

  return (
    <>
      <VirtualCardComingSoonModal
        title="Coming soon!"
        body="Just another way we’re making it easier to get the money you need, when you need it."
        buttonTitle="OK"
        visible={isModalVisible}
        onDismiss={() => setIsModalVisible(false)}
      />
      <Animatable.View animation="slideInUp" duration={800} useNativeDriver>
        <VirtualCard onPress={() => setIsModalVisible(true)} dismissible={false} />
      </Animatable.View>
    </>
  );
};

export default VirtualCardContainer;
