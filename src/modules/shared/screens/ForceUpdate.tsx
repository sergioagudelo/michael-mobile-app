/* eslint-disable global-require */
import React from "react";
import { getSystemName } from "react-native-device-info";
import { useSelector } from "react-redux";
import { SafeAreaView, Image, ScrollView, Linking } from "react-native";
import {
  ThemedComponentProps,
  withStyles,
  Text,
  Layout,
  Button,
} from "react-native-ui-kitten";
import { NavigationStackScreenProps } from "react-navigation-stack";

type ForceUpdateProps = ThemedComponentProps & NavigationStackScreenProps;

const IS_SOFT_UPDATE_NAVIGATION_PARAM = "isSoftUpdate";
const ANDROID_SYSTEM_NAME = "Android";

const ForceUpdate = ({ themedStyle, navigation }: ForceUpdateProps) => {
  const previousRouteName = useSelector(
    (state: any) => state.shared.navigationHistory.previousRouteName
  );
  const isSoftUpdate = navigation.getParam(
    IS_SOFT_UPDATE_NAVIGATION_PARAM,
    false
  );

  // TODO KILL OTHER APP NAVIGATION CALLBACKS
  const handleStoreButton = async () => {
    // TODO DEEPLINKING DYNAMIC AND WORKING
    if (getSystemName() === ANDROID_SYSTEM_NAME) {
      await Linking.openURL(
        "market://details?id=com.lendingpoint.customerportal"
      );
    } else {
      await Linking.openURL(
        "itms-apps://itunes.apple.com/us/app/id1488985079?mt=8"
      );
    }
  };

  const goBackToPreviousRoute = () => {
    const paramsToOverrideBlocking = {
      overrideBlocking: true,
    };
    if (!previousRouteName) {
      navigation.navigate("Login", paramsToOverrideBlocking);
    } else {
      navigation.navigate(previousRouteName, paramsToOverrideBlocking);
    }
  };

  return (
    <SafeAreaView style={themedStyle.screen}>
      <ScrollView
        contentContainerStyle={{
          flexGrow: 1,
        }}
      >
        <Layout style={themedStyle.screenContainer}>
          <Layout style={themedStyle.logoContainer}>
            <Image
              source={{ uri: "lp_logo_hz" }}
              resizeMode="contain"
              style={themedStyle.logo}
            />
          </Layout>
          <Layout style={themedStyle.titleContainer}>
            <Text category="metatext" style={themedStyle.uppercase}>
              NEW VERSION AVAILABLE
            </Text>
          </Layout>
          <Layout style={themedStyle.imageContainer}>
            <Image
              source={
                isSoftUpdate
                  ? require("../../../img/icons/ico-update-soft.png")
                  : require("../../../img/icons/ico-update-hard.png")
              }
            />
          </Layout>
          {!isSoftUpdate ? (
            <Layout style={themedStyle.sorryContainer}>
              <Layout style={themedStyle.sorry}>
                <Text
                  style={{ textAlign: "center" }}
                  category="h1"
                  status="warning"
                >
                  Update is required
                </Text>
              </Layout>
            </Layout>
          ) : null}
          {isSoftUpdate ? (
            <Layout style={themedStyle.somethingContainer}>
              <Layout style={themedStyle.something}>
                <Text
                  style={{ textAlign: "center" }}
                  category="h2"
                  status="primary"
                >
                  A new version is waiting for you!
                </Text>
              </Layout>
            </Layout>
          ) : null}

          <Layout style={themedStyle.callUsContainer}>
            <Text style={themedStyle.callUsText}>
              {isSoftUpdate
                ? "We are constantly working on our application, making it faster and more reliable for you."
                : "The current version is no longer supported. Please update the app"}
            </Text>
          </Layout>

          {isSoftUpdate ? (
            <Layout style={themedStyle.auxDescription}>
              <Text style={themedStyle.callUsText} status="primary">
                That's why is so important having always the lastest version.
              </Text>
            </Layout>
          ) : null}

          <Button style={themedStyle.button} onPress={handleStoreButton}>
            UPDATE NOW
          </Button>
          {isSoftUpdate ? (
            <Button
              style={[themedStyle.buttonContinue]}
              textStyle={[themedStyle.callUsText]}
              size="small"
              appearance="link"
              onPress={goBackToPreviousRoute}
            >
              SKIP & CONTINUE
            </Button>
          ) : null}
        </Layout>
      </ScrollView>
    </SafeAreaView>
  );
};

export default withStyles(ForceUpdate, (theme) => ({
  screen: {
    flex: 1,
  },
  screenContainer: {
    flexGrow: 1,
    paddingHorizontal: 30,
    paddingBottom: 50,
  },
  logoContainer: {
    borderBottomColor: theme["color-basic-500"],
    borderBottomWidth: 2,
  },
  logo: {
    alignSelf: "center",
    width: "80%",
    minHeight: 80,
  },
  imageContainer: {
    justifyContent: "center",
    alignItems: "center",
    marginHorizontal: 30,
    marginTop: 30,
    elevation: 2,
  },
  titleContainer: {
    paddingTop: 15,
    alignSelf: "center",
  },
  uppercase: {
    textTransform: "uppercase",
  },
  sorryContainer: {
    paddingBottom: 8,
  },
  sorry: {
    alignSelf: "center",
    paddingTop: 25,
  },
  somethingContainer: {
    paddingBottom: 20,
    marginHorizontal: 30,
  },
  something: {
    alignSelf: "center",
    paddingTop: 25,
  },
  callUsContainer: {
    justifyContent: "center",
    alignItems: "center",
    marginHorizontal: 50,
    marginTop: 15,
  },
  callUsText: {
    textAlign: "center",
  },
  button: {
    marginTop: 40,
    marginHorizontal: 70,
  },
  buttonContinue: {
    marginTop: 20,
    marginHorizontal: 90,
  },
  auxDescription: {
    marginTop: 30,
  },
}));
