/* eslint-disable global-require */
import React from 'react';
import { SafeAreaView, Image, ScrollView } from 'react-native';
import { ThemedComponentProps, withStyles, Text, Layout, Button } from 'react-native-ui-kitten';
import { NavigationStackScreenProps } from 'react-navigation-stack';
import { parsePhoneNumberFromString } from 'libphonenumber-js';

import { RootState } from 'typesafe-actions';
import { connect } from 'react-redux';
import * as helpers from '../helpers';
import * as store from '../store';
import config from '../../../config';

const mapStateToProps = (state: RootState) => ({
  navigationHistory: store.selectors.getNavigationHistory(state),
});

type TryAgainPageProps = ThemedComponentProps &
  NavigationStackScreenProps &
  ReturnType<typeof mapStateToProps>;

const TryAgainPage = ({
  themedStyle,
  navigation,
  navigationHistory: { previousRouteName },
}: TryAgainPageProps) => {
  const isLogged = navigation.getParam('isLogged');

  // TODO: Refactor to external component
  const contactPhone = parsePhoneNumberFromString(config.contactPhone, 'US');
  let formattedContactPhone = '';
  if (contactPhone) {
    formattedContactPhone = contactPhone.formatNational();
  }

  return (
    <SafeAreaView style={themedStyle.screen}>
      <ScrollView
        contentContainerStyle={{
          flexGrow: 1,
        }}
      >
        <Layout style={themedStyle.screenContainer}>
          <Layout style={themedStyle.logoContainer}>
            <Image source={{ uri: 'lp_logo_hz' }} resizeMode="contain" style={themedStyle.logo} />
          </Layout>

          <Layout style={themedStyle.titleContainer}>
            <Text category="metatext" style={themedStyle.uppercase}>
              Something is not right
            </Text>
          </Layout>

          <Layout style={themedStyle.imageContainer}>
            <Image source={require('../../../img/icons/ico_alert_error.png')} />
          </Layout>

          <Layout style={themedStyle.sorryContainer}>
            <Layout style={themedStyle.sorry}>
              <Text style={{ textAlign: 'center' }} category="h1" status="warning">
                Sorry... it's not you it's us.
              </Text>
            </Layout>
          </Layout>

          <Layout style={themedStyle.somethingContainer}>
            <Layout style={themedStyle.something}>
              <Text style={{ textAlign: 'center' }} category="h2" status="primary">
                Something didn't go right. Let's try again.
              </Text>
            </Layout>
          </Layout>

          <Layout style={themedStyle.callUsContainer}>
            <Text style={themedStyle.callUsText}>
              {'You can also give us a call\nat '}
              <Text
                onPress={() => {
                  helpers.callNumber(config.contactPhone);
                }}
                status="primary"
              >
                {formattedContactPhone}
              </Text>
            </Text>
          </Layout>

          <Button
            style={themedStyle.button}
            onPress={() =>
              navigation.navigate(
                helpers.isRouteBlacklisted(previousRouteName) ? 'Dashboard' : previousRouteName,
              )
            }
          >
            TRY AGAIN
          </Button>
          <Button
            status="basic"
            style={themedStyle.button}
            onPress={() =>
              isLogged
                ? navigation.navigate(previousRouteName === 'Loans' ? 'Login' : 'Loans')
                : navigation.navigate('Login')
            }
          >
            GO HOME
          </Button>
        </Layout>
      </ScrollView>
    </SafeAreaView>
  );
};

export default connect(mapStateToProps)(
  withStyles(TryAgainPage, theme => ({
    screen: {
      flex: 1,
    },
    screenContainer: {
      flexGrow: 1,
      paddingHorizontal: 30,
      paddingBottom: 50,
    },
    logoContainer: {
      borderBottomColor: theme['color-basic-500'],
      borderBottomWidth: 2,
    },
    logo: {
      alignSelf: 'center',
      width: '80%',
      minHeight: 80,
    },
    imageContainer: {
      justifyContent: 'center',
      alignItems: 'center',
      marginHorizontal: 30,
      marginTop: 50,
      elevation: 2,
    },
    titleContainer: {
      paddingTop: 15,
      alignSelf: 'center',
    },
    uppercase: {
      textTransform: 'uppercase',
    },
    sorryContainer: {
      paddingBottom: 8,
    },
    sorry: {
      alignSelf: 'center',
      paddingTop: 25,
    },
    somethingContainer: {
      paddingBottom: 20,
      marginHorizontal: 30,
    },
    something: {
      alignSelf: 'center',
      paddingTop: 25,
    },
    callUsContainer: {
      justifyContent: 'center',
      alignItems: 'center',
      marginHorizontal: 50,
      marginTop: 15,
    },
    callUsText: {
      textAlign: 'center',
    },
    button: {
      marginTop: 25,
      marginHorizontal: 70,
    },
  })),
);
