import React from 'react';
import { ThemedComponentProps, withStyles, Text } from 'react-native-ui-kitten';
import { NavigationStackProp } from 'react-navigation-stack';
import { withNavigation } from 'react-navigation';

// Modules
import VirtualCardLayout from './VirtualCardLayout';

// Components

type VirtualCardProps = {
  navigation: NavigationStackProp;
  dismissible: boolean;
  onPress: () => void;
} & ThemedComponentProps;

const VirtualCard = ({ dismissible, navigation, onPress, themedStyle }: VirtualCardProps) => {
  return (
    <VirtualCardLayout
      // onPress={() => navigation.navigate('VirtualCard')}
      onPress={() => onPress()}
      dismissible={dismissible}
      renderContent={() => (
        <Text
          category="h2"
          appearance="alternative"
          style={[themedStyle.uppercase, themedStyle.text, themedStyle.cardNumber]}
        >
          VIRTUAL CARD
        </Text>
      )}
      renderFooter={() => (
        <>
          <Text
            category="h2"
            appearance="alternative"
            style={[themedStyle.uppercase, themedStyle.text, themedStyle.cardName]}
          >
            Coming soon!
          </Text>
          {/* <Text
            category="h2"
            appearance="alternative"
            style={[themedStyle.uppercase, themedStyle.text, themedStyle.cardExpDate]}
          >
            {shared.helpers.format.formatDate(testData.virtualCard.valid, 'MM/YY')}
          </Text> */}
        </>
      )}
    />
  );
};

export default withNavigation(
  withStyles(VirtualCard, () => ({
    text: {
      fontFamily: 'OCRAExtended',
      textShadowColor: '#092335',
      textShadowOffset: { width: 0, height: 0 },
      textShadowRadius: 10,
      letterSpacing: -0.1,
    },
    uppercase: {
      textTransform: 'uppercase',
    },
    cardNumber: {
      fontSize: 23,
    },
    cardName: {
      fontSize: 20,
    },
    cardExpDate: {
      fontSize: 20,
    },
  })),
);
