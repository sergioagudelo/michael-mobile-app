import React from 'react';
import * as Progress from 'react-native-progress';
import { Layout, Modal, Text, withStyles, ThemedComponentProps } from 'react-native-ui-kitten';

type NetworkIndicatorProps = ThemedComponentProps & {
  isNetConnected: boolean;
};

const NetworkIndicatorComponent = ({
  isNetConnected,
  theme,
  themedStyle,
}: NetworkIndicatorProps) => (
  <Modal
    allowBackdrop
    onBackdropPress={() => {}}
    backdropStyle={themedStyle.backdrop}
    visible={!isNetConnected}
  >
    <Layout style={themedStyle.container}>
      <Layout style={themedStyle.loading}>
        <Progress.CircleSnail
          color={[theme['color-primary-500'], theme['color-warning-500'], theme['color-info-500']]}
          animating={!isNetConnected}
        />
      </Layout>
      <Text style={themedStyle.text}>Waiting for Network...</Text>
    </Layout>
  </Modal>
);

const NetworkIndicator = withStyles(NetworkIndicatorComponent, () => ({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
  },
  loading: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  backdrop: {
    backgroundColor: 'black',
    opacity: 0.5,
  },
  text: {
    marginLeft: 8,
  },
}));

export default NetworkIndicator;
