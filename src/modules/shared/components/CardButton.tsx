import React, { PropsWithChildren } from 'react';
import { Image, ImageSourcePropType, TouchableOpacity } from 'react-native';
import { withStyles, Layout, ThemedComponentProps } from 'react-native-ui-kitten';
import LinearGradient from 'react-native-linear-gradient';

// Helpers
import * as helpers from '../helpers';

type CardButtonComponent = ThemedComponentProps &
  PropsWithChildren<{
    icon: ImageSourcePropType;
    onPress: () => void;
    disabled?: boolean;
  }>;

const CardButtonComponent = ({
  icon,
  themedStyle,
  children,
  onPress,
  disabled = false,
}: CardButtonComponent) => (
  <TouchableOpacity disabled={disabled} onPress={onPress}>
    <Layout style={themedStyle.container}>
      <LinearGradient
        useAngle
        angle={270}
        angleCenter={{ x: 0.5, y: 0.5 }}
        style={themedStyle.iconContainer}
        colors={['rgba(255, 255, 255, 1)', 'rgba(237, 238, 240, 0.6)']}
      >
        <Image source={icon} style={themedStyle.icon} />
      </LinearGradient>

      {children && (
        <>
          <Layout style={themedStyle.divider} />
          <Layout style={themedStyle.content}>{children}</Layout>
        </>
      )}
    </Layout>
  </TouchableOpacity>
);

const CardButton = withStyles(CardButtonComponent, theme => ({
  container: {
    flex: 1,
    height: 70,
    marginTop: 25,
    flexDirection: 'row',
    borderColor: theme['color-basic-400'],
    borderWidth: 1,
    elevation: 2,
    ...helpers.shadow(2),
  },
  iconContainer: {
    width: 70,
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    width: 43,
    height: 43,
  },
  divider: {
    backgroundColor: theme['border-basic-color-2'],
    marginVertical: 10,
    width: 2,
    borderStyle: 'solid',
    marginRight: 10,
  },
  content: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
}));

export default CardButton;
