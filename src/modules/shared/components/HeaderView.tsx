import React from 'react';
import { Text, ThemedComponentProps, withStyles } from 'react-native-ui-kitten';
import { View } from 'react-native';

type HeaderViewElementProps = ThemedComponentProps & {
  title: string;
  subTitle?: string;
};

const HeaderViewElement = ({
  themedStyle,
  title,
  subTitle,
}: HeaderViewElementProps): React.ReactElement => {
  return (
    <View>
      <Text category="h1" status="accent" style={themedStyle.centerText}>
        {title}
      </Text>
      <Text
        category="label"
        appearance="hint"
        style={[themedStyle.centerText, themedStyle.uppercase, themedStyle.subtitle]}
      >
        {subTitle}
      </Text>
    </View>
  );
};

const HeaderView = withStyles(HeaderViewElement, () => ({
  centerText: {
    textAlign: 'center',
  },
  uppercase: {
    textTransform: 'uppercase',
  },
  subtitle: {
    paddingTop: 10,
  },
}));

export default HeaderView;
