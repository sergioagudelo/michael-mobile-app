import React, { PropsWithChildren } from 'react';
import { View, StyleProp, ViewStyle } from 'react-native';
import { withStyles, ThemedComponentProps } from 'react-native-ui-kitten';
import LinearGradient, { LinearGradientProps } from 'react-native-linear-gradient';
import { Omit } from 'utility-types';

// Helpers
import * as helpers from '../helpers';

type BackgroundComponentProps = PropsWithChildren<
  {
    colors: LinearGradientProps['colors'];
    linearGradientProps?: Omit<Omit<LinearGradientProps, 'colors'>, 'style'>;
    wrapperStyle?: StyleProp<ViewStyle>;
    minHeight?: number;
  } & ThemedComponentProps
>;

const BackgroundComponent = ({
  colors,
  linearGradientProps,
  wrapperStyle,
  themedStyle,
  minHeight,
}: BackgroundComponentProps) => {
  const { width } = helpers.hooks.useScreenDimensions();
  return (
    <View style={[themedStyle.main, wrapperStyle]}>
      <LinearGradient
        useAngle
        angle={-225}
        angleCenter={{ x: 0.5, y: 0.5 }}
        colors={colors}
        style={{ minHeight: minHeight || 200 }}
        {...linearGradientProps}
      />
      <View
        style={[
          themedStyle.middle,
          {
            borderLeftWidth: width / 2,
            borderRightWidth: width / 2,
          },
        ]}
      />
    </View>
  );
};

const Background = withStyles(BackgroundComponent, theme => ({
  main: {
    position: 'absolute',
  },
  middle: {
    top: -50,
    borderTopWidth: 50,
    borderLeftColor: theme['color-basic-100'],
    borderRightColor: theme['color-basic-100'],
    borderTopColor: 'transparent',
  },
}));

export default Background;
