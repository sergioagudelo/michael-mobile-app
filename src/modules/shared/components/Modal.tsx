import React, { useEffect, useCallback, useRef } from 'react';
import { View } from 'react-native';
import {
  withStyles,
  Text,
  Layout,
  ListItem,
  Button,
  ThemedComponentProps,
  ModalService,
} from 'react-native-ui-kitten';
import color from 'color';
import { useScreenDimensions } from '../helpers/hooks';
import Dashed from './Dashed';

export type ModalProps = ThemedComponentProps & {
  visible: boolean;
  options: string[];
  onDismiss?: () => void;
  title: string;
  buttonTitle: string;
};

const ModalComponent = ({
  title,
  buttonTitle,
  visible,
  options,
  onDismiss,
  theme,
  themedStyle,
}: ModalProps) => {
  const { height, width } = useScreenDimensions();
  const modalId = useRef('');

  const hideModal = useCallback(() => {
    modalId.current = ModalService.hide(modalId.current);
    if (onDismiss && visible) {
      onDismiss();
    }
  }, [onDismiss, visible]);

  const renderModal = useCallback(
    () => (
      <Layout style={themedStyle.choosePaymentModal}>
        <Text
          category="h3"
          status="accent"
          style={[themedStyle.centerText, themedStyle.modalTitle]}
        >
          {title}
        </Text>
        <Dashed color={theme['color-primary-700']} customStyle={themedStyle.itemSeparator} />
        {options.map((option, i) => (
          <React.Fragment key={i}>
            {options.length === 1 ? (
              <ListItem description={option} />
            ) : (
              <ListItem style={themedStyle.uppercase} title={option} />
            )}
            {i !== options.length - 1 && (
              <Dashed color={theme['color-basic-600']} customStyle={themedStyle.itemSeparator} />
            )}
          </React.Fragment>
        ))}
        <View style={themedStyle.modalCloseButton}>
          <Button appearance="outline" status="basic" onPress={hideModal}>
            {buttonTitle}
          </Button>
        </View>
      </Layout>
    ),
    [
      buttonTitle,
      hideModal,
      options,
      theme,
      themedStyle.centerText,
      themedStyle.choosePaymentModal,
      themedStyle.itemSeparator,
      themedStyle.modalCloseButton,
      themedStyle.modalTitle,
      themedStyle.uppercase,
      title,
    ],
  );

  const showModal = useCallback(() => {
    const ModalContent = renderModal();

    if (modalId.current) {
      ModalService.update(modalId.current, ModalContent);
    } else {
      const Modal = (
        <Layout
          style={[
            themedStyle.backdrop,
            {
              width,
              height,
            },
          ]}
          pointerEvents="box-none"
        >
          {ModalContent}
        </Layout>
      );
      modalId.current = ModalService.show(Modal, {
        allowBackdrop: true,
        onBackdropPress: () => hideModal(),
      });
    }
  }, [renderModal, themedStyle.backdrop, width, height, hideModal]);

  useEffect(() => {
    if (visible) {
      showModal();
    } else {
      hideModal();
    }
  }, [hideModal, showModal, visible]);

  return null;
};

export default withStyles(ModalComponent, theme => ({
  backdrop: {
    position: 'absolute',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: color(theme['color-basic-900'])
      .alpha(0.7)
      .rgb()
      .string(),
  },
  choosePaymentModal: {
    margin: 10,
    padding: 30,
    borderRadius: 10,
  },
  modalTitle: {
    paddingBottom: 10,
  },
  modalCloseButton: {
    paddingTop: 25,
    alignSelf: 'center',
  },
  itemSeparator: {
    marginVertical: 10,
  },
  centerText: {
    textAlign: 'center',
  },
  uppercase: {
    textTransform: 'uppercase',
  },
}));
