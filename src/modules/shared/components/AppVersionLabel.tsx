/* eslint-disable global-require */
import React from 'react';
import { withStyles, Layout, Text, ThemedComponentProps } from 'react-native-ui-kitten';

// Config
import { connect } from 'react-redux';
import { RootState } from 'typesafe-actions';
import * as store from '../store';

const mapStateToProps = (state: RootState) => ({
  appLocalVersion: store.selectors.getAppLocalVersion(state),
});

type AppVersionLabelProps = ThemedComponentProps & ReturnType<typeof mapStateToProps>;

const AppVersionLabel = ({ themedStyle, appLocalVersion }: AppVersionLabelProps) => {
  // config is optional to be passed in on Android
  return (
    <Layout style={themedStyle.appLocalVersionContainer}>
      {appLocalVersion && <Text appearance="hint">{`v ${Object.values(appLocalVersion)}`}</Text>}
    </Layout>
  );
};

export default connect(mapStateToProps)(
  withStyles(AppVersionLabel, () => ({
    appLocalVersionContainer: {
      alignItems: 'center',
      marginTop: 40,
      marginBottom: 20,
    },
  })),
);
