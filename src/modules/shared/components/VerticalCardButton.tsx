import React from 'react';
import {
  Image,
  ImageSourcePropType,
  TouchableOpacity,
  View,
  StyleProp,
  ViewStyle,
} from 'react-native';
import { withStyles, Text, ThemedComponentProps, TextProps } from 'react-native-ui-kitten';
import LinearGradient from 'react-native-linear-gradient';

type CardButtonComponent = {
  icon: ImageSourcePropType;
  onPress?: () => void;
  containerStyle?: StyleProp<ViewStyle>;
  captionContainerStyle?: StyleProp<ViewStyle>;
  children?: TextProps['children'];
  disabled?: boolean;
} & ThemedComponentProps;

const CardButtonComponent = ({
  icon,
  onPress,
  containerStyle,
  captionContainerStyle,
  themedStyle,
  children,
  disabled,
}: CardButtonComponent) => {
  if (disabled) {
    return (
      <LinearGradient
        useAngle
        angle={180}
        angleCenter={{ x: 0.5, y: 0.5 }}
        style={[themedStyle.container, containerStyle]}
        colors={['#eee', '#ddd', '#ccc', '#bbb', '#aaa', '#999']}
      >
        <Image source={icon} />
        {children && (
          <View style={[themedStyle.captionContainer, captionContainerStyle]}>
            <Text
              category="c2"
              status="primary"
              style={[themedStyle.centerText, themedStyle.uppercase]}
            >
              {children}
            </Text>
          </View>
        )}
      </LinearGradient>
    );
  }
  return (
    <TouchableOpacity onPress={onPress}>
      <LinearGradient
        useAngle
        angle={180}
        angleCenter={{ x: 0.5, y: 0.5 }}
        style={[themedStyle.container, containerStyle]}
        colors={['#FFFFFF', '#FCFCFD', '#F8F9FA', '#F5F6F8', '#F1F3F5', '#EEF0F3']}
      >
        <Image source={icon} />
        {children && (
          <View style={[themedStyle.captionContainer, captionContainerStyle]}>
            <Text
              category="c2"
              status="primary"
              style={[themedStyle.centerText, themedStyle.uppercase]}
            >
              {children}
            </Text>
          </View>
        )}
      </LinearGradient>
    </TouchableOpacity>
  );
};

const CardButton = withStyles(CardButtonComponent, theme => ({
  centerText: {
    textAlign: 'center',
  },
  uppercase: {
    textTransform: 'uppercase',
  },
  container: {
    padding: 10,
    borderColor: theme['color-basic-500'],
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  captionContainer: {
    marginTop: 8,
  },
}));

export default CardButton;
