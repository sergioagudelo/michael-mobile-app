import React from 'react';
import { Image, ImageSourcePropType, StyleProp, ImageStyle, View} from 'react-native';
import { withStyles, ThemedComponentProps } from 'react-native-ui-kitten';
import * as Animatable from 'react-native-animatable';

/**
 * props for ImageBackground
 */
type TabBarIconProps = ThemedComponentProps & {
  icon: ImageSourcePropType;
  routeName: string;
  currentRouteName: string;
  animRouteName: string | undefined;
  style?: StyleProp<ImageStyle>;
  hasNotifications?: boolean;
};

/**
 * animation definitions when tab bar is touched
 */
const bigPulse = {
  0: { scale: 1 },
  0.5: { scale: 0.6 },
  1: { scale: 1 },
};

/**
 * render image for each tabBar navigation
 */
const TabBarIconElement = ({
  icon,
  routeName,
  style,
  themedStyle,
  currentRouteName,
  animRouteName,
  hasNotifications
}: TabBarIconProps) => {
  const animation =
    routeName === currentRouteName && animRouteName !== 'zoomIn' ? bigPulse : animRouteName;
  const duration = animation === 'zoomIn' ? 800 : 400;
  return (
    <Animatable.View
      style={themedStyle.flex}
      animation={animation as Animatable.CustomAnimation}
      duration={duration}
      useNativeDriver
    >
      <View>
       {hasNotifications ? <View style={themedStyle.notificationCircle}></View> : null}
       <Image source={icon} style={[style, themedStyle.imgBack]} />
      </View>
    </Animatable.View>
  );
};

const TabBarIcon = withStyles(TabBarIconElement, (theme) => ({
  flex: {
    flex: 1,
  },
  imgBack: {
    width: 32,
    height: 32,
  },
  notificationCircle: {
    position: 'absolute',
    width: 10,
    height: 10,
    borderRadius: 5,
    left: 20,
    zIndex: 1,
    top: 6,
    backgroundColor: theme['color-secondary-600']
  }
}));

export default TabBarIcon;
