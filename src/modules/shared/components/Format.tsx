import React from 'react';
import { Text, TextProps } from 'react-native-ui-kitten';

// Modules
import { format as formatter } from '../helpers';

/**
 * type format
 */
type Format = {
  type?: 'date' | 'number';
  number?: string;
  date?: string;
  fraction?: number;
  format?: 'MM/DD/YYYY' | string;
} & TextProps;

/**
 * React component will resturn a <Text>{formatedValue}</Text>
 * by default will render number
 * @param `Format`
 */
const Format = ({
  type = 'number',
  number,
  fraction = 0,
  date,
  format,
  children,
  ...props
}: Format) => {
  let formatted;
  if (type === 'number') {
    const numValue: string = (number || children) as string;
    formatted = formatter.formatNumber(numValue, fraction);
  } else {
    const dateValue: string = (date || children) as string;
    formatted = formatter.formatDate(dateValue, format);
  }
  return <Text {...props}>{formatted}</Text>;
};

export default Format;
