import React, { useState, useEffect, useCallback, useRef } from 'react';
import { View, ListRenderItemInfo, BackHandler } from 'react-native';
import {
  withStyles,
  Text,
  Layout,
  ListItem,
  Button,
  Icon,
  ThemedComponentProps,
  ModalService,
} from 'react-native-ui-kitten';
import color from 'color';

// Types
import { useFocusEffect } from 'react-navigation-hooks';

// Components
import Dashed from './Dashed';
import * as helpers from '../helpers';

type VirtualCardComingSoonModalProps = ThemedComponentProps & {
  onDismiss: () => void;
  visible: boolean;
  title: string;
  body: string;
  buttonTitle: string;
};

const VirtualCardComingSoonModal = ({
  visible,
  title,
  body,
  buttonTitle,
  onDismiss,
  theme,
  themedStyle,
}: VirtualCardComingSoonModalProps) => {
  const { height, width } = helpers.hooks.useScreenDimensions();

  const modalId = useRef('');

  const hideModal = useCallback(() => {
    modalId.current = ModalService.hide(modalId.current);
    if (onDismiss) {
      onDismiss();
    }
  }, [onDismiss]);

  useFocusEffect(
    useCallback(() => {
      const onBackPress = () => {
        if (visible) {
          hideModal();
          return true;
        }
        return false;
      };

      BackHandler.addEventListener('hardwareBackPress', onBackPress);

      return () => BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    }, [visible, hideModal]),
  );

  // TODO CARD NUMBER PARSER SHARED
  const renderModal = useCallback(() => {
    return (
      <Layout style={themedStyle.choosePaymentModal}>
        <Text
          category="h3"
          status="accent"
          style={[themedStyle.centerText, themedStyle.modalTitle]}
        >
          {title}
        </Text>

        <Dashed color={theme['color-primary-700']} customStyle={themedStyle.itemSeparator} />

        <Text style={[themedStyle.centerText, themedStyle.modalBody]}>{body}</Text>

        <View style={themedStyle.modalCloseButton}>
          <Button
            appearance="outline"
            status="basic"
            size="medium"
            onPress={() => {
              hideModal();
            }}
          >
            {buttonTitle}
          </Button>
        </View>
      </Layout>
    );
  }, [
    body,
    buttonTitle,
    hideModal,
    theme,
    themedStyle.centerText,
    themedStyle.choosePaymentModal,
    themedStyle.itemSeparator,
    themedStyle.modalBody,
    themedStyle.modalCloseButton,
    themedStyle.modalTitle,
    title,
  ]);

  const showModal = useCallback(() => {
    const ModalContent = renderModal();

    if (modalId.current) {
      ModalService.update(modalId.current, ModalContent);
    } else {
      const Modal = (
        <Layout
          style={[
            themedStyle.backdrop,
            {
              width,
              height,
            },
          ]}
          pointerEvents="box-none"
        >
          {ModalContent}
        </Layout>
      );
      modalId.current = ModalService.show(Modal, {
        allowBackdrop: true,
        onBackdropPress: () => hideModal(),
      });
    }
  }, [renderModal, themedStyle.backdrop, width, height, hideModal]);

  useEffect(() => {
    if (visible) {
      showModal();
    } else {
      hideModal();
    }
  }, [hideModal, showModal, visible]);

  return null;
};

export default withStyles(VirtualCardComingSoonModal, theme => ({
  backdrop: {
    position: 'absolute',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: color(theme['color-basic-900'])
      .alpha(0.7)
      .rgb()
      .string(),
  },
  choosePaymentModal: {
    margin: 30,
    padding: 30,
    borderRadius: 10,
  },
  modalTitle: {
    paddingBottom: 10,
  },
  modalBody: {
    marginTop: 25,
  },
  modalCloseButton: {
    paddingTop: 25,
    alignSelf: 'center',
  },
  itemSeparator: {
    marginVertical: 10,
  },
  centerText: {
    textAlign: 'center',
  },
  uppercase: {
    textTransform: 'uppercase',
  },
}));
