import React from 'react';
import { View, Image, StyleProp, ImageStyle } from 'react-native';
import { withStyles, ThemedComponentProps } from 'react-native-ui-kitten';
import * as Animatable from 'react-native-animatable';

type HeaderBarComponentProps = { imageStyle?: StyleProp<ImageStyle> } & ThemedComponentProps;

/**
 * custom header with image
 */
const HeaderBarComponent = ({ imageStyle, themedStyle }: HeaderBarComponentProps) => {
  return (
    <Animatable.View animation="pulse" duration={1000} useNativeDriver>
      <View style={themedStyle.container}>
        <Image
          source={{ uri: 'lp_logotopbar' }}
          resizeMode="contain"
          style={[themedStyle.img, imageStyle]}
        />
      </View>
    </Animatable.View>
  );
};

const HeaderBar = withStyles(HeaderBarComponent, () => ({
  container: {
    alignItems: 'center',
    paddingVertical: 5,
    flex: 1,
  },
  img: {
    flex: 1,
    minWidth: 100,
  },
}));

export default HeaderBar;
