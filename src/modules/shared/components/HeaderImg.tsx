import React from 'react';
import { Image } from 'react-native';
import { withStyles, ThemedComponentProps } from 'react-native-ui-kitten';

/**
 * custom image for IOS back button
 */
const HeaderImgElement = ({ themedStyle }: ThemedComponentProps): React.ReactElement => {
  return (
    <Image
      source={require('../../../img/icons/ico_back_arrow.png')}
      resizeMode="contain"
      style={themedStyle.img}
    />
  );
};

/**
 * styles to custom image
 */
const HeaderImg = withStyles(HeaderImgElement, () => ({
  img: {
    margin: 10,
    width: 40,
    height: 40,
  },
}));

export default HeaderImg;
