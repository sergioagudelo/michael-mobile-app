import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { getHasUnreadNotifications } from '../../notifications/store/selectors';
import { SafeAreaView } from 'react-native';
import { NavigationTabScreenProps } from 'react-navigation-tabs';
import {
  withStyles,
  BottomNavigation,
  BottomNavigationTab,
  ThemedComponentProps,
} from 'react-native-ui-kitten';

// Components
import TabBarIcon from './TabBarIcon';

type TabBarProps = ThemedComponentProps & NavigationTabScreenProps;

const TabBarComponent = ({ navigation, themedStyle }: TabBarProps) => {
  const hasUnreadNotifications = useSelector(getHasUnreadNotifications);
  /**
   * initial route name before tabbar is touched
   */
  const route = navigation.state.routes[navigation.state.index];

  /**
   * setup state when tabBar is touched
   */
  const [currentRouteName, setCurrentRouteName] = useState<string>(route.routeName);
  const [animRouteName, setAnimRouteName] = useState<string | undefined>('zoomIn');

  /**
   * tab was selected navigate to the selected tab
   * @param selectedIndex number
   */
  const onTabSelect = (selectedIndex: number) => {
    const { [selectedIndex]: selectedRoute } = navigation.state.routes;
    setCurrentRouteName(selectedRoute.routeName);
    setAnimRouteName(undefined);
    navigation.navigate(selectedRoute.routeName);
  };

  return (
    <SafeAreaView>
      <BottomNavigation
        appearance="noIndicator"
        selectedIndex={navigation.state.index}
        onSelect={onTabSelect}
        style={[themedStyle.tabBar]}
      >
        <BottomNavigationTab
          icon={style => (
            <TabBarIcon
              animRouteName={animRouteName}
              currentRouteName={currentRouteName}
              routeName="Loans"
              icon={require('../../../img/tabBar/home.png')}
              style={style}
            />
          )}
        />
        <BottomNavigationTab
          icon={style => (
            <TabBarIcon
              animRouteName={animRouteName}
              currentRouteName={currentRouteName}
              hasNotifications={hasUnreadNotifications}
              routeName="Notifications"
              icon={require('../../../img/tabBar/notifications.png')}
              style={style}
            />
          )}
        />
        <BottomNavigationTab
          icon={style => (
            <TabBarIcon
              animRouteName={animRouteName}
              currentRouteName={currentRouteName}
              routeName="Profile"
              icon={require('../../../img/tabBar/profile.png')}
              style={style}
            />
          )}
        />
        <BottomNavigationTab
          icon={style => (
            <TabBarIcon
              animRouteName={animRouteName}
              currentRouteName={currentRouteName}
              routeName="FinancialLife"
              icon={require('../../../img/tabBar/education.png')}
              style={style}
            />
          )}
        />
        <BottomNavigationTab
          icon={style => (
            <TabBarIcon
              animRouteName={animRouteName}
              currentRouteName={currentRouteName}
              // routeName="Offers"
              routeName="HelpCenter"
              icon={require('../../../img/tabBar/helpCenter.png')}
              // icon={require('../../../img/tabBar/offers.png')}
              style={style}
            />
          )}
        />
      </BottomNavigation>
    </SafeAreaView>
  );
};

const TabBar = withStyles(TabBarComponent, theme => ({
  tabBar: {
    borderColor: theme['color-basic-600'],
    borderStyle: 'solid',
    borderTopWidth: 2,
  },
  imgBack: {
    width: 32,
    height: 32,
  },
}));

export default TabBar;
