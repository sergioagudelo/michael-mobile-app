import React from 'react';
import { StyleProp, ViewStyle } from 'react-native';
import { ThemedComponentProps, withStyles } from 'react-native-ui-kitten';
import Dash, { DashProps } from 'react-native-dash';
import { Optional } from 'utility-types';

type DashedProps = {
  color?: string;
  direction?: 'row' | 'column';
  customStyle?: StyleProp<ViewStyle>;
} & Optional<Omit<DashProps, 'dashColor'>> &
  ThemedComponentProps;

const DashedComponent = ({
  theme,
  color = theme['color-basic-500'],
  direction = 'row',
  customStyle,
  themedStyle,
  ...dashProps
}: DashedProps) => {
  return (
    <Dash
      dashThickness={2}
      dashGap={4}
      dashLength={2}
      {...dashProps}
      dashColor={color}
      style={[direction === 'row' ? themedStyle.row : themedStyle.column, customStyle]}
    />
  );
};

const Dashed = withStyles(DashedComponent, () => ({
  row: {
    flexDirection: 'row',
  },
  column: {
    flexDirection: 'column',
  },
}));

export default Dashed;
