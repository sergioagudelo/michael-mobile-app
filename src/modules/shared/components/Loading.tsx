import React from 'react';
import { View } from 'react-native';
import * as Progress from 'react-native-progress';
import { ThemedComponentProps, withStyles, Text } from 'react-native-ui-kitten';

type LoadingComponent = ThemedComponentProps & {
  isLoading?: boolean;
  color?: string;
  spinnerColors?: [string];
};

const LoadingComponent = ({
  themedStyle,
  theme,
  isLoading,
  color,
  spinnerColors,
}: LoadingComponent) => {
  const indicatorColor = color || theme['color-primary-700'];
  const defaultSpinnerColors = spinnerColors || [
    theme['color-info-500'],
    theme['color-primary-500'],
    theme['color-warning-500'],
  ];

  return (
    (isLoading && (
      <View style={themedStyle.wrapper}>
        <View style={themedStyle.loading}>
          <Progress.CircleSnail color={defaultSpinnerColors} />
        </View>
        <Text status="default" style={[themedStyle.text, { color: indicatorColor }]}>
          Loading...
        </Text>
      </View>
    )) || <></>
  );
};

const Loading = withStyles(LoadingComponent, () => ({
  wrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
  },
  loading: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  backdrop: {
    backgroundColor: 'black',
    opacity: 0.5,
  },
  text: {
    marginLeft: 10,
  },
}));

export default Loading;
