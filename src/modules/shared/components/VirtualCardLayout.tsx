import React, { useState } from 'react';
import { View, TouchableHighlight, ImageBackground, Image, TouchableOpacity } from 'react-native';
import { ThemedComponentProps, withStyles, Layout, Icon } from 'react-native-ui-kitten';
import { moderateScale } from 'react-native-size-matters';

type VirtualCardLayoutProps = {
  onPress?: () => void;
  dismissible: boolean;
  renderContent: () => JSX.Element;
  renderFooter: () => JSX.Element;
} & ThemedComponentProps;

const VirtualCardLayout = ({
  onPress,
  dismissible,
  renderContent,
  renderFooter,
  themedStyle,
}: VirtualCardLayoutProps) => {
  // TODO: Initial value should be retrieved from persisted data in the device, where the previous selection by the user for this setting should be stored
  const [visible, setVisible] = useState(true);

  if (!visible) {
    return null;
  }

  return (
    <Layout style={[themedStyle.container, themedStyle.border]}>
      <TouchableHighlight onPress={onPress} style={themedStyle.border}>
        <ImageBackground
          source={require('../../../img/lp_card_bg.png')}
          resizeMode="stretch"
          style={[themedStyle.background]}
        >
          {dismissible && (
            <TouchableOpacity style={themedStyle.close} onPress={() => setVisible(false)}>
              <Icon name="close" width={30} height={30} fill="#FFF" />
            </TouchableOpacity>
          )}
          <View style={themedStyle.header}>
            <Image source={require('../../../img/chip.png')} />
            <Image source={require('../../../img/lp_card_logo.png')} />
          </View>

          <View style={themedStyle.content}>{renderContent()}</View>

          <View style={themedStyle.footer}>{renderFooter()}</View>
        </ImageBackground>
      </TouchableHighlight>
    </Layout>
  );
};

export default withStyles(VirtualCardLayout, () => ({
  container: {
    marginHorizontal: 30,
    marginVertical: 15,
  },
  background: {
    minHeight: moderateScale(180),
    padding: 20,
    justifyContent: 'space-around',
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  border: {
    borderRadius: 6,
  },
  centerText: {
    textAlign: 'center',
  },
  uppercase: {
    textTransform: 'uppercase',
  },
  content: {
    marginVertical: 25,
  },
  footer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  close: {
    position: 'absolute',
    right: 0,
    top: 0,
  },
}));
