import React from 'react';
import { Input, Icon, StyleType, InputProps } from 'react-native-ui-kitten';

const PasswordInput = ({ value, onChangeText, ...rest }: InputProps, ref) => {
  const [secureTextEntry, setSecureTextEntry] = React.useState(true);

  const renderIcon = (style: StyleType) => {
    const iconName = secureTextEntry ? 'eye-off-outline' : 'eye-outline';
    return <Icon {...style} name={iconName} />;
  };

  return (
    <Input
      ref={ref}
      value={value}
      icon={renderIcon}
      secureTextEntry={secureTextEntry}
      onIconPress={() => setSecureTextEntry(!secureTextEntry)}
      onChangeText={onChangeText}
      {...rest}
    />
  );
};

export default React.forwardRef(PasswordInput);
