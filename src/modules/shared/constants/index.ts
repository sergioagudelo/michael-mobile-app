import FSStorage, { DocumentDir } from 'redux-persist-fs-storage';
import RNFS from 'react-native-fs';

export const NAME = 'shared';

export const routesBlacklist = ['MakePayment', 'PaymentReceipt', 'ExtraPayment'];

export const persistor = {
  config: {
    key: 'deviceInfo.db',
    keyPrefix: 'lp_cp_',
    storage: FSStorage(DocumentDir, 'CustomerPortal'),
    whitelist: [
      'biometricAuthPermission',
      'isBiometricAuthSupported',
      'appLocalVersion',
      'bioAuthPromptRejected',
    ],
  },
};

export const APP_TEMP_DIRECTORY = RNFS.TemporaryDirectoryPath;

export const stateValues = [
  'AL',
  'AK',
  'AZ',
  'AR',
  'CA',
  'CO',
  'CT',
  'DC',
  'DE',
  'FL',
  'GA',
  'HI',
  'ID',
  'IL',
  'IN',
  'IA',
  'KS',
  'KY',
  'LA',
  'ME',
  'MD',
  'MA',
  'MI',
  'MN',
  'MS',
  'MO',
  'MT',
  'NE',
  'NV',
  'NH',
  'NJ',
  'NM',
  'NY',
  'NC',
  'ND',
  'OH',
  'OK',
  'OR',
  'PA',
  'RI',
  'SC',
  'SD',
  'TN',
  'TX',
  'UT',
  'VT',
  'VA',
  'WA',
  'WV',
  'WI',
  'WY',
];
