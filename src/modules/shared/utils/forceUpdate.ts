import {DistributionPlatforms} from '../services';

type AppUpdateInfo = {
  shouldAppUpdate: boolean;
  isSoftUpdate?: boolean;
};

const checkIsBiggerVersion = (_local: string, _remote: string) =>
  parseFloat(_local.slice(2)) < parseFloat(_remote.slice(2));

export const checkAvailableUpdate = async (
  localVersion: string
): Promise<AppUpdateInfo> => {
  const remote = await DistributionPlatforms.fetchRemoteAppVersionNumber();
  if (remote.success && remote.payload && remote.payload.version) {
    if (checkIsBiggerVersion(localVersion, remote.payload.version)) {
      return {
        shouldAppUpdate: true,
        isSoftUpdate: remote.payload.type === "soft",
      };
    }
  }
  return {
    shouldAppUpdate: false
  };
};
