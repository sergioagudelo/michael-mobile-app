import { PromiseType } from 'utility-types';

import * as testData from './testData';
import {checkAvailableUpdate} from './forceUpdate';

// as seen in AMCP-245
export const formatDateOptions = ['MMM D, YYYY', 'dddd, MMM D, YYYY'];

export type ComponentFetchingErrorType = {
  failed: boolean;
  details?: string;
};

export type NavigationHistoryEntry = {
  previousRouteName: string;
  currentRouteName: string;
};

export const rand = () => Math.round(Math.random());

/**
 * Validates the case and return its value (or default case)
 * @param {Object} cases - cases to evaluate
 * @param {string} key - key to find in 'cases' object
 */
export const switchCase = (cases: { [key: string]: unknown; default: unknown }) => (
  key: string | number,
) => (Object.prototype.hasOwnProperty.call(cases, key) ? cases[key] : cases.default);

/**
 * Validates if the case (from the switch) is a function or not
 * @param {*} f - case to evaluate
 */
export const executeIfFunction = (f: unknown) => (f instanceof Function ? f() : f);

/**
 * This method evaluates if the case to evaluate is a function and evaluate it
 * @param {Object} cases - cases to evaluate and execute if a function
 */
export const switchCaseF = (cases: { [key: string]: unknown; default: unknown }) => (
  key: string | number,
) => executeIfFunction(switchCase(cases)(key));

/**
 *
 * @param {Array} array - Array to iterate over
 * @param {function} callback - Function to call in each iteration over the array
 */
export const asyncForEach = async (
  array: never[],
  callback: (value: never, index: number, array: never[]) => PromiseType<Promise<never>>,
) => {
  for (let index = 0; index < array.length; index += 1) {
    await callback(array[index], index, array);
  }
};

export { testData, checkAvailableUpdate };