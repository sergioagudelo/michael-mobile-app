export type NotificationType = {
  id: number;
  from: string;
  to: string;
  subject: string;
  date: string;
  read: boolean;
  body?: {
    img: string;
    text: string;
  };
};

export type MailGetAllResponse = {
  message: string;
  total: number;
  mails: NotificationType[];
};
export type MailGetDetailsResponse = NotificationType;

export type MailId = number;
