import React from 'react';
import { Image, View } from 'react-native';
import { withStyles, ThemedComponentProps, ListItem } from 'react-native-ui-kitten';

// Types
import { NotificationType } from '../utils';

// Modules
import shared from '../../shared';

type NotificationItemProps = {
  onPress: () => void;
  notification: NotificationType;
} & ThemedComponentProps;

const NotificationItem = ({ notification, onPress, themedStyle }: NotificationItemProps) => {
  const source = notification.read
    ? require('../../../img/icons/ico_notification_default.png')
    : require('../../../img/icons/ico_notification_info.png');

  return (
    <ListItem
      title={notification.subject}
      description={`${shared.helpers.format.formatDate(notification.date)}`}
      accessory={() => <Image source={{ uri: 'ico_plus' }} style={themedStyle.plus} />}
      icon={() => (
        <View>
          <Image source={source} style={themedStyle.type} />
        </View>
      )}
      onPress={onPress}
    />
  );
};

export default withStyles(NotificationItem, () => ({
  type: {
    width: 32,
    height: 23,
  },
  plus: {
    width: 44,
    height: 44,
  },
}));
