import React from 'react';
import { ListRenderItemInfo, FlatListProps } from 'react-native';
import { ThemedComponentProps, List, Text } from 'react-native-ui-kitten';
import * as Animatable from 'react-native-animatable';

// Types
import { NotificationType } from '../utils';

// Modules
import shared from '../../shared';

// Components
import NotificationItem from './NotificationItem';
import { Loading } from '../../shared/components';

const { Dashed } = shared.components;

/**
 * component type
 */
type NotificationsListProps = {
  notifications: NotificationType[];
  renderHeader?: FlatListProps<NotificationType>['ListHeaderComponent'];
  onNotificationPress: (notification: NotificationType) => void;
} & ThemedComponentProps;

const NotificationsList = ({ notifications, onNotificationPress }: NotificationsListProps) => {
  const renderSeparator = () => <Dashed />;

  const renderItem = ({ item, index, ...rest }: ListRenderItemInfo<NotificationType>) => {
    return (
      <Animatable.View
        animation="fadeInDown"
        duration={300}
        delay={(index && index * 150) || 150}
        useNativeDriver
      >
        <NotificationItem notification={item} onPress={() => onNotificationPress(item)} {...rest} />
      </Animatable.View>
    );
  };
  if (notifications) {
    return (
      <List
        data={notifications}
        renderItem={renderItem}
        ItemSeparatorComponent={renderSeparator}
        showsVerticalScrollIndicator={false}
        ListEmptyComponent={() => (
          <Animatable.View animation="fadeInDown" duration={300} delay={150} useNativeDriver>
            <Text style={{ textAlign: 'center', marginTop: 50 }}>You don't have notifications</Text>
          </Animatable.View>
        )}
      />
    );
  }
  return <Loading isLoading={!notifications} />;
};

export default NotificationsList;
