import React, { useState } from 'react';
import { ScrollView, Image, Dimensions } from 'react-native';

import {
  Layout,
  Text,
  Button,
  ThemedComponentProps,
  withStyles,
  Icon,
} from 'react-native-ui-kitten';

import { NavigationStackProp } from 'react-navigation-stack';

// Types
import { NotificationType } from '../utils';

// Modules
import shared from '../../shared';
import support from '../../support';
import { EmailComposerFormValues } from '../../support/utils';
import { ApiResponse } from '../../../services/utils';
const { width: screenWidth } = Dimensions.get('window');

// Components
const { Dashed } = shared.components;
const { EmailUs } = support.components;

type NotificationDetailProps = {
  data: NotificationType;
  onDelete: (notification: NotificationType) => void;
  onReply: (
    notificationId: number,
    emailFormValues: EmailComposerFormValues,
  ) => Promise<ApiResponse>;
  navigation: NavigationStackProp;
} & ThemedComponentProps;

const NotificationDetail = ({
  navigation,
  theme,
  themedStyle,
  onDelete,
  onReply,
  data,
}: NotificationDetailProps) => {
  const notification = data;
  const [showReplyModal, setShowReplyModal] = useState(false);

  const handleDelete = () => {
    if (onDelete) {
      onDelete(notification);
    }
    navigation.goBack();
  };
  const handleReply = async (notificationId: number, emailFormValues: EmailComposerFormValues) => {
    const { success } = await onReply(notificationId, emailFormValues);
    if (success) {
      setShowReplyModal(false);
    }
    return success;
  };

  if (!notification) {
    return (
      <Text category="h1" status="danger">
        Error
      </Text>
    );
  }
  return (
    <>
      <EmailUs
        notification={notification}
        visible={showReplyModal}
        onDismiss={() => setShowReplyModal(false)}
        onSubmit={handleReply}
      />
      <ScrollView contentContainerStyle={themedStyle.body}>
        <Layout style={themedStyle.content}>

           <Text category="h1" status="primary" style={themedStyle.centerText}>
            {notification.subject}
          </Text>
          <Dashed color={theme['color-primary-700']} customStyle={themedStyle.itemSeparator} />
          <Text category="label" appearance="hint">
            {notification.from ?? ''}
          </Text>
          <Text category="label" appearance="hint">
            {notification.date &&
              shared.helpers.format.formatDate(
                notification.date,
                shared.utils.formatDateOptions[1],
              )}
          </Text>
          <Dashed customStyle={themedStyle.itemSeparator} />



         <Layout style={themedStyle.imageContainer}>
              <Image
                resizeMode="cover"
                style={themedStyle.image}
                source={{
                  uri: 'https://www.lendingpoint.com/wp-content/uploads/2016/03/lendingpoint.png',
                }}
              />
          </Layout>

          <Text>{notification.body && notification.body.text}</Text>

          <Dashed customStyle={themedStyle.itemSeparator} />
          <Layout style={themedStyle.modalFooter}>
            <Button
              status="basic"
              size="small"
              onPress={handleDelete}
              textStyle={themedStyle.uppercase}
              {...shared.helpers.setTestID('NotificationDetailsDeleteBtn')}
            >
              Delete
            </Button>
            <Button
              size="small"
              icon={style => <Icon name="corner-up-left" {...style} />}
              onPress={() => {
                setShowReplyModal(true);
              }}
              textStyle={themedStyle.uppercase}
              {...shared.helpers.setTestID('NotificationDetailsReplyBtn')}
              style={themedStyle.replyButton}
            >
              Reply
            </Button>
          </Layout>
        </Layout>
      </ScrollView>
    </>
  );
};

export default withStyles(NotificationDetail, () => ({
  centerText: {
    textAlign: 'center',
  },
  uppercase: {
    textTransform: 'uppercase',
  },
  screen: {
    flex: 1,
  },
  body: {
    flexGrow: 1,
  },
  content: {
    marginHorizontal: 30,
    marginVertical: 20,
  },
  itemSeparator: {
    marginVertical: 15,
  },
  modalFooter: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  notificationBody: {
    tr: {
      paddingVertical: 20,
    },
  },
  replyButton: {
    flexDirection: 'row-reverse',
  },
  imageContainer: {
    overflow: 'visible',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 20,
  },
  image: { width: screenWidth * 0.5, height: screenWidth * 0.1 * 2.16, overflow: 'visible' },
}));
