import React, { useEffect } from 'react';
import { SafeAreaView } from 'react-native';
import { ThemedComponentProps, withStyles } from 'react-native-ui-kitten';
import { NavigationStackProp } from 'react-navigation-stack';
import { withNavigation } from 'react-navigation';

import { Dispatch, bindActionCreators, compose } from 'redux';
import { connect } from 'react-redux';

// store
import { RootState } from 'typesafe-actions';
import * as store from '../store';

// Modules
import shared from '../../shared';

// Components
import { NotificationDetail } from '../components';

import { mailService } from '../services';

const { Loading } = shared.components;

type NotificationDetailsMapStateProps = { notificationId: number };

const mapStateToProps = (
  state: RootState,
  { notificationId }: NotificationDetailsMapStateProps,
) => ({
  notification: store.selectors.getNotificationById(notificationId)(state),
});

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(
    {
      deleteMail: store.actions.thunks.deleteNotification,
      getMailDetails: store.actions.thunks.getNotificationDetails,
    },
    dispatch,
  );

type NotificationDetailsProps = {
  notificationId: string;
  navigation: NavigationStackProp;
} & NotificationDetailsMapStateProps &
  ThemedComponentProps &
  ReturnType<typeof mapDispatchToProps> &
  ReturnType<typeof mapStateToProps>;

const NotificationDetails = ({
  navigation,
  themedStyle,
  deleteMail,
  notification,
  getMailDetails,
}: NotificationDetailsProps) => {
  const [isLoading, setIsLoading] = React.useState(false);
  const [failed, setFailed] = React.useState(false);

  useEffect(() => {
    const getNotificationDetails = async (id: number) => {
      setIsLoading(true);
      const result = await getMailDetails(id);

      setIsLoading(false);
      if (result && !result.success) {
        setFailed(true);
      }
    };

    if (!notification) {
      navigation.goBack();
    } else if (!notification.body && !isLoading && !failed) {
      getNotificationDetails(notification.id);
    }
  }, [failed, getMailDetails, isLoading, navigation, notification]);

  if (failed) {
    // navigation.navigate // TODO: General error
  }

  return (
    <SafeAreaView
      style={themedStyle.screen}
      {...shared.helpers.setTestID('NotificationDetailsScreen')}
    >
      {isLoading && <Loading isLoading={isLoading} />}
      {notification && notification.body && (
        <NotificationDetail
          onReply={mailService.replyMail}
          onDelete={deleteMail}
          navigation={navigation}
          data={notification}
        />
      )}
    </SafeAreaView>
  );
};

export default compose(
  withNavigation,
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(
  withStyles(NotificationDetails, () => ({
    centerText: {
      textAlign: 'center',
    },
    uppercase: {
      textTransform: 'uppercase',
    },
    screen: {
      flex: 1,
    },
    body: {
      flexGrow: 1,
    },
    content: {
      marginHorizontal: 30,
      marginVertical: 20,
    },
    itemSeparator: {
      marginVertical: 15,
    },
    modalFooter: {
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    notificationBody: {
      tr: {
        paddingVertical: 20,
      },
    },
    replyButton: {
      flexDirection: 'row-reverse',
    },
  })),
) as React.ComponentType;
