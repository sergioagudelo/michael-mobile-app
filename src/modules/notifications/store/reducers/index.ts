import { combineReducers } from 'redux';
import { createReducer, Action, PayloadAction } from 'typesafe-actions';

import { NotificationType } from '../../utils';

import { actions } from '../actions';

const list = createReducer(null as NotificationType[] | null)
  .handleAction(actions.AddNotification, (state, action) => {
    if (state) {
      const notificationIndex = state.findIndex(
        (notification) => notification.id === action.payload.id
      );
      if (notificationIndex < 0) {
        return [action.payload, ...state];
      } else {
        return state.map((notification, index) => {
          if (notificationIndex === index) {
            return action.payload;
          }

          return notification;
        });
      }
    } else {
      return [action.payload];
    }
  })
  .handleAction(
    actions.NotificationList.success,
    (state, action) => action.payload as NotificationType[] | null
  )
  .handleAction(
    actions.NotificationDetails.success,
    (state, action) =>
      state &&
      state.map((entry) =>
        action.payload && entry.id === action.payload.id
          ? { ...entry, body: action.payload.body }
          : entry
      )
  )
  .handleAction(
    actions.SetAsDeleted,
    (state, action: PayloadAction<string, NotificationType>) =>
      state && state.filter((entry) => entry.id !== action.payload.id)
  )
  .handleAction(
    actions.SetAsRead,
    (state, action: PayloadAction<string, NotificationType>) =>
      state &&
      state.map((entry: NotificationType) => {
        if (entry.id === action.payload.id) {
          entry.read = true;
        }
        return entry;
      })
  );

const notificationReducer = combineReducers({
  list,
});

export default notificationReducer;
export type NotificationState = ReturnType<typeof notificationReducer>;
