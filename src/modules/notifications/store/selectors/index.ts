import { createSelector } from 'reselect';
import { RootState } from 'typesafe-actions';
import { NAME } from '../../constants';

// type
import { NotificationType } from '../../utils';

const notificationListSelector = (state: RootState) => state[NAME].list;

export const getNotificationList = createSelector(
  notificationListSelector,
  (notifications: NotificationType[]) => notifications
);

export const getHasUnreadNotifications = createSelector(
  notificationListSelector,
  (notifications: NotificationType[]) => {
    if (!notifications) {
      return false;
    }

    for (const notification of notifications) {
      if (!notification.read || (notification.read as any) === 'false') {
        return true;
      }
    }

    return false;
  }
);

const singleNotificationSelector = (notificationId: number) => (
  state: RootState
): NotificationType | null =>
  state[NAME].list.find((entry) => entry.id === notificationId) || null;

export const getNotificationById = (notificationId: number) =>
  createSelector(
    singleNotificationSelector(notificationId),
    (notification: NotificationType | null) => notification
  );
