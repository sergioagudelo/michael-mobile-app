import { createAsyncAction, createAction } from 'typesafe-actions';

import { NotificationType } from '../../utils';

import types from './types';

export const NotificationList = createAsyncAction(
  types.NOTIFICATION_LIST.ATTEMPT,
  types.NOTIFICATION_LIST.SUCCESS,
  types.NOTIFICATION_LIST.FAILURE,
)<undefined, NotificationType[] | undefined, Error | undefined>();

export const AddNotification = createAction(
  types.NOTIFICATION_LIST.ADD,
  action => (notification: NotificationType) => action(notification),
);

export const SetAsRead = createAction(
  types.NOTIFICATION_DETAILS.SET_AS_READ,
  action => (notification: NotificationType) => action(notification),
);

export const SetAsDeleted = createAction(
  types.NOTIFICATION_DETAILS.SET_AS_DELETED,
  action => (notification: NotificationType) => action(notification),
);

export const NotificationDetails = createAsyncAction(
  types.NOTIFICATION_DETAILS.ATTEMPT,
  types.NOTIFICATION_DETAILS.SUCCESS,
  types.NOTIFICATION_DETAILS.FAILURE,
)<undefined, NotificationType | undefined, Error | undefined>();
