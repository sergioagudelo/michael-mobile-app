import { Dispatch } from 'redux';
import { NotificationList, NotificationDetails, SetAsDeleted } from './actions';

import * as MailServices from '../../services/mail';
import { NotificationType } from '../../utils';

export const getNotificationList = () => async (
  dispatch: Dispatch,
): Promise<{ success: boolean }> => {
  dispatch(NotificationList.request());
  const { success, response, error } = await MailServices.getMails();
  if (success && response) {
    dispatch(NotificationList.success(response.mails));
    return { success: true };
  }
  dispatch(NotificationList.failure(error));
  return { success: false };
};

export const getNotificationDetails = (notificationId: number) => async (dispatch: Dispatch) => {
  dispatch(NotificationDetails.request());
  const { success, response, error } = await MailServices.getMailDetail(notificationId);
  if (success && response) {
    dispatch(NotificationDetails.success({ id: notificationId, ...response }));
    return { success: true };
  }
  dispatch(NotificationDetails.failure(error));
};

export const deleteNotification = (notification: NotificationType) => async (
  dispatch: Dispatch,
) => {
  dispatch(NotificationDetails.request());
  const { success, error } = await MailServices.deleteMail(notification.id);
  if (!success) {
    dispatch(NotificationDetails.failure(error));
  }
  dispatch(SetAsDeleted(notification));
};

export {};
