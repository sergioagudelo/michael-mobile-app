import Notifications from './Notifications';
import NotificationDetails from './NotificationDetails';

export { Notifications, NotificationDetails };
