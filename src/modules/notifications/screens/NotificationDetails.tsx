import React from 'react';
import { NavigationStackProp } from 'react-navigation-stack';

import { NotificationDetails as NotificationDetailContainer } from '../containers';

type NotificationDetailsProps = {
  navigation: NavigationStackProp<{
    notificationId: number;
  }>;
};

const NotificationDetails = ({ navigation }: NotificationDetailsProps) => {
  // get notification from selector not from navigation
  const notificationId = navigation.getParam('notificationId');

  if (!notificationId) {
    navigation.goBack();
  }

  return <NotificationDetailContainer notificationId={notificationId} />;
};

export default NotificationDetails;
