import React, { useState, useEffect } from 'react';
import { SafeAreaView } from 'react-native';
import {
  ThemedComponentProps,
  withStyles,
  Layout,
} from 'react-native-ui-kitten';
import { NavigationTabScreenProps } from 'react-navigation-tabs';

import { Dispatch, bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// store
import { RootState } from 'typesafe-actions';
import * as store from '../store';

// Types
import { NotificationType } from '../utils';

// Modules
import shared from '../../shared';
import { NotificationsList } from '../components';

// Components
const { Dashed, HeaderView } = shared.components;

const mapStateToProps = (state: RootState) => ({
  notifications: store.selectors.getNotificationList(state),
});
const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(
    {
      getMails: store.actions.thunks.getNotificationList,
      setAsRead: store.actions.actions.SetAsRead,
    },
    dispatch
  );

type NotificationsProps = NavigationTabScreenProps &
  ThemedComponentProps &
  ReturnType<typeof mapDispatchToProps> &
  ReturnType<typeof mapStateToProps>;

const Notifications = ({
  navigation,
  theme,
  themedStyle,
  getMails,
  setAsRead,
  notifications,
}: NotificationsProps) => {
  const [failedNotifications, setFailedNotifications] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    const getNotifications = async () => {
      setIsLoading(true);
      const { success } = await getMails();
      setIsLoading(false);
      if (!success) {
        setFailedNotifications(true);
      }
    };

    if (!notifications && !isLoading && !failedNotifications) {
      getNotifications();
    }
  }, [failedNotifications, getMails, isLoading, notifications]);

  if (failedNotifications) {
    // navigation.navigate // TODO: General error
  }

  const onNotificationPress = (notification: NotificationType) => {
    setAsRead(notification);
    navigation.navigate('NotificationDetails', {
      notificationId: notification.id,
    });
  };

  return (
    <SafeAreaView
      style={themedStyle.screen}
      {...shared.helpers.setTestID('NotificationsScreen')}
    >
      <Layout style={themedStyle.body}>
        <>
          <HeaderView
            title='Notification Center'
            subTitle='NEWS &amp; EXCLUSIVE OFFERS'
          />
          <Dashed
            color={theme['color-primary-700']}
            customStyle={themedStyle.separator}
          />
        </>
        <NotificationsList
          notifications={notifications as NotificationType[]}
          onNotificationPress={onNotificationPress}
        />
      </Layout>
    </SafeAreaView>
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(
  withStyles(Notifications, () => ({
    screen: {
      flex: 1,
    },
    body: {
      flexGrow: 1,
      marginHorizontal: 30,
    },
    separator: {
      marginVertical: 20,
    },
  }))
);
