import * as testData from './testData';

export type ArticleType = {
  id: number;
  date: string;
  title: {
    rendered: string;
  };
  excerpt: {
    rendered: string;
  };
  content: {
    rendered: string;
  };
};

export const fixUnicodeCharsInTitle = (data: ArticleType[]): ArticleType[] =>
  data.map((entry: ArticleType) => {
    const title = entry.title.rendered;
    const regexArray = title.match(/&#\w*;/g) || [''];
    const htmlEntityNumber = regexArray[0];
    if (!htmlEntityNumber) return entry;
    const charCode = htmlEntityNumber.slice(2, htmlEntityNumber.length - 1);
    const parsedTitle = title.replace(htmlEntityNumber, String.fromCharCode(parseInt(charCode)));
    entry.title.rendered = parsedTitle;
    return entry;
  });
