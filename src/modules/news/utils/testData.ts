/* eslint-disable @typescript-eslint/camelcase */

export const newsData = [
  {
    id: 44310,
    date: '2019-09-05T10:21:31',
    date_gmt: '2019-09-05T14:21:31',
    guid: {
      rendered: 'https://ww1.lendingpoint.com/?p=44310',
    },
    modified: '2019-09-05T10:55:51',
    modified_gmt: '2019-09-05T14:55:51',
    slug: 'lendingpoint-closes-178-million-personal-loans-securitization',
    status: 'publish',
    type: 'post',
    link:
      'https://www.lendingpoint.com/blog/lendingpoint-closes-178-million-personal-loans-securitization/',
    title: {
      rendered: 'LendingPoint Closes $178 Million Personal Loans Securitization',
    },
    content: {
      rendered:
        '<header>\n<div>\n<div class="bw-release-subhead">\n<p>&nbsp;</p>\n<p class="bwalignc"><em><b>Inaugural securitization rated through 95% of pool balance with blended note yield of 4.05%</b></em></p>\n</div>\n</div>\n</header>\n<p><span style="color: #444444; font-family: \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; font-size: 14px; background-color: #fefefe;">KENNESAW, Ga.&#8211;(</span><span style="color: #444444; font-family: \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; font-size: 14px; background-color: #fefefe;"><a style="text-decoration-line: none; color: #79a2bd; outline: none;" href="https://www.businesswire.com/" rel="nofollow">BUSINESS WIRE</a></span><span style="color: #444444; font-family: \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; font-size: 14px; background-color: #fefefe;">)&#8211;LendingPoint, the company revolutionizing and democratizing commerce, announced today that it closed its inaugural securitization of consumer loans. LendingPoint Receivables Trust 2019-1 (&#8220;LDPT 2019-1&#8221;) issued $177.85 million of notes backed by a pool of $187.22 million of direct-to-consumer loans originated on the LendingPoint platform.</span></p>\n<p>Guggenheim Securities acted as the sole structuring advisor and sole book-running manager. All collateral contributed to the transaction had been held on LendingPoint\u2019s balance sheet and, as the sponsor of the transaction, LendingPoint served as the risk retention sponsor.</p>\n<p>&#8220;We are very pleased that our first securitization received such an enthusiastic response from the ABS market,&#8221; said Tom Burnside, CEO of LendingPoint. &#8220;The strong rating report, the efficient deal structure, and the attractive pricing all validate our decision to focus first on credit as the initial step on our mission to revolutionize and democratize commerce. As we continue to expand our presence in the point of sale market, as well as grow our online, direct-to-consumer lending program, we intend to return frequently to the ABS markets with loans arising from both origination channels. Our unique focus on excellent credit fundamentals was reflected in the execution of 2019-1. Our journey to revolutionize commerce will continue to be focused on credit excellence, phenomenal customer experience, rapid growth, and continued profitability.\u201d</p>\n<p>The LendingPoint Receivables Trust securitization was rated by Kroll Bond Rating Agency, Inc. and includes $117.76 million of Class A notes rated &#8220;A-&#8220;, $24.74 million of Class B notes rated &#8220;BBB-&#8220;, $23.68 million of Class C notes rated &#8220;BB-&#8221; and $10.67 million of Class D notes rated &#8220;B-.&#8221; The notes priced at a blended yield of 4.05% per annum and provided for a 95% advance rate. The transaction has a 5% overcollateralization Deposit and a 5% overcollateralization Target. The risk adjusted yield of the receivables securing the notes is expected to be 13.14% per annum.</p>\n<p>\u201cFor an inaugural ABS deal, the execution of LDPT 2019-1 was nothing short of amazing,\u201d according to Luke Graham, a veteran of the ABS and structured finance markets and a LendingPoint advisor. \u201cThe quality of the note buyers, the blended note rate, the overcollateralization target and the adjusted yield are all extraordinary and seem to signal that LendingPoint\u2019s plan to issue periodic securitizations will be welcomed in the markets.\u201d</p>\n<p>Since making its first loan in 2015, the LendingPoint platform has helped fund more than $1.4 billion of personal loans to more than 125,000 borrowers nationwide. To date, the company has raised $190 million in capital and continues to expand its product lines and distribution, now offering its Point of Sale financing program through thousands of merchants and service providers to help them close more sales.</p>\n<p><b>About LendingPoint</b></p>\n<p>LendingPoint is on a mission to revolutionize and democratize commerce. Beginning with using data and technology to provide credit to underserved NearPrime consumers online, then expanding to serve virtually all customers with point of sale financing, LendingPoint seeks to accelerate commerce and help people live better financial lives. Its award-winning leadership team brings unmatched experience in FinTech and credit. In August 2019, LendingPoint was named #17 on the Inc. 5000 list of the fastest growing companies in the U.S. LendingPoint is a privately-held company with headquarters in Kennesaw, Ga. LendingPoint originates loans directly and originates on behalf of FinWise Bank, a Utah-chartered bank, Member FDIC and First Electronic Bank, a Utah-chartered bank, Member FDIC. For more information, visit\u00a0<a href="https://cts.businesswire.com/ct/CT?id=smartlink&amp;url=https%3A%2F%2Fwww.lendingpoint.com&amp;esheet=52086591&amp;newsitemid=20190904005150&amp;lan=en-US&amp;anchor=https%3A%2F%2Fwww.lendingpoint.com&amp;index=1&amp;md5=626bd7eabd12b2ba7972eba74e27c4f3" target="_blank" rel="noopener" shape="rect">https://www.lendingpoint.com</a>.</p>\n',
      protected: false,
    },
    excerpt: {
      rendered: '<p>LendingPoint Closes $178 Million Personal Loans Securitization</p>',
      protected: false,
    },
    author: 9,
    featured_media: 44311,
    comment_status: 'closed',
    ping_status: 'closed',
    sticky: false,
    template: '',
    format: 'standard',
    meta: [],
    categories: [31, 615],
    tags: [],
    yst_prominent_words: [
      3370,
      3363,
      3358,
      3366,
      3377,
      3376,
      995,
      678,
      3371,
      3364,
      3368,
      3360,
      3367,
      2986,
      3362,
      3369,
      3361,
      3365,
      3359,
      3375,
    ],
    _links: {
      self: [
        {
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/posts/44310',
        },
      ],
      collection: [
        {
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/posts',
        },
      ],
      about: [
        {
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/types/post',
        },
      ],
      author: [
        {
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/users/9',
        },
      ],
      replies: [
        {
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/comments?post=44310',
        },
      ],
      'version-history': [
        {
          count: 3,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/posts/44310/revisions',
        },
      ],
      'predecessor-version': [
        {
          id: 44313,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/posts/44310/revisions/44313',
        },
      ],
      'wp:featuredmedia': [
        {
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/media/44311',
        },
      ],
      'wp:attachment': [
        {
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/media?parent=44310',
        },
      ],
      'wp:term': [
        {
          taxonomy: 'category',
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/categories?post=44310',
        },
        {
          taxonomy: 'post_tag',
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/tags?post=44310',
        },
        {
          taxonomy: 'yst_prominent_words',
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/yst_prominent_words?post=44310',
        },
      ],
      curies: [
        {
          name: 'wp',
          href: 'https://api.w.org/{rel}',
          templated: true,
        },
      ],
    },
  },
  {
    id: 44292,
    date: '2019-08-14T10:41:26',
    date_gmt: '2019-08-14T14:41:26',
    guid: {
      rendered: 'https://ww1.lendingpoint.com/?p=44292',
    },
    modified: '2019-08-14T10:55:52',
    modified_gmt: '2019-08-14T14:55:52',
    slug: 'atlanta-based-lendingpoint-17-inc-500-list-fastest-growing-private-companies-usa',
    status: 'publish',
    type: 'post',
    link:
      'https://www.lendingpoint.com/blog/atlanta-based-lendingpoint-17-inc-500-list-fastest-growing-private-companies-usa/',
    title: {
      rendered:
        'Atlanta-Based LendingPoint #17 on Inc. 500 List of Fastest Growing Private Companies in the USA',
    },
    content: {
      rendered:
        '<header>\n<div>\n<div class="bw-release-subhead">\n<p>&nbsp;</p>\n<p class="bwalignc" style="text-align: center;">LendingPoint Posts Three-Year Revenue Growth of 9,265%</p>\n<p>KENNESAW, Ga.&#8211;(<a href="https://www.businesswire.com/" target="_blank" rel="nofollow noopener">BUSINESS WIRE</a>)&#8211;<i>Inc</i>. Magazine today ranked commerce platform\u00a0<a href="https://cts.businesswire.com/ct/CT?id=smartlink&amp;url=http%3A%2F%2Fwww.lendingpoint.com%2F&amp;esheet=52077937&amp;newsitemid=20190814005133&amp;lan=en-US&amp;anchor=LendingPoint&amp;index=1&amp;md5=2a41bd24761572e874adc3672d1a88bf" target="_blank" rel="noopener" shape="rect">LendingPoint</a>\u00a0No. 17 on its 37th annual Inc. 5000, the most prestigious ranking of the nation&#8217;s fastest-growing private companies. LendingPoint joins companies such as Microsoft, Dell, Pandora, Timberland, LinkedIn, Yelp, Zillow, and many other well-known names who gained their first national exposure as honorees on the Inc. 5000. LendingPoint has hit successive funding records year-over-year and is on pace to reach $100 million per month in loan originations by the end of 2019.</p>\n</div>\n</div>\n</header>\n<blockquote>\n<div class="bw-release-body  ">\u201cOur platform saw more originations in 2018 than in 2015, 2016 and 2017 combined\u201d</div>\n</blockquote>\n<div>\n<p>\u201cOur platform saw more originations in 2018 than in 2015, 2016 and 2017 combined,\u201d says LendingPoint CEO Tom Burnside, \u201cand at the same time our credit performance improved allowing us to facilitate more financing for consumers online and at the point of sale. We are incredibly grateful to our customers and proud of the LendingPoint team.\u201d</p>\n<p>The 2019 Inc. 5000 is ranked according to percentage revenue growth when comparing 2015 and 2018. Not only have the companies on this year\u2019s list been very competitive within their markets, but the list as a whole shows staggering growth compared to years past. The 2019 Inc. 5000 achieved a three-year average growth of 454 percent, and a median rate of 157 percent. The Inc. 5000\u2019s aggregate revenue was $237.7 billion in 2018, accounting for 1,216,308 jobs over the past three years.</p>\n<p>\u201cThe companies on this year\u2019s Inc. 5000 have followed so many different paths to success,\u201d says\u00a0<i>Inc</i>. editor-in-chief James Ledbetter. \u201cThere\u2019s no single course you can follow or investment you can take that will guarantee this kind of spectacular growth. But what they have in common is persistence and seizing opportunities.\u201d</p>\n<p>\u201cOur mission is to revolutionize and democratize commerce for everyone,\u201d says Burnside. \u201cWe started by using data and technology to provide access to credit to underserved populations, expanded to providing financing options at the point of sale to virtually everyone, and are now working on ways to integrate financing and payments with loyalty using blockchain to protect PII and enhance the customer experience. It has been quite a ride for the last three years, and we\u2019ve only scratched the surface.\u201d</p>\n<p>Complete results of the Inc. 5000, including company profiles and an interactive database that can be sorted by industry, region, and other criteria, can be found at\u00a0<a href="https://cts.businesswire.com/ct/CT?id=smartlink&amp;url=http%3A%2F%2Fwww.inc.com%2Finc5000&amp;esheet=52077937&amp;newsitemid=20190814005133&amp;lan=en-US&amp;anchor=www.inc.com%2Finc5000&amp;index=2&amp;md5=fb494eb4c5825a2db12df88f7d8d610c" target="_blank" rel="noopener" shape="rect">www.inc.com/inc5000</a>. The top 500 companies will be featured in the September issue of\u00a0<i>Inc.</i>, available on newsstands August 20. All companies on the list will be honored during the annual Inc. 5000 event on October 10-12 at the JW Marriott Desert Ridge Resort and Spa in Phoenix, Arizona.</p>\n<p><b>About LendingPoint:</b></p>\n<p><a href="https://cts.businesswire.com/ct/CT?id=smartlink&amp;url=https%3A%2F%2Fwww.lendingpoint.com%2F&amp;esheet=52077937&amp;newsitemid=20190814005133&amp;lan=en-US&amp;anchor=LendingPoint&amp;index=3&amp;md5=3746ff2b0e06e41cbbd6498a7b1a69b1" target="_blank" rel="noopener" shape="rect">LendingPoint</a>\u00a0is a FinTech lending platform providing direct to consumer loans as well as product and service financing at the point of sale. The company is creating a better lending and borrowing experience by looking for more reasons to say \u201cyes\u201d &#8212; helping consumers across the credit spectrum unlock access to affordable loans and live better financial lives. Its\u00a0<a href="https://cts.businesswire.com/ct/CT?id=smartlink&amp;url=https%3A%2F%2Flendingpointmerchantsolutions.com%2F&amp;esheet=52077937&amp;newsitemid=20190814005133&amp;lan=en-US&amp;anchor=LendingPoint+Merchant+Solutions&amp;index=4&amp;md5=da9de47ac6f47c378d8bea59f59d65ec" target="_blank" rel="noopener" shape="rect">LendingPoint Merchant Solutions</a>\u00a0platform provides merchants, service providers and medical institutions a fully integrated, one-stop retail financing in-a-box solution to convert more customers at the point of sale. LendingPoint was named in 2018 one of the nation&#8217;s best consumer loan companies by\u00a0<i>U.S. News &amp; World Report,\u00a0</i>one of the Best Personal Loans by NerdWallet, one of the GA Fast 40 by the Atlanta Chapter of The ACG, and one of the Top Workplaces by the Atlanta Journal Constitution. Founded in 2014, LendingPoint is a privately held company headquartered in Kennesaw, Ga. with offices in New York City and San Diego. For more information, visit\u00a0<a href="https://cts.businesswire.com/ct/CT?id=smartlink&amp;url=http%3A%2F%2Fwww.lendingpoint.com%2F&amp;esheet=52077937&amp;newsitemid=20190814005133&amp;lan=en-US&amp;anchor=www.lendingpoint.com&amp;index=5&amp;md5=55fc04ecd11ff087f6fccc9731b09e1b" target="_blank" rel="noopener" shape="rect">www.lendingpoint.com</a>.</p>\n<p><b>About Inc. Media</b></p>\n<p>Founded in 1979 and acquired in 2005 by Mansueto Ventures,\u00a0<i>Inc.</i>\u00a0is the only major brand dedicated exclusively to owners and managers of growing private companies, with the aim to deliver real solutions for today\u2019s innovative company builders.\u00a0<i>Inc.\u00a0</i>took home the National Magazine Award for General Excellence in both 2014 and 2012. The total monthly audience reach for the brand has been growing significantly, from 2,000,000 in 2010 to more than 20,000,000 today. For more information, visit\u00a0<span class="bwuline"><a href="https://cts.businesswire.com/ct/CT?id=smartlink&amp;url=http%3A%2F%2Fwww.inc.com&amp;esheet=52077937&amp;newsitemid=20190814005133&amp;lan=en-US&amp;anchor=www.inc.com&amp;index=6&amp;md5=cc99cefb4348095735555527a3015975" target="_blank" rel="noopener" shape="rect">www.inc.com</a></span>.</p>\n<p>The Inc. 5000 is a list of the fastest-growing private companies in the nation. Started in 1982, this prestigious list has become the hallmark of entrepreneurial success. The Inc. 5000 Conference &amp; Awards Ceremony is an annual event that celebrates the remarkable achievements of these companies. The event also offers informative workshops, celebrated keynote speakers, and evening functions.</p>\n<p>For more information on\u00a0<i>Inc.</i>\u00a0and the Inc. 5000 Conference, visit\u00a0<span class="bwuline"><a href="https://cts.businesswire.com/ct/CT?id=smartlink&amp;url=http%3A%2F%2Fconference.inc.com%2F&amp;esheet=52077937&amp;newsitemid=20190814005133&amp;lan=en-US&amp;anchor=http%3A%2F%2Fconference.inc.com%2F&amp;index=7&amp;md5=bbcb1a2510321fb6165123ad1e573239" target="_blank" rel="noopener" shape="rect">http://conference.inc.com/</a></span>.</p>\n<p><img src="https://cts.businesswire.com/ct/CT?id=bwnews&amp;sty=20190814005133r1&amp;sid=web01&amp;distro=nx&amp;lang=en" alt="" /></p>\n</div>\n',
      protected: false,
    },
    excerpt: {
      rendered:
        '<p>KENNESAW, Ga.&#8211;(BUSINESS WIRE)&#8211;Inc. Magazine today ranked commerce platform\u00a0LendingPoint\u00a0No. 17 on its 37th annual Inc. 5000, the most prestigious ranking of the nation&#8217;s fastest-growing private companies. LendingPoint joins companies such as Microsoft, Dell, Pandora, Timberland, LinkedIn, Yelp, Zillow, and many other well-known names who gained their first national exposure as honorees on the Inc. 5000. LendingPoint has hit successive funding records year-over-year and is on pace to reach $100 million per month in loan originations by the end of 2019.</p>\n',
      protected: false,
    },
    author: 13,
    featured_media: 4114,
    comment_status: 'closed',
    ping_status: 'closed',
    sticky: false,
    template: '',
    format: 'standard',
    meta: [],
    categories: [31],
    tags: [],
    yst_prominent_words: [
      3331,
      3328,
      3338,
      3332,
      3325,
      3340,
      1124,
      3334,
      3341,
      3333,
      3324,
      995,
      678,
      3337,
      3330,
      2986,
      3335,
      3339,
      3336,
      3329,
    ],
    _links: {
      self: [
        {
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/posts/44292',
        },
      ],
      collection: [
        {
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/posts',
        },
      ],
      about: [
        {
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/types/post',
        },
      ],
      author: [
        {
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/users/13',
        },
      ],
      replies: [
        {
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/comments?post=44292',
        },
      ],
      'version-history': [
        {
          count: 4,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/posts/44292/revisions',
        },
      ],
      'predecessor-version': [
        {
          id: 44296,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/posts/44292/revisions/44296',
        },
      ],
      'wp:featuredmedia': [
        {
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/media/4114',
        },
      ],
      'wp:attachment': [
        {
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/media?parent=44292',
        },
      ],
      'wp:term': [
        {
          taxonomy: 'category',
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/categories?post=44292',
        },
        {
          taxonomy: 'post_tag',
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/tags?post=44292',
        },
        {
          taxonomy: 'yst_prominent_words',
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/yst_prominent_words?post=44292',
        },
      ],
      curies: [
        {
          name: 'wp',
          href: 'https://api.w.org/{rel}',
          templated: true,
        },
      ],
    },
  },
  {
    id: 44226,
    date: '2019-06-20T09:59:17',
    date_gmt: '2019-06-20T13:59:17',
    guid: {
      rendered: 'https://ww1.lendingpoint.com/?p=44226',
    },
    modified: '2019-06-20T14:23:20',
    modified_gmt: '2019-06-20T18:23:20',
    slug: 'lendingpoint-ceo-tom-burnside-chosen-entrepreneur-year-finalist-southeast-ey',
    status: 'publish',
    type: 'post',
    link:
      'https://www.lendingpoint.com/blog/lendingpoint-ceo-tom-burnside-chosen-entrepreneur-year-finalist-southeast-ey/',
    title: {
      rendered:
        'LendingPoint CEO, Tom Burnside, Chosen as Entrepreneur of the Year Finalist Southeast by EY',
    },
    content: {
      rendered:
        '<p>LendingPoint, the company working to revolutionize access to credit, is excited to announce that CEO and founder Tom Burnside was selected as a finalist for EY\u2019s Entrepreneur of the Year Southeast. The program recognizes entrepreneurs in more than 145 cities around the world who demonstrate excellence and extraordinary success in areas such as innovation, financial performance, and personal commitment to their businesses and communities.</p>\n<p>LendingPoint also was recognized as the fastest growing company in the financial services category and eighth overall fastest-growing private company during the Atlanta Business Chronicle\u2019s Pacesetter award ceremony. The annual list ranks 100 local companies based on their revenue and employee growth. To be eligible, Atlanta-based companies must have had revenue between $1 million and $300 million in 2018 and have had two-year growth in sales of at least 50 percent.</p>\n<p>\u201cWe are delighted that Tom\u2019s been selected as an EY Entrepreneur of the Year finalist. His leadership has guided our company through a period of strong growth, and his dedication to the LendingPoint mission of providing exceptional loan products is unmatched,\u201d says Mark Lorimer, LendingPoint Chief Communications and Public Affairs Officer. \u201cWe are also honored to be recognized as an Atlanta Pacesetter and will continue to support our local community in the years to come.\u201d</p>\n<p>An accomplished credit and financial services leader as well as a respected data scientist, Tom brings more than 25 years of experience and a wealth of industry knowledge to LendingPoint. Also listed as a finalist for Executive of the Year by LendIt, Tom has led LendingPoint to fund more than $1 billion in personal unsecured loans. Its $500mm in loans in 2018 was more than twice the amount of 2017, and in 2019, the company is on track to double again.</p>\n<p>The finalists for EYs Entrepreneur of the Year were selected by a panel of independent judges and the winners will be announced for the first time at a black-tie awards gala on Thursday, June 27 in Atlanta, Georgia.</p>\n<h2><strong>About LendingPoint</strong></h2>\n<p>LendingPoint is a FinTech lending platform providing direct to consumer loans as well as product and service financing at the point of sale. The company is creating a better lending and borrowing experience by looking for more reasons to say \u201cyes\u201d &#8212; helping consumers across the credit spectrum unlock access to affordable loans and live better financial lives. Its LendingPoint Merchant Solutions platform provides merchants, service providers and medical institutions a fully integrated, one-stop retail financing in-a-box solution to convert more customers at the point of sale. LendingPoint was named in 2018 one of the nation&#8217;s best consumer loan companies by U.S. News &amp; World Report, one of the Best Personal Loans by NerdWallet, one of the GA Fast 40 by the Atlanta Chapter of The ACG, and one of the Top Workplaces by the Atlanta Journal Constitution. Founded in 2014, LendingPoint is a privately held company headquartered in Kennesaw, Ga. with offices in New York City and San Diego. For more information, visit www.lendingpoint.com.</p>\n<h2>About Entrepreneur Of The Year\u00ae</h2>\n<p>Entrepreneur Of The Year\u00ae, founded by EY, is the world\u2019s most prestigious business awards program for entrepreneurs. The program makes a difference through the way it encourages entrepreneurial activity among those with potential and recognizes the contribution of people who inspire others with their vision, leadership and achievement. As the first and only truly global awards program of its kind, Entrepreneur Of The Year celebrates those who are building and leading successful, growing and dynamic businesses, recognizing them through regional, national and global awards programs in more than 145 cities and more than 60 countries. ey.com/eoy</p>\n',
      protected: false,
    },
    excerpt: {
      rendered:
        '<p>LendingPoint is excited to announce that CEO and founder Tom Burnside was selected as a finalist for EY\u2019s Entrepreneur of the Year Southeast. </p>\n',
      protected: false,
    },
    author: 9,
    featured_media: 44233,
    comment_status: 'closed',
    ping_status: 'closed',
    sticky: false,
    template: '',
    format: 'standard',
    meta: [],
    categories: [570],
    tags: [],
    yst_prominent_words: [
      3272,
      1336,
      3276,
      3278,
      3273,
      832,
      702,
      3275,
      3271,
      3280,
      825,
      764,
      3274,
      678,
      666,
      682,
      2986,
      3277,
      3279,
      1041,
    ],
    _links: {
      self: [
        {
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/posts/44226',
        },
      ],
      collection: [
        {
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/posts',
        },
      ],
      about: [
        {
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/types/post',
        },
      ],
      author: [
        {
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/users/9',
        },
      ],
      replies: [
        {
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/comments?post=44226',
        },
      ],
      'version-history': [
        {
          count: 1,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/posts/44226/revisions',
        },
      ],
      'predecessor-version': [
        {
          id: 44227,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/posts/44226/revisions/44227',
        },
      ],
      'wp:featuredmedia': [
        {
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/media/44233',
        },
      ],
      'wp:attachment': [
        {
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/media?parent=44226',
        },
      ],
      'wp:term': [
        {
          taxonomy: 'category',
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/categories?post=44226',
        },
        {
          taxonomy: 'post_tag',
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/tags?post=44226',
        },
        {
          taxonomy: 'yst_prominent_words',
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/yst_prominent_words?post=44226',
        },
      ],
      curies: [
        {
          name: 'wp',
          href: 'https://api.w.org/{rel}',
          templated: true,
        },
      ],
    },
  },
  {
    id: 44211,
    date: '2019-05-29T09:24:44',
    date_gmt: '2019-05-29T13:24:44',
    guid: {
      rendered: 'https://ww1.lendingpoint.com/?p=44211',
    },
    modified: '2019-05-29T09:24:44',
    modified_gmt: '2019-05-29T13:24:44',
    slug: '10-tax-moves-make-summer-begins',
    status: 'publish',
    type: 'post',
    link: 'https://www.lendingpoint.com/blog/10-tax-moves-make-summer-begins/',
    title: {
      rendered: '10 tax moves to make before Summer begins',
    },
    content: {
      rendered:
        '<p><em>Take advantage of warm weather to make tax moves!</p>\n<p></em>While its more enjoyable to travel, go on adventures or relax during Summer, it is also a good time to plan for your future and \u00a0revisit your finances on the slower days of summer.</p>\n<h2><strong>Revisit your retirement accounts</strong></h2>\n<p>Make your savings work harder for you, <u><a href="https://www.forbes.com/sites/kellyphillipserb/2018/05/25/10-tax-moves-to-make-before-summer-begins/#5a605518bbfa">this Forbes article explains that</a></u> \u201cthe more money that you can sock away into your 401(k) or other retirement savings plan, the better, since most contributions are made with pre-tax money. That goes for self-employed persons, too.\u201d</p>\n<p>Take advantage of your days by the pool to assess if you\u2019re contributing enough and dive into what your long-term plan looks like.</p>\n<h2><strong>Think about your healthcare accounts</strong></h2>\n<p>If you have a Flexible Savings Account (FSA) making sure that you\u2019re contributing an amount commensurate with your annual healthcare spend and using your funds before you lose them is important. </p>\n<p>On the other hand, if you have a healthcare savings account (HSA), you can contribute pre-tax dollars directly from your paycheck &#8211; even without an employer contribution. Just keep in mind that with an HSA you must have a high deductible medical plan to contribute, and your funds won\u2019t expire until you use them all.</p>\n<h2><strong>Check your withholdings</strong></h2>\n<p>During Summer you should check the number of deductions you take on your paycheck, because if you take too many you may have taxes to pay at the end of the year. <u><a href="https://www.forbes.com/sites/kellyphillipserb/2018/05/18/if-you-itemize-your-deductions-its-time-for-a-checkup-on-your-taxes/#6873215e7ed7">According to Forbes,</a></u> \u201cthe Internal Revenue Service (IRS) encourages you to do a quick check up on your taxes. By plugging your current tax data into the withholding calculator on the IRS website, you can do a &#8220;paycheck checkup&#8221; and avoid any nasty surprises at year end.\u201d<strong><br />\n</strong></p>\n<h2><strong>Prepare taxes for your side business</strong></h2>\n<p>If you have recently started a side business in addition to your full-time job, you know what it\u2019s like putting in extra hours; don\u2019t forget to spend extra time on you taxes as well. If you find some additional time this Summer, whether you have an LLC, a partnership or gig income, remember that if you are not paying estimated taxes and are earning extra income, you will have to make up for that at the end of the year. However, if you are losing money, then that will be helpful in reducing your tax load.</p>\n<p><u><a href="https://www.fool.com/retirement/2019/02/24/have-a-side-hustle-heres-what-you-need-to-know-abo.aspx">Here are a few other ways to prepare.</a></u></p>\n<h2><strong>Pay your estimated tax</strong></h2>\n<p>Being surprised during tax season and having to pay money you weren\u2019t prepared for can be hard. Luckily, you can pay quarterly estimated taxes. <u><a href="https://www.irs.gov/newsroom/heres-how-and-when-to-pay-estimated-taxes">According to the IRS</a></u>,\u00a0 \u201cTaxpayers must generally pay at least 90 percent of their taxes throughout the year through withholding, estimated tax payments or a combination of the two. If they don\u2019t, they may owe an estimated tax penalty.\u201d<strong><br />\n</strong></p>\n<h2><strong>Has your household size changed?</strong></h2>\n<p>If the answer is yes, then you should look at your pre-tax withholding. \u201cThis past April, the IRS said that the average federal tax refund was $2,864 \u2013 the rough equivalent of a month\u2019s salary for many people. Adjusting the withholding on your W-4 may bring you more take-home pay. Ideally, you would adjust it so that you end up owing no tax and receiving no refund,\u201d <u><a href="https://argifinancialgroup.com/tax-moves-to-consider-in-summer/">this site explains</a></u>.</p>\n<h2><strong>Pay tuition bills</strong></h2>\n<p>If you, your kids or your spouse are getting higher education, there are ways to get reimbursement for tuition and other associated costs, like textbooks. <u><a href="https://www.efile.com/student-education-college-tuition-tax-credits-hope-life-time-learning-credit/">Click here for more on education tax breaks, like the American Opportunity Credit.<br />\n</a></u></p>\n<h2><strong>Go to the doctor</strong></h2>\n<p><u><a href="https://turbotax.intuit.com/tax-tips/health-care/can-i-claim-medical-expenses-on-my-taxes/L1htkVqq9">TurboTax explains that</a></u> \u201cThe Internal Revenue Service allows taxpayers some relief, making some medical expenses partly tax-deductible. To take advantage of this tax deduction, you need to know what counts as a medical expense and how to claim the deduction.\u201d</p>\n<h2><strong>Consider home improvements</strong></h2>\n<p>Did you know that many states offer<u><a href="https://www.nolo.com/legal-encyclopedia/what-home-improvements-tax-deductible.html"> tax breaks on some home improvements</a></u>? If you\u2019re a homeowner, this is a great time to get that solar panel you\u2019ve been thinking about and explore how this can benefit you financially as well!</p>\n<h2><strong>Find the right tax professional for you</strong></h2>\n<p>If you haven\u2019t already, now\u2019s the time to start looking for the right tax professional that will assist with tax preparations when the season comes. Before deciding on someone, make sure to ask questions about their experience, references and fees.</p>\n<p><u><a href="https://www.forbes.com/sites/kellyphillipserb/2018/05/25/10-tax-moves-to-make-before-summer-begins/#374df519bbfa">Click here for more on tax moves for Summer</a></u>!</p>\n',
      protected: false,
    },
    excerpt: {
      rendered:
        '<p>While its more enjoyable to travel, go on adventures or relax during Summer, it is also a good time to plan for your future and \u00a0revisit your finances on the slower days of summer.</p>\n',
      protected: false,
    },
    author: 9,
    featured_media: 44212,
    comment_status: 'closed',
    ping_status: 'closed',
    sticky: false,
    template: '',
    format: 'standard',
    meta: [],
    categories: [655, 570],
    tags: [],
    yst_prominent_words: [
      3269,
      3270,
      1645,
      3264,
      3261,
      1397,
      1399,
      3265,
      3268,
      3262,
      1009,
      1559,
      3266,
      1122,
      1966,
      3267,
      3263,
      1964,
      2921,
      936,
    ],
    _links: {
      self: [
        {
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/posts/44211',
        },
      ],
      collection: [
        {
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/posts',
        },
      ],
      about: [
        {
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/types/post',
        },
      ],
      author: [
        {
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/users/9',
        },
      ],
      replies: [
        {
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/comments?post=44211',
        },
      ],
      'version-history': [
        {
          count: 1,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/posts/44211/revisions',
        },
      ],
      'predecessor-version': [
        {
          id: 44213,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/posts/44211/revisions/44213',
        },
      ],
      'wp:featuredmedia': [
        {
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/media/44212',
        },
      ],
      'wp:attachment': [
        {
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/media?parent=44211',
        },
      ],
      'wp:term': [
        {
          taxonomy: 'category',
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/categories?post=44211',
        },
        {
          taxonomy: 'post_tag',
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/tags?post=44211',
        },
        {
          taxonomy: 'yst_prominent_words',
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/yst_prominent_words?post=44211',
        },
      ],
      curies: [
        {
          name: 'wp',
          href: 'https://api.w.org/{rel}',
          templated: true,
        },
      ],
    },
  },
  {
    id: 44201,
    date: '2019-05-21T15:09:06',
    date_gmt: '2019-05-21T19:09:06',
    guid: {
      rendered: 'https://ww1.lendingpoint.com/?p=44201',
    },
    modified: '2019-06-04T08:51:58',
    modified_gmt: '2019-06-04T12:51:58',
    slug: '10-budget-apps-make-budget-bae',
    status: 'publish',
    type: 'post',
    link: 'https://www.lendingpoint.com/blog/10-budget-apps-make-budget-bae/',
    title: {
      rendered: '10 budget apps that make your budget \u201cbae\u201d',
    },
    content: {
      rendered:
        '<p><em>Find out how you can make your smartphone your personal financial advisor.</em></p>\n<p>If you\u2019re one of <a href="https://www.statista.com/statistics/330695/number-of-smartphone-users-worldwide/">2.5 billion smartphone users in the world</a>, chances are you know that there\u2019s an app for pretty much any of your needs. From getting groceries to checking on your pets at home or ordering a cab, everything can be taken care of with just a few clicks! But, did you know that your finances can also be covered by your phone? Here are ten apps you can download immediately to take control of your budget just like you control your Instagram!</p>\n<h2><strong>Mint</strong></h2>\n<p><a href="https://www.mint.com/">Mint</a> is one of the most popular budgeting apps and there\u2019s a reason for it \u2026 it really works! This app helps you easily create budgets that match your income and financial needs. It even provides templates to help you get started. Mint also lets you check your credit score and gives you tips on how to improve it.</p>\n<p>If you\u2019re struggling with your credit score, <a href="https://www.lendingpoint.com/blog/credit-simulator-find-one-helpful/">click here to learn about credit simulators</a> and how you can predict your score based on your financial activity!</p>\n<h2><strong>Acorns</strong></h2>\n<p>Have you ever considered investing but decided against it because you don\u2019t have enough money &#8212; or knowledge &#8212; to even get started? If so, <a href="https://www.acorns.com/">Acorns</a> might be the app your life has been missing. Other than having a cool name, Acorns lets you invest your spare change and guides you through the process. For instance, if you spend $3.95 on coffee, you can make it an even $4 and save the difference for future investment.\u00a0</p>\n<h2>Pocket Guard</h2>\n<p>Even though most credit card providers and financial institutions have apps for you to check your balances, having to go to multiple places to track your expenditures can be maddening. Thankfully, the <a href="https://pocketguard.com/">Pocket Guard app</a> allows you to check your finances all in one place; categorize your transactions and know how much spending-money you\u2019re left with after paying all your bills.</p>\n<h2>Wally</h2>\n<p>If you\u2019ve been keeping up with our blogs, you know that setting financial goals is one of the first steps to financial freedom, and that\u2019s why we love the <a href="http://wally.me/">Wally app</a>. Wally allows you to set financial goals and easily achieve them by helping you balance income versus expenses.</p>\n<h2>Prism</h2>\n<p>This free app allows you to efficiently control your money and pay your bills all in one place. <a href="https://www.prismmoney.com/about">According to the Prism website</a>, \u201cOnce you download Prism for your phone or tablet, we&#8217;ll ask you to choose all of your billers. Once you connect them, we will sync your account balances and bills and present them to you directly in Prism. From there, you can pay your bills right from the app.\u201d\u00a0</p>\n<h2>Every Dollar</h2>\n<p>Because every dollar counts, <a href="https://www.everydollar.com/?gclid=CjwKCAjwtYXmBRAOEiwAYsyl3Cc3xURSkMNcjA3soJPpjPmOsukydCPth-dN9GdNE-srlKJrejuCvBoCW64QAvD_BwE">Every Dollar</a> lets you create a budget in less than 10 minutes by adding the income you expect, customizing for monthly expenses and tracking your spending. After a few weeks of using this app you\u2019ll be shocked at how much money you\u2019re spending and how much you could be saving instead.</p>\n<h2>You Need a Budget (YNAB)</h2>\n<p>As if you already didn\u2019t know that you need a budget, the YNAB app focuses on teaching you how to manage your money and helping you walk away from living paycheck to paycheck. Even though this is not a free app, <a href="https://www.youneedabudget.com/?utm_source=google&amp;utm_medium=cpc&amp;utm_campaign=%28roi%29%20branded&amp;utm_content=ynab&amp;utm_term=ynab%20software&amp;ar_clx=yes&amp;ar_channel=sem&amp;ar_campaign=36195583&amp;ar_adgroup=1420857373&amp;ar_ad=168916279579&amp;gclid=CjwKCAjwtYXmBRAOEiwAYsyl3COCCVjeiMYEzXW6-aIv5eEguRt6NQ2uW-vN8a8_OpKm2eG8_za99RoCa3kQAvD_BwE">you can start a free trial for 34 days here</a>.</p>\n<h2>Goodbudget</h2>\n<p>The <a href="https://goodbudget.com/how-it-works/#.XMJnIOhKjIU">Goodbudget app</a> takes budgeting to a whole new level by allowing you to interact with friends and family you choose to share your finances with. This is great for couples, friends planning big trips together and for parents checking on their kids to see if everyone is following their budgeting plan.</p>\n<h2>CountAbout</h2>\n<p>With <a href="https://countabout.com/">CountAbout</a> you can take advantage of their integration with thousands of financial institutions and easily create a budget, categorize expenses, check your progress with easy-to-read graphs and even send invoices! This app offers a free trial for 15 days.</p>\n<h2>Clarity Money</h2>\n<p>In addition to great budgeting tools, the free <a href="https://claritymoney.com/">Clarity Money app</a> helps you look at your active subscriptions and cancel the ones you no longer use! Get ready to get better insight on your expenses and stop overspending for no reason.</p>\n<p>Staying organized and focused when it comes to money can be challenging, so why not take advantage of available resources that will assist you in the process? Take the next step!</p>\n<div>\u00a0</div>\n',
      protected: false,
    },
    excerpt: {
      rendered:
        '<p>Find out how you can make your smartphone your personal financial advisor.</p>\n',
      protected: false,
    },
    author: 9,
    featured_media: 44202,
    comment_status: 'closed',
    ping_status: 'closed',
    sticky: false,
    template: '',
    format: 'standard',
    meta: [],
    categories: [655, 570],
    tags: [],
    yst_prominent_words: [
      959,
      3254,
      3252,
      705,
      963,
      1403,
      3257,
      1007,
      694,
      3258,
      825,
      1893,
      2719,
      3259,
      3255,
      3256,
      893,
      740,
      3251,
      3253,
    ],
    _links: {
      self: [
        {
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/posts/44201',
        },
      ],
      collection: [
        {
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/posts',
        },
      ],
      about: [
        {
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/types/post',
        },
      ],
      author: [
        {
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/users/9',
        },
      ],
      replies: [
        {
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/comments?post=44201',
        },
      ],
      'version-history': [
        {
          count: 4,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/posts/44201/revisions',
        },
      ],
      'predecessor-version': [
        {
          id: 44218,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/posts/44201/revisions/44218',
        },
      ],
      'wp:featuredmedia': [
        {
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/media/44202',
        },
      ],
      'wp:attachment': [
        {
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/media?parent=44201',
        },
      ],
      'wp:term': [
        {
          taxonomy: 'category',
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/categories?post=44201',
        },
        {
          taxonomy: 'post_tag',
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/tags?post=44201',
        },
        {
          taxonomy: 'yst_prominent_words',
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/yst_prominent_words?post=44201',
        },
      ],
      curies: [
        {
          name: 'wp',
          href: 'https://api.w.org/{rel}',
          templated: true,
        },
      ],
    },
  },
  {
    id: 44082,
    date: '2019-03-13T09:54:53',
    date_gmt: '2019-03-13T13:54:53',
    guid: {
      rendered: 'https://ww1.lendingpoint.com/?p=44082',
    },
    modified: '2019-03-20T15:56:32',
    modified_gmt: '2019-03-20T19:56:32',
    slug: 'womens-history-month-ladies-behind-lendingpoint',
    status: 'publish',
    type: 'post',
    link: 'https://www.lendingpoint.com/blog/womens-history-month-ladies-behind-lendingpoint/',
    title: {
      rendered: 'Women&#8217;s History Month: Featuring the Ladies of LendingPoint',
    },
    content: {
      rendered:
        '<p>March is Women\u2019s History Month, an annual declaration that highlights the contributions of women in history and contemporary society. We\u2019ve decided to highlight some of our women leaders who help make LendingPoint shine. Here\u2019s a list of some of our fintech LendingPoint Ladies:</p>\n<p><strong>Amanda Flashner SVP, Customer Experience:\u00a0</strong></p>\n<p><img class="wp-image-44084 alignleft" src="https://ww1.lendingpoint.com/wp-content/uploads/2019/03/0V4A3976_edit-1024x683.jpg" alt="" width="800" height="533" srcset="https://www.lendingpoint.com/wp-content/uploads/2019/03/0V4A3976_edit-1024x683.jpg 1024w, https://www.lendingpoint.com/wp-content/uploads/2019/03/0V4A3976_edit-600x400.jpg 600w, https://www.lendingpoint.com/wp-content/uploads/2019/03/0V4A3976_edit-1440x960.jpg 1440w, https://www.lendingpoint.com/wp-content/uploads/2019/03/0V4A3976_edit-24x16.jpg 24w, https://www.lendingpoint.com/wp-content/uploads/2019/03/0V4A3976_edit-36x24.jpg 36w, https://www.lendingpoint.com/wp-content/uploads/2019/03/0V4A3976_edit-48x32.jpg 48w" sizes="(max-width: 800px) 100vw, 800px" /></p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>Amanda Flashner, is an innovative and driven advocate for LendingPoint\u2019s customers. As someone who leads the customer experience practice, she ensures all of our customers\u2019 journeys are enjoyable, educational and efficient. We\u2019re proud to have such a compassionate spirit lead our customers to success.</p>\n<p>\n<strong>Christy Mahon SVP, Human Resources:</strong></p>\n<p><img class="wp-image-44087 alignleft" src="https://ww1.lendingpoint.com/wp-content/uploads/2019/03/christy.jpg" alt="" width="800" height="445" srcset="https://www.lendingpoint.com/wp-content/uploads/2019/03/christy.jpg 848w, https://www.lendingpoint.com/wp-content/uploads/2019/03/christy-600x334.jpg 600w, https://www.lendingpoint.com/wp-content/uploads/2019/03/christy-24x13.jpg 24w, https://www.lendingpoint.com/wp-content/uploads/2019/03/christy-36x20.jpg 36w, https://www.lendingpoint.com/wp-content/uploads/2019/03/christy-48x27.jpg 48w" sizes="(max-width: 800px) 100vw, 800px" /></p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>As SVP of human resources, Christy thrives in making our company\u2019s culture one for the books. She creates ways for employees to be more involved through internal company initiatives and vigorous non-profit efforts. She works hard every day to enhance our employees\u2019 experience. We are proud to highlight Christy Mahon as one of the most charismatic and positive female members of our team.</p>\n<p>\n<strong>Shylee McBride SVP, Governance:</strong></p>\n<p><img class="wp-image-44085 alignleft" src="https://ww1.lendingpoint.com/wp-content/uploads/2019/03/0V4A3780_edit-1024x683.jpg" alt="" width="800" height="533" srcset="https://www.lendingpoint.com/wp-content/uploads/2019/03/0V4A3780_edit-1024x683.jpg 1024w, https://www.lendingpoint.com/wp-content/uploads/2019/03/0V4A3780_edit-600x400.jpg 600w, https://www.lendingpoint.com/wp-content/uploads/2019/03/0V4A3780_edit-1440x960.jpg 1440w, https://www.lendingpoint.com/wp-content/uploads/2019/03/0V4A3780_edit-24x16.jpg 24w, https://www.lendingpoint.com/wp-content/uploads/2019/03/0V4A3780_edit-36x24.jpg 36w, https://www.lendingpoint.com/wp-content/uploads/2019/03/0V4A3780_edit-48x32.jpg 48w" sizes="(max-width: 800px) 100vw, 800px" /></p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>Shylee\u2019s dedication to the LendingPoint team never ceases. She is responsible for the creation and enforcement of all company policies and procedures all while overseeing quality operations and corporate training to maintain service levels, standardized processes and operational efficiencies.</p>\n<p>\n<strong>Nydia Johnson, Quality Manager:</strong></p>\n<p><img class="wp-image-44088 alignleft" src="https://ww1.lendingpoint.com/wp-content/uploads/2019/03/nydia.jpg" alt="" width="800" height="600" srcset="https://www.lendingpoint.com/wp-content/uploads/2019/03/nydia.jpg 640w, https://www.lendingpoint.com/wp-content/uploads/2019/03/nydia-600x450.jpg 600w, https://www.lendingpoint.com/wp-content/uploads/2019/03/nydia-24x18.jpg 24w, https://www.lendingpoint.com/wp-content/uploads/2019/03/nydia-36x27.jpg 36w, https://www.lendingpoint.com/wp-content/uploads/2019/03/nydia-48x36.jpg 48w" sizes="(max-width: 800px) 100vw, 800px" /></p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>As quality manager of LendingPoint, Nydia oversees all complaints, audits, risk testing assessments, bank partner reports and much more. She not only ensures that we are abiding by the thresholds of compliance, but also that we are following established internal procedures. We are proud to have such a thorough leader in our organization making sure that we are taking care of all things quality and compliance.</p>\n<p>\n<strong>Karen Louis, Associate General Counsel:</strong></p>\n<p><img class="wp-image-44089 alignleft" src="https://ww1.lendingpoint.com/wp-content/uploads/2019/03/karen.jpg" alt="" width="800" height="600" srcset="https://www.lendingpoint.com/wp-content/uploads/2019/03/karen.jpg 640w, https://www.lendingpoint.com/wp-content/uploads/2019/03/karen-600x450.jpg 600w, https://www.lendingpoint.com/wp-content/uploads/2019/03/karen-24x18.jpg 24w, https://www.lendingpoint.com/wp-content/uploads/2019/03/karen-36x27.jpg 36w, https://www.lendingpoint.com/wp-content/uploads/2019/03/karen-48x36.jpg 48w" sizes="(max-width: 800px) 100vw, 800px" /></p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>LendingPoint\u2019s Associate General Counsel, Karen Louis, is a force to be reckoned with. She has conquered the realm of regulatory compliance where she manages LendingPoint\u2019s operational and regulatory controlled environment. We are proud to have such a fun and diligent woman on our team!</p>\n<p>\n<strong>Robin Hooker, Manager of LendingPoint\u2019s Refinance Program</strong></p>\n<p><img class="wp-image-44090 alignleft" src="https://ww1.lendingpoint.com/wp-content/uploads/2019/03/robin.jpg" alt="" width="800" height="600" srcset="https://www.lendingpoint.com/wp-content/uploads/2019/03/robin.jpg 640w, https://www.lendingpoint.com/wp-content/uploads/2019/03/robin-600x450.jpg 600w, https://www.lendingpoint.com/wp-content/uploads/2019/03/robin-24x18.jpg 24w, https://www.lendingpoint.com/wp-content/uploads/2019/03/robin-36x27.jpg 36w, https://www.lendingpoint.com/wp-content/uploads/2019/03/robin-48x36.jpg 48w" sizes="(max-width: 800px) 100vw, 800px" /></p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>Robin is LendingPoint\u2019s Refinance Supervisor. She manages day to day operations and oversees our customer retention program. Her optimistic attitude and drive makes her one of our proud LendingPoint ladies. </p>\n<p><strong>Aileen Wyer, VP of eCRM and Marketing Manager, Andrea Williams:</strong></p>\n<p><img class="wp-image-44091 alignleft" src="https://ww1.lendingpoint.com/wp-content/uploads/2019/03/aileen-and-andrea-1024x447.jpg" alt="" width="800" height="349" srcset="https://www.lendingpoint.com/wp-content/uploads/2019/03/aileen-and-andrea-1024x447.jpg 1024w, https://www.lendingpoint.com/wp-content/uploads/2019/03/aileen-and-andrea-600x262.jpg 600w, https://www.lendingpoint.com/wp-content/uploads/2019/03/aileen-and-andrea-1440x628.jpg 1440w, https://www.lendingpoint.com/wp-content/uploads/2019/03/aileen-and-andrea-24x10.jpg 24w, https://www.lendingpoint.com/wp-content/uploads/2019/03/aileen-and-andrea-36x16.jpg 36w, https://www.lendingpoint.com/wp-content/uploads/2019/03/aileen-and-andrea-48x21.jpg 48w" sizes="(max-width: 800px) 100vw, 800px" /></p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>Aileen and Andrea are our marketing mavens making it happen. Aileen is responsible for marketing technology, SMS and emailing efforts and Andrea is our queen of marketing project management, tradeshow design and execution, overseer of marketing compliance as well as support for LendingPoint branding materials. Between these two, LendingPoint\u2019s marketing team is in very good hands!</p>\n<p><strong>Jessica Campos and Denise Hurley, Collections Supervisors:</strong></p>\n<p><img class="wp-image-44092 alignleft" src="https://ww1.lendingpoint.com/wp-content/uploads/2019/03/collections-ladies-1024x543.jpg" alt="" width="800" height="425" srcset="https://www.lendingpoint.com/wp-content/uploads/2019/03/collections-ladies-1024x543.jpg 1024w, https://www.lendingpoint.com/wp-content/uploads/2019/03/collections-ladies-600x318.jpg 600w, https://www.lendingpoint.com/wp-content/uploads/2019/03/collections-ladies-24x13.jpg 24w, https://www.lendingpoint.com/wp-content/uploads/2019/03/collections-ladies-36x19.jpg 36w, https://www.lendingpoint.com/wp-content/uploads/2019/03/collections-ladies-48x25.jpg 48w, https://www.lendingpoint.com/wp-content/uploads/2019/03/collections-ladies.jpg 1417w" sizes="(max-width: 800px) 100vw, 800px" /></p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>LendingPoint\u2019s Collections Supervisor, Jessica Campos (on the left), has been with LendingPoint for 3 and a half years. She is dedicated to making sure that our inventory is managed, and our collections are down. Together, Jessica and Denise Hurley, another talented LendingPoint Collections Supervisor, comanage all stages of consumer delinquency. Jessica\u2019s focus is on the early stages of delinquency and Denise focuses on the later stages. These dynamite ladies are passionate about assisting customers get their accounts back to current through one of LendingPoint\u2019s various programs. We\u2019re proud to have these two ladies heading our collections department!</p>\n<p><strong>Customer Service Manager, Carnissa Kilgore and Customer service supervisors, Rebeca Castro, and Tabitha Blackstock:</strong></p>\n<p><img class="wp-image-44093 alignleft" src="https://ww1.lendingpoint.com/wp-content/uploads/2019/03/customer-service-ladies-1024x447.jpg" alt="" width="800" height="349" srcset="https://www.lendingpoint.com/wp-content/uploads/2019/03/customer-service-ladies-1024x447.jpg 1024w, https://www.lendingpoint.com/wp-content/uploads/2019/03/customer-service-ladies-600x262.jpg 600w, https://www.lendingpoint.com/wp-content/uploads/2019/03/customer-service-ladies-1440x628.jpg 1440w, https://www.lendingpoint.com/wp-content/uploads/2019/03/customer-service-ladies-24x10.jpg 24w, https://www.lendingpoint.com/wp-content/uploads/2019/03/customer-service-ladies-36x16.jpg 36w, https://www.lendingpoint.com/wp-content/uploads/2019/03/customer-service-ladies-48x21.jpg 48w" sizes="(max-width: 800px) 100vw, 800px" /></p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>Carnissa Kilgore and, Rebeca Castro and Tabitha Blackstock make up the power house trio that leads our customer service team. Their top priority is to ensure that each and every customer that we touch is taken care of every step of the way. Whether it\u2019s answering a question about a customer\u2019s account or helping our customers get to the next step in our loan process, these ladies deserve to be recognized for their can-do attitudes and proficiency.</p>\n<p><strong>Christina Watts, ASA Supervisor:</strong></p>\n<p><img class="wp-image-44094 alignleft" src="https://ww1.lendingpoint.com/wp-content/uploads/2019/03/christina-watts.jpg" alt="" width="800" height="600" srcset="https://www.lendingpoint.com/wp-content/uploads/2019/03/christina-watts.jpg 640w, https://www.lendingpoint.com/wp-content/uploads/2019/03/christina-watts-600x450.jpg 600w, https://www.lendingpoint.com/wp-content/uploads/2019/03/christina-watts-24x18.jpg 24w, https://www.lendingpoint.com/wp-content/uploads/2019/03/christina-watts-36x27.jpg 36w, https://www.lendingpoint.com/wp-content/uploads/2019/03/christina-watts-48x36.jpg 48w" sizes="(max-width: 800px) 100vw, 800px" /></p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>As LendingPoint\u2019s ASA Supervisor, Chris makes sure that all applicants are handled quickly for the best customer experience. Her enthusiasm and passion for her people helps drive her team to be positive and efficient. We are proud to have such an inspirational woman leading our ASA team to success and beyond.</p>\n',
      protected: false,
    },
    excerpt: {
      rendered:
        '<p>We\u2019ve decided to highlight some of our women leaders who help make LendingPoint shine. Here\u2019s a list of some of our fintech LendingPoint Ladies.</p>\n',
      protected: false,
    },
    author: 9,
    featured_media: 44119,
    comment_status: 'closed',
    ping_status: 'closed',
    sticky: false,
    template: '',
    format: 'standard',
    meta: [],
    categories: [570],
    tags: [],
    yst_prominent_words: [
      3195,
      3211,
      3209,
      3224,
      3223,
      3241,
      3219,
      3194,
      1210,
      3218,
      3212,
      3199,
      3217,
      3210,
      3215,
      1215,
      3214,
      3206,
      3222,
      3221,
    ],
    _links: {
      self: [
        {
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/posts/44082',
        },
      ],
      collection: [
        {
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/posts',
        },
      ],
      about: [
        {
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/types/post',
        },
      ],
      author: [
        {
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/users/9',
        },
      ],
      replies: [
        {
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/comments?post=44082',
        },
      ],
      'version-history': [
        {
          count: 21,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/posts/44082/revisions',
        },
      ],
      'predecessor-version': [
        {
          id: 44139,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/posts/44082/revisions/44139',
        },
      ],
      'wp:featuredmedia': [
        {
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/media/44119',
        },
      ],
      'wp:attachment': [
        {
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/media?parent=44082',
        },
      ],
      'wp:term': [
        {
          taxonomy: 'category',
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/categories?post=44082',
        },
        {
          taxonomy: 'post_tag',
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/tags?post=44082',
        },
        {
          taxonomy: 'yst_prominent_words',
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/yst_prominent_words?post=44082',
        },
      ],
      curies: [
        {
          name: 'wp',
          href: 'https://api.w.org/{rel}',
          templated: true,
        },
      ],
    },
  },
  {
    id: 44037,
    date: '2019-03-11T14:43:15',
    date_gmt: '2019-03-11T18:43:15',
    guid: {
      rendered: 'https://ww1.lendingpoint.com/?p=44037',
    },
    modified: '2019-03-11T14:43:39',
    modified_gmt: '2019-03-11T18:43:39',
    slug:
      'rate-borrowers-using-loans-debt-management-drops-growing-proportion-borrowers-use-loans-new-purchases',
    status: 'publish',
    type: 'post',
    link:
      'https://www.lendingpoint.com/blog/rate-borrowers-using-loans-debt-management-drops-growing-proportion-borrowers-use-loans-new-purchases/',
    title: {
      rendered:
        'The rate of borrowers using loans for debt management drops; growing proportion of borrowers use loans for new purchases',
    },
    content: {
      rendered:
        '<p>[vc_row][vc_column][vc_row_inner][vc_column_inner width=&#8221;2/3&#8243;][vc_column_text css=&#8221;.vc_custom_1552323876513{margin-top: 50px !important;margin-bottom: 50px !important;}&#8221;] Most people who take out personal loans do so to help manage their pre-existing debt. That is true of all borrowers, no matter the lender involved. LendingPoint is no exception. The majority of our borrowers tell us they intend to use their loans to pay off outstanding balances with multiple creditors &#8212; student loans, credit cards, or short term, small dollar advances for example &#8212; and then repay the one loan in fixed monthly payments. It\u2019s a practice known as \u201cdebt consolidation.\u201d So, when our latest Data Lab analysis unearthed a shift in this long-standing trend, we took note. It turns out that, over the past two years, the proportion of our borrowers who say they are earmarking their loans for debt consolidation has decreased markedly, from about 60% in 2017 to about 54% in 2018. The percent using loans to pay for new merchandise or services has grown during those two years. Home improvement jumped from 6% to 8%; loans for medical expenses rose from 2% to 7%. This suggests there\u2019s a potential sea-change underway in how personal loans fit into people\u2019s budgets and financial lives. They\u2019re evolving from debt-management tools to credit sources for new purchases that may have previously been paid for by other means. Millennials are at the forefront of this shift. In 2017, the percent of millennial consolidators was about 61%. In 2018, that dropped a full 10%, down to 51%, a bigger decrease than any other age cohort. At that rate, non-consolidators will be the majority of our millennial borrowers this year. That would be a first. A few potential contributing factors:</p>\n<ul>\n<li>Credit cards may be less popular among all consumers, <strong>but particularly among millennials.</strong> That means other types of credit, including personal loans, fill the gap.</li>\n<li>Personal loans are more ubiquitous and readily available than ever, in large part because of the growth of FinTech lenders like LendingPoint over the past decade, making loans a viable option to credit cards for paying regular bills.</li>\n<li>Cultural shifts drive demand for certain types of big-ticket purchases, prompting people to seek out funding. This is true of the popularity of home improvement TV shows sparking people to take on their own home remodel projects, as American Banker <strong>recently reported</strong>, which contributes to the demand for the use of personal loans for that purpose.</li>\n</ul>\n<p>The following table summarizes the percent of loans extended for all use cases in 2017 and 2018, overall and by age. [/vc_column_text][vc_column_text css=&#8221;.vc_custom_1552325158091{margin-top: 35px !important;}&#8221;]</p>\n<div class="uk-overflow-container">\n<table class="uk-table uk-table-striped">\n<thead>\n<tr>\n<th>\u00a0</th>\n<th>2017</th>\n<th>2018</th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<td>&nbsp;</td>\n<td><strong>Percentage of reported loan usage by age cohort in 2017</strong></td>\n<td><strong>Percentage of reported loan usage by age cohort in 2018</strong></td>\n</tr>\n<tr>\n<td><strong>CONSOLIDATION</strong></td>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n</tr>\n<tr>\n<td>22-37</td>\n<td>61.3%</td>\n<td>51.2%</td>\n</tr>\n<tr>\n<td>22-37</td>\n<td>61.3%</td>\n<td>51.2%</td>\n</tr>\n<tr>\n<td>38-53</td>\n<td>62.5%</td>\n<td>57.2%%</td>\n</tr>\n<tr>\n<td>54-72</td>\n<td>55.8%</td>\n<td>54.5%</td>\n</tr>\n<tr>\n<td><strong>Overall</strong></td>\n<td>60.4%</td>\n<td>54.5%</td>\n</tr>\n<tr>\n<td><strong>GENERAL PURCHASE</strong></td>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n</tr>\n<tr>\n<td>22-37</td>\n<td>25.5%</td>\n<td>25.5%</td>\n</tr>\n<tr>\n<td>38-53</td>\n<td>24.8%</td>\n<td>23.2%</td>\n</tr>\n<tr>\n<td>54-72</td>\n<td>29.4%</td>\n<td>25.3%</td>\n</tr>\n<tr>\n<td><strong>Overall</strong></td>\n<td>26.2%</td>\n<td>24.5%</td>\n</tr>\n<tr>\n<td><strong>HOME IMPROVEMENT</strong></td>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n</tr>\n<tr>\n<td>22-37</td>\n<td>4.6%</td>\n<td>5.4%</td>\n</tr>\n<tr>\n<td>38-53</td>\n<td>6.2%</td>\n<td>8.3%</td>\n</tr>\n<tr>\n<td>38-53</td>\n<td>6.2%</td>\n<td>8.3%</td>\n</tr>\n<tr>\n<td>54-72</td>\n<td>7.9%</td>\n<td>10.6%</td>\n</tr>\n<tr>\n<td><strong>Overall</strong></td>\n<td>6.2%</td>\n<td>8.0%</td>\n</tr>\n<tr>\n<td><strong>MEDICAL</strong></td>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n</tr>\n<tr>\n<td>22-37L</td>\n<td>2.1%</td>\n<td>10.6%</td>\n</tr>\n<tr>\n<td>38-53</td>\n<td>2.0%</td>\n<td>5.9%</td>\n</tr>\n<tr>\n<td>54-72</td>\n<td>2.7%</td>\n<td>4.4%</td>\n</tr>\n<tr>\n<td><strong>Overall</strong></td>\n<td>2.2%</td>\n<td>7.0%</td>\n</tr>\n<tr>\n<td><strong>MOVING</strong></td>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n</tr>\n<tr>\n<td>22-37</td>\n<td>2.3%</td>\n<td>2.5%</td>\n</tr>\n<tr>\n<td>38-53</td>\n<td>1.4%</td>\n<td>1.3%</td>\n</tr>\n<tr>\n<td>54-72</td>\n<td>1.0%</td>\n<td>1.0%</td>\n</tr>\n<tr>\n<td><strong>Overall</strong></td>\n<td>1.6%</td>\n<td>1.6%</td>\n</tr>\n<tr>\n<td><strong>WEDDING</strong></td>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n</tr>\n<tr>\n<td>22-37</td>\n<td>1.8%</td>\n<td>1.5%</td>\n</tr>\n<tr>\n<td>38-53</td>\n<td>0.9%</td>\n<td>0.7%</td>\n</tr>\n<tr>\n<td>54-72</td>\n<td>0.8%</td>\n<td>0.9%</td>\n</tr>\n<tr>\n<td><strong>Overall</strong></td>\n<td>1.1%</td>\n<td>1.0%</td>\n</tr>\n<tr>\n<td><strong>VACATION</strong></td>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n</tr>\n<tr>\n<td>22-37</td>\n<td>1.0%</td>\n<td>1.3%</td>\n</tr>\n<tr>\n<td>38-53</td>\n<td>1.2%</td>\n<td>1.2%</td>\n</tr>\n<tr>\n<td>54-72</td>\n<td>1.1%</td>\n<td>1.1%</td>\n</tr>\n<tr>\n<td><strong>Overall</strong></td>\n<td>1.1%</td>\n<td>1.2%</td>\n</tr>\n<tr>\n<td><strong>AUTO</strong></td>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n</tr>\n<tr>\n<td>22-37</td>\n<td>0.9%</td>\n<td>1.5%</td>\n</tr>\n<tr>\n<td>38-53</td>\n<td>0.7%</td>\n<td>1.5%</td>\n</tr>\n<tr>\n<td>54-72</td>\n<td>0.8%</td>\n<td>1.6%</td>\n</tr>\n<tr>\n<td><strong>Overall</strong></td>\n<td>0.8%</td>\n<td>1.5%</td>\n</tr>\n</tbody>\n</table>\n</div>\n<p style="text-align: center;"><strong>*did not include taxes and funerals, both of which are less than 1% of total loans in each year</strong></p>\n<h3>About LendingPoint Data Lab:</h3>\n<p>The LendingPoint Data Lab analyzes data, from both LendingPoint\u2019s own business and from third party sources, to unearth trends and insights into Americans\u2019 borrowing behavior.</p>\n<p>For this study, the LendingPoint Data Lab analyzed the loan applications of individuals to whom the company extended personal loans in 2017 and 2018. LendingPoint borrowers provide the intended use of the loan when they apply, from a menu of options LendingPoint provides. LendingPoint categorizes those into eleven use cases.</p>\n<p>An individual\u2019s age factors neither into their creditworthiness nor LendingPoint\u2019s decision to extend a loan to them.</p>\n<p>[/vc_column_text][/vc_column_inner][vc_column_inner width=&#8221;1/3&#8243;][vc_single_image image=&#8221;44043&#8243; img_size=&#8221;full&#8221; alignment=&#8221;center&#8221;][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row][vc_row][vc_column][/vc_column][/vc_row]</p>\n',
      protected: false,
    },
    excerpt: {
      rendered:
        '<p>Most people who take out personal loans do so to help manage their pre-existing debt. That is true of all borrowers, no matter the lender involved. LendingPoint is no exception. </p>\n',
      protected: false,
    },
    author: 2,
    featured_media: 44051,
    comment_status: 'closed',
    ping_status: 'closed',
    sticky: false,
    template: '',
    format: 'standard',
    meta: [],
    categories: [2280],
    tags: [],
    yst_prominent_words: [
      2640,
      3184,
      3176,
      753,
      662,
      674,
      672,
      660,
      3188,
      3187,
      666,
      3178,
      3179,
      3174,
      3173,
      676,
      3175,
      3186,
      3190,
      3189,
    ],
    _links: {
      self: [
        {
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/posts/44037',
        },
      ],
      collection: [
        {
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/posts',
        },
      ],
      about: [
        {
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/types/post',
        },
      ],
      author: [
        {
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/users/2',
        },
      ],
      replies: [
        {
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/comments?post=44037',
        },
      ],
      'version-history': [
        {
          count: 14,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/posts/44037/revisions',
        },
      ],
      'predecessor-version': [
        {
          id: 44054,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/posts/44037/revisions/44054',
        },
      ],
      'wp:featuredmedia': [
        {
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/media/44051',
        },
      ],
      'wp:attachment': [
        {
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/media?parent=44037',
        },
      ],
      'wp:term': [
        {
          taxonomy: 'category',
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/categories?post=44037',
        },
        {
          taxonomy: 'post_tag',
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/tags?post=44037',
        },
        {
          taxonomy: 'yst_prominent_words',
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/yst_prominent_words?post=44037',
        },
      ],
      curies: [
        {
          name: 'wp',
          href: 'https://api.w.org/{rel}',
          templated: true,
        },
      ],
    },
  },
  {
    id: 43992,
    date: '2019-02-21T12:03:11',
    date_gmt: '2019-02-21T16:03:11',
    guid: {
      rendered: 'https://ww1.lendingpoint.com/?p=43992',
    },
    modified: '2019-02-26T12:29:41',
    modified_gmt: '2019-02-26T16:29:41',
    slug: 'lendingpoint-will-hire-100-new-team-members-atlanta-headquarters',
    status: 'publish',
    type: 'post',
    link:
      'https://www.lendingpoint.com/blog/lendingpoint-will-hire-100-new-team-members-atlanta-headquarters/',
    title: {
      rendered:
        'LendingPoint will hire 100 new team members in atlanta headquarters, increasing local headcount to nearly 300',
    },
    content: {
      rendered:
        '<h2>\u00a0</h2>\n<h2 style="text-align: justify;"><span style="font-weight: 400;">Staffing expansion comes after online lender posted its most successful year to date</span></h2>\n<p><span style="font-weight: 400;">KENNESAW, Ga.\u2014February 21, 2019 &#8212; Coming off a year of its most significant growth to date, </span><a href="http://www.lendingpoint.com"><span style="font-weight: 400;">LendingPoint</span></a><span style="font-weight: 400;">, the consumer lending platform, announced it will add 100 new fintech jobs to its Atlanta metro headquarters, growing its staff here from about 190 to nearly 300 this year.</span></p>\n<p><span style="font-weight: 400;">The new hires will fill entry-level to executive positions, across a range of departments including risk and fraud management, engineering, customer service, and marketing. \u00a0</span></p>\n<p><span style="font-weight: 400;">\u201cWe\u2019re absolutely thrilled that our success these past years means we can create so many new jobs for talented individuals in Atlanta,\u201d said Mark Lorimer, LendingPoint\u2019s Chief Communications and Public Affairs Officer. \u00a0\u201c</span><span style="font-weight: 400;">We considered hiring in several other cities, but decided the depth of the labor pool for fintech right here made it the best choice for us.\u201d</span></p>\n<p><span style="font-weight: 400;">LendingPoint secured more than $1.2 billion in financing between August 2017 and this month, including two senior credit facilities arranged by Guggenheim Securities and two mezzanine financing deals led by Paragon Outcomes Management LLC. \u00a0Last March, it debuted Merchant Solutions, its point-of-sale lending platform that thousands of retailers and service providers now use to provide financing to customers at the moment of purchase. </span></p>\n<p><span style="font-weight: 400;">In 2018, LendingPoint funded about 51,000 loans, more than it did in the previous three years combined. Last year, The Atlanta Business Chronicle named LendingPoint the metro\u2019s </span><a href="https://www.bizjournals.com/atlanta/news/2018/04/27/2018-pacesetter-awards-lendingpoint-is-the-fastest.html" target="_blank" rel="noopener"><span style="font-weight: 400;">fastest-growing private company</span></a><span style="font-weight: 400;">. </span></p>\n<p><span style="font-weight: 400;">Since launching in 2015, LendingPoint has provided affordable loans to individuals across the credit spectrum, from 580 to 850. Its unique credit assessment capabilities mean its loans are tailored to borrowers\u2019 specific financial situations, \u00a0ensuring borrowers are able to repay successfully. All of the company\u2019s loans are unsecured personal loans with 12- to 60-month terms, from $500 to $25,000 with APR\u2019s ranging from 9.99% to 35.99%</span></p>\n<p><span style="font-weight: 400;">In addition to its Atlanta metro headquarters, LendingPoint maintains offices in New York City and San Diego.</span></p>\n<h2>About LendingPoint</h2>\n<p><a href="https://www.lendingpoint.com/"><span style="font-weight: 400;">LendingPoint</span></a><span style="font-weight: 400;">\u00a0is a FinTech lending platform providing direct to consumer loans as well as product and service financing at the point of sale. The company is creating a better lending and borrowing experience by looking for more reasons to say \u201cyes\u201d &#8212; helping consumers across the credit spectrum unlock access to affordable loans and live better financial lives. Its\u00a0</span><a href="https://lendingpointmerchantsolutions.com" target="_blank" rel="noopener"><span style="font-weight: 400;">LendingPoint Merchant Solutions</span></a><span style="font-weight: 400;"> platform provides merchants, service providers and medical institutions a fully integrated, one-stop retail financing in-a-box solution to convert more customers at the point of sale. \u00a0</span></p>\n<p><span style="font-weight: 400;">LendingPoint was\u00a0named in 2018 one of the nation&#8217;s best consumer loan companies by\u00a0</span><i><span style="font-weight: 400;">U.S. News &amp; World Report, </span></i><span style="font-weight: 400;">one of the Best Personal Loans by NerdWallet, the fastest-growing private company in metro Atlanta by the</span><i><span style="font-weight: 400;"> Atlanta Business Chronicle</span></i><span style="font-weight: 400;">, one of the GA Fast 40 by the Atlanta Chapter of The ACG, and one of the Top Workplaces by the Atlanta Journal Constitution. \u00a0Founded in 2014, LendingPoint is a privately held company headquartered in Kennesaw, Ga. with offices in New York City and San Diego. \u00a0For more information, visit www.lendingpoint.com.</span></p>\n<p><span style="font-weight: 400;">FICO is a trademark of Fair Isaacs Corporation. </span></p>\n',
      protected: false,
    },
    excerpt: {
      rendered:
        '<p>Staffing expansion comes after online lender posted its most successful year to date</p>\n',
      protected: false,
    },
    author: 2,
    featured_media: 4114,
    comment_status: 'closed',
    ping_status: 'closed',
    sticky: false,
    template: '',
    format: 'standard',
    meta: [],
    categories: [31],
    tags: [],
    yst_prominent_words: [
      3155,
      3147,
      3152,
      3146,
      3151,
      3145,
      3148,
      995,
      2982,
      678,
      1712,
      3153,
      3150,
      3143,
      3149,
      3142,
      2986,
      3154,
      3144,
      3141,
    ],
    _links: {
      self: [
        {
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/posts/43992',
        },
      ],
      collection: [
        {
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/posts',
        },
      ],
      about: [
        {
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/types/post',
        },
      ],
      author: [
        {
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/users/2',
        },
      ],
      replies: [
        {
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/comments?post=43992',
        },
      ],
      'version-history': [
        {
          count: 8,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/posts/43992/revisions',
        },
      ],
      'predecessor-version': [
        {
          id: 44024,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/posts/43992/revisions/44024',
        },
      ],
      'wp:featuredmedia': [
        {
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/media/4114',
        },
      ],
      'wp:attachment': [
        {
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/media?parent=43992',
        },
      ],
      'wp:term': [
        {
          taxonomy: 'category',
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/categories?post=43992',
        },
        {
          taxonomy: 'post_tag',
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/tags?post=43992',
        },
        {
          taxonomy: 'yst_prominent_words',
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/yst_prominent_words?post=43992',
        },
      ],
      curies: [
        {
          name: 'wp',
          href: 'https://api.w.org/{rel}',
          templated: true,
        },
      ],
    },
  },
  {
    id: 43980,
    date: '2019-02-15T15:49:35',
    date_gmt: '2019-02-15T19:49:35',
    guid: {
      rendered: 'https://ww1.lendingpoint.com/?p=43980',
    },
    modified: '2019-04-23T08:18:19',
    modified_gmt: '2019-04-23T12:18:19',
    slug: 'credit-card-spring-cleaning-time-cut-credit-card',
    status: 'publish',
    type: 'post',
    link: 'https://www.lendingpoint.com/blog/credit-card-spring-cleaning-time-cut-credit-card/',
    title: {
      rendered: 'Credit card spring cleaning: Is it time to cut the credit card?',
    },
    content: {
      rendered:
        '<p><em>Take advantage of spring cleaning and start decluttering your debt!\u00a0</em></p>\n<p>If credit card debt keeps you up at night, you are not alone. In 2018, the average American had a <a href="https://www.cnbc.com/2018/01/23/credit-card-debt-hits-record-high.html">credit card balance of $6,375.</a>\u00a0That&#8217;s up nearly 3 percent from the year before. Additionally, the 2017\u00a0<a href="https://www.experian.com/blogs/ask-experian/state-of-credit/">Experian State of Credit</a>\u00a0stated that:</p>\n<ul>\n<li>Minnesota has the highest credit score while Mississippi has the lowest</li>\n<li>Gen X have the most credit card debt while Generation Z has the lowest credit score</li>\n<li>Credit card debt has hit a record high</li>\n<li>Millennials are getting into mortgage debt</li>\n</ul>\n<p>The mental clutter of debt can be overwhelming. Consider decluttering your debt while you&#8217;re spring cleaning and decluttering your home. Here&#8217;s how that could help you move forward financially:</p>\n<h2>Stop Using Your Cards</h2>\n<p>If you\u2019re dieting by cutting down on carbs and sugars, would you keep cake and candy at your house? The same concept applies to credit card debt. Even though it sounds like an obvious benefit to cutting your credit card, not having the plastic at your fingertips prevents you from increasing your debt and digging a bigger debt hole.</p>\n<h2>Stop Paying High Interest Rates</h2>\n<p>The more you use your credit card the more interest you\u2019ll pay, bringing your overall debt up and making it harder to pay it off. If you owe $6,000 on your credit card with a 14% interest rate and a plan to pay off that debt in 18 months, you\u2019ll end up paying $994 in interest. Use this <a href="https://www.bankrate.com/calculators/credit-cards/credit-card-payoff-calculator.aspx">credit card payoff calculator</a> from <a href="https://www.bankrate.com/calculators/credit-cards/credit-card-payoff-calculator.aspx">Bankrate.com</a> to see how much you\u2019ll end up paying if you keep using your card. Remember, if you have a credit card without a fixed interest rate, <a href="https://www.lendingpoint.com/blog/why-you-dont/">paying off becomes even harder</a>.</p>\n<h2>Develop a Realistic Budget</h2>\n<p>Getting rid of your credit card will force you to develop a budget because cash and/or your debit card will be the only way to pay for the things you need and want. Even though it might be hard at first (it will be), you will benefit long term and you&#8217;ll joyfully watch your credit card debt decrease. <a href="https://www.lendingpoint.com/blog/personal-budgeting-whats-income-come/">Click here</a> to learn more about personal budgeting with LendingPoint.</p>\n<h2>Improve Your Credit Score</h2>\n<p>Did you know that credit card debt accounts for about 30% of your credit score? \u00a0<a href="https://www.thebalance.com/balance-affects-credit-score-960446">According to The Balance</a>, \u201cThe level of debt, the second biggest factor that affects your credit score, is referred to as your credit utilization, which is your credit card balances compared to your credit limits. A lower credit utilization is better because it demonstrates you can responsibly use credit and that you haven&#8217;t overextended yourself with high credit card balances. Lower credit card balances compared to your credit card limits will reward you with higher credit scores. The opposite is also true. Higher credit card balances will lower your credit score.\u201d</p>\n<p>If you&#8217;re able to reduce the amount of outstanding credit card debt you owe instead of increasing it every month, your credit score will eventually increase. Decreasing your debt-to-income ratio while increasing your available credit reflects favorably on your credit report.\u00a0\u00a0<a href="https://www.thebalance.com/balance-affects-credit-score-960446">Click here</a> to learn more about how your credit card balance affects your credit score.</p>\n<h2>Refinance with a Personal Loan</h2>\n<p>You may want to jump start your debt decluttering by utilizing a personal loan which will provide you with one monthly payment, a fixed interest rate, and a final payoff date. LendingPoint has loaned more than $1 billion to customers looking for a better way to manage and pay off their debt.\u00a0</p>\n<p>Even though <a href="https://www.lendingpoint.com/blog/credit-card-refinancing-pros-cons/">there are pros and cons to credit card refinancing</a>, you should always explore your options and make sure you avoid missing payments, which could further hurt your credit score.</p>\n<div class="content_block" id="custom_post_widget-5212"><p>\u2014</p>\n<p><i>LendingPoint is a personal loan provider specializing in NearPrime consumers. Typically, NearPrime consumers\u00a0are people with credit scores in the 600s. If this is you, we\u2019d love to talk to you about how we might be able to help you meet your financial goals. We offer\u00a0loans from $2,000 to $25,000, all with fixed payments and simple interest.</i></p>\n</div>\n',
      protected: false,
    },
    excerpt: {
      rendered: '<p>Take advantage of Spring cleaning to start getting out of debt!</p>\n',
      protected: false,
    },
    author: 9,
    featured_media: 44061,
    comment_status: 'closed',
    ping_status: 'closed',
    sticky: false,
    template: '',
    format: 'standard',
    meta: [],
    categories: [2045, 10],
    tags: [],
    yst_prominent_words: [
      3139,
      3132,
      3131,
      3134,
      3133,
      1693,
      3135,
      3130,
      3128,
      3129,
      1683,
      694,
      687,
      3245,
      3138,
      3250,
      2191,
      692,
      1327,
      3243,
    ],
    _links: {
      self: [
        {
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/posts/43980',
        },
      ],
      collection: [
        {
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/posts',
        },
      ],
      about: [
        {
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/types/post',
        },
      ],
      author: [
        {
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/users/9',
        },
      ],
      replies: [
        {
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/comments?post=43980',
        },
      ],
      'version-history': [
        {
          count: 5,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/posts/43980/revisions',
        },
      ],
      'predecessor-version': [
        {
          id: 44165,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/posts/43980/revisions/44165',
        },
      ],
      'wp:featuredmedia': [
        {
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/media/44061',
        },
      ],
      'wp:attachment': [
        {
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/media?parent=43980',
        },
      ],
      'wp:term': [
        {
          taxonomy: 'category',
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/categories?post=43980',
        },
        {
          taxonomy: 'post_tag',
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/tags?post=43980',
        },
        {
          taxonomy: 'yst_prominent_words',
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/yst_prominent_words?post=43980',
        },
      ],
      curies: [
        {
          name: 'wp',
          href: 'https://api.w.org/{rel}',
          templated: true,
        },
      ],
    },
  },
  {
    id: 43978,
    date: '2019-02-15T15:45:05',
    date_gmt: '2019-02-15T19:45:05',
    guid: {
      rendered: 'https://ww1.lendingpoint.com/?p=43978',
    },
    modified: '2019-04-23T08:17:38',
    modified_gmt: '2019-04-23T12:17:38',
    slug: 'wage-garnishment-avoid',
    status: 'publish',
    type: 'post',
    link: 'https://www.lendingpoint.com/blog/wage-garnishment-avoid/',
    title: {
      rendered: 'Wage garnishment: What is it and how to avoid it?',
    },
    content: {
      rendered:
        '<p><em>Stay on top of your finances to avoid this worst-case scenario.</em></p>\n<p>Finding out that you\u2019re in an excessive amount of debt that you can\u2019t afford to pay off can be disheartening. However, finding out that part of your wages are being withheld from you and going straight to the creditor or person you owe money to, can be even harder. That\u2019s what wage garnishment is.</p>\n<p><a href="https://www.dol.gov/general/topic/wages/garnishments">According to the U.S. Department of Labor</a>, \u201cWage garnishment is a legal procedure in which a person\u2019s earnings are required by court order to be withheld by an employer for the payment of a debt such as child support. <a href="https://www.dol.gov/whd/regs/statutes/garn01.pdf">Title III of the Consumer Credit Protection Act (CCPA)</a> prohibits an employer from discharging an employee whose earnings have been subject to garnishment for any one debt, regardless of the number of levies made or proceedings brought to collect it.\u201d</p>\n<p>Here are the most common questions we get regarding wage garnishment and how can it be avoided:</p>\n<p><strong>Question: Why does wage garnishment happen?</strong><br />\n<strong>Answer:</strong><a href="https://www.nerdwallet.com/blog/finance/wage-garnishment/"> According to this nerdwallet.com article</a>, garnishment often happens when a creditor sues you for nonpayment of a debt and wins in court. Sometimes, though, a creditor can force garnishment without a court order, for instance, if you owe child support, back taxes or a balance on federal student loans.</p>\n<p><strong>Q: How common is wage garnishment?</strong><br />\n<strong>A:</strong> <a href="https://www.adp.com/tools-and-resources/adp-research-institute/insights/~/media/RI/pdf/Garnishment-whitepaper.ashx">This report by Automatic Data Processing (ADP)</a> Inc. shows that, unfortunately, wage garnishments are more common than you might think. The ADP found that 7.2% of the 13 million employees it assessed had wages garnished in 2013. For workers ages 35 to 44, the number hit 10.5%. The top reasons? Child support; consumer debts and student loans; and tax liens.</p>\n<p><strong>Q: What are the two types of garnishment?</strong><br />\n<strong>A:</strong> There are two types of garnishment: wage and non-wage. When it comes to wage garnishment, creditors can legally require your employer to hand over part of your earnings to pay off your debts. In non-wage garnishment, commonly referred to as a bank levy, creditors can tap into your bank account.</p>\n<p><strong>Q: When does the garnishment end?</strong><br />\n<strong>A:</strong> Once you receive legal notice, the court will send notices to you and your bank or employer, and the garnishment will begin in five to 30 business days, depending on your creditor and state. The garnishment continues until the debt, potentially including court fees and interest, is paid.</p>\n<p>If you have the option, you can borrow money from someone you know or take out a personal loan and pay off the entire sum at once. <a href="https://www.lendingpoint.com/blog/pre-qualify-personal-loan/">Click here</a> to see how to pre-qualify for a personal loan.</p>\n<p><strong>Q: How much of my wages can be garnished?</strong><br />\n<strong>A:</strong> If a judgment creditor is garnishing your wages, federal law provides that it can take no more than 25% of your disposable income, or the amount that your income exceeds 30 times the federal minimum wage, whichever is less. <a href="https://www.alllaw.com/articles/nolo/bankruptcy/wage-garnishment-amount.html">Click here</a> for more on federal and state laws that limit this amount.</p>\n<p><strong>Q: What if I don\u2019t truly own the debt?</strong><br />\n<strong>A:</strong> If you think the legal notice has inaccurate information or you don\u2019t truly own this debt, <a href="https://www.nolo.com/legal-encyclopedia/how-object-wage-garnishment.html">you can file a dispute</a>.</p>\n<p><strong>Q: Are there rights that protect me from this?</strong><br />\n<strong>A:</strong> Yes.<a href="https://www.dol.gov/whd/regs/compliance/whdfs30.pdf"> Here are the Department of Labor laws</a> that limit how much can be taken from your paycheck and clearly defines concepts for you to better understand the implications of having your wages garnished.</p>\n<p><strong>Q: Can LendingPoint help me prevent this?</strong><br />\n<strong>A:</strong> Yes! At LendingPoint we\u2019re passionate about helping you become financially stronger. A personal loan may allow you to payoff your debt before a wage garnishment situation occurs.\u00a0</p>\n<p><strong>Q: How do I move forward?</strong><br />\n<strong>A:</strong> Having your wages garnished is not the end of the world, but there are many steps you can take before even getting to this point. If you know you\u2019re behind on debts, are struggling to get out of the paycheck to paycheck rut, or have special circumstances that prevent you from making your monthly payments, contact us and talk to a Loan Specialist who can help.</p>\n<div class="content_block" id="custom_post_widget-5212"><p>\u2014</p>\n<p><i>LendingPoint is a personal loan provider specializing in NearPrime consumers. Typically, NearPrime consumers\u00a0are people with credit scores in the 600s. If this is you, we\u2019d love to talk to you about how we might be able to help you meet your financial goals. We offer\u00a0loans from $2,000 to $25,000, all with fixed payments and simple interest.</i></p>\n</div>\n',
      protected: false,
    },
    excerpt: {
      rendered: '<p>Stay on top of your finances to avoid this worst-case scenario.</p>\n',
      protected: false,
    },
    author: 9,
    featured_media: 44059,
    comment_status: 'closed',
    ping_status: 'closed',
    sticky: false,
    template: '',
    format: 'standard',
    meta: [],
    categories: [10, 634],
    tags: [],
    yst_prominent_words: [
      1024,
      3126,
      3121,
      3125,
      687,
      3117,
      3114,
      3123,
      3115,
      3119,
      3122,
      684,
      697,
      692,
      1688,
      3118,
      3120,
      3113,
      3124,
      3116,
    ],
    _links: {
      self: [
        {
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/posts/43978',
        },
      ],
      collection: [
        {
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/posts',
        },
      ],
      about: [
        {
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/types/post',
        },
      ],
      author: [
        {
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/users/9',
        },
      ],
      replies: [
        {
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/comments?post=43978',
        },
      ],
      'version-history': [
        {
          count: 7,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/posts/43978/revisions',
        },
      ],
      'predecessor-version': [
        {
          id: 44167,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/posts/43978/revisions/44167',
        },
      ],
      'wp:featuredmedia': [
        {
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/media/44059',
        },
      ],
      'wp:attachment': [
        {
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/media?parent=43978',
        },
      ],
      'wp:term': [
        {
          taxonomy: 'category',
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/categories?post=43978',
        },
        {
          taxonomy: 'post_tag',
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/tags?post=43978',
        },
        {
          taxonomy: 'yst_prominent_words',
          embeddable: true,
          href: 'https://www.lendingpoint.com/wp-json/wp/v2/yst_prominent_words?post=43978',
        },
      ],
      curies: [
        {
          name: 'wp',
          href: 'https://api.w.org/{rel}',
          templated: true,
        },
      ],
    },
  },
];
