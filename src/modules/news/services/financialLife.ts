import { wordpress } from '../../../services';
import { ApiResponse } from '../../../services/utils';
import { handleError } from '../../../utils/services';
import { ArticleType, fixUnicodeCharsInTitle } from '../utils';

const {
  api,
  constants: { paths },
} = wordpress;

export const getPosts = async (params: {
  page: number;
  per_page: number;
}): Promise<ApiResponse<{ total: number; data: ArticleType[] }>> => {
  try {
    const response = await api.get(paths.financialLife, { params });
    const parsedData = fixUnicodeCharsInTitle(response.data);
    return {
      success: true,
      response: {
        data: parsedData,
        total: response.headers['x-wp-totalpages'],
      },
    };
  } catch (err) {
    return handleError(err);
  }
};
