import React from 'react';
import { SafeAreaView, ScrollView, View, Image } from 'react-native';
import { ThemedComponentProps, withStyles } from 'react-native-ui-kitten';
import { NavigationTabScreenProps } from 'react-navigation-tabs';

// Modules
import shared from '../../shared';

// Components
import { FinancialList } from '../containers/index';

const { HeaderView } = shared.components;

type FinancialLifeProps = {} & NavigationTabScreenProps & ThemedComponentProps;

const FinancialLife = ({ themedStyle }: FinancialLifeProps) => {
  const { width } = shared.helpers.hooks.useScreenDimensions();

  return (
    <SafeAreaView style={themedStyle.screen} {...shared.helpers.setTestID('FinancialLifeScreen')}>
      <ScrollView contentContainerStyle={themedStyle.body}>
        <HeaderView title="Financial Life" subTitle="Learn about personal finances" />
        <View>
          <View style={themedStyle.container}>
            <Image style={{ width }} source={require('../../../img/news_background.png')} />
          </View>
        </View>
        <FinancialList />
      </ScrollView>
    </SafeAreaView>
  );
};

export default withStyles(FinancialLife, () => ({
  centerText: {
    textAlign: 'center',
  },
  uppercase: {
    textTransform: 'uppercase',
  },
  screen: {
    flex: 1,
  },
  body: {
    flexGrow: 1,
  },
  container: {
    position: 'absolute',
    marginTop: 10,
  },
}));
