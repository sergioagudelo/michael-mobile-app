import React from 'react';
import { SafeAreaView, ScrollView, View, Image, TouchableOpacity } from 'react-native';
import { ThemedComponentProps, withStyles, Text, Layout } from 'react-native-ui-kitten';
import { withNavigation } from 'react-navigation';
import { NavigationStackScreenProps } from 'react-navigation-stack';
import HTMLView from 'react-native-htmlview';
import color from 'color';

// Types
import { ArticleType } from '../utils';

// Modules
import shared from '../../shared';
import { Styles } from '../helpers';

// Components
const { Format } = shared.components;

// Helpers
const { Ico } = shared.helpers;

type NewsDetailsProps = ThemedComponentProps & NavigationStackScreenProps<{ article: ArticleType }>;

/**
 * Render News Element
 */
const NewsDetailsElement = ({ themedStyle, navigation }: NewsDetailsProps) => {
  const { width } = shared.helpers.hooks.useScreenDimensions('screen');

  const article = navigation.getParam('article');

  return (
    <>
      <SafeAreaView style={themedStyle.topSafeArea} />
      <SafeAreaView style={[themedStyle.flex, themedStyle.bg]}>
        <View style={[themedStyle.flex]}>
          <Image
            resizeMode="cover"
            style={[themedStyle.backImg, { width }]}
            source={require('../../../img/news_details_background.png')}
          />
          <View style={[themedStyle.flex, themedStyle.close]}>
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <Ico name="close-circle-outline" width={60} height={60} />
            </TouchableOpacity>
          </View>
          <Layout style={[themedStyle.container]}>
            <ScrollView style={themedStyle.flex} showsVerticalScrollIndicator={false}>
              <Layout style={[themedStyle.flex]}>
                <View style={[themedStyle.flex]}>
                  <Text category="h3" status="accent" style={themedStyle.title}>
                    {article.title.rendered}
                  </Text>
                </View>
                <View style={[themedStyle.flex]}>
                  <Format type="date" category="label" appearance="hint" style={themedStyle.title}>
                    {article.date}
                  </Format>
                </View>
                <View style={[themedStyle.flex]}>
                  <HTMLView addLineBreaks value={article.content.rendered} />
                </View>
              </Layout>
            </ScrollView>
          </Layout>
        </View>
      </SafeAreaView>
    </>
  );
};

/**
 * apply theme styles to News
 * @see https://akveo.github.io/react-native-ui-kitten/docs/components/withstyles/overview#withstyles
 */
const NewsDetails = withStyles(withNavigation(NewsDetailsElement), theme => ({
  ...Styles,
  topSafeArea: {
    flex: 0,
    backgroundColor: theme['color-basic-100'],
  },
  close: {
    flex: 0.2,
    justifyContent: 'center',
    alignItems: 'flex-end',
    marginRight: 20,
  },
  bg: {
    flex: 1,
    with: '100%',
    backgroundColor: color(theme['color-basic-1100'])
      .alpha(0.7)
      .rgb()
      .string(),
    justifyContent: 'center',
  },
  backImg: {
    position: 'absolute',
    top: 0,
  },
  container: {
    flex: 0.8,
    backgroundColor: theme['color-basic-100'],
    marginHorizontal: 30,
    marginBottom: 30,
    with: '90%',
    padding: 30,
  },
}));

export default NewsDetails;
