import React from 'react';
import { View, TouchableHighlight } from 'react-native';
import { ThemedComponentProps, withStyles, Text, Button, Layout } from 'react-native-ui-kitten';
import HTMLView from 'react-native-htmlview';
import { truncate } from 'lodash';

// Types
import { ArticleType } from '../utils';

// Modules
import shared from '../../shared';

type NewsItemProps = {
  article: ArticleType;
  onPress: () => void;
} & ThemedComponentProps;

const NewsItem = ({ article, onPress, themedStyle }: NewsItemProps) => {
  return (
    <View style={themedStyle.cardContainer}>
      <TouchableHighlight onPress={onPress}>
        <Layout style={themedStyle.card}>
          <HTMLView
            addLineBreaks={false}
            value={article.title.rendered}
            TextComponent={Text}
            textComponentProps={{
              category: 'h3',
              status: 'accent',
              numberOfLines: 2,
            }}
          />
          <View style={themedStyle.articleContent}>
            <HTMLView
              value={truncate(article.excerpt.rendered, { length: 144, separator: ' ' })}
              TextComponent={Text}
            />
          </View>
          <View style={themedStyle.articleFooter}>
            <Button onPress={onPress} appearance="link" size="small">
              LEARN MORE
            </Button>
          </View>
        </Layout>
      </TouchableHighlight>
    </View>
  );
};

export default withStyles(NewsItem, theme => ({
  cardContainer: {
    marginVertical: 10,
    borderColor: theme['color-basic-400'],
    borderWidth: 1,
    elevation: 2,
    ...shared.helpers.shadow(2),
  },
  card: {
    padding: 20,
  },
  articleContent: {
    marginVertical: 10,
  },
  articleFooter: {
    alignSelf: 'center',
  },
}));
