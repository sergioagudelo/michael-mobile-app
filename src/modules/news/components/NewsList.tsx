import React from 'react';
import { View, ListRenderItemInfo } from 'react-native';
import { ThemedComponentProps, withStyles, List, Button, Text } from 'react-native-ui-kitten';
import * as Animatable from 'react-native-animatable';

// Modules
import shared from '../../shared';

// Types
import { ArticleType } from '../utils';

// Components
import NewsItem from './NewsItem';

const { Loading } = shared.components;

/**
 * component type
 */
type NewsListProps = {
  isLoading: boolean;
  isCompleted: boolean;
  news: ArticleType[] | undefined;
  onPress: (article: ArticleType) => void;
  onLoadMore: () => void;
} & ThemedComponentProps;

/**
 * render news title and excerpt
 */
const NewsList = ({
  isLoading,
  isCompleted,
  news,
  onPress,
  onLoadMore,
  themedStyle,
}: NewsListProps) => {
  const renderItem = ({ item, index, ...rest }: ListRenderItemInfo<ArticleType>) => {
    return (
      <Animatable.View
        animation="fadeInDown"
        duration={300}
        delay={(index && index * 150) || 150}
        useNativeDriver
        style={index === 0 ? themedStyle.featured : null}
      >
        <NewsItem article={item} onPress={() => onPress(item)} {...rest} />
      </Animatable.View>
    );
  };

  const emptyComponent = () => {
    return (
      <>
        {news !== undefined && (
          <Text style={{ marginTop: 50 }}>You don't have any financial life recommendation</Text>
        )}
      </>
    );
  };

  return (
    <View style={themedStyle.list}>
      <List
        data={news??null}
        renderItem={renderItem}
        showsVerticalScrollIndicator={false}
        ListEmptyComponent={emptyComponent}
      />

      {isLoading && <Loading isLoading />}
      {!isLoading && !isCompleted && (
        <View style={themedStyle.listFooter}>
          <Button
            onPress={onLoadMore}
            appearance="outline"
            status="basic"
            textStyle={themedStyle.uppercase}
          >
            LOAD MORE
          </Button>
        </View>
      )}
    </View>
  );
};

export default withStyles(NewsList, () => ({
  list: {
    marginHorizontal: 30,
  },
  featuredTitle: {
    marginTop: 40,
  },
  featured: {
    marginTop: 40,
  },
  listFooter: {
    alignSelf: 'center',
  },
  uppercase: {
    textTransform: 'uppercase',
  },
}));
