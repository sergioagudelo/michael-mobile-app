import React, { useState, useEffect, useCallback } from 'react';
import { compose } from 'redux';
import { withNavigation } from 'react-navigation';
import { NavigationStackProp } from 'react-navigation-stack';
import { ArticleType } from '../utils';

// Services
import { getPosts } from '../services/financialLife';

// Components
import { NewsList } from '../components';

type FinancialListConnectProps = { navigation: NavigationStackProp };
const FinancialListContainer = ({ navigation }: FinancialListConnectProps) => {
  const [newsList, setNewsList] = useState<ArticleType[] | undefined>();
  const [totalPages, setTotalPages] = useState(0);
  const [apiOptions, setApiOptions] = useState({ page: 1, per_page: 5 });
  const [failedResponse, setFailedResponse] = useState(false);
  const [isLoading, setIsLoading] = React.useState(false);
  const [isCompleted, setIsCompleted] = React.useState(false);

  // Get FinancialLife posts
  const getData = useCallback(async () => {
    setIsLoading(true);
    const result = await getPosts(apiOptions);
    if (result.success && result.response) {
      const { total, data } = result.response;
      setTotalPages(total);
      setNewsList([...(newsList || []), ...data]);
      setIsLoading(false);
      return;
    }

    setNewsList([]);
    setFailedResponse(true);
    setIsLoading(false);
    setIsCompleted(true);
  }, [apiOptions, newsList]);

  useEffect(() => {
    setIsCompleted(apiOptions.page >= totalPages);
    if (newsList === undefined && !isLoading && !failedResponse) {
      getData();
    }
  }, [failedResponse, isLoading, newsList, getData, apiOptions.page, totalPages]);

  if (failedResponse) {
    // navigation.navigate // TODO: General error
  }

  // Load more posts event
  const onLoadMore = () => {
    if (apiOptions.page >= totalPages) {
      return;
    }
    setApiOptions({ ...apiOptions, page: apiOptions.page += 1 });
    getData();
  };

  // choose post event
  const onPress = (article: ArticleType) => {
    navigation.navigate('NewsDetails', { article });
  };

  return (
    <NewsList
      isLoading={isLoading}
      news={newsList}
      onPress={onPress}
      onLoadMore={onLoadMore}
      isCompleted={isCompleted}
    />
  );
};

export default compose(withNavigation)(FinancialListContainer);
