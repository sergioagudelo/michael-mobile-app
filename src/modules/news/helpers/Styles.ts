import { ViewStyle, TextStyle } from 'react-native';

/**
 * common styles for profile module
 */
const Styles: { [key: string]: ViewStyle | TextStyle } = {
  flex: {
    flex: 1,
  },
  flexRow: {
    flex: 1,
    flexDirection: 'row',
  },
  itemsCenter: {
    alignItems: 'center',
  },
  center: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  textRight: {
    textAlign: 'right',
  },
  textCenter: {
    textAlign: 'center',
  },
  textLeft: {
    textAlign: 'left',
  },
  textUpper: {
    textTransform: 'uppercase',
  },
  full: {
    width: '100%',
    height: '100%',
  },
  right: {
    alignItems: 'flex-end',
  },
  left: {
    alignItems: 'flex-start',
  },
};

export default Styles;
