import React from 'react';
import { Image } from 'react-native';
import { withStyles, Layout, Text, Button, ThemedComponentProps } from 'react-native-ui-kitten';
import { withNavigation } from 'react-navigation';
import { NavigationStackScreenProps } from 'react-navigation-stack';
import { parsePhoneNumberFromString } from 'libphonenumber-js';

// Config
import config from '../../../config';

// Modules
import shared from '../../shared';

type PasswordExpiredComponentProps = ThemedComponentProps & NavigationStackScreenProps;

const PasswordExpiredComponent = ({ themedStyle, navigation }: PasswordExpiredComponentProps) => {
  const contactPhone = parsePhoneNumberFromString(config.contactPhone, 'US');
  let formattedContactPhone = '';
  if (contactPhone) {
    formattedContactPhone = contactPhone.formatNational();
  }

  return (
    <>
      <Layout style={themedStyle.content}>
        <Text category="p2" style={themedStyle.center}>
          Your reset password link has expired.
        </Text>
      </Layout>

      <Layout style={themedStyle.content}>
        <Image source={require('../../../img/resetpass-expired.png')} />
      </Layout>

      <Layout style={themedStyle.content}>
        <Text category="c1" status="primary" style={[themedStyle.center, themedStyle.uppercase]}>
          Please contact us at{' '}
          <Text
            category="c1"
            status="primary"
            style={[themedStyle.center, themedStyle.uppercase]}
            onPress={() => {
              shared.helpers.callNumber(config.contactPhone);
            }}
          >
            {formattedContactPhone}
          </Text>{' '}
          to speak to a representative.
        </Text>
        <Text category="c1" status="primary" style={[themedStyle.center, themedStyle.text]}>
          Or
        </Text>
        <Layout style={themedStyle.home}>
          <Button
            appearance="link"
            onPress={() => navigation.popToTop()}
            textStyle={themedStyle.uppercase}
            {...shared.helpers.setTestID('BackToLoginFromResetPwdBtn')}
          >
            Go Home
          </Button>
        </Layout>
      </Layout>
    </>
  );
};

const PasswordExpired = withStyles(PasswordExpiredComponent, () => ({
  content: {
    paddingVertical: 30,
    alignSelf: 'center',
  },
  center: {
    textAlign: 'center',
  },
  uppercase: {
    textTransform: 'uppercase',
  },
  home: {
    alignSelf: 'center',
  },
  text: {
    paddingVertical: 20,
  },
}));

export default withNavigation(PasswordExpired);
