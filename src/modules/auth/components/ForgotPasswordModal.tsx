/* eslint-disable global-require */
import React, { useEffect, useCallback, useRef } from 'react';
import { View, BackHandler, Image } from 'react-native';
import {
  withStyles,
  Text,
  Layout,
  Button,
  ThemedComponentProps,
  ModalService,
} from 'react-native-ui-kitten';
import color from 'color';

// Types
import { useFocusEffect } from 'react-navigation-hooks';

// Modules
import { TouchableOpacity } from 'react-native-gesture-handler';
import shared from '../../shared';

type containerProps = ThemedComponentProps & {
  email: string;
  status: any;
  visible: boolean;
  onDismiss: () => void;
};

const ForgotPasswordModal = ({
  status,
  email,
  visible,
  onDismiss,
  theme,
  themedStyle,
}: containerProps) => {
  const { height, width } = shared.helpers.hooks.useScreenDimensions();

  const modalId = useRef('');

  const hideModal = useCallback(() => {
    modalId.current = ModalService.hide(modalId.current);
    if (onDismiss) {
      onDismiss();
    }
  }, [onDismiss]);

  useFocusEffect(
    useCallback(() => {
      const onBackPress = () => {
        if (visible) {
          hideModal();
          return true;
        }
        return false;
      };

      BackHandler.addEventListener('hardwareBackPress', onBackPress);

      return () => BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    }, [visible, hideModal]),
  );

  // TODO CARD NUMBER PARSER SHARED
  const renderModal = useCallback(() => {
    return status && status.success ? (
      <Layout style={themedStyle.container}>
        <Image
          style={themedStyle.image}
          source={require('../../../img/secure-account-shield.png')}
        />
        <Text category="p1" style={themedStyle.center}>
          {status && status.successMsg}
        </Text>
        <Text style={[themedStyle.centerText, themedStyle.modalTitle]} category="p1" status="info">
          {email}
        </Text>

        <View style={themedStyle.modalCloseButton}>
          <TouchableOpacity
            onPress={hideModal}
            {...shared.helpers.setTestID('ForgotPasswordConfirmBtn')}
          >
            <Image
              source={require('../../../img/icons/ico_ok.png')}
              style={themedStyle.confirmationButton}
            />
          </TouchableOpacity>
        </View>
      </Layout>
    ) : (
      <Layout style={themedStyle.container}>
        <Image
          style={themedStyle.image}
          source={require('../../../img/secure-account-shield.png')}
        />
        <Text category="p1" status="danger" style={themedStyle.center}>
            {status && (status.error??'error')}
        </Text>

        <View style={themedStyle.modalCloseButton}>
          <TouchableOpacity
            onPress={hideModal}
            {...shared.helpers.setTestID('ForgotPasswordConfirmBtn')}
          >
              <Button onPress={hideModal}>TRY AGAIN</Button>
          </TouchableOpacity>
        </View>
      </Layout>
    );
  }, [
    email,
    hideModal,
    status,
    themedStyle.center,
    themedStyle.centerText,
    themedStyle.confirmationButton,
    themedStyle.container,
    themedStyle.image,
    themedStyle.modalCloseButton,
    themedStyle.modalTitle,
  ]);

  const showModal = useCallback(() => {
    const ModalContent = renderModal();

    if (modalId.current) {
      ModalService.update(modalId.current, ModalContent);
    } else {
      const Modal = (
        <Layout
          style={[
            themedStyle.backdrop,
            {
              width,
              height,
            },
          ]}
          pointerEvents="box-none"
        >
          {ModalContent}
        </Layout>
      );
      modalId.current = ModalService.show(Modal, {
        allowBackdrop: true,
        onBackdropPress: () => hideModal(),
      });
    }
  }, [renderModal, themedStyle.backdrop, width, height, hideModal]);

  useEffect(() => {
    if (visible) {
      showModal();
    } else {
      hideModal();
    }
  }, [hideModal, showModal, visible]);

  return null;
};

export default withStyles(ForgotPasswordModal, theme => ({
  backdrop: {
    position: 'absolute',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: color(theme['color-basic-900'])
      .alpha(0.7)
      .rgb()
      .string(),
  },
  container: {
    margin: 10,
    padding: 30,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
  },
  image: { alignSelf: 'center', marginVertical: 15, marginBottom: 30 },
  modalTitle: {
    paddingVertical: 10,
  },
  modalCloseButton: {
    paddingTop: 25,
    alignSelf: 'center',
  },
  itemSeparator: {
    marginVertical: 10,
  },
  centerText: {
    textAlign: 'center',
  },
  uppercase: {
    textTransform: 'uppercase',
  },
  addNewButton: {
    marginTop: 15,
  },
  confirmationButton: {
    alignSelf: 'center',
    width: 60,
    height: 60,
    tintColor: theme['color-success-600'],
  },
}));
