import React, { useState } from "react";
import { View, Image, TouchableWithoutFeedback, Modal } from "react-native";
import {
  withStyles,
  ThemedComponentProps,
  Text,
  Button,
} from "react-native-ui-kitten";

type ResumptiveUserModalProps = ThemedComponentProps;

const ResumptiveUserModal = ({ themedStyle }: ResumptiveUserModalProps) => {
  const [visible, setVisible] = useState(true);

  return (
    <View>
      <Modal
        visible={true}
        transparent={true}
        onRequestClose={() => console.log("chai")}
      >
        <TouchableWithoutFeedback onPress={() => console.log("Chai")}>
          <View style={themedStyle.modalOverlay}></View>
        </TouchableWithoutFeedback>
        <View style={themedStyle.modalCenter}>
          <View style={themedStyle.modalContainer}>
            <View style={themedStyle.iconContainer}>
              <Image
                style={[themedStyle.marginBottomMedium]}
                source={require("../../../img/icons/ico_coin.png")}
              ></Image>
            </View>
            <Text
              category="h2"
              status="primary"
              style={[
                themedStyle.textCenter,
                themedStyle.marginBottomBig,
                themedStyle.boldText,
              ]}
            >
              You are on top of it, [FirstName].
            </Text>
            <Text style={[themedStyle.marginBottomSmall]}>
              But first, let’s get your application completed so you can use our
              mobile app for customers once approved.
            </Text>
            <Text style={[themedStyle.marginBottomBig]}>
              You can pick up right where you left off by clicking “continue”
              below.
            </Text>
            <Button onPress={() => console.log("Holi")}>CONTINUE</Button>
          </View>
        </View>
      </Modal>
    </View>
  );
};

export default withStyles(ResumptiveUserModal, (theme) => ({
  modalOverlay: {
    flex: 1,
    width: "100%",
    height: "100%",
    backgroundColor: "rgba(0,0,0,0.5)",
  },
  iconContainer: {
    alignItems: "center",
  },
  modalCenter: {
    flex: 1,
    position: 'absolute',
    top: '25%',
    left: '2%',
    justifyContent: "center",
    alignItems: "center",
    padding: 16,
  },
  textCenter: {
    textAlign: "center",
  },
  textJustify: {
    textAlign: "center",
  },
  marginBottomSmall: {
    marginBottom: 8,
  },
  marginBottomMedium: {
    marginBottom: 16,
  },
  marginBottomBig: {
    marginBottom: 24,
  },
  boldText: {
    fontWeight: "bold",
  },
  modalContainer: {
    backgroundColor: "white",
    paddingHorizontal: 32,
    paddingVertical: 56,
    justifyContent: "center",
  },
}));
