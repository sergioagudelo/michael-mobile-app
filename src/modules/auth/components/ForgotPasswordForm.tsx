import React, { useState, useEffect } from 'react';
import { View, TouchableWithoutFeedbackBase } from 'react-native';
import { withStyles, Layout, Input, Button, ThemedComponentProps } from 'react-native-ui-kitten';
import * as Progress from 'react-native-progress';
import { FormikProps } from 'formik';
import { NavigationStackScreenProps } from 'react-navigation-stack';
import color from 'color';

// Types
import { withNavigation } from 'react-navigation';
import { ForgotPasswordFormValues } from '../utils';

// Modules
import shared from '../../shared';

// Constants
import { forms } from '../constants';

import ForgotPasswordModal from './ForgotPasswordModal';

type ForgotPasswordFormComponentProps = ThemedComponentProps &
  NavigationStackScreenProps &
  FormikProps<ForgotPasswordFormValues>;

const ForgotPasswordFormComponent = ({
  values,
  touched,
  errors,
  status,
  setStatus,
  handleBlur,
  handleChange,
  handleSubmit,
  isValid,
  isSubmitting,
  theme,
  themedStyle,
  navigation,
}: ForgotPasswordFormComponentProps) => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [modalWasClosed, setModalWasClosed] = useState(false);

  const onDismiss = () => {
    setIsModalVisible(false);
    setStatus({});
    if (status.success) {
      navigation.navigate('Login');
    }
  };

  useEffect(() => {
    if (!isModalVisible && status && (status.success || status.error)) {
      setIsModalVisible(true);
    }
  }, [isModalVisible, status]);

  return (
    <>
      {isModalVisible && (
        <ForgotPasswordModal
          status={status}
          visible={isModalVisible}
          onDismiss={onDismiss}
          email={values.email}
        />
      )}
      <Layout style={themedStyle.input}>
        <Input
          status={shared.helpers.getInputStatus<ForgotPasswordFormValues>('email', {
            touched,
            errors,
          })}
          value={values.email}
          onBlur={handleBlur('email')}
          onChangeText={handleChange('email')}
          label={forms.forgotPassword.labels.email}
          placeholder={forms.forgotPassword.placeholders.email}
          caption={touched.email && errors.email ? errors.email : ''}
          autoCapitalize="none"
          keyboardType="email-address"
          textContentType="username"
          autoCompleteType="username"
        />
      </Layout>
      {!isSubmitting ? (
        <Button
          status="primary"
          size="giant"
          disabled={!isValid || isSubmitting}
          onPress={handleSubmit}
          textStyle={themedStyle.uppercase}
          {...shared.helpers.setTestID('ForgotPasswordFormSubmitBtn')}
        >
          Submit
        </Button>
      ) : (
        <View style={themedStyle.formLoadingContainer}>
          <Progress.CircleSnail
            style={themedStyle.formLoading}
            color={[
              theme['color-primary-500'],
              theme['color-warning-500'],
              theme['color-info-500'],
            ]}
          />
        </View>
      )}
    </>
  );
};

const ForgotPasswordForm = withStyles(ForgotPasswordFormComponent, theme => ({
  uppercase: {
    textTransform: 'uppercase',
  },
  input: {
    paddingBottom: 20,
  },
  backdrop: {
    position: 'absolute',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: color(theme['color-basic-900'])
      .alpha(0.7)
      .rgb()
      .string(),
  },
  content: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 30,
    borderRadius: 10,
    elevation: 2,
    ...shared.helpers.shadow(2),
  },
  title: {
    paddingVertical: 15,
  },
  center: {
    textAlign: 'center',
    marginVertical: 15,
  },
  body: {
    paddingVertical: 15,
    paddingBottom: 30,
  },
  confirmationButton: {
    width: 40,
    height: 40,
    tintColor: theme['color-success-600'],
  },
  formLoadingContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  formLoading: {
    marginVertical: 14,
  },
}));

export default withNavigation(ForgotPasswordForm);
