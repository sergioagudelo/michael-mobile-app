import React from 'react';
import { Image, TouchableOpacity, View } from 'react-native';
import { withStyles, Layout, Text, ThemedComponentProps } from 'react-native-ui-kitten';
import * as Progress from 'react-native-progress';
import { FormikProps } from 'formik';
import CodeInput from 'react-native-confirmation-code-field';
import { NavigationStackProp } from 'react-navigation-stack';
import wait from 'waait';

// Types
import { VerificationCodeFormValues } from '../utils';

// Modules
import shared from '../../shared';

type VerificationCodeFormComponentProps = FormikProps<VerificationCodeFormValues> &
  ThemedComponentProps & {
    navigation: NavigationStackProp;
    onSubmit: () => void;
  };

const VerificationCodeFormComponent: React.FC<VerificationCodeFormComponentProps> = ({
  theme,
  themedStyle,
  handleChange,
  isValid,
  touched,
  errors,
  isSubmitting,
  handleSubmit,
}: VerificationCodeFormComponentProps) => {
  return (
    <>
      <Layout style={themedStyle.body} {...shared.helpers.setTestID('VerificationCodeForm')}>
        <Text category="p1" style={themedStyle.center}>
          We just send to your phone a verification code number
        </Text>

        <Layout style={[themedStyle.content, themedStyle.title]}>
          <Text category="c2" status="info" style={themedStyle.uppercase}>
            Verification code:
          </Text>
        </Layout>

        <CodeInput
          autoFocus
          codeLength={4}
          keyboardType="phone-pad"
          inputProps={{
            onChangeText: handleChange('code'),
            textContentType: 'oneTimeCode',
          }}
          cellProps={({ index }) => ({
            ...shared.helpers.setTestID(`VerificationCodeInput_${index}`),
            style: {
              borderColor: theme['color-basic-600'],
            },
          })}
          activeColor={theme['color-basic-800']}
          space={10}
          onFulfill={async () => {
            await wait();
            handleSubmit();
          }}
        />

        {touched.code && errors.code && (
          <View style={themedStyle.errorMsg}>
            <Text category="c2" status="danger">
              {errors.code}
            </Text>
          </View>
        )}

        {errors.submit && (
          <View style={themedStyle.errorMsg}>
            <Text category="c2" status="danger">
              {errors.submit}
            </Text>
          </View>
        )}
      </Layout>

      {isValid && !isSubmitting ? (
        <TouchableOpacity
          onPress={handleSubmit}
          {...shared.helpers.setTestID('VerificationCodeFormSubmitBtn')}
        >
          <Image
            source={require('../../../img/icons/ico_ok.png')}
            style={themedStyle.submitButton}
          />
        </TouchableOpacity>
      ) : null}
      {isSubmitting ? (
        <Progress.CircleSnail
          color={[theme['color-primary-500'], theme['color-warning-500'], theme['color-info-500']]}
        />
      ) : null}
    </>
  );
};

const VerificationCodeForm = withStyles(VerificationCodeFormComponent, theme => ({
  uppercase: {
    textTransform: 'uppercase',
  },
  body: {
    paddingVertical: 15,
  },
  content: {
    paddingTop: 20,
    alignSelf: 'center',
  },
  title: {
    paddingVertical: 15,
  },
  center: {
    textAlign: 'center',
  },
  errorMsg: {
    alignSelf: 'center',
    marginTop: 4,
    flexDirection: 'row',
    alignItems: 'center',
  },
  submitButton: {
    width: 50,
    height: 50,
    tintColor: theme['color-success-600'],
  },
}));

export default VerificationCodeForm;
