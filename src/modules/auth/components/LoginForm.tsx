import React from 'react';
import { View } from 'react-native';
import {
  withStyles,
  Layout,
  Input,
  Button,
  ThemedComponentProps,
  Text,
} from 'react-native-ui-kitten';
import { InputComponent } from 'react-native-ui-kitten/ui/input/input.component';
import { FormikProps } from 'formik';
import * as Progress from 'react-native-progress';

// Types
import { RootState } from 'typesafe-actions';
import { connect } from 'react-redux';
import { LoginFormValues } from '../utils';

import * as authStore from '../store';

// Modules
import shared from '../../shared';

// Constants
import { forms } from '../constants';

// Components
const { PasswordInput } = shared.components;

const mapStateToProps = (state: RootState) => ({
  isAuthLoading: authStore.selectors.getIsAuthLoading(state),
});

type LoginFormComponentProps = ThemedComponentProps &
  FormikProps<LoginFormValues> &
  ReturnType<typeof mapStateToProps>;

const LoginFormComponent = ({
  theme,
  themedStyle,
  values,
  touched,
  errors,
  handleBlur,
  handleChange,
  isValid,
  isSubmitting,
  handleSubmit,
  isAuthLoading,
}: LoginFormComponentProps) => {
  const emailInputRef = React.useRef<InputComponent>(null);
  const passwordInputRef = React.useRef<InputComponent>(null);

  return (
    <>
      <Layout style={themedStyle.input}>
        <Input
          disabled={isAuthLoading}
          ref={emailInputRef}
          status={shared.helpers.getInputStatus<LoginFormValues>('email', { touched, errors })}
          value={values.email}
          onBlur={handleBlur('email')}
          onChangeText={handleChange('email')}
          label={forms.login.labels.email}
          labelStyle={themedStyle.label}
          placeholder={forms.login.placeholders.email}
          caption={touched.email && errors.email ? errors.email : ''}
          autoCapitalize="none"
          keyboardType="email-address"
          textContentType="emailAddress"
          autoCompleteType="email"
          onSubmitEditing={() => {
            if (passwordInputRef && passwordInputRef.current) {
              passwordInputRef.current.focus();
            }
          }}
          returnKeyType="next"
        />
      </Layout>
      <Layout style={themedStyle.input}>
        <PasswordInput
          disabled={isAuthLoading}
          ref={passwordInputRef}
          status={shared.helpers.getInputStatus<LoginFormValues>('password', { touched, errors })}
          value={values.password}
          onBlur={handleBlur('password')}
          onChangeText={handleChange('password')}
          label={forms.login.labels.password}
          caption={touched.password && errors.password ? errors.password : ''}
          labelStyle={themedStyle.label}
          textContentType="password"
          autoCompleteType="password"
        />
      </Layout>

      {errors.submit && (
        <View style={themedStyle.errorMsg}>
          <Text category="c2" status="danger">
            {errors.submit}
          </Text>
        </View>
      )}

      {!isSubmitting ? (
        <Button
          status="primary"
          size="giant"
          disabled={!isValid || isSubmitting || isAuthLoading}
          onPress={handleSubmit}
          textStyle={themedStyle.uppercase}
          {...shared.helpers.setTestID('LoginFormSubmitBtn')}
        >
          Enter
        </Button>
      ) : (
        <View style={themedStyle.formLoadingContainer}>
          <Progress.CircleSnail
            style={themedStyle.formLoading}
            color={[
              theme['color-primary-500'],
              theme['color-warning-500'],
              theme['color-info-500'],
            ]}
          />
        </View>
      )}
    </>
  );
};

const LoginForm = withStyles(LoginFormComponent, () => ({
  uppercase: {
    textTransform: 'uppercase',
  },
  input: {
    paddingBottom: 20,
  },
  label: {
    textAlign: 'center',
  },
  formLoadingContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  formLoading: {
    marginVertical: 14,
  },
  errorMsg: {
    alignSelf: 'center',
    marginTop: 4,
    marginBottom: 14,
    flexDirection: 'row',
    alignItems: 'center',
  },
}));

export default connect(mapStateToProps)(LoginForm);
