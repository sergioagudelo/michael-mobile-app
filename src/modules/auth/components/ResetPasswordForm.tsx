import React from 'react';
import { View } from 'react-native';
import { withStyles, Layout, Text, Button, ThemedComponentProps } from 'react-native-ui-kitten';
import * as Progress from 'react-native-progress';
import { FormikProps } from 'formik';

// Types
import { ResetPasswordFormValues } from '../utils';

// Modules
import shared from '../../shared';

// Constants
import { forms } from '../constants';

// Components
const { PasswordInput } = shared.components;

type ResetPasswordFormComponentProps = ThemedComponentProps & FormikProps<ResetPasswordFormValues>;

const ResetPasswordFormComponent: React.FC<ResetPasswordFormComponentProps> = ({
  theme,
  themedStyle,
  values,
  touched,
  errors,
  handleBlur,
  handleChange,
  isValid,
  isSubmitting,
  handleSubmit,
}: ResetPasswordFormComponentProps) => {
  return (
    <>
      <Layout style={themedStyle.titleContainer}>
        <Text category="p2" style={themedStyle.center}>
          To create a new password for your account, please use at least 6 characters
        </Text>
      </Layout>
      <Layout style={themedStyle.input}>
        <PasswordInput
          status={shared.helpers.getInputStatus<ResetPasswordFormValues>('password', {
            touched,
            errors,
          })}
          value={values.password}
          onBlur={handleBlur('password')}
          onChangeText={handleChange('password')}
          label={forms.resetPassword.labels.password}
          caption={touched.password && errors.password ? errors.password : ''}
          textContentType="newPassword"
          autoCompleteType="password"
        />
      </Layout>
      {!isSubmitting ? (
        <Button
          status="primary"
          size="giant"
          disabled={!isValid || isSubmitting}
          onPress={handleSubmit}
          textStyle={themedStyle.uppercase}
          {...shared.helpers.setTestID('ResetPasswordFormSubmitBtn')}
        >
          Submit
        </Button>
      ) : (
        <View style={themedStyle.formLoadingContainer}>
          <Progress.CircleSnail
            style={themedStyle.formLoading}
            color={[
              theme['color-primary-500'],
              theme['color-warning-500'],
              theme['color-info-500'],
            ]}
          />
        </View>
      )}
    </>
  );
};

const ResetPasswordForm = withStyles(ResetPasswordFormComponent, () => ({
  uppercase: {
    textTransform: 'uppercase',
  },
  titleContainer: {
    paddingTop: 15,
    alignSelf: 'center',
  },
  center: {
    textAlign: 'center',
  },
  input: {
    paddingVertical: 20,
  },
  formLoadingContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  formLoading: {
    marginVertical: 14,
  },
}));

export default ResetPasswordForm;
