import * as Keychain from 'react-native-keychain';
import { getApiLevel } from 'react-native-device-info';
import { LoginFormValues } from '../utils';
import config from '../../../config';

export type KeychainResponseType<T = any > = {
  success: boolean;
  payload: T;
  error?: any;
}
export const BiometryTypes = {
  TouchId: Keychain.BIOMETRY_TYPE.TOUCH_ID,
  FaceId: Keychain.BIOMETRY_TYPE.FACE_ID,
  Fingerprint: Keychain.BIOMETRY_TYPE.FINGERPRINT,
  Default: 'default',
};
type handleKeychainResponsePropsType = { payload?: any; error?: any };

const handleKeychainError = (error: any): KeychainResponseType => ({ success: false, error, payload:"Error" })

export const handleKeychainResponse = ({ payload, error }: handleKeychainResponsePropsType): KeychainResponseType => payload ? { success: true, payload } : handleKeychainError(error ?? 'undefined error')

export const handleBiometryType = {
  [BiometryTypes.TouchId]: () =>  'Touch ID',
  [BiometryTypes.FaceId]: () =>  'Face ID',
  [BiometryTypes.Fingerprint]: () =>  'Fingerprint',
  [BiometryTypes.Default]: () =>  'No biometry Support!',
};


export const checkBiometrySupport = async (): Promise<KeychainResponseType<string>> => {
  try {
    const supportedType = await Keychain.getSupportedBiometryType();
    const androidApiLevel = await getApiLevel() // will return -1 if it's not android
    const isAndroid = androidApiLevel !== -1


  // bio will be supported only if not android
    if ( isAndroid  && config.disableAndroidBiometrics ) {
      return handleKeychainResponse({ error: handleBiometryType[BiometryTypes.Default]() })
    }

    return ( supportedType) ? handleKeychainResponse({ payload: handleBiometryType[supportedType]() }) : handleKeychainResponse({ error: handleBiometryType[BiometryTypes.Default]()})
  } catch (error) {
    return handleKeychainResponse({error})
  }
}

export const getSupportedBiometryType = async () => await Keychain.getSupportedBiometryType()

export const setGenericCredentials = async ({ email, password }: LoginFormValues) => {
  try {
      const res = await Keychain.setGenericPassword( email, password , {
        // service: 'com.lendingpoint.customerportal'
      // accessControl: Keychain.ACCESS_CONTROL.BIOMETRY_ANY_OR_DEVICE_PASSCODE,
      // accessible: Keychain.ACCESSIBLE.WHEN_UNLOCKED
    })
  } catch (error) {

    return handleKeychainResponse({ error })
  }
}

export const getGenericCredentials = async (): Promise<KeychainResponseType<Keychain.SharedWebCredentials>> => {
  try {
    const credentials = await Keychain.getGenericPassword();

    if(!credentials){
      return handleKeychainError("Credentials couldn't be retrieved!")
    }
    return {success:true, payload: credentials}
  } catch (error) {
    return handleKeychainResponse({ error })
  }
}

export const resetCredentials = async () => {
  try {
    await Keychain.resetGenericPassword()

  } catch (error) {
    return handleKeychainResponse({ error })

  }
}
