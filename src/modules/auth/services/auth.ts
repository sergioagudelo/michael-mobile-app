import { lendingpoint } from '../../../services';
import { ApiResponse } from '../../../services/utils';
import { LoginPayload, LoginResponse } from '../utils';
import shared from '../../shared';
import config from '../../../config';

const {
  api: { consumer },
  constants: { paths },
  helpers: { setAuthTokens, handleSubModuleError },
} = lendingpoint;

const { PushNotifications, DeviceInfo } = shared.services;

export const login = async (payload: LoginPayload): Promise<ApiResponse<LoginResponse>> => {
  try {
    let headers = null;
    if (__DEV__) {
      headers = {
        'lp-device-token': await PushNotifications.registerForPush(),
        'lp-device-info': await DeviceInfo.getDeviceInfoForPush(),
      };
    }

    const response = await consumer.post(paths.login, payload, headers ? { headers } : undefined);
    setAuthTokens(response.data);
    return { success: true, response: response.data };
  } catch (err) {
    return handleSubModuleError(err);
  }
};

export const resetPassword = async (email: string): Promise<ApiResponse<any>> => {
  try {
    const response = await consumer.post(paths.resetPassword, { email });
    return { success: true, response: response.data };
  } catch (err) {
    return handleSubModuleError(err);
  }
};
