import TouchID from 'react-native-touch-id';
import { SharedWebCredentials } from 'react-native-keychain';
import { theme } from '../../../theme';
import * as keychain from './keychain';

const optionalConfigObject = {
  title: 'Authentication Required', // Android
  color: theme['color-secondary-600'], // Android,
  fallbackLabel: 'Try Again', // iOS (if empty, then label is hidden)
};

export const authenticate = async (): Promise<
  keychain.KeychainResponseType<SharedWebCredentials>
> => {
  try {
    const result = await TouchID.authenticate('', optionalConfigObject);

    if (result) {
      const res = await keychain.getGenericCredentials();
      if (res.success) {
        return res;
      }
    }
    return keychain.handleKeychainResponse({ payload: result });
  } catch (error) {
    // todo type this
    if (
      error.name === 'LAErrorUserFallback' ||
      error.name === 'LAErrorUserCancel' ||
      error.name === 'LAErrorSystemCancel'
    ) {
      return keychain.handleKeychainResponse({ error: { message: '', name: '' } });
    }
    return keychain.handleKeychainResponse({
      error: {
        ...error,
        unenrolled: error.name === 'LAErrorTouchIDNotEnrolled',
      },
    });
  }
};
