import { compose } from 'redux';
import { withFormik } from 'formik';
import { NavigationStackProp } from 'react-navigation-stack';

// Types
import { VerificationCodeFormValues } from '../utils';

// Constants
import { forms } from '../constants';

// Component
import VerificationCodeForm from '../components/VerificationCodeForm';

type VerificationCodeFormProps = {
  navigation: NavigationStackProp;
  onSubmit: () => void;
};

export default compose(
  withFormik<VerificationCodeFormProps, VerificationCodeFormValues>({
    mapPropsToValues: () => ({
      code: forms.verificationCode.initialValues.code,
    }),
    isInitialValid: props => forms.verificationCode.schema.isValidSync(props.initialValues),
    validationSchema: forms.verificationCode.schema,
    handleSubmit: (values, { setSubmitting, setFieldError, props }) => {
      // TODO: Call Redux Action with the form data and wait for the result
      setTimeout(() => {
        setSubmitting(false);
        // TODO: Check if call failed
        const success = values.code === '1234';
        if (success) {
          // TODO: Handle success case, e.g. navigate to another screen, show modal with success message, etc.
          props.onSubmit();
          return props.navigation.navigate('Loans');
        }
        // ? Show the errors for the requests that failed
        // TODO: Show appropriate error message
        setFieldError('submit', "Invalid code. Try with '1234'");
      }, 2000);
    },
  }),
)(VerificationCodeForm);
