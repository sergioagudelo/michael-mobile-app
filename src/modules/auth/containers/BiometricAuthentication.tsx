/* eslint-disable global-require */
import React, { useState, useEffect, useCallback } from 'react';
import { Image } from 'react-native';
import { withStyles, Layout, ThemedComponentProps, Text } from 'react-native-ui-kitten';
import { NavigationStackProp } from 'react-navigation-stack';
import { TouchableOpacity } from 'react-native-gesture-handler';

import * as Animatable from 'react-native-animatable';

import { RootState } from 'typesafe-actions';
import { withNavigation } from 'react-navigation';
import { compose, bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';

import { BIOMETRY_TYPE } from 'react-native-keychain';
import { biometrics, auth, keychain } from '../services';
import shared from '../../shared';
import profile from '../../profile';

import * as authStore from '../store';

const { store: profileStore } = profile;
const { store: sharedStore } = shared;

const { Loading } = shared.components;

const mapStateToProps = (state: RootState) => ({
  isBiometricAllowed: sharedStore.selectors.getBiometricAuthPermission(state),
  isAuthLoading: authStore.selectors.getIsAuthLoading(state),
});

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(
    {
      getUserInfo: profileStore.actions.thunks.userInfo,
      setIsAuthLoading: authStore.actions.actions.setIsAuthLoading,
    },
    dispatch,
  );

type BiometricAuthenticationLayoutProps = {
  disableButtons: React.Dispatch<React.SetStateAction<boolean>>;
  navigation: NavigationStackProp;
} & ThemedComponentProps &
  ReturnType<typeof mapDispatchToProps> &
  ReturnType<typeof mapStateToProps>;

const BiometricAuthentication = ({
  themedStyle,
  navigation,
  isBiometricAllowed,
  getUserInfo,
  setIsAuthLoading,
  isAuthLoading,
}: BiometricAuthenticationLayoutProps) => {
  const [wasPrompt, setWasPrompt] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [failed, setFailed] = useState();
  const [biometryType, setBiometryType] = useState<BIOMETRY_TYPE | null>();

  if (!isBiometricAllowed) {
    return null;
  }

  const onTouchPress = async () => {
    setWasPrompt(true);
    setIsLoading(true);
    setIsAuthLoading(true);
    setFailed(undefined);
    const { success, payload, error } = await biometrics.authenticate();
    const { username, password } = payload;
    if (success && username && password) {
      // how dow I know if bio was success
      // try login
      // const { username, password } = payload;
      const result = await auth.login({ username, password });
      if (result.success) {
        await getUserInfo();
        setIsLoading(false);
        setIsAuthLoading(false);
        return navigation.navigate('Loans');
      }
      navigation.navigate('TryAgain');
    }

    setIsLoading(false);
    setIsAuthLoading(false);
    setFailed(error);
  };

  const getBiometryType = async () => {
    if (!biometryType) {
      const type = await keychain.getSupportedBiometryType();
      setBiometryType(type);
    }
  };

  if (!wasPrompt) {
    onTouchPress();
  }

  const renderIcon = () => {
    getBiometryType();
    if (biometryType) {
      return biometryType === keychain.BiometryTypes.FaceId ? (
        <Image style={themedStyle.icon} source={require('../../../img/face-id-orange.png')} />
      ) : (
        <Image style={themedStyle.icon} source={require('../../../img/touch-id-orange.png')} />
      );
    }
    return null;
  };

  return (
    <Layout style={themedStyle.biometricsContainer}>
      {isLoading ? (
        <Loading isLoading={isLoading} />
      ) : (
        <>
          <Animatable.View
            iterationCount={2}
            animation="pulse"
            duration={600}
            delay={250}
            useNativeDriver
          >
            <TouchableOpacity
              disabled={isAuthLoading}
              style={themedStyle.touchable}
              onPress={onTouchPress}
            >
              {renderIcon()}
            </TouchableOpacity>
            <Text style={themedStyle.cta}>
              {`Enter with ${biometryType && keychain.handleBiometryType[biometryType]()}`}
            </Text>
          </Animatable.View>
        </>
      )}
      {/* {failed &&
        (failed.message ? (
          <Text status="danger" style={themedStyle.errorMessage}>
            {failed.unenrolled
              ? `${biometryType &&
                  keychain.handleBiometryType[biometryType]()} has no enrolled ${
                  biometryType === keychain.BiometryTypes.FaceId ? 'face' : 'fingers'
                } `
              : failed.message}
          </Text>
        ) : null)} */}
    </Layout>
  );
};

export default compose(
  withNavigation,
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(
  withStyles(BiometricAuthentication, () => ({
    biometricsContainer: {
      justifyContent: 'center',
      alignContent: 'center',
      alignItems: 'center',
      marginVertical: 10,
      marginTop: 40,
    },
    cta: {
      color: '#B5BAC1',
      marginTop: 10,
      paddingHorizontal: 30,
      textAlign: 'center',
    },
    touchable: {
      alignContent: 'center',
      justifyContent: 'center',
      alignItems: 'center',
    },
    icon: { width: 80, height: 80, resizeMode: 'contain' },
    errorMessage: { marginTop: 30 },
  })),
) as React.ComponentType;
