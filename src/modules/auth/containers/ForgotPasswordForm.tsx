import { Keyboard } from 'react-native';
import { compose } from 'redux';
import { withFormik } from 'formik';
import { withNavigation } from 'react-navigation';

// Types
import { ForgotPasswordFormValues } from '../utils';

// Constants
import { forms } from '../constants';

// Component
import ForgotPasswordForm from '../components/ForgotPasswordForm';

// services
import * as loginService from '../services/auth';

type ForgotPasswordFormProps = { initialValues?: ForgotPasswordFormValues };

export default compose(
  withNavigation,
  withFormik<ForgotPasswordFormProps, ForgotPasswordFormValues>({
    mapPropsToValues: () => ({
      email: forms.forgotPassword.initialValues.email,
    }),
    isInitialValid: props => forms.forgotPassword.schema.isValidSync(props.initialValues),
    validationSchema: forms.forgotPassword.schema,
    handleSubmit: (values, { setSubmitting, setStatus }) => {
      setStatus({});
      const sendResetPasswordMail = async (email: string) => {
        Keyboard.dismiss();

        setSubmitting(true);
        const result = await loginService.resetPassword(email);
        setSubmitting(false);

        if (result.success) {
          setStatus({
            success: true,
            successMsg: "We've sent a password reset link to",
          });
        } else {
          // ? Show the errors for the requests that failed
          setStatus({
            success: false,

            error: result.details || 'An error ocurred.',
          });
        }
      };
      sendResetPasswordMail(values.email);
    },
  }),
)(ForgotPasswordForm);
