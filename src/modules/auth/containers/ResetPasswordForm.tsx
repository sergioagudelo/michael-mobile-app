import { compose } from 'redux';
import { withFormik } from 'formik';
import { withNavigation } from 'react-navigation';
import { NavigationStackScreenProps } from 'react-navigation-stack';

// Types
import { ResetPasswordFormValues } from '../utils';

// Constants
import { forms } from '../constants';

// Component
import ResetPasswordForm from '../components/ResetPasswordForm';

type ResetPasswordFormProps = NavigationStackScreenProps & {
  initialValues?: ResetPasswordFormValues;
};

export default compose(
  withNavigation,
  withFormik<ResetPasswordFormProps, ResetPasswordFormValues>({
    mapPropsToValues: () => ({
      password: forms.resetPassword.initialValues.password,
    }),
    isInitialValid: props => forms.resetPassword.schema.isValidSync(props.initialValues),
    validationSchema: forms.resetPassword.schema,
    handleSubmit: (values, { setSubmitting, setStatus, props }) => {
      // TODO: Call Redux Action with the form data and wait for the result
      setTimeout(() => {
        setSubmitting(false);
        // TODO: Check if call failed
        const failed = false;
        if (!failed) {
          // TODO: Handle success case, e.g. navigate to another screen, show modal with success message, etc.
          return props.navigation.popToTop();
        }
        // ? Show the errors for the requests that failed
        setStatus({
          success: false,
          // TODO: Show appropriate error message
          error: 'Error calling API',
        });
      }, 2000);
    },
  }),
)(ResetPasswordForm);
