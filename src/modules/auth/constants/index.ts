import FSStorage, { DocumentDir } from 'redux-persist-fs-storage';
import * as Yup from 'yup';
import config from '../../../config';

export const NAME = 'auth';

export const persistor = {
  config: {
    key: 'auth.db',
    keyPrefix: 'lp_cp_',
    storage: FSStorage(DocumentDir, 'CustomerPortal'),
  },
};

export const customValidationMessages = {
  login: {
    email: {
      email: 'Please use a valid email address',
      required: 'Email address is required',
    },
    password: {
      required: 'Password is required',
    },
  },
};

export const forms = {
  login: {
    initialValues: {
      email: __DEV__ ? config.login.initialValues.username : '',
      password: __DEV__ ? config.login.initialValues.password : '',
    },
    labels: {
      email: 'Email Address',
      password: 'Password',
    },
    placeholders: {
      email: 'myemail@email.com',
      password: '',
    },
    schema: Yup.object({
      email: Yup.string()
        .email(customValidationMessages.login.email.email)
        .required(customValidationMessages.login.email.required)
        .ensure()
        .trim()
        .lowercase(),
      password: Yup.string().required(customValidationMessages.login.password.required),
    }),
  },
  forgotPassword: {
    initialValues: {
      email: __DEV__ ? config.login.initialValues.username : '',
    },
    labels: {
      email: 'Enter account email address',
    },
    placeholders: {
      email: 'myemail@email.com',
    },
    schema: Yup.object({
      email: Yup.string()
        .required(customValidationMessages.login.email.required)
        .ensure()
        .trim()
        .lowercase()
        .email(customValidationMessages.login.email.email),
    }),
  },
  resetPassword: {
    initialValues: {
      password: __DEV__ ? 'Test12345' : '',
    },
    labels: {
      password: 'New Password',
    },
    placeholders: {
      password: '',
    },
    schema: Yup.object({
      password: Yup.string().required(),
    }),
  },
  verificationCode: {
    initialValues: {
      code: '',
    },
    labels: {
      code: '',
    },
    placeholders: {
      code: '',
    },
    schema: Yup.object({
      code: Yup.string()
        .ensure()
        .trim()
        .lowercase()
        .required()
        .matches(/[0-9]{4}/),
    }),
  },
};
