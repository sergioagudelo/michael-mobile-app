import { createSelector } from 'reselect';
import { RootState } from 'typesafe-actions';

const isAuthLoadingSelector = (state: RootState) => state.auth.isAuthLoading;

const _getIsAuthLoading = createSelector(
  isAuthLoadingSelector,
  (payload: boolean) => payload,
);

export const getIsAuthLoading = (state: RootState) => _getIsAuthLoading(state);

export const getUserInfo = createSelector(
  (state: RootState) => state.profile.userInfo,
  (userInfo: any) => userInfo
);

export const getCurrentRouteName = createSelector(
  (state: RootState) => state.shared.navigationHistory.currentRouteName,
  (currentRouteName: string) => currentRouteName
);

export const getPreviousRouteName = createSelector(
  (state: RootState) => state.shared.navigationHistory.previousRouteName,
  (previousRouteName: string) => previousRouteName
);