import { NAME } from '../../constants';
import shared from '../../../shared';

export default shared.helpers.createActionTypes(NAME, ['USER.LOGOUT', 'AUTH.LOADING']);
