import { combineReducers } from 'redux';
import { createReducer } from 'typesafe-actions';
import { actions } from '../actions';

const isAuthLoading = createReducer(false as boolean).handleAction(
  actions.setIsAuthLoading,
  (state, action) => action.payload,
);

export default combineReducers({ isAuthLoading });
