import React from 'react';
import { SafeAreaView, Image, Platform } from 'react-native';
import { withStyles, Layout, Text, ThemedComponentProps } from 'react-native-ui-kitten';
import { NavigationStackScreenProps } from 'react-navigation-stack';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

// Modules
import { ResetPasswordForm } from '../containers';
import { PasswordExpired } from '../components';
import shared from '../../shared';

// Components

type ResetPasswordProps = ThemedComponentProps & NavigationStackScreenProps;

const ResetPasswordComponent = ({ themedStyle }: ResetPasswordProps) => {
  return (
    <SafeAreaView style={themedStyle.screen}>
      <KeyboardAwareScrollView
        enableOnAndroid
        contentContainerStyle={themedStyle.screenContainer}
        keyboardShouldPersistTaps={Platform.select({
          ios: 'never',
          android: 'handled',
        })}
      >
        <Layout style={themedStyle.logoContainer}>
          <Image source={{ uri: 'lp_logo_hz' }} resizeMode="contain" style={themedStyle.logo} />
        </Layout>

        <Layout style={themedStyle.titleContainer}>
          <Text category="metatext" style={themedStyle.uppercase}>
            Create new password
          </Text>
        </Layout>

        <Layout style={themedStyle.form}>
          {shared.utils.rand() ? <ResetPasswordForm /> : <PasswordExpired />}
        </Layout>
      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
};

const ResetPassword = withStyles(ResetPasswordComponent, theme => ({
  screen: {
    flex: 1,
  },
  screenContainer: {
    flexGrow: 1,
    paddingHorizontal: 30,
  },
  logoContainer: {
    borderBottomColor: theme['color-basic-500'],
    borderBottomWidth: 2,
  },
  logo: {
    alignSelf: 'center',
    width: '80%',
    minHeight: 80,
  },
  titleContainer: {
    paddingTop: 15,
    alignSelf: 'center',
  },
  form: {
    paddingVertical: 20,
  },
  back: {
    alignSelf: 'center',
    paddingBottom: 25,
  },
  uppercase: {
    textTransform: 'uppercase',
  },
}));

export default ResetPassword;
