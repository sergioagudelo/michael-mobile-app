/* eslint-disable global-require */
import React, {useEffect, useState} from 'react';
import { SafeAreaView, Image, Platform } from 'react-native';
import { withStyles, Layout, Text, Button, ThemedComponentProps } from 'react-native-ui-kitten';
import { NavigationStackScreenProps } from 'react-navigation-stack';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

// Config
import { connect } from 'react-redux';
import { RootState } from 'typesafe-actions';

import config from '../../../config';

// Modules
import { LoginForm, BiometricAuthentication } from '../containers';
import shared from '../../shared';
import * as authStore from '../store';

// Components
const { CardButton } = shared.components;

const mapStateToProps = (state: RootState) => ({
  isAuthLoading: authStore.selectors.getIsAuthLoading(state),
  userInfo: authStore.selectors.getUserInfo(state),
  currentRouteName: authStore.selectors.getCurrentRouteName(state),
  previousRouteName: authStore.selectors.getPreviousRouteName(state)
});

type LoginProps = ThemedComponentProps &
  NavigationStackScreenProps &
  ReturnType<typeof mapStateToProps>;

const LoginComponent = ({ themedStyle, navigation, isAuthLoading, userInfo, currentRouteName, previousRouteName }: LoginProps) => {
  return (
    <SafeAreaView style={themedStyle.screen}>
      <KeyboardAwareScrollView
        enableOnAndroid
        contentContainerStyle={themedStyle.screenContainer}
        keyboardShouldPersistTaps={Platform.select({
          ios: 'handled',
          android: 'handled',
        })}
      >
        <Layout style={themedStyle.logoContainer}>
          <Image source={{ uri: 'lp_logo_hz' }} resizeMode="contain" style={themedStyle.logo} />
        </Layout>

        <BiometricAuthentication />

        <Layout style={themedStyle.form}>
          <LoginForm />
        </Layout>

        <Layout style={themedStyle.forgot}>
          <Button
            disabled={isAuthLoading}
            appearance="link"
            size="small"
            status="info"
            onPress={() => navigation.navigate('ForgotPassword')}
            textStyle={themedStyle.uppercase}
            {...shared.helpers.setTestID('ForgotPasswordBtn')}
          >
            Forgot Password?
          </Button>
        </Layout>

        <Layout style={themedStyle.notCustomerContainer}>
          <Layout style={themedStyle.notCustomer}>
            <Text category="h1" status="primary">
              Don't have an account?
            </Text>
          </Layout>

          <CardButton
            disabled={isAuthLoading}
            icon={require('../../../img/icons/ico_coin.png')}
            onPress={() => shared.helpers.openUrl(config.applyUrl)}
          >
            <Text category="s1" status="primary">
              Get Pre-qualified
            </Text>
            <Text category="c2" appearance="hint" style={themedStyle.uppercase}>
              Financing options up to $25,000
            </Text>
          </CardButton>

          <CardButton
            disabled={isAuthLoading}
            icon={require('../../../img/icons/ico_help.png')}
            onPress={() => shared.helpers.openUrl(config.contactUrl)}
          >
            <Text category="s1" status="primary">
              Contact Us
            </Text>
            <Text category="c2" appearance="hint" style={themedStyle.uppercase}>
              How can we help?
            </Text>
          </CardButton>
        </Layout>
      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
};

export default connect(mapStateToProps)(
  withStyles(LoginComponent, theme => ({
    screen: {
      flex: 1,
    },
    screenContainer: {
      flexGrow: 1,
      paddingHorizontal: 30,
    },
    logoContainer: {
      borderBottomColor: theme['color-basic-500'],
      borderBottomWidth: 2,
    },
    logo: {
      alignSelf: 'center',
      width: '80%',
      minHeight: 80,
    },
    biometricsContainer: {
      justifyContent: 'center',
      alignContent: 'center',
      alignItems: 'center',
      marginVertical: 10,
      marginTop: 40,
    },
    titleContainer: {
      paddingTop: 15,
      alignSelf: 'center',
    },
    form: {
      paddingTop: 40,
      paddingBottom: 20,
    },
    forgot: {
      alignSelf: 'center',
      paddingBottom: 25,
    },
    notCustomerContainer: {
      paddingBottom: 20,
    },
    notCustomer: {
      alignSelf: 'center',
      paddingTop: 25,
    },
    uppercase: {
      textTransform: 'uppercase',
    },
  })),
);
