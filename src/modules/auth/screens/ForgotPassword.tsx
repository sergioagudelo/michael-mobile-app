import React from 'react';
import { SafeAreaView, Image, Platform } from 'react-native';
import { withStyles, Layout, Text, Button, ThemedComponentProps } from 'react-native-ui-kitten';
import { NavigationStackScreenProps } from 'react-navigation-stack';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

// Modules
import shared from '../../shared';

// Components
import { ForgotPasswordForm } from '../containers';

type ForgotPasswordProps = ThemedComponentProps & NavigationStackScreenProps;

const ForgotPasswordComponent = ({ themedStyle, navigation }: ForgotPasswordProps) => {
  return (
    <SafeAreaView style={themedStyle.screen}>
      <KeyboardAwareScrollView
        enableOnAndroid
        contentContainerStyle={themedStyle.screenContainer}
        keyboardShouldPersistTaps={Platform.select({
          ios: 'never',
          android: 'handled',
        })}
      >
        <Layout style={themedStyle.logoContainer}>
          <Image source={{ uri: 'lp_logo_hz' }} resizeMode="contain" style={themedStyle.logo} />
        </Layout>

        <Layout style={themedStyle.titleContainer}>
          <Text category="metatext" style={themedStyle.uppercase}>
            Reset your password
          </Text>
        </Layout>

        <Layout style={themedStyle.form}>
          <ForgotPasswordForm />
        </Layout>

        <Layout style={themedStyle.back}>
          <Button
            appearance="link"
            size="small"
            status="info"
            onPress={() => navigation.navigate('Login')}
            textStyle={themedStyle.uppercase}
            {...shared.helpers.setTestID('BackToLoginBtn')}
          >
            Back to sign-in
          </Button>
        </Layout>
      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
};

const ForgotPassword = withStyles(ForgotPasswordComponent, theme => ({
  screen: {
    flex: 1,
  },
  screenContainer: {
    flexGrow: 1,
    paddingHorizontal: 30,
  },
  logoContainer: {
    borderBottomColor: theme['color-basic-500'],
    borderBottomWidth: 2,
  },
  logo: {
    alignSelf: 'center',
    width: '80%',
    minHeight: 80,
  },
  titleContainer: {
    paddingTop: 15,
    alignSelf: 'center',
  },
  form: {
    paddingVertical: 20,
  },
  back: {
    alignSelf: 'center',
    paddingBottom: 25,
  },
  uppercase: {
    textTransform: 'uppercase',
  },
}));

export default ForgotPassword;
