import React from 'react';
import { SafeAreaView, Image, Platform } from 'react-native';
import {
  withStyles,
  Layout,
  Text,
  Button,
  ThemedComponentProps,
  ModalService,
} from 'react-native-ui-kitten';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { NavigationStackScreenProps } from 'react-navigation-stack';
import color from 'color';

// Modules
import { VerificationCodeForm } from '../containers';

import shared from '../../shared';

// Components

type WelcomeComponentProps = ThemedComponentProps & NavigationStackScreenProps;

const WelcomeComponent = ({ themedStyle, navigation }: WelcomeComponentProps) => {
  const { height, width } = shared.helpers.hooks.useScreenDimensions();
  let modalId = '';

  const hideVerificationCodeModal = () => {
    modalId = ModalService.hide(modalId);
  };

  const renderVerificationCodeModal = () => {
    return (
      <Layout
        style={[
          themedStyle.backdrop,
          {
            width,
            height,
          },
        ]}
      >
        <Layout style={themedStyle.modalContainer}>
          <Image source={require('../../../img/secure-account-shield.png')} />

          <VerificationCodeForm navigation={navigation} onSubmit={hideVerificationCodeModal} />
        </Layout>
      </Layout>
    );
  };

  const showVerificationCodeModal = () => {
    const modal = renderVerificationCodeModal();

    if (modalId) {
      ModalService.update(modalId, modal);
    } else {
      modalId = ModalService.show(modal, {
        allowBackdrop: true,
        onBackdropPress: () => {},
      });
    }
  };

  return (
    <SafeAreaView style={themedStyle.screen}>
      <KeyboardAwareScrollView
        enableOnAndroid
        contentContainerStyle={themedStyle.screenContainer}
        keyboardShouldPersistTaps={Platform.select({
          ios: 'never',
          android: 'handled',
        })}
      >
        <Layout style={themedStyle.logoContainer}>
          <Image source={{ uri: 'lp_logo_hz' }} resizeMode="contain" style={themedStyle.logo} />
        </Layout>

        <Layout style={themedStyle.titleContainer}>
          <Text category="h1" status="primary">
            {/*
              // TODO: Get name from redux data
            */}
            Welcome John
          </Text>
        </Layout>

        <Layout style={themedStyle.content}>
          <Text category="p2" style={themedStyle.center}>
            You must secure your account the first time you access the application.
          </Text>
        </Layout>

        <Layout style={themedStyle.content}>
          <Image source={require('../../../img/secure-account.png')} />
        </Layout>

        <Layout style={themedStyle.content}>
          <Text category="p1" status="primary" style={themedStyle.center}>
            To secure your account, link this device to your phone number.
          </Text>
        </Layout>

        <Layout style={themedStyle.cta}>
          <Button
            status="primary"
            size="giant"
            onPress={showVerificationCodeModal}
            textStyle={themedStyle.uppercase}
            {...shared.helpers.setTestID('SecureAccountBtn')}
          >
            SECURE ACCOUNT NOW
          </Button>
        </Layout>
      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
};

const Welcome = withStyles(WelcomeComponent, theme => ({
  screen: {
    flex: 1,
  },
  screenContainer: {
    flexGrow: 1,
    paddingHorizontal: 30,
  },
  logoContainer: {
    borderBottomColor: theme['color-basic-500'],
    borderBottomWidth: 2,
  },
  logo: {
    alignSelf: 'center',
    width: '80%',
    minHeight: 80,
  },
  titleContainer: {
    paddingTop: 15,
    alignSelf: 'center',
  },
  content: {
    paddingTop: 20,
    alignSelf: 'center',
  },
  center: {
    textAlign: 'center',
  },
  uppercase: {
    textTransform: 'uppercase',
  },
  cta: {
    alignSelf: 'center',
    paddingVertical: 30,
  },
  backdrop: {
    position: 'absolute',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: color(theme['color-basic-900'])
      .alpha(0.7)
      .rgb()
      .string(),
  },
  modalContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    margin: 30,
    padding: 30,
    borderRadius: 10,
    borderColor: theme['color-basic-400'],
    borderWidth: 1,
    elevation: 2,
    ...shared.helpers.shadow(2),
  },
}));

export default Welcome;
