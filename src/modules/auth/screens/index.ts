import AuthLoading from './AuthLoading';
import ForgotPassword from './ForgotPassword';
import ResetPassword from './ResetPassword';
import Login from './Login';
import Welcome from './Welcome';

export default { AuthLoading, ForgotPassword, ResetPassword, Login, Welcome };
