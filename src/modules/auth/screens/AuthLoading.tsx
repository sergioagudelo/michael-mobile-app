import React, { useEffect } from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import { AUTH_TOKEN_ASYNC_STORAGE_KEY } from '../../../services/modules/lendingpoint/api/consumer';
import { NavigationStackScreenProps } from 'react-navigation-stack';
import * as Progress from 'react-native-progress';
import {
  Layout,
  withStyles,
  ThemedComponentProps,
} from 'react-native-ui-kitten';
import messaging from '@react-native-firebase/messaging';

type AuthLoadingComponentProps = ThemedComponentProps &
  NavigationStackScreenProps;

const checkRedirection = async (navigation: any) => {
  try {
    const authTokenString = await AsyncStorage.getItem(
      AUTH_TOKEN_ASYNC_STORAGE_KEY
    );
    if (authTokenString) {
      const initialNotification = await messaging().getInitialNotification();
      if (
        initialNotification &&
        initialNotification.data &&
        initialNotification.data.newNotification
      ) {
        const parsedData = JSON.parse(initialNotification.data.newNotification);
        navigation.navigate('NotificationDetails', {
          notificationId: parsedData.id,
        });
      } else {
        navigation.navigate('Loans');
      }
    } else {
      navigation.navigate('Auth');
    }
  } catch (e) {
    navigation.navigate('Auth');
  }
};

const AuthLoadingComponent = ({
  theme,
  navigation,
  themedStyle,
}: AuthLoadingComponentProps) => {
  useEffect(() => {
    checkRedirection(navigation);
  }, [navigation]);

  return (
    <Layout style={themedStyle.loading}>
      <Progress.CircleSnail
        color={[
          theme['color-primary-500'],
          theme['color-warning-500'],
          theme['color-info-500'],
        ]}
      />
    </Layout>
  );
};

const AuthLoading = withStyles(AuthLoadingComponent, () => ({
  loading: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
}));

export default AuthLoading;
