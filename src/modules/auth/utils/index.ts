import * as Yup from 'yup';

import { forms } from '../constants';

export type LoginFormValues = Yup.InferType<typeof forms.login.schema> & {
  submit?: string;
};
export type ForgotPasswordFormValues = Yup.InferType<typeof forms.forgotPassword.schema>;
export type ResetPasswordFormValues = Yup.InferType<typeof forms.resetPassword.schema>;
export type VerificationCodeFormValues = Yup.InferType<typeof forms.verificationCode.schema> & {
  submit?: string;
};

// Login API
export type LoginPayload = {
  email?: string;
  username: string;
  password: string;
};

export type LoginResponse = {
  access_token: string;
  token_type: string;
  refresh_token: string;
  expires_in: number;
  scope: string;
  funded: boolean;
};
