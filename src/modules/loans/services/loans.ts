import { lendingpoint } from '../../../services';

import { ApiResponse, HTTPServicesResponse } from '../../../services/utils';
import { LoansResponse, LoanDetailsType, PaymentHistoryResponse } from '../utils';
import { PaymentMethodType } from '../../payments/utils';

const {
  api: { consumer },
  constants: { paths },
  helpers: { handleSubModuleError },
} = lendingpoint;

export const getLoans = async (): Promise<ApiResponse<LoansResponse>> => {
  try {
    const response = await consumer.get(paths.loans);
    return { success: true, response: response.data };
  } catch (err) {
    return handleSubModuleError(err);
  }
};

export const getLoanDetails = async (idLoan: string): Promise<ApiResponse<LoanDetailsType>> => {
  try {
    const response = await consumer.get(`${paths.loans}/${idLoan}`);
    return { success: true, response: response.data };
  } catch (err) {
    return handleSubModuleError(err);
  }
};

export const getPaymentHistory = async (
  idLoan: string,
  params: {
    offset: number;
    limit: number;
    days: number;
  },
): Promise<ApiResponse<PaymentHistoryResponse>> => {
  try {
    const response = await consumer.get(`${paths.loans}/${idLoan}/payments`, { params });
    return { success: true, response: response.data };
  } catch (err) {
    return handleSubModuleError(err);
  }
};

export const setAutoPayMethod = async (
  loanId: string,
  method: PaymentMethodType,
): Promise<ApiResponse> => {
  try {
    const response = await consumer.patch(`${paths.loans}/${loanId}`, {
      [method.isACH ? 'bankId' : 'accountId']: method.paymentMethodId,
      paymentMethod: method.mode,
    });
    return { success: true, response: response.data };
  } catch (err) {
    return handleSubModuleError(err);
  }
};
