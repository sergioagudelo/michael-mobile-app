import React, { useState, useEffect, useCallback } from 'react';
import { Dispatch, bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Types
import { RootState } from 'typesafe-actions';
import { LoanDetailsType } from '../utils';
import { PaymentMethodType } from '../../payments/utils';

// Modules
import * as paymentsStore from '../../payments/store';

// Components
import SetUpAutoPayComponent from '../components/SetUpAutoPay';

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(
    {
      getPaymentMethods: paymentsStore.actions.thunks.getPaymentMethods,
    },
    dispatch,
  );

const mapStateToProps = (state: RootState) => ({
  paymentMethods: paymentsStore.selectors.getPaymentMethods(state),
});

type SetUpAutoPayProps = { loanId: string; loanDetails: LoanDetailsType } & ReturnType<
  typeof mapDispatchToProps
> &
  ReturnType<typeof mapStateToProps>;

const SetUpAutoPay = ({
  loanId,
  loanDetails,
  paymentMethods,
  getPaymentMethods,
}: SetUpAutoPayProps) => {
  const [isLoading, setIsLoading] = useState(false);
  const [selectedPayment, setSelectedPayment] = React.useState<PaymentMethodType>();

  useEffect(
    useCallback(() => {
      const getData = async () => {
        setIsLoading(true);
        await getPaymentMethods();
        setIsLoading(false);
      };
      if (!isLoading) {
        if (!paymentMethods) {
          getData();
        } else if (paymentMethods) {
          const currentPayment = paymentMethods.find(p => p.default);
          setSelectedPayment(currentPayment);
        }
      }
    }, [getPaymentMethods, isLoading, paymentMethods]),
  );

  return (
    <SetUpAutoPayComponent
      isLoading={isLoading}
      loanDetails={{ ...loanDetails, loanId }}
      paymentMethods={paymentMethods}
      selectedPayment={selectedPayment}
    />
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SetUpAutoPay);
