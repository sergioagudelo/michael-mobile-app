import React, { useState, useEffect } from 'react';
import { compose, Dispatch, bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withNavigation } from 'react-navigation';
import { NavigationStackProp } from 'react-navigation-stack';

// Types
import { RootState } from 'typesafe-actions';

// store
import * as store from '../store';

// Components
import { LoanDetails } from '../components';

type LoanDetailsMapStateProps = {
  loanId: string;
  navigation: NavigationStackProp;
};

const mapStateToProps = (state: RootState, { loanId }: LoanDetailsMapStateProps) => ({
  loan: store.selectors.getLoanById(state, loanId),
  loanDetails: store.selectors.getLoanDetails(state),
});

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(
    {
      getLoanDetails: store.actions.thunks.getDetails,
    },
    dispatch,
  );

type LoanDetailsConnectProps = LoanDetailsMapStateProps &
  ReturnType<typeof mapStateToProps> &
  ReturnType<typeof mapDispatchToProps>;
const LoanDetailsContainer = ({
  loan,
  navigation,
  getLoanDetails,
  loanDetails,
}: LoanDetailsConnectProps) => {
  const [failed, setFailed] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    if (!loan) {
      return;
    }
    const getDetails = async (loanId: string) => {
      setIsLoading(true);
      await getLoanDetails(loanId);
      setIsLoading(false);
      // TODO HANDlE ERROR
      // setFailed(true);
    };

    if (!isLoading) {
      if (!loanDetails || loanDetails.loanId !== loan.loanId) {
        getDetails(loan.loanId);
      }
    }
  }, [failed, isLoading, loan, getLoanDetails, loanDetails]);

  if (!loan || (!isLoading && failed)) {
    navigation.goBack();
    return null;
  }

  return <LoanDetails loan={loan} loanDetails={loanDetails} isLoading={isLoading} />;
};

export default compose(
  withNavigation,
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(LoanDetailsContainer) as React.ElementType;
