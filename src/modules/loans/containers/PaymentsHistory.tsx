import React, { useState, useEffect, useCallback } from 'react';
import { SelectOption } from 'react-native-ui-kitten';
import { compose } from 'redux';
import { withNavigation } from 'react-navigation';
import { NavigationStackProp } from 'react-navigation-stack';

// Components
import { PaymentsComponent } from '../components';

// Constants
import { selectPaymentOptions } from '../constants';

// Utils
import { PaymentHistoryItemType } from '../utils';
import * as loansService from '../services/loans';
import { paymentHistoryParser } from '../helpers';

type LoanDetailsMapStateProps = {
  navigation: NavigationStackProp;
};
const PaymentsHistory = ({ navigation }: LoanDetailsMapStateProps) => {
  const loanId = navigation.getParam('loanId');
  const [loading, setLoading] = useState(false);
  const [failed, setFailed] = useState(false);
  const [payments, setPayments] = useState<PaymentHistoryItemType[]>();
  const [selected, setSelected] = useState<SelectOption>({ text: 'Select ...' });
  const [isModalVisible, setModalVisible] = useState(false);
  const [modalData, setModalData] = useState<PaymentHistoryItemType>();

  const getPayments = useCallback(async (id: string) => {
    setLoading(true);
    const result = await loansService.getPaymentHistory(id, { days: 60, offset: 0, limit: 5 });
    if (result.success && result.response) {
      const parsedPayments = paymentHistoryParser(result.response);
      setPayments(parsedPayments);
      setLoading(false);
      return;
    }
    setFailed(true);
    setLoading(false);
  }, []);

  useEffect(() => {
    if (!payments && !loading && !failed) {
      getPayments(loanId);
    }
  }, [failed, getPayments, loading, loanId, payments]);

  /**
   * the select change, reload the data
   */
  const onChangeSelect = (selectedOption: SelectOption): void => {
    setSelected(selectedOption);
    setPayments([]);
    getPayments(loanId);
  };

  /**
   * show modal and setup selected item
   * @param item touched item
   */
  const onTouchPlus = (item: PaymentHistoryItemType) => {
    setModalVisible(true);
    setModalData(item);
  };

  /**
   * close modal remove data from the state
   * @param item touched item
   */
  const onTouchClose = () => {
    setModalVisible(false);
  };

  return (
    <PaymentsComponent
      payments={payments}
      selectOptions={selectPaymentOptions}
      selected={selected}
      isLoading={loading}
      isModalVisible={isModalVisible}
      modalData={modalData}
      onChangeSelect={onChangeSelect}
      onTouchPlus={onTouchPlus}
      onTouchClose={onTouchClose}
      failed={failed}
    />
  );
};

export default compose(withNavigation)(PaymentsHistory);
