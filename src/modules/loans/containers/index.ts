import PaymentsHistory from './PaymentsHistory';
import LoansDashboard from './LoansDashboard';
import LoanDetails from './LoanDetails';
import SetUpAutoPayContainer from './SetUpAutoPay';

export { PaymentsHistory, LoansDashboard, LoanDetails, SetUpAutoPayContainer };
