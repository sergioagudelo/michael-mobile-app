import React from 'react';
import { SafeAreaView } from 'react-native';
import { ThemedComponentProps, withStyles } from 'react-native-ui-kitten';
import { NavigationStackScreenProps } from 'react-navigation-stack';

// Components
import shared from '../../shared';
import { Styles } from '../helpers';
import { PaymentsHistory } from '../containers';

const { HeaderView } = shared.components;

type PaymentsHistoryLayoutProps = ThemedComponentProps &
  NavigationStackScreenProps<{ loanId: string }>;

const PaymentsHistoryElement = ({
  themedStyle,
}: PaymentsHistoryLayoutProps): React.ReactElement => {
  /**
   * return React.ReactElement with select and List
   */
  return (
    <SafeAreaView style={themedStyle.flexColumn}>
      <HeaderView title="Payment History" />
      <PaymentsHistory />
    </SafeAreaView>
  );
};

/**
 * apply theme styles to PaymentsComponent
 */
export default withStyles(PaymentsHistoryElement, () => ({
  ...Styles,
}));
