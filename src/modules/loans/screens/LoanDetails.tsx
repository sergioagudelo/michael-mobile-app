import React from 'react';
import { SafeAreaView, ScrollView, View } from 'react-native';
import { withStyles, Text, Button, ThemedComponentProps } from 'react-native-ui-kitten';
import { NavigationStackScreenProps } from 'react-navigation-stack';

// Modules
import shared from '../../shared';

// Components
import { LoanDetails } from '../containers';

const { Background } = shared.components;

type LoanDetailsLayoutProps = ThemedComponentProps & NavigationStackScreenProps<{ loanId: string }>;

const LoanDetailsLayout = ({ navigation, themedStyle }: LoanDetailsLayoutProps) => {
  const loanId = navigation.getParam('loanId');
  return (
    <SafeAreaView style={themedStyle.screen} {...shared.helpers.setTestID('LoanDetailsScreen')}>
      <ScrollView>
        <Text category="h1" status="accent" style={themedStyle.centerText}>
          Your Loan Details
        </Text>
        <Text
          category="metatext"
          style={[themedStyle.centerText, themedStyle.uppercase, themedStyle.subtitle]}
        >
          Payments &amp; options
        </Text>
        <View>
          <Background
            colors={['#004B8D', '#003267', '#0072BB']}
            wrapperStyle={themedStyle.wrapper}
          />
        </View>

        <View style={[themedStyle.content]}>
          <LoanDetails loanId={loanId} />

          <View style={[themedStyle.item, themedStyle.lastPaymentButton]}>
            <Button
              appearance="outline"
              status="basic"
              textStyle={themedStyle.uppercase}
              onPress={() => navigation.navigate('PaymentsHistory', { loanId })}
            >
              View Past Payments
            </Button>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default withStyles(LoanDetailsLayout, () => ({
  screen: {
    flex: 1,
  },
  body: {
    flexGrow: 1,
  },
  centerText: {
    textAlign: 'center',
  },
  uppercase: {
    textTransform: 'uppercase',
  },
  subtitle: {
    paddingTop: 10,
  },
  wrapper: {
    marginTop: 20,
  },
  content: {
    paddingTop: 40,
    paddingHorizontal: 30,
  },
  lastPaymentButton: {
    marginTop: 10,
    marginBottom: 80,
  },
}));
