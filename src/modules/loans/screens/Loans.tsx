import React from 'react';
import { SafeAreaView, ScrollView } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

import { ThemedComponentProps, withStyles } from 'react-native-ui-kitten';
import { NavigationTabScreenProps } from 'react-navigation-tabs';

// Modules
import refinance from '../../refinance';
import shared from '../../shared';

import { LoansDashboard } from '../containers';
// Components
import { BgArrow, NiceToSeeYou } from '../components';
// containers
const { VirtualCard } = shared.containers;
const { Dashboard: RefinanceDashboard } = refinance.containers;

type LoansProps = ThemedComponentProps & NavigationTabScreenProps;

const Loans = ({ themedStyle, theme, navigation }: LoansProps) => {
  return (
    <SafeAreaView style={themedStyle.screen} {...shared.helpers.setTestID('LoansScreen')}>
      <ScrollView contentContainerStyle={themedStyle.body}>
        <LinearGradient
          useAngle
          angle={225}
          angleCenter={{ x: 0, y: 0.6 }}
          colors={['#0072BB', '#003267', '#004B8D']}
          locations={[0, 0.45, 1]}
          style={themedStyle.background}
        >
          <BgArrow />
          <NiceToSeeYou />
          <VirtualCard />
          <RefinanceDashboard />
          <LoansDashboard />
        </LinearGradient>
      </ScrollView>
    </SafeAreaView>
  );
};

export default withStyles(Loans, () => ({
  centerText: {
    textAlign: 'center',
  },
  screen: {
    flex: 1,
  },
  body: {
    flexGrow: 1,
  },
  background: {
    flex: 1,
  },
  title: {
    marginBottom: 20,
  },
}));
