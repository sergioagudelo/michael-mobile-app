import Loans from './Loans';
import LoanDetails from './LoanDetails';
import PaymentsHistory from './PaymentsHistory';

export { Loans, LoanDetails, PaymentsHistory };
