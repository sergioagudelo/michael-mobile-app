import { createSelector } from 'reselect';
import { RootState } from 'typesafe-actions';
import { NAME } from '../../constants';

// type
import { Loan, LoanDetailsType } from '../../utils';

export const getLoans = (state: RootState) => state[NAME].list;

export const getLoanById = (state: RootState, loanId: string): Loan | undefined => {
  return state[NAME].list.find((loan: Loan) => loan.loanId === loanId);
};

const loanDetailsSelector = (state: RootState) => state[NAME].details;
export const getLoanDetails = createSelector(
  loanDetailsSelector,
  (details: LoanDetailsType) => details,
);
