import { combineReducers } from 'redux';
import { createReducer } from 'typesafe-actions';

import { Loan, LoanDetailsType } from '../../utils';

import { actions } from '../actions';

const list = createReducer(null as Loan[] | null).handleAction(
  actions.LoanDashboard.success,
  (state, action) => action.payload as Loan[] | null,
);

const details = createReducer(null as LoanDetailsType | null).handleAction(
  actions.LoanDetails.success,
  (state, action) => action.payload as LoanDetailsType | null,
);

const loansReducer = combineReducers({
  list,
  details,
});

export default loansReducer;
export type LoansState = ReturnType<typeof loansReducer>;
