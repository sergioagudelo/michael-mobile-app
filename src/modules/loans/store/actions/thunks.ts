import { Dispatch } from 'redux';
import { LoanDashboard as LoanDashboardActions, LoanDetails } from './actions';

import * as loanServices from '../../services/loans';
import { loansResponseParser } from '../../helpers';
import { LoansResponse } from '../../utils';
import { HTTPServicesResponse } from '../../../../services/utils';

export const getAll = () => async (dispatch: Dispatch) => {
  dispatch(LoanDashboardActions.request());
  const res = await loanServices.getLoans();
  if (res.success && res.response) {
    const loanAccounts = loansResponseParser(res.response
      .loanAccounts as LoansResponse['loanAccounts']);
    dispatch(LoanDashboardActions.success(loanAccounts));
    return;
  }
  dispatch(LoanDashboardActions.failure(res.error));
  return res;
};

export const getDetails = (loanId: string) => async (
  dispatch: Dispatch,
): Promise<HTTPServicesResponse> => {
  dispatch(LoanDetails.request());
  const res = await loanServices.getLoanDetails(loanId);
  if (res.success && res.response) {
    dispatch(
      LoanDetails.success({
        ...res.response,
        loanId,
        yearlyPaymentAmount: res.response.paymentAmount * 12,
      }),
    );
    return { success: true, payload: res.response };
  }
  dispatch(LoanDetails.failure(res.error));
  return { success: false, payload: res.error };
};
