import { createAsyncAction } from 'typesafe-actions';

// Types
import { Loan, LoanDetailsType } from '../../utils';

import types from './types';

export const LoanDashboard = createAsyncAction(
  types.LOAN_DASHBOARD.ATTEMPT,
  types.LOAN_DASHBOARD.SUCCESS,
  types.LOAN_DASHBOARD.FAILURE,
)<undefined, Loan[] | undefined, Error | undefined>();

export const LoanDetails = createAsyncAction(
  types.LOAN_DETAILS.ATTEMPT,
  types.LOAN_DETAILS.SUCCESS,
  types.LOAN_DETAILS.FAILURE,
)<undefined, LoanDetailsType | undefined, Error | undefined>();
