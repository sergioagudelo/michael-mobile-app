import React from 'react';
import { View } from 'react-native';
import { withStyles, ThemedComponentProps } from 'react-native-ui-kitten';
import shared from '../../shared';

/**
 * using borders make a triangle
 */
const BgArrowComponent = ({ themedStyle }: ThemedComponentProps) => {
  /**
   * get height from screen
   */
  const { height, width } = shared.helpers.hooks.useScreenDimensions('screen');

  return (
    <View style={[[themedStyle.backArrow], { minHeight: height }]}>
      <View style={[themedStyle.header]} />
      <View
        style={[
          themedStyle.arrow,
          {
            borderBottomWidth: width / 2,
            borderLeftWidth: width / 2,
            borderRightWidth: width / 2,
          },
        ]}
      />
    </View>
  );
};

const BgArrow = withStyles(BgArrowComponent, theme => ({
  backArrow: {
    position: 'absolute',
    flex: 1,
    flexDirection: 'column',
  },
  header: {
    backgroundColor: theme['color-basic-100'],
    width: '100%',
    height: 100,
  },
  arrow: {
    width: '100%',
    borderTopColor: theme['color-basic-100'],
    borderBottomColor: 'transparent',
    borderLeftColor: 'transparent',
    borderRightColor: 'transparent',
    borderTopWidth: 100,
  },
}));

export default BgArrow;
