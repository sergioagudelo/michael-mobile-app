import React from 'react';
import { View } from 'react-native';
import { withStyles, Text, ThemedComponentProps } from 'react-native-ui-kitten';

// Types
import { Loan, LoanDetailsType } from '../utils';

// Modules
import shared from '../../shared';
import { ExtraPaymentContainer } from '../../payments/containers';

// Components
import LoanBalanceHeader from './LoanBalanceHeader';
import LoanAutoPay from './LoanAutoPay';
import LoanNoAutoPay from './LoanNoAutoPay';
import LoanMoreDetails from './LoanMoreDetails';

const { Loading } = shared.components;

type LoanDetailsComponentProps = {
  loan: Loan;
  loanDetails: LoanDetailsType;
  isLoading: boolean;
} & ThemedComponentProps;
const LoanDetailsComponent = ({
  loan,
  loanDetails,
  isLoading,
  themedStyle,
}: LoanDetailsComponentProps) => (
  <>
    <LoanBalanceHeader lai={loan.lai} balance={loan.balance} />

    <View style={themedStyle.lastPaymentInfo}>
      {loanDetails && loanDetails.lastPaymentDate && (
        <Text
          appearance="alternative"
          category="c1"
          style={[themedStyle.centerText, themedStyle.uppercase, themedStyle.subtitle]}
        >
          {`${`Last payment ${shared.helpers.format.formatMoney(loanDetails.lastPaymentAmount, {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2,
          })} received on`} ${shared.helpers.format.formatDate(
            loanDetails.lastPaymentDate,
            shared.utils.formatDateOptions[0],
          )}`}
        </Text>
      )}
    </View>

    {!isLoading && loanDetails ? (
      <>
        {loanDetails.isAutopay ? (
          <LoanAutoPay loan={loan} details={loanDetails} />
        ) : (
          <LoanNoAutoPay loan={loan} />
        )}
        {!loanDetails.overDueDays && <ExtraPaymentContainer loan={loan as Loan} />}
        <LoanMoreDetails loanId={loan.loanId} loanDetails={loanDetails} />
      </>
    ) : (
      <Loading isLoading color="#fff" spinnerColors={['#fff']} />
    )}
  </>
);

const LoanDetails = withStyles(LoanDetailsComponent, theme => ({
  loanHeader: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  loanBalance: {
    paddingLeft: 20,
  },
  lastPaymentInfo: {
    paddingVertical: 15,
  },
  item: {
    paddingVertical: 15,
  },
  lastPaymentButton: {
    alignSelf: 'center',
  },
  uppercase: {
    textTransform: 'uppercase',
  },
  centerText: {
    textAlign: 'center',
  },
  container: {
    paddingVertical: 30,
    paddingHorizontal: 20,
    borderColor: theme['color-basic-400'],
    borderWidth: 1,
    elevation: 2,
    ...shared.helpers.shadow(2),
  },
  content: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  separator: {
    marginVertical: 15,
  },
}));

export default LoanDetails;
