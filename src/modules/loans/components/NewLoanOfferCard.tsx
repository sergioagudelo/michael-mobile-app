import React from 'react';
import { View } from 'react-native';
import { withStyles, Layout, Button, ThemedComponentProps } from 'react-native-ui-kitten';

// Utils
import { NewLoanOfferType } from '../utils';

// Modules
import shared from '../../shared';

// Components
import OfferCardItem from './OfferCardItem';

const { Dashed } = shared.components;

const defaultNewLoanOfferCardComponentProps = {
  status: 'info',
};

type NewLoanOfferCardComponentProps = {
  offer: NewLoanOfferType;
  onPress: () => void;
} & typeof defaultNewLoanOfferCardComponentProps &
  ThemedComponentProps;

const NewLoanOfferCardComponent = ({
  status,
  offer,
  onPress,
  theme,
  themedStyle,
}: NewLoanOfferCardComponentProps) => (
  <View>
    <Layout style={themedStyle.body}>
      <OfferCardItem
        status={status}
        label="Payment"
        content={shared.helpers.format.formatMoney(offer.payment, {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2,
        })}
        contentCategory="number"
      />

      <Dashed color={theme['color-basic-600']} customStyle={themedStyle.separator} />

      <OfferCardItem
        status={status}
        label="Loan Amount"
        content={shared.helpers.format.formatMoney(offer.amount)}
      />

      <OfferCardItem
        status={status}
        label="NEW APR"
        content={shared.helpers.format.formatPercent(offer.apr)}
      />

      <OfferCardItem status={status} label="Term" content={`${offer.term} Months`} />
    </Layout>
    <View style={themedStyle.cta}>
      <Button status={status} size="large" textStyle={themedStyle.uppercase} onPress={onPress}>
        Choose
      </Button>
    </View>
  </View>
);

NewLoanOfferCardComponent.defaultProps = defaultNewLoanOfferCardComponentProps;

const NewLoanOfferCard = withStyles(NewLoanOfferCardComponent, theme => ({
  uppercase: {
    textTransform: 'uppercase',
  },
  cta: {
    alignSelf: 'center',
    marginTop: -25,
    minWidth: 200,
    elevation: 2,
  },
  body: {
    padding: 20,
    paddingBottom: 40,
    borderColor: theme['color-basic-400'],
    borderWidth: 1,
    elevation: 2,
    ...shared.helpers.shadow(2),
  },
  separator: {
    marginTop: 10,
    marginBottom: 15,
  },
}));

export default NewLoanOfferCard;
