import React from 'react';
import { Image, ListRenderItemInfo } from 'react-native';
import { ThemedComponentProps, withStyles, ListItem } from 'react-native-ui-kitten';

// Components
import shared from '../../shared';

// Constants
import { PaymentHistoryItemType } from '../utils';

const { Dashed } = shared.components;

/**
 * component type
 */
type PaymentProps = ThemedComponentProps &
  ListRenderItemInfo<PaymentHistoryItemType> & {
    onTouchPlus: () => void;
  };

/**
 * render ListItem with paymentInfo
 */
const PaymentElement = ({
  themedStyle,
  theme,
  onTouchPlus,
  item,
  index,
}: PaymentProps): React.ReactElement => {
  const rowStyle = index % 2 === 0 ? themedStyle.bgPair : themedStyle.bgOdd;

  /**
   * render button with image
   */
  const renderButton = () => <Image source={{ uri: 'ico_plus' }} style={themedStyle.plus} />;

  /**
   * Return ListItem with payment info
   */
  return (
    <>
      {index === 0 && <Dashed color={theme['color-primary-700']} />}
      <ListItem
        title={`${shared.helpers.format.formatDate(
          item.paymentDate,
          shared.utils.formatDateOptions[0],
        )} • ${shared.helpers.format.formatMoney(item.payment, {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2,
        })}`}
        description={`${item.lpt} • ${item.method}`}
        style={[themedStyle.row, rowStyle]}
        accessory={renderButton}
        onPress={onTouchPlus}
      />
    </>
  );
};

/**
 * apply theme styles to PaymentsComponent
 */
const PaymentComponent = withStyles(PaymentElement, theme => ({
  plus: {
    width: 44,
    height: 44,
  },
  row: {
    paddingTop: 15,
    paddingBottom: 15,
  },
  bgPair: {
    backgroundColor: theme['color-basic-100'],
  },
  bgOdd: {
    backgroundColor: theme['color-basic-200'],
  },
}));

export default PaymentComponent;
