import React from 'react';
import { View } from 'react-native';
import { withStyles, Text, ThemedComponentProps } from 'react-native-ui-kitten';

import shared from '../../shared';

type LoanBalanceHeaderProps = {
  lai: string;
  balance?: number;
} & ThemedComponentProps;

const LoanBalanceHeader = ({ lai, balance, themedStyle }: LoanBalanceHeaderProps) => (
  <View style={themedStyle.loanHeader}>
    <View>
      <Text
        appearance="alternative"
        style={[themedStyle.centerText, themedStyle.uppercase, themedStyle.lai]}
      >
        {lai}
      </Text>
      {balance ? (
        <Text
          appearance="alternative"
          style={[themedStyle.centerText, themedStyle.uppercase, themedStyle.balanceLabel]}
        >
          Today's Balance
        </Text>
      ) : null}
    </View>
    {balance ? (
      <View style={themedStyle.loanBalance}>
        <Text appearance="alternative" category="h1" style={[themedStyle.balance]}>
          {shared.helpers.format.formatMoney(balance, {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2,
          })}
        </Text>
      </View>
    ) : null}
  </View>
);

export default withStyles(LoanBalanceHeader, () => ({
  loanHeader: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginVertical: 10,
  },
  loanBalance: {
    paddingLeft: 15,
  },
  uppercase: {
    textTransform: 'uppercase',
  },
  centerText: {
    textAlign: 'right',
  },
  lai: {
    fontSize: 13,
  },
  balanceLabel: {
    fontSize: 17,
    fontWeight: '500',
  },
  balance: {
    fontWeight: '700',
  },
}));
