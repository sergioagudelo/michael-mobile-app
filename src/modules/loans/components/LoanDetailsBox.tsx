import React, { ReactNode } from 'react';
import { View } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { withStyles, Text, ThemedComponentProps, Layout } from 'react-native-ui-kitten';
import shared from '../../shared';

type LoanDetailsBoxProps = {
  title: string;
  children?: ReactNode;
} & ThemedComponentProps;

const LoanDetailsBox: React.FC<LoanDetailsBoxProps> = ({
  title,
  children,
  themedStyle,
  theme,
}: LoanDetailsBoxProps) => {
  return (
    <View style={themedStyle.item}>
      <Layout style={themedStyle.container}>
        <LinearGradient
          useAngle
          angle={225}
          angleCenter={{ x: 0, y: 0.6 }}
          colors={[theme['color-basic-200'], theme['color-basic-500'], theme['color-basic-700']]}
          style={themedStyle.titleContainer}
        >
          <Text category="h2" status="primary" style={themedStyle.centerText}>
            {title}
          </Text>
        </LinearGradient>
        <Layout style={themedStyle.body}>{children}</Layout>
      </Layout>
    </View>
  );
};

export default withStyles(LoanDetailsBox, theme => ({
  centerText: {
    textAlign: 'center',
  },
  item: {
    paddingVertical: 15,
  },
  container: {
    borderColor: theme['color-basic-400'],
    borderWidth: 1,
    elevation: 2,
    ...shared.helpers.shadow(2),
  },
  titleContainer: {
    padding: 15,
  },
  body: {
    paddingTop: 15,
    paddingVertical: 30,
    paddingHorizontal: 20,
  },
}));
