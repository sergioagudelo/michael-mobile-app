import React from 'react';
import { View, StyleProp, TextStyle } from 'react-native';
import { withStyles, Text, ThemedComponentProps, Layout } from 'react-native-ui-kitten';

// Types
import { LoanDetailsType } from '../utils';

// Modules
import shared from '../../shared';

import SetUpAutoPayContainer from '../containers/SetUpAutoPay';
// Components
import LoanDetailsBox from './LoanDetailsBox';

type DetailItemComponentProps = ThemedComponentProps & {
  label: string;
  content: string;
  labelStyle?: StyleProp<TextStyle>;
  contentStyle?: StyleProp<TextStyle>;
};

const DetailItemComponent = ({
  label,
  content,
  labelStyle,
  contentStyle,
  themedStyle,
}: DetailItemComponentProps) => (
  <View style={themedStyle.container}>
    <View style={themedStyle.labelContainer}>
      <Text category="metatext" style={[themedStyle.uppercase, labelStyle]}>
        {label}
      </Text>
    </View>
    <View style={themedStyle.separator} />
    <View style={themedStyle.contentContainer}>
      <Text category="s1" status="accent" style={[themedStyle.content, contentStyle]}>
        {content}
      </Text>
    </View>
  </View>
);

const DetailItem = withStyles(DetailItemComponent, _theme => ({
  uppercase: {
    textTransform: 'uppercase',
  },
  container: {
    paddingTop: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  labelContainer: {
    flex: 1,
    alignItems: 'flex-end',
  },
  separator: {
    width: 10,
  },
  contentContainer: {
    flex: 1,
    alignItems: 'flex-start',
    marginRight: 30,
    marginLeft: 15,
  },
  content: {
    color: _theme['color-info-800'],
  },
}));

type LoanMoreDetailsComponentProps = {
  loanId: string;
  loanDetails: LoanDetailsType;
} & ThemedComponentProps;
const LoanMoreDetailsComponent = ({
  loanId,
  loanDetails,
  theme,
  themedStyle,
}: LoanMoreDetailsComponentProps) => {
  return (
    <>
      <LoanDetailsBox title="Loan Details">
        <View style={themedStyle.content}>
          <DetailItem
            label="LOAN AMOUNT"
            content={shared.helpers.format.formatMoney(loanDetails.disbursalAmount, {
              minimumFractionDigits: 2,
              maximumFractionDigits: 2,
            })}
          />
          <DetailItem
            label="PRINCIPAL PAID"
            content={shared.helpers.format.formatMoney(loanDetails.principalPaid, {
              minimumFractionDigits: 2,
              maximumFractionDigits: 2,
            })}
          />
          <DetailItem
            label="INTEREST PAID"
            content={shared.helpers.format.formatMoney(loanDetails.interestPaid, {
              minimumFractionDigits: 2,
              maximumFractionDigits: 2,
            })}
          />
        </View>
      </LoanDetailsBox>
      <LoanDetailsBox title="Autopay Settings">
        <SetUpAutoPayContainer loanId={loanId} loanDetails={loanDetails} />
      </LoanDetailsBox>

      <View style={themedStyle.item}></View>
    </>
  );
};

export default withStyles(LoanMoreDetailsComponent, theme => ({
  item: {
    paddingVertical: 15,
  },
  lastPaymentButton: {
    alignSelf: 'center',
  },
  uppercase: {
    textTransform: 'uppercase',
  },
  centerText: {
    textAlign: 'center',
  },
  container: {
    paddingVertical: 30,
    paddingHorizontal: 20,
    borderColor: theme['color-basic-400'],
    borderWidth: 1,
    elevation: 2,
    ...shared.helpers.shadow(2),
  },
  content: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  separator: {
    marginVertical: 15,
  },
}));
