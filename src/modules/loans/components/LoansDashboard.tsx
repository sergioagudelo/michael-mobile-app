import React from 'react';
import { ThemedComponentProps, withStyles, Text, List, Layout } from 'react-native-ui-kitten';
import { NavigationStackProp } from 'react-navigation-stack';

// Types
import { withNavigation } from 'react-navigation';
import { ListRenderItemInfo } from 'react-native';
import { Loan } from '../utils';

// Components
import LoanCard from './LoanCard';
// import { Loading } from '../../shared/components';

/**
 * type Loans type
 */
type LoansListProps = {
  navigation: NavigationStackProp;
  loans: Loan[];
} & ThemedComponentProps;

/**
 * Render loans and cards
 */
const LoansList = ({ themedStyle, navigation, loans }: LoansListProps) => {
  const emptyItem = () => (
    <Layout style={themedStyle.emptyListContainer}>
      <Text style={{ color: '#fff' }}>You don't have any loan account.</Text>
    </Layout>
  );
  const renderItem = ({ item }: ListRenderItemInfo<Loan>) => (
    <LoanCard
      type={item.overDue ? 'danger' : 'default'}
      loan={item}
      onPress={() => navigation.navigate('LoanDetails', { loanId: item.loanId })}
    />
  );
  return (
    <>
      <List
        data={loans}
        renderItem={renderItem}
        showsVerticalScrollIndicator={false}
        ListEmptyComponent={emptyItem}
      />
    </>
  );
};

const LoansDashboard = withNavigation(LoansList);

/**
 * apply theme styles to Loans
 */
export default withStyles(LoansDashboard, () => ({
  emptyListContainer: { flex: 1, alignItems: 'center', marginVertical: 50 },
}));
