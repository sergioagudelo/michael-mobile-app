import React from 'react';
import { View, Image } from 'react-native';
import { withStyles, Layout, Text, Button, ThemedComponentProps } from 'react-native-ui-kitten';
import { withNavigation } from 'react-navigation';
import { NavigationStackProp } from 'react-navigation-stack';

// Type
import { Loan } from '../utils';

// Modules
import shared from '../../shared';

type LoanNoAutoPayComponentProps = {
  loan: Loan;
  navigation: NavigationStackProp;
} & ThemedComponentProps;

const LoanNoAutoPayComponent = ({
  loan,
  navigation,
  themedStyle,
  theme,
}: LoanNoAutoPayComponentProps) => {
  return (
    <View style={themedStyle.container}>
      <Layout style={themedStyle.body}>
        <View style={themedStyle.content}>
          <View style={themedStyle.iconContainer}>
            <Image
              source={{ uri: 'ico_last_payment' }}
              style={themedStyle.icon}
              resizeMode="contain"
            />
          </View>
          <View>
            <View>
              <Text
                category="metatext"
                style={[
                  themedStyle.uppercase,
                  loan.overDueDays ? { color: theme['color-danger-500'] } : null,
                ]}
              >
                {loan.overDueDays ? 'PAYMENT WAS DUE ON' : 'NEXT PAYMENT DUE'}
              </Text>
              <Text category="s2" status="info">
                {shared.helpers.format.formatDate(
                  loan.paymentDate || '',
                  shared.utils.formatDateOptions[0],
                )}
              </Text>
            </View>
            <View style={themedStyle.paymentInfo}>
              <Text category="metatext" style={themedStyle.uppercase}>
                Payment amount
              </Text>
              <Text category="number" status="info">
                {shared.helpers.format.formatMoney(loan.payment, {
                  minimumFractionDigits: 2,
                  maximumFractionDigits: 2,
                })}
              </Text>
            </View>
          </View>
        </View>
      </Layout>
      <View style={themedStyle.cta}>
        <Button
          status="primary"
          size="large"
          textStyle={themedStyle.uppercase}
          onPress={() => navigation.navigate('MakePayment', { loanId: loan.loanId })}
        >
          Pay Now
        </Button>
      </View>
    </View>
  );
};

const LoanNoAutoPay = withStyles(withNavigation(LoanNoAutoPayComponent), theme => ({
  container: {
    marginBottom: 20,
  },
  item: {
    paddingVertical: 15,
  },
  uppercase: {
    textTransform: 'uppercase',
  },
  cta: {
    backgroundColor: 'red',
    alignSelf: 'center',
    marginTop: -25,
    minWidth: 200,
    elevation: 2,
  },
  body: {
    padding: 20,
    borderColor: theme['color-basic-400'],
    borderWidth: 1,
    elevation: 2,
    ...shared.helpers.shadow(2),
  },
  content: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  iconContainer: {
    paddingRight: 10,
  },
  icon: {
    width: 40,
    height: 40,
  },
  paymentInfo: {
    paddingVertical: 15,
  },
}));

export default LoanNoAutoPay;
