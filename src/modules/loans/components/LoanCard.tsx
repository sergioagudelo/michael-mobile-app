import React from 'react';
import { View, TouchableHighlight } from 'react-native';
import { ThemedComponentProps, withStyles, Layout, Text } from 'react-native-ui-kitten';
import { moderateScale } from 'react-native-size-matters';

// Components
import shared from '../../shared';
import { LoanType } from '../constants';
import { Loan } from '../utils';

// Helpers
const { Ico } = shared.helpers;

type LoanCardProps = {
  type: 'default' | 'danger';
  loan: Loan;
  onPress: () => void;
} & ThemedComponentProps;

const LoanCard = ({ themedStyle, type, loan, onPress }: LoanCardProps) => {
  /**
   * default type for making the loan danger/default
   */
  const behave = LoanType[type || 'default'];

  return (
    <View style={[themedStyle.loanContainer]}>
      <TouchableHighlight
        onPress={onPress}
        {...shared.helpers.setTestID(`LoanDetailsBtn_${loan.lai}`)}
      >
        <Layout style={[themedStyle.loan, themedStyle[behave.border]]}>
          <View style={[themedStyle.row, themedStyle.cardTop]}>
            <Text category="c1" appearance="dark">
              {loan.lai} •{' '}
              {shared.helpers.format.formatMoney(loan.amount, {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2,
              })}
            </Text>
            {/* {loan.image && (
              <Image
                style={themedStyle.imgPartner}
                resizeMode="contain"
                source={{ uri: loan.image }}
              />
            )} */}
          </View>
          <View style={[themedStyle.row, themedStyle.cardBody]}>
            <View style={themedStyle.loanBalance}>
              <Text category="number" status="accent">
                {shared.helpers.format.formatMoney(loan.balance, {
                  minimumFractionDigits: 2,
                  maximumFractionDigits: 2,
                })}
              </Text>
              <Text category="metatext" appearance="hint">
                TODAY'S BALANCE
              </Text>
            </View>
            <View style={themedStyle.loanNextPayment}>
              <Text category="number" status="info">
                {shared.helpers.format.formatMoney(loan.payment, {
                  minimumFractionDigits: 2,
                  maximumFractionDigits: 2,
                })}
              </Text>
              <Text category="metatext" appearance="hint">
                {loan.overDueDays ? 'WAS DUE ON' : 'NEXT PAYMENT'}
              </Text>
              <View style={[themedStyle.row, themedStyle.calendar]}>
                <Ico name="calendar-outline" fill="#0085CB" />
                <Text category="metatext" status="primary">
                  {shared.helpers.format.formatDate(
                    loan.paymentDate,
                    shared.utils.formatDateOptions[0],
                  )}
                </Text>
              </View>
            </View>
          </View>
        </Layout>
      </TouchableHighlight>
      <Layout style={[themedStyle.row, themedStyle.footer, themedStyle[behave.back]]}>
        <View style={themedStyle.footerCaption}>
          <Text appearance="alternative" style={themedStyle.centerText}>
            {loan.bottomTxt && loan.bottomTxt.toUpperCase()}
          </Text>
        </View>
        {/* {type === 'danger' && <Ico name="close" fill="#FFF" style={themedStyle.cardBottomIcon} />} */}
      </Layout>
    </View>
  );
};

export default withStyles(LoanCard, theme => ({
  centerText: {
    textAlign: 'center',
  },
  row: {
    flexDirection: 'row',
  },
  loanContainer: {
    marginHorizontal: 30,
    marginVertical: 15,
  },
  loan: {
    padding: 20,
    borderWidth: 2,
    minHeight: moderateScale(180),
    justifyContent: 'space-around',
  },
  borderDanger: {
    borderColor: theme['text-danger-color'],
  },
  borderInfo: {
    borderColor: theme['text-info-color'],
  },
  backDanger: {
    backgroundColor: theme['text-danger-color'],
  },
  backInfo: {
    backgroundColor: theme['text-info-color'],
  },
  cardTop: {
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  imgPartner: {
    width: 96,
    height: 23,
  },
  cardBody: {
    justifyContent: 'space-between',
    paddingVertical: 10,
  },
  calendar: {
    paddingTop: 5,
    alignItems: 'center',
  },
  loanBalance: {
    flex: 0.5,
    padding: 2,
  },
  loanNextPayment: {
    flex: 0.5,
    padding: 2,
    textAlign: 'right',
    justifyContent: 'flex-end',
  },
  cardCta: {
    flex: 0.2,
    justifyContent: 'center',
  },
  cta: {
    width: moderateScale(50),
    height: moderateScale(50),
    tintColor: theme['color-basic-500'],
  },
  footer: {
    top: -20,
    width: '85%',
    padding: 10,
    borderRadius: 20,
    borderWidth: 0,
    shadowRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
  footerCaption: {
    flex: 1,
    alignItems: 'center',
  },
}));
