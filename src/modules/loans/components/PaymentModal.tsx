import React, { useState } from 'react';
import { View, Modal, TouchableOpacity, TouchableHighlight } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { ThemedComponentProps, withStyles, Layout, Text, Button } from 'react-native-ui-kitten';

import shared from '../../shared';

// Components
import { Styles } from '../helpers';
import { PaymentHistoryItemType } from '../utils';
import { PaymentMethodType } from '../../payments/utils';

const { components:{ Dashed, Format}, helpers:{ format } } = shared;

type PaymentModalProps = ThemedComponentProps & {
  visible: boolean;
  onTouchClose: () => void;
  modalData: PaymentHistoryItemType;
};
/**
 * Modal with payment inside
 */
const PaymentModalElement = ({
  theme,
  themedStyle,
  visible,
  modalData,
  onTouchClose,
}: PaymentModalProps) => {
  return (
    <Modal animationType="fade" visible={visible} transparent>
      <Layout style={themedStyle.parent}>
        <TouchableHighlight style={themedStyle.parent} onPress={() => onTouchClose()}>
          <Layout style={themedStyle.container}>
            <Text category="h1" status="primary" style={themedStyle.title}>
              Payment Details
            </Text>

            <Dashed color={theme['color-primary-700']} customStyle={themedStyle.separator} />

            <View style={[themedStyle.flexRow]}>
              {/* rows */}
              <View style={[themedStyle.flexMid, themedStyle.rightPad]}>
                <View style={[themedStyle.flexColumn, themedStyle.right]}>
                  <Text style={themedStyle.pad} category="metatext" appearance="hint">
                    TRANSACTION #
                  </Text>
                  <Text style={themedStyle.pad} category="metatext" appearance="hint">
                    DATE
                  </Text>
                  <Text style={themedStyle.pad} category="metatext" appearance="hint">
                    AMOUNT
                  </Text>
                  <Text style={themedStyle.pad} category="metatext" appearance="hint">
                    PRINCIPAL
                  </Text>
                  <Text style={themedStyle.pad} category="metatext" appearance="hint">
                    INTEREST
                  </Text>
                  <Text style={themedStyle.pad} category="metatext" appearance="hint">
                    METHOD
                  </Text>
                </View>
              </View>
              <View style={[themedStyle.flexMid, themedStyle.leftPad]}>
                <View style={[themedStyle.flexColumn, themedStyle.left]}>
                  <Text style={themedStyle.pad} category="metatext" status="default">
                    {modalData && modalData.lpt}
                  </Text>
                  <Format
                    type="date"
                    format="MMM D, YYYY"
                    style={themedStyle.pad}
                    category="metatext"
                    status="default"
                  >
                    {(modalData && modalData.paymentDate) || ''}
                  </Format>
                  <Text style={themedStyle.pad} category="metatext" status="default">
                    {(modalData &&
                      shared.helpers.format.formatMoney(modalData.payment, {
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2,
                      })) ||
                      ''}
                  </Text>
                  <Text style={themedStyle.pad} category="metatext" status="default">
                    {(modalData &&
                      shared.helpers.format.formatMoney(modalData.principal, {
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2,
                      })) ||
                      ''}
                  </Text>
                  <Format style={themedStyle.pad} category="metatext" status="default">
                    {(modalData &&
                      shared.helpers.format.formatMoney(modalData.interest, {
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2,
                      })) ||
                      ''}
                  </Format>
                  <Text style={themedStyle.pad} category="metatext" status="default">
                    {`${(modalData.method) ?? ''} ${format.formatPaymentMethodNumber({ isACH: modalData.method === 'ACH' , cardNumber: modalData.methodNumber ?? '••••' } as PaymentMethodType)}`}
                  </Text>
                </View>
              </View>
            </View>
            <View style={themedStyle.closeButtonContainer}>
              <Button
                onPress={() => onTouchClose()}
                size="large"
                appearance="outline"
                status="basic"
              >
                CLOSE
              </Button>
            </View>
          </Layout>
        </TouchableHighlight>
      </Layout>
    </Modal>
  );
};

const PaymentModalComponent = withStyles(PaymentModalElement, theme => ({
  ...Styles,
  title: {
    alignSelf: 'center',
  },
  parent: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,.45)',
  },
  container: {
    flex: 0.7,
    borderRadius: 10,
    justifyContent: 'center',
    backgroundColor: theme['color-basic-100'],
    margin: 30,
    padding: 30,
  },

  backdrop: {
    backgroundColor: 'black',
    opacity: 0.5,
  },
  separator: {
    marginVertical: 20,
  },
  pad: {
    paddingVertical: 5,
  },
  flexMid: {
    flex: 0.5,
  },
  leftPad: {
    paddingLeft: 5,
    justifyContent: 'space-evenly',
  },
  rightPad: {
    paddingRight: 5,
    justifyContent: 'space-evenly',
  },
  closeButtonContainer: {
    paddingTop: 20,
    alignSelf: 'center',
  },
}));

export default PaymentModalComponent;
