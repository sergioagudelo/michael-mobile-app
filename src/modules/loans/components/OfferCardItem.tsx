import React from 'react';
import { View, StyleProp, TextStyle } from 'react-native';
import { Text, withStyles, ThemedComponentProps } from 'react-native-ui-kitten';

const defaultOfferCardItemProps = {
  status: 'primary',
  labelCategory: 'metatext',
  contentCategory: 's2',
};

type OfferCardItemProps = {
  containerStyle?: StyleProp<TextStyle>;
  label: string;
  labelStyle?: StyleProp<TextStyle>;
  content: string;
  contentStyle?: StyleProp<TextStyle>;
} & typeof defaultOfferCardItemProps &
  ThemedComponentProps;

const OfferCardItem = ({
  status,
  containerStyle,
  label,
  labelCategory,
  labelStyle,
  content,
  contentCategory,
  contentStyle,
  themedStyle,
}: OfferCardItemProps) => (
  <View style={[themedStyle.itemContainer, containerStyle]}>
    <View style={themedStyle.labelContainer}>
      <Text category={labelCategory} style={[themedStyle.uppercase, labelStyle]}>
        {label}
      </Text>
    </View>
    <View style={themedStyle.itemSeparator} />
    <View style={themedStyle.contentContainer}>
      <Text category={contentCategory} status={status} style={contentStyle}>
        {content}
      </Text>
    </View>
  </View>
);

OfferCardItem.defaultProps = defaultOfferCardItemProps;

export default withStyles(OfferCardItem, () => ({
  uppercase: {
    textTransform: 'uppercase',
  },
  itemContainer: {
    paddingVertical: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  itemSeparator: {
    width: 10,
  },
  labelContainer: {
    flex: 1,
    alignItems: 'flex-end',
  },
  contentContainer: {
    flex: 1,
    alignItems: 'flex-start',
  },
}));
