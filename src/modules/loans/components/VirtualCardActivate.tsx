import React from 'react';
import { View } from 'react-native';
import { ThemedComponentProps, withStyles, Text, Button } from 'react-native-ui-kitten';
import { NavigationStackProp } from 'react-navigation-stack';
import { withNavigation } from 'react-navigation';

// Types
import { VirtualCardType } from '../utils';

// Modules
import shared from '../../shared';

// Helpers
import { Styles } from '../helpers';

const { Format } = shared.components;

/**
 * component type
 */
type VirtualCardActivateProps = ThemedComponentProps & {
  data: VirtualCardType;
  navigation: NavigationStackProp;
};

/**
 * render card status/valid/limit
 */
const VirtualCardActivateElement = ({
  data,
  navigation,
  themedStyle,
}: VirtualCardActivateProps): React.ReactElement => {
  /**
   * return React.ReactElement
   */
  return (
    <View style={[themedStyle.flex, themedStyle.activate]}>
      <View style={[themedStyle.flexRow, themedStyle.status]}>
        <View style={[themedStyle.flex, themedStyle.alignJustifyCenter]}>
          <Text category="metatext">STATUS</Text>
          <Text category="label" status="warning">
            {data.status}
          </Text>
        </View>
        <View style={[themedStyle.flex, themedStyle.alignJustifyCenter]}>
          <Text category="metatext">VALID</Text>
          <Format type="date" format="DD/YY" category="label" status="warning">
            {data.valid}
          </Format>
        </View>
        <View style={[themedStyle.flex, themedStyle.alignJustifyCenter]}>
          <Text category="metatext">LIMIT</Text>
          <Text category="label" status="warning">
            {shared.helpers.format.formatMoney(data.amount)}
          </Text>
        </View>
      </View>
      <View style={[themedStyle.flex, themedStyle.button]}>
        <Text style={{ textAlign: 'center' }}>Coming soon!</Text>
        <Button disabled onPress={() => navigation.navigate('LoanAgreement', { loan: data })}>
          ACTIVATE NOW
        </Button>
      </View>
    </View>
  );
};

/**
 * create styles using theme
 * @see https://akveo.github.io/react-native-ui-kitten/docs/components/withstyles/overview#withstyles
 */
const VirtualCardActivate = withStyles(VirtualCardActivateElement, () => ({
  ...Styles,
  activate: {
    marginHorizontal: 35,
    paddingVertical: 12,
  },
  status: {
    marginBottom: 15,
  },
  button: {},
}));

export default withNavigation(VirtualCardActivate);
