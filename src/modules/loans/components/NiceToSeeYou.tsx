import React from 'react';
import { Text } from 'react-native-ui-kitten';

import { connect } from 'react-redux';
import { RootState } from 'typesafe-actions';

import * as store from '../../profile/store';

const mapStateToProps = (state: RootState) => ({
  userInfo: store.selectors.getUserInfo(state),
});

type NiceToSeeYouProps = ReturnType<typeof mapStateToProps>;

const NiceToSeeYou = ({ userInfo }: NiceToSeeYouProps) => (
  <Text category="h1" status="accent" style={{ textAlign: 'center' }}>
    Nice to see you, {userInfo ? userInfo.firstName : ''}
  </Text>
);

export default connect(mapStateToProps)(NiceToSeeYou);
