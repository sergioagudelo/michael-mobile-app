import React from 'react';
import { View, Image } from 'react-native';
import { withStyles, Layout, Text, ThemedComponentProps } from 'react-native-ui-kitten';
import { withNavigation } from 'react-navigation';
import * as Progress from 'react-native-progress';

// Type
import { Loan, LoanDetailsType } from '../utils';

// Modules
import shared from '../../shared';

type LoanAutoPayComponentProps = {
  loan: Loan;
  details: LoanDetailsType;
} & ThemedComponentProps;

const LoanAutoPayComponent = ({ loan, details, themedStyle, theme }: LoanAutoPayComponentProps) => {
  const autopayPaymentMethod = details.isAutopay
    ? shared.helpers.format.getLoanAutopayPaymentMethod(details)
    : null;

  return (
    <View style={themedStyle.item}>
      <Layout style={themedStyle.body}>
        <View style={themedStyle.content}>
          <View style={themedStyle.iconContainer}>
            <Image
              source={{ uri: 'ico_last_payment' }}
              style={themedStyle.icon}
              resizeMode="contain"
            />
          </View>
          <View>
            <View>
              <Text
                category="metatext"
                style={[
                  themedStyle.uppercase,
                  loan.overDueDays ? { color: theme['color-danger-500'] } : null,
                ]}
              >
                {loan.overDueDays ? 'PAYMENT WAS DUE ON' : 'NEXT PAYMENT DUE'}
              </Text>
              {!loan.overDueDays && (
                <Text category="s2" status="info">
                  will be automatically
                </Text>
              )}
              <Text category="s2" status="info">
                {`${loan.overDueDays ? '' : 'withdrawn on '}${shared.helpers.format.formatDate(
                  loan.paymentDate || '',
                  shared.utils.formatDateOptions[0],
                )}`}
              </Text>
            </View>
            {!autopayPaymentMethod ? (
              <View style={themedStyle.paymentLoader}>
                <Progress.CircleSnail
                  color={[
                    theme['color-info-500'],
                    theme['color-primary-500'],
                    theme['color-warning-500'],
                  ]}
                />
              </View>
            ) : (
              <View style={themedStyle.paymentMethodInfo}>
                <Text category="metatext" style={themedStyle.uppercase}>
                  Payment Method
                </Text>
                {autopayPaymentMethod ? (
                  <Text category="s2" status="info">
                    {`${autopayPaymentMethod.name} ${autopayPaymentMethod.number}`}
                  </Text>
                ) : null}
              </View>
            )}
            <View style={themedStyle.paymentInfo}>
              <Text category="metatext" style={themedStyle.uppercase}>
                Payment amount
              </Text>
              <Text
                category="number"
                status="info"
                style={loan.overDueDays ? { color: theme['color-danger-500'] } : null}
              >
                {shared.helpers.format.formatMoney(loan.payment, {
                  minimumFractionDigits: 2,
                  maximumFractionDigits: 2,
                })}
              </Text>
            </View>
          </View>
        </View>
      </Layout>
    </View>
  );
};

export default withStyles(withNavigation(LoanAutoPayComponent), theme => ({
  item: {
    paddingVertical: 15,
  },
  uppercase: {
    textTransform: 'uppercase',
  },
  cta: {
    backgroundColor: 'red',
    alignSelf: 'center',
    marginTop: -25,
    minWidth: 200,
    elevation: 2,
  },
  body: {
    paddingTop: 20,
    paddingHorizontal: 30,
    borderColor: theme['color-basic-400'],
    borderWidth: 1,
    elevation: 2,
    ...shared.helpers.shadow(2),
  },
  content: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  iconContainer: {
    paddingRight: 10,
  },
  icon: {
    width: 40,
    height: 40,
  },
  paymentLoader: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 10,
  },
  paymentMethodInfo: {
    paddingTop: 15,
  },
  paymentInfo: {
    paddingVertical: 15,
  },
}));
