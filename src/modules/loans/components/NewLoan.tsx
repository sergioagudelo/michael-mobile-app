import React from 'react';
import { View, TouchableOpacity, Image } from 'react-native';
import { ThemedComponentProps, withStyles, Text } from 'react-native-ui-kitten';
import { NavigationStackProp } from 'react-navigation-stack';
import { withNavigation } from 'react-navigation';
import { moderateScale } from 'react-native-size-matters';

// Types
import { Loan } from '../utils';

// Modules
import shared from '../../shared';

type NewLoanProps = {
  loan: Loan;
  navigation: NavigationStackProp;
} & ThemedComponentProps;

const NewLoan = ({ navigation, themedStyle, loan }: NewLoanProps) => {
  return (
    <View style={[themedStyle.container, themedStyle.border]}>
      <TouchableOpacity activeOpacity={0.7} onPress={() => navigation.navigate('NewLoanOffers')}>
        <View style={themedStyle.content}>
          <View style={themedStyle.copy}>
            <Text
              category="c1"
              appearance="alternative"
              style={[themedStyle.uppercase, themedStyle.centerText]}
            >
              YOU HAVE 25 DAYS TO GET
            </Text>

            <Text category="number" appearance="alternative">
              +{' '}
              {shared.helpers.format.formatMoney(loan.amount, {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2,
              })}
            </Text>

            <Text
              style={[themedStyle.uppercase, themedStyle.centerText]}
              category="c1"
              appearance="alternative"
            >
              IN NEW LOANS JUST FOR YOU
            </Text>
          </View>
          <View>
            <Image source={require('../../../img/icons/ico_plus.png')} />
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default withNavigation(
  withStyles(NewLoan, theme => ({
    uppercase: {
      textTransform: 'uppercase',
    },
    centerText: {
      textAlign: 'center',
    },
    container: {
      marginHorizontal: 30,
      marginVertical: 15,
      padding: 25,
      minHeight: moderateScale(130),
      justifyContent: 'center',
    },
    border: {
      borderStyle: 'dotted',
      borderColor: theme['color-basic-100'],
      borderWidth: 2,
      borderRadius: 5,
    },
    content: {
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
    },
    copy: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
  })),
);
