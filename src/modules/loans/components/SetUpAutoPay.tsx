import React from 'react';
import {
  withStyles,
  ThemedComponentProps,
  Button,
  ListItem,
  Icon,
  Text,
  Layout,
} from 'react-native-ui-kitten';

// Types
import { withNavigation } from 'react-navigation';
import { NavigationStackScreenProps } from 'react-navigation-stack';
import { LoanDetailsType } from '../utils';
import { PaymentMethodType } from '../../payments/utils';

// Modules
import shared from '../../shared';

// Components
const { Loading, Dashed } = shared.components;

type SetUpAutoPayProps = {
  isLoading: boolean;
  loanDetails: LoanDetailsType;
  selectedPayment?: PaymentMethodType;
  paymentMethods: PaymentMethodType[];
} & ThemedComponentProps &
  NavigationStackScreenProps;

const SetUpAutoPay = ({
  isLoading,
  loanDetails,
  selectedPayment,
  paymentMethods,
  theme,
  themedStyle,
  navigation,
}: SetUpAutoPayProps) => {
  const autopayPaymentMethod = loanDetails.isAutopay
    ? shared.helpers.format.getLoanAutopayPaymentMethod(loanDetails)
    : null;

  const handleSetupAutopay = () => {
    navigation.navigate('AutopaySetup', { loanId: loanDetails.loanId });
  };

  const renderChangeButton = () => (
    <Button
      appearance="ghost"
      size="small"
      textStyle={[themedStyle.uppercase]}
      onPress={handleSetupAutopay}
      disabled={loanDetails.isAutopay && paymentMethods && paymentMethods.length === 1}
      icon={style => (
        <Icon
          name="edit-2-outline"
          {...style}
          width={40}
          height={40}
          fill={theme['color-basic-600']}
        />
      )}
    ></Button>
  );

  const badgeAdditionalStyles = loanDetails.isAutopay
    ? {
        borderColor: theme['color-success-500'],
        color: loanDetails.isAutopay ? theme['color-success-800'] : theme['color-basic-1000'],
      }
    : {
        borderColor: theme['color-basic-500'],

        fontWeight: '600',
      };

  const renderBadge = () => (
    <Text style={[themedStyle.uppercase, badgeAdditionalStyles, themedStyle.badge]}>
      {loanDetails.isAutopay ? `Active` : `Not enabled`}
    </Text>
  );

  return (
    <>
      {isLoading && !paymentMethods ? (
        <Loading isLoading />
      ) : (
        <>
          <ListItem
            title="AUTOPAY OPTION"
            titleStyle={[themedStyle.uppercase, themedStyle.title]}
            accessory={renderBadge}
            onPress={handleSetupAutopay}
          />
          <Dashed color={theme['color-basic-700']} customStyle={themedStyle.headerSeparator} />
          {loanDetails.isAutopay ? (
            <ListItem
              title={autopayPaymentMethod ? 'Payment Method' : 'Select a payment method'}
              titleStyle={[themedStyle.uppercase, themedStyle.title]}
              descriptionStyle={[themedStyle.description]}
              description={
                loanDetails.isAutopay && autopayPaymentMethod
                  ? `${autopayPaymentMethod.name} ${autopayPaymentMethod.number}`
                  : shared.helpers.format.formatPaymentMethodNumber(selectedPayment)
              }
              accessory={() => renderChangeButton()}
              disabled
            />
          ) : (
            <>
              <Text
                status="primary"
                category="s2"
                style={[themedStyle.centerText, themedStyle.nonAutopayLabel]}
              >
                Setup Autopay now and don't worry for due dates anymore!
              </Text>
              <Layout style={themedStyle.ctaContainer}>
                <Button
                  size="medium"
                  status="info"
                  appearance="outline"
                  textStyle={[themedStyle.uppercase, themedStyle.centerText, themedStyle.ctaLabel]}
                  style={themedStyle.cta}
                  onPress={handleSetupAutopay}
                  icon={style => (
                    <Icon
                      name="settings-2-outline"
                      {...style}
                      width={24}
                      height={24}
                      fill={theme['color-info-500']}
                    />
                  )}
                >
                  Setup Autopay
                </Button>
              </Layout>
            </>
          )}
        </>
      )}
    </>
  );
};

export default withStyles(withNavigation(SetUpAutoPay), theme => ({
  uppercase: {
    textTransform: 'uppercase',
  },
  centerText: {
    textAlign: 'center',
  },
  icon: {
    width: 40,
    height: 40,
  },
  badge: {
    borderWidth: 2,
    borderRadius: 20,
    fontSize: 13,
    padding: 8,
    paddingHorizontal: 15,
  },
  title: {
    fontSize: 15,
    lineHeight: 15,
    fontWeight: '400',
    color: theme['color-basic-800'],
  },
  description: {
    color: theme['color-info-500'],
    marginTop: 10,
    fontSize: 18,
    padding: 0,
  },
  headerSeparator: {
    marginTop: 10,
    marginBottom: 5,
  },

  nonAutopayLabel: {
    paddingHorizontal: 20,
    padding: 10,
    fontSize: 18,
    lineHeight: 20,
  },
  ctaContainer: {
    maxWidth: '70%',
    alignSelf: 'center',
    maxHeight: 10,
  },
  ctaLabel: {
    fontSize: 16,
  },
  cta: {
    borderColor: theme['color-info-700'],
    backgroundColor: 'white',
    flexDirection: 'row-reverse',
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 2,
    marginTop: 15,
    marginBottom: -55,
    paddingHorizontal: 15,
  },
}));
