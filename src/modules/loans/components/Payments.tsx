import React from 'react';
import { View, GestureResponderEvent, ListRenderItemInfo } from 'react-native';
import * as Animatable from 'react-native-animatable';
import {
  ThemedComponentProps,
  withStyles,
  Select,
  List,
  SelectOption,
  SelectOptionType,
  Text,
} from 'react-native-ui-kitten';

// Modules
import shared from '../../shared';

// Components
import PaymentComponent from './Payment';
import PaymentModalComponent from './PaymentModal';

import { PaymentHistoryItemType } from '../utils';
import { Styles } from '../helpers';

const { Loading, Dashed } = shared.components;

/**
 * component type
 */
export type PaymentsComponentProps = ThemedComponentProps & {
  payments: PaymentHistoryItemType[] | undefined;
  modalData?: PaymentHistoryItemType;
  selectOptions: SelectOptionType[];
  onChangeSelect: (option: SelectOption, event?: GestureResponderEvent) => void;
  isLoading: boolean;
  failed: boolean;
  selected: SelectOption;
  isModalVisible: boolean;
  onTouchPlus: (item: PaymentHistoryItemType) => void;
  onTouchClose: () => void;
};

/**
 * render two types of Loan danger/normal
 */
const PaymentsComponentElement = ({
  theme,
  themedStyle,
  payments,
  onChangeSelect,
  selectOptions,
  isLoading,
  failed,
  selected,
  isModalVisible,
  onTouchPlus,
  onTouchClose,
  modalData,
}: PaymentsComponentProps): React.ReactElement => {
  const renderSeparator = () => <Dashed color={theme['color-basic-600']} />;

  /**
   * Return Payment Component
   * @param param
   */
  const renderItem = ({ item, index, ...props }: ListRenderItemInfo<PaymentHistoryItemType>) => {
    return (
      <Animatable.View
        animation="fadeInDown"
        duration={300}
        delay={(index && index * 150) || 150}
        useNativeDriver
      >
        <PaymentComponent
          item={item}
          onTouchPlus={() => onTouchPlus(item)}
          index={index}
          {...props}
        />
      </Animatable.View>
    );
  };

  /**
   * return React.ReactElement with select and List
   */
  if (failed) {
    return <Text>FAILED</Text>;
  }

  if (payments && payments.length === 0) {
    return (
      <View style={[themedStyle.flexColumn]}>
        <View style={[themedStyle.container, themedStyle.flexColumn]}>
          <Text style={{ textAlign: 'center', marginTop: 50 }}>No payment history found.</Text>
        </View>
      </View>
    );
  }

  return (
    <View style={[themedStyle.flexColumn]}>
      <View style={[themedStyle.container, themedStyle.flexColumn]}>
        {/* // TODO add select functionality
        <Select
          style={themedStyle.select}
          data={selectOptions}
          selectedOption={selected}
          onSelect={onChangeSelect}
        /> */}
        <Loading isLoading={isLoading} />
        {!isLoading && (
          <List
            style={themedStyle.list}
            data={payments}
            renderItem={renderItem}
            ItemSeparatorComponent={renderSeparator}
            showsVerticalScrollIndicator={false}
          />
        )}
      </View>
      {modalData && (
        <PaymentModalComponent
          modalData={modalData}
          onTouchClose={onTouchClose}
          visible={isModalVisible}
        />
      )}
    </View>
  );
};

/**
 * apply theme styles to PaymentsComponent
 */
const PaymentsComponent = withStyles(PaymentsComponentElement, () => ({
  ...Styles,
  container: {
    flex: 1,
    marginHorizontal: 30,
  },
  title: {
    alignSelf: 'center',
    marginBottom: 5,
  },
  select: {
    marginTop: 20,
  },
  list: {
    marginTop: 30,
    marginBottom: 30,
  },
}));

export default PaymentsComponent;
