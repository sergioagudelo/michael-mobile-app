import React, { useState, useEffect, useCallback, useRef } from 'react';
import { View, BackHandler, Image } from 'react-native';
import {
  withStyles,
  Text,
  Layout,
  Button,
  ThemedComponentProps,
  ModalService,
} from 'react-native-ui-kitten';
import color from 'color';

// Types
import { useFocusEffect } from 'react-navigation-hooks';

// Modules
import shared from '../../shared';

type ChoosePaymentModalProps = ThemedComponentProps & {
  bioType: string;
  visible: boolean;
  onDismiss: () => void;
  onSubmit: () => void;
};

const AllowBiometricAuthModal = ({
  bioType,
  onSubmit,
  visible,
  onDismiss,
  theme,
  themedStyle,
}: ChoosePaymentModalProps) => {
  const { height, width } = shared.helpers.hooks.useScreenDimensions();

  const modalId = useRef('');

  const hideModal = useCallback(() => {
    modalId.current = ModalService.hide(modalId.current);
    if (onDismiss) {
      onDismiss();
    }
  }, [onDismiss]);

  useFocusEffect(
    useCallback(() => {
      const onBackPress = () => {
        if (visible) {
          hideModal();
          return true;
        }
        return false;
      };

      BackHandler.addEventListener('hardwareBackPress', onBackPress);

      return () => BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    }, [visible, hideModal]),
  );

  // TODO CARD NUMBER PARSER SHARED
  const renderModal = useCallback(() => {
    return (
      <Layout style={themedStyle.choosePaymentModal}>
        <Text
          category="h5"
          status="accent"
          style={[themedStyle.centerText, themedStyle.modalTitle]}
        >
          {`Log in with ${bioType}?`}
        </Text>
        <Text style={[themedStyle.centerText, themedStyle.modalTitle]}>Fast, easy and secure</Text>

        <View style={themedStyle.modalCloseButton}>
          <Button
            appearance="outline"
            status="info"
            onPress={() => {
              onSubmit();
              hideModal();
            }}
          >
            Great, thanks!
          </Button>
          <Button
            style={themedStyle.addNewButton}
            appearance="ghost"
            status="info"
            size="small"
            onPress={() => {
              hideModal();
            }}
          >
            No
          </Button>
        </View>
      </Layout>
    );
  }, [
    bioType,
    hideModal,
    onSubmit,
    themedStyle.addNewButton,
    themedStyle.centerText,
    themedStyle.choosePaymentModal,
    themedStyle.modalCloseButton,
    themedStyle.modalTitle,
  ]);

  const showModal = useCallback(() => {
    const ModalContent = renderModal();

    if (modalId.current) {
      ModalService.update(modalId.current, ModalContent);
    } else {
      const Modal = (
        <Layout
          style={[
            themedStyle.backdrop,
            {
              width,
              height,
            },
          ]}
          pointerEvents="box-none"
        >
          {ModalContent}
        </Layout>
      );
      modalId.current = ModalService.show(Modal, {
        allowBackdrop: true,
        onBackdropPress: () => hideModal(),
      });
    }
  }, [renderModal, themedStyle.backdrop, width, height, hideModal]);

  useEffect(() => {
    if (visible) {
      showModal();
    } else {
      hideModal();
    }
  }, [hideModal, showModal, visible]);

  return null;
};

export default withStyles(AllowBiometricAuthModal, theme => ({
  backdrop: {
    position: 'absolute',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: color(theme['color-basic-900'])
      .alpha(0.7)
      .rgb()
      .string(),
  },
  choosePaymentModal: {
    margin: 10,
    padding: 30,
    borderRadius: 10,
  },
  modalTitle: {
    paddingBottom: 10,
  },
  modalCloseButton: {
    paddingTop: 25,
    alignSelf: 'center',
  },
  itemSeparator: {
    marginVertical: 10,
  },
  centerText: {
    textAlign: 'center',
  },
  uppercase: {
    textTransform: 'uppercase',
  },
  addNewButton: {
    marginTop: 15,
  },
  confirmationIcon: {
    alignSelf: 'center',
    width: 60,
    height: 60,
    tintColor: theme['color-success-600'],
    marginBottom: 30,
  },
}));
