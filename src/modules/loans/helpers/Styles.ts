import { ViewStyle, Dimensions, TextStyle } from 'react-native';
import { ScaledSheet } from 'react-native-size-matters';

/**
 * get dimensions
 */
const win = Dimensions.get('window');

/**
 * common styles for loans module
 */
const LoansStyles: { [key: string]: ViewStyle | TextStyle } = {
  loanContainer: {
    marginHorizontal: 25,
    marginBottom: 10,
  },
  loan: {
    padding: 20,
    borderWidth: 2,
  },
  cardContainer: {
    marginHorizontal: 25,
    marginBottom: 25,
  },
  cardBgImage: {
    padding: 20,
  },
  flex: {
    flex: 1,
  },
  flexRow: {
    flex: 1,
    flexDirection: 'row',
  },
  flexColumn: {
    flex: 1,
    flexDirection: 'column',
  },
  alignItemsCenter: {
    alignItems: 'center',
  },
  alignJustifyCenter: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  center: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  textRight: {
    textAlign: 'right',
  },
  textLeft: {
    textAlign: 'left',
  },
  textUpper: {
    textTransform: 'uppercase',
  },
  borderRadius: {
    borderRadius: 5,
  },
  bgImage: {
    width: '100%',
    height: '100%',
  },
  right: {
    alignItems: 'flex-end',
  },
  left: {
    alignItems: 'flex-start',
  },
  full: {
    width: '100%',
    height: '100%',
  },
  absolute: {
    position: 'absolute',
  },
};

/**
 * ScaledSheet with functions
 * @see https://github.com/nirsky/react-native-size-matters
 */
const ScaledStyles = ScaledSheet.create({
  cardBgPadding: {
    padding: win.height > 667 ? '88@vs' : '86@vs',
  },
  greenPadding: {
    padding: win.height > 667 ? '25@vs' : '23@vs',
  },
});

const Styles = { ...LoansStyles, ...ScaledStyles };
export default Styles;
