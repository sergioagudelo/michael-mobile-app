import Styles from './Styles';
import { Loan, LoansResponse, PaymentHistoryResponse, PaymentHistoryItemType } from '../utils';
import shared from '../../shared';

const { formatDate } = shared.helpers.format;

export { Styles };

export const isDateExpired = (date: string): boolean => Date.parse(date) < Date.now();

export const bottomTextMaker = (overDueDays: number, paymentDate: string) => {
  if (overDueDays === 1) {
    return `${overDueDays} day late on payment due`.toUpperCase();
  }
  return overDueDays
    ? `${overDueDays} days late on payment due`.toUpperCase()
    : `Next payment due: ${formatDate(
        paymentDate,
        shared.utils.formatDateOptions[0],
      )}`.toUpperCase();
};

export const loansResponseParser = (list: LoansResponse['loanAccounts']): Loan[] => {
  if (list) {
    return list.map(loan => {
      // const isOverDue = isDateExpired(loan.nextDueDate); // NOT NEEDED ANYMORE
      return {
        loanId: loan.contractId,
        lai: loan.contractName,
        payment: loan.loanPaymentAmount,
        amount: loan.disbursalAmount || 0,
        balance: loan.loanPayOffAmountAsOfToday,
        paymentDate: loan.nextDueDate,
        subSource: loan.subSource,
        bottomTxt: bottomTextMaker(loan.overDueDays, loan.nextDueDate),
        overDue: !!loan.overDueDays,
        overDueDays: loan.overDueDays,
        isAutopay: loan.isAutopay || false,
        lastPaymentAmount: loan.lastPaymentAmount,
        loanPaymentAmount: loan.loanPaymentAmount,
        remainingTerm: loan.remainingTerm,
        frequency: loan.frequency
      };
    });
  }
  return [];
};

export const paymentHistoryParser = (payments: PaymentHistoryResponse): PaymentHistoryItemType[] =>
  payments.details.map(payment => ({
    principal: payment.principal,
    interest: payment.interest,
    payment: payment.total,
    methodNumber: payment.paymentMethodNumber,
    method: payment.method,
    paymentDate: payment.date,
    lpt: payment.transactionNumber,
    id: payment.transactionId,
  }));
