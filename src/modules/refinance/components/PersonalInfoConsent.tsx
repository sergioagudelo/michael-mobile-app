import React from 'react';
import { View } from 'react-native';
import { withStyles, ThemedComponentProps, Layout, Text, ListItem, Button } from 'react-native-ui-kitten';
import { User } from '../../profile/utils';
import shared from '../../shared';
import config from "../../../config";
import { LoanDetailsType } from '../../loans/utils';

const { Dashed, Loading } = shared.components;
const { format } = shared.helpers;
type PersonalInfoConsentComponentProps = {
  isLoading: boolean;
  errorMessage: string;
  userInfo: User;
  handlePress: ()=>Promise<void>;
  loanDetails: LoanDetailsType
} & ThemedComponentProps;

const PersonalInfoConsent = ({ themedStyle, theme, userInfo, loanDetails,handlePress, isLoading, errorMessage }: PersonalInfoConsentComponentProps) => {
  return (
    <Layout>
      <View style={themedStyle.receiptContainer}>
        <ListItem
          title="ADDRESS"
          description={`${userInfo.street}, ${userInfo.city}, ${userInfo.state} ${userInfo.postalCode}`}
          style={themedStyle.receiptItem}
          titleStyle={[themedStyle.uppercase, themedStyle.titleStyle]}
          descriptionStyle={themedStyle.descriptionStyle}
          disabled
        />
        <Dashed color={theme['color-basic-600']} customStyle={themedStyle.itemSeparator} />
        <ListItem
          title="PHONE"
          description={format.formatPhone(userInfo.phoneNumber)}
          style={themedStyle.receiptItem}
          titleStyle={[themedStyle.uppercase, themedStyle.titleStyle]}
descriptionStyle={themedStyle.descriptionStyle}


          disabled
        />

         <Dashed color={theme['color-basic-600']} customStyle={themedStyle.itemSeparator} />
        <ListItem
          title="EMAIL"
          description={userInfo.email}
          style={themedStyle.receiptItem}
          titleStyle={[themedStyle.uppercase, themedStyle.titleStyle]}
descriptionStyle={themedStyle.descriptionStyle}


          disabled
        />
        <Dashed color={theme['color-basic-600']} customStyle={themedStyle.itemSeparator} />
        <ListItem
          title="INCOME"
          description={format.formatMoney(userInfo.annualIncome??0)}
          style={themedStyle.receiptItem}
          titleStyle={[themedStyle.uppercase, themedStyle.titleStyle]}
descriptionStyle={themedStyle.descriptionStyle}


          disabled
        />
        <Dashed color={theme['color-basic-600']} customStyle={themedStyle.itemSeparator} />
        <ListItem
          title="INCOME TYPE"
          description={userInfo.employmentType}
          style={themedStyle.receiptItem}
          titleStyle={[themedStyle.uppercase, themedStyle.titleStyle]}
descriptionStyle={themedStyle.descriptionStyle}


          disabled
        />
        <Dashed color={theme['color-basic-600']} customStyle={themedStyle.itemSeparator} />
        <ListItem
          title="EMPLOYER"
          description={userInfo.employerName}
          style={themedStyle.receiptItem}
          titleStyle={[themedStyle.uppercase, themedStyle.titleStyle]}
descriptionStyle={themedStyle.descriptionStyle}


          disabled
        />
        <Dashed color={theme['color-basic-600']} customStyle={themedStyle.itemSeparator} />
        <ListItem
          title="Funds will be deposited in your bank account"
          description={`${loanDetails.depositBankName} ${shared.helpers.format.formatBankAccountNumber(loanDetails.depositAccountNumber??'')}`}
          style={themedStyle.receiptItem}
          titleStyle={[themedStyle.uppercase, themedStyle.titleStyle]}
          descriptionStyle={themedStyle.descriptionStyle}
          disabled
        />
        <Dashed color={theme['color-basic-600']} customStyle={themedStyle.itemSeparator} />
        <ListItem
          title="Payments will be automatically withdrawn from"
          description={loanDetails.isAch ? `${loanDetails.withdrawalBankName ?? ''} ${shared.helpers.format.formatBankAccountNumber(loanDetails.withdrawalAccountNumber??'')}` : `Debit Card ending with ${shared.helpers.format.formatCardNumber(loanDetails.debitCardNumber??'')}`}
          style={themedStyle.receiptItem}
          titleStyle={[themedStyle.uppercase, themedStyle.titleStyle]}
          descriptionStyle={themedStyle.descriptionStyle}
          disabled
        />
        </View>
      <View style={[themedStyle.ctaContainer]}>

      {
        isLoading?
        <Loading isLoading/>
        :
        <Button themedStyle={[themedStyle.cta]} onPress={handlePress} >
              CONFIRM &amp; CONTINUE
      </Button>
      }
      { errorMessage? <Text
                        status="danger"
                        style={[themedStyle.centerText, themedStyle.uppercase, themedStyle.               subtitle, themedStyle.subHeader]}
                      >
        {errorMessage}
      </Text>:null}
      </View>

      <Text
        category="label"
        style={[themedStyle.centerText, themedStyle.uppercase, themedStyle.subtitle]}
      >
        {`To update your information:\n`}
      </Text>
      <Text
        onPress={() => {
          shared.helpers.callNumber(config.contactPhone);
        }}
        category="label"
        style={[themedStyle.centerText, themedStyle.uppercase, themedStyle.subHeader, themedStyle.callUs]}
        >
        call us at {shared.helpers.format.formatPhone(config.contactPhone.slice(2) /* remove +1 */ )}
      </Text>
    </Layout>
  );
};

export default withStyles(PersonalInfoConsent, theme => ({
  screen: {
    flex: 1,
  },
  body: {
    flexGrow: 1,
  },
  centerText: {
    textAlign: 'center',
  },
  uppercase: {
    textTransform: 'uppercase',
  },
  subtitle: {
    paddingTop: 10,
  },
  content: {
    paddingVertical: 15,
  },
  item: {
    marginBottom: 20,
    marginHorizontal: 30,
  },
  receiptMessageContainer: {
    padding: 20,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme['color-success-600'],
  },
  receiptMessageIcon: {
    tintColor: theme['color-success-100'],
  },
  receiptMessage: {
    textAlign: 'center',
    paddingTop: 10,
    justifyContent: 'center',
  },
  receiptTitle: {
    paddingVertical: 20,
  },
  receiptContainer: {
    marginTop:30,
    paddingVertical: 10,
    marginBottom: 20,
    marginHorizontal: 30,
    borderWidth: 2,
    borderColor: theme['color-basic-600'],
  },
  receiptItem: {
    paddingVertical: 10,
  },
  receiptActions: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  homeButton: {
    alignSelf: 'center',
  },
  itemIcon: {
    width: 40,
    height: 40,
    tintColor: theme['color-basic-600'],
  },
  subHeader:{
    marginTop:0,
    marginBottom: 30
  }
    ,
    ctaContainer:{
      marginHorizontal: 50,
    },
    cta:{
    marginVertical: 30,
  },
  titleStyle: {
    color: theme['color-basic-800'],
  },
  descriptionStyle: {
    color: theme['color-primary-700'],
  },
  callUs: {
    color: theme['color-primary-800'],
    fontWeight: '500'
  },
}));
