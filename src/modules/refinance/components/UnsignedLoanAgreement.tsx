import React from 'react';
import { View } from 'react-native';
import { withStyles, ThemedComponentProps, Text } from 'react-native-ui-kitten';
import TilaUnsignedContract from './TilaUnsignedContract';

type UnsignedLoanAgreementProps = {
  contractIsLoading: boolean;
  unsignedContract: any;
  contractError: string;
  electronicCommunicationsChecked: any;
  setElectronicCommunicationsChecked: any;
  loanAgreementChecked: any;
  setLoanAgreementChecked: any;
  handleFile: (attachment: string) => Promise<any>;
} & ThemedComponentProps;

const UnsignedLoanAgreement = ({
  themedStyle,
  unsignedContract,
  electronicCommunicationsChecked,
  setElectronicCommunicationsChecked,
  loanAgreementChecked,
  setLoanAgreementChecked,
  handleFile,
}: UnsignedLoanAgreementProps) => {
  return (
    <>
      <View style={themedStyle.plainTextBox}>
        <Text style={themedStyle.small}>
          Your lender is FinWise Bank (LendingPoint LLC if you are resident of Colorado, South
          Dakota, Utah or Georgia, for loan amounts greater tan $3,000)
        </Text>
      </View>
      <View style={themedStyle.unsignedContractContainer}>
        <TilaUnsignedContract
          handleFile={handleFile}
          electronicCommunicationsChecked={electronicCommunicationsChecked}
          setElectronicCommunicationsChecked={setElectronicCommunicationsChecked}
          setLoanAgreementChecked={setLoanAgreementChecked}
          loanAgreementChecked={loanAgreementChecked}
          unsignedContract={unsignedContract}
        />
      </View>
    </>
  );
};

export default withStyles(UnsignedLoanAgreement, theme => ({
  bold: { fontWeight: 'bold' },
  uppercase: { textTransform: 'uppercase' },
  centerText: { textAlign: 'center' },
  justifyText: { textAlign: 'justify' },

  container: {
    marginTop: 15,
    marginHorizontal: 50,
    margin: 0,
  },
  contractInfoContainer: {
    marginTop: 15,
    padding: 0,
  },
  tilaInfoContainer: {
    flex: 1,
    marginBottom: 15,
    flexDirection: 'row',
    alignContent: 'stretch',
    flexWrap: 'wrap',
    justifyContent: 'center',
    borderColor: theme['color-basic-600'],
    borderWidth: 1,
    margin: 0,
    padding: 10,
  },
  disclosuresTitle: {
    marginVertical: 5,
    fontWeight: '500',
    fontSize: 13,
    color: theme['color-basic-1000'],
  },
  scheduleTitle: {
    marginVertical: 0,
    fontWeight: '500',
    fontSize: 13,
    color: theme['color-basic-1000'],
  },
  infoBox: {
    width: '45%',
    margin: 5,
  },
  infoBoxFull: {
    width: '95%',
    margin: 5,
  },
  plainTextBox: {
    width: '95%',
    margin: 5,
    borderColor: theme['color-basic-800'],
    borderWidth: 1,
    padding: 8,
    paddingRight: 5,
  },
  small: {
    fontSize: 12,
    lineHeight: 13,
    color: theme['color-basic-900'],
  },
  additionalInfo: {
    marginTop: 8,
    marginBottom: 16,
  },
  scheduleInfo: {
    paddingHorizontal: 20,
  },
  contractTermsDisclaimer: {
    fontSize: 14,
    lineHeight: 14,
    marginTop: 7,
    marginHorizontal: 5,
  },
  unsignedContractContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  lastDisclaimer: {
    marginBottom: 25,
    fontSize: 11,
    lineHeight: 14,
    fontWeight: 'bold',
  },
  customBody: {
    paddingHorizontal: 5,
    paddingVertical: 8,
    flex: 1,
    width: '100%',
    flexDirection: 'row',
    alignContent: 'space-between',
  },
  customChild: {
    alignSelf: 'flex-start',
  },
  customSib: {
    alignSelf: 'flex-end',
  },
  itemizationContainer: {
    borderColor: theme['color-basic-800'],
    borderWidth: 1,
  },
  itemizationRow: {
    flex: 1,
    flexDirection: 'row',
  },
  itemizationBox: {
    flex: 0.5,
    borderColor: theme['color-basic-800'],
    borderRightWidth: 1,
    borderBottomWidth: 1,
    paddingVertical: 5,
    justifyContent: 'center',
  },
}));
