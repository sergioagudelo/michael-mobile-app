import React from 'react';
import { View } from 'react-native';
import { withStyles, ThemedComponentProps, Text } from 'react-native-ui-kitten';
import shared from '../../shared';
import ContractTilaInfoBox from './ContractTilaInfoBox';
import { Tila as TilaType } from '../utils';

const { Loading } = shared.components;
const { format } = shared.helpers;

type TilaProps = {
  opportunityId: string;
  isLoading: boolean;
  errorMessage: string;
  tila: TilaType;
} & ThemedComponentProps;

const Tila = ({ themedStyle, isLoading, tila }: TilaProps) => {
  return (
    <>
      <Text style={[themedStyle.centerText]}>
        Please confirm your loan details, complete the electronic records consent and sign your loan
        agreement.
      </Text>
      {tila ? (
        <>
          <View style={themedStyle.contractInfoContainer}>
            <View style={themedStyle.tilaInfoContainer}>
              <Text style={[themedStyle.uppercase, themedStyle.disclosuresTitle]}>
                Federal truth-in-lending disclosures
              </Text>
              <View style={themedStyle.infoBox}>
                <ContractTilaInfoBox
                  title="total of payments"
                  caption="The amount you will have paid after you've made all payments as scheduled."
                  body={`${format.formatMoney(tila.totalLoanAmount)}`}
                  isDark
                />
              </View>
              <View style={themedStyle.infoBox}>
                <ContractTilaInfoBox
                  title="Amount financed"
                  caption="The amount of credit provided to you or on your behalf."
                  body={`${format.formatMoney(tila.loanAmount)}`}
                  isDark
                />
              </View>
              <View style={themedStyle.infoBox}>
                <ContractTilaInfoBox
                  title="Finance charge"
                  caption="The dollar amount the credit will cost you."
                  body={`${format.formatMoney(tila.financeCharged)}`}
                  darkBg
                  isDark
                />
              </View>
              <View style={themedStyle.infoBox}>
                <ContractTilaInfoBox
                  title="annual percentage rate"
                  caption="The cost of your credit as a yearly rate."
                  body={`${tila.apr}%`}
                  darkBg
                  isDark
                />
              </View>

              <Text style={[themedStyle.uppercase, themedStyle.scheduleTitle]}>
                Your payment schedule will be
              </Text>

              <View style={themedStyle.infoBox}>
                <ContractTilaInfoBox title="number of payments" body={`${tila.term}`} />
              </View>

              <View style={themedStyle.infoBox}>
                <ContractTilaInfoBox
                  title="amount of payments"
                  body={`${format.formatMoney(tila.paymentAmount)}`}
                />
              </View>

              <View style={themedStyle.infoBoxFull}>
                <ContractTilaInfoBox
                  title="When payments are due"
                  customBody={() => (
                    <View style={[themedStyle.customBody]}>
                      <Text
                        style={[themedStyle.uppercase, themedStyle.small, themedStyle.customChild]}
                      >
                        Monthly
                      </Text>
                      <Text
                        style={[themedStyle.uppercase, themedStyle.small, themedStyle.customSib]}
                      >
                        Beginning on {shared.helpers.format.formatDate(tila.firstPaymentDate)}
                      </Text>
                    </View>
                  )}
                />
              </View>

              <View style={themedStyle.plainTextBox}>
                <Text style={themedStyle.small}>
                  <Text style={[themedStyle.bold, themedStyle.uppercase, themedStyle.small]}>
                    Late charge:{' '}
                  </Text>
                  If payment is not received in full within 15 fays after it is due, you will pay a
                  late charge of $30 ($15 for Colorado residents or 5% of the loan amount for Utah
                  residents).{'\n'}
                  <Text style={[themedStyle.bold, themedStyle.uppercase, themedStyle.small]}>
                    Prepayment:{' '}
                  </Text>
                  If you pay off all or part of the loan early, you may be entitled to a partial
                  refund of the unearned portion of any origination fee (if one was charged) that
                  exceeds 5% of the Final Amount Financed. If you are a resident of Colorado, South
                  Dakota or Georgia, for loans above $3,000, and you prepay all of the loan early,
                  you will not be entitled to a refund of any finance charge.
                </Text>
              </View>

              <Text
                category="label"
                style={[themedStyle.uppercase, themedStyle.contractTermsDisclaimer]}
              >
                See your loan agreement terms for any additional information about nonpayment,
                default, any required repayment in full before the scheduled due date, and
                prepayment refunds and penalties.
              </Text>
            </View>
          </View>
          <Text style={[themedStyle.small, themedStyle.additionalInfo]}>
            The Payment Schedule and all above numerical disclosures are estimates and may change
            based upon the final amount financed and the date the loan is funded.
          </Text>
          <View style={themedStyle.infoBoxFull}>
            <ContractTilaInfoBox
              title="itemization of estimated amount financed"
              customBody={() => (
                <View style={themedStyle.itemizationContainer}>
                  <View style={themedStyle.itemizationRow}>
                    <View style={themedStyle.itemizationBox}>
                      <Text style={[themedStyle.small, themedStyle.leftText]}>
                        1. Amount given directly to you
                      </Text>
                    </View>
                    <View style={themedStyle.itemizationBox}>
                      <Text style={[themedStyle.small, themedStyle.rightText]}>
                        {shared.helpers.format.formatMoney(tila.remainingRefinanceAmount)}
                      </Text>
                    </View>
                  </View>

                  <View style={themedStyle.itemizationRow}>
                    <View style={themedStyle.itemizationBox}>
                      <Text style={[themedStyle.small, themedStyle.leftText]}>
                        2. Amount paid on your account with us
                      </Text>
                    </View>
                    <View style={themedStyle.itemizationBox}>
                      <Text style={[themedStyle.small, themedStyle.rightText]}>
                        {shared.helpers.format.formatMoney(tila.refinancePayOffAmount)}
                      </Text>
                    </View>
                  </View>

                  <View style={themedStyle.itemizationRow}>
                    <View style={themedStyle.itemizationBox}>
                      <Text style={[themedStyle.small, themedStyle.leftText]}>
                        3. Amount paid to others on your behalf
                      </Text>
                    </View>
                    <View style={themedStyle.itemizationBox}>
                      <Text style={[themedStyle.small, themedStyle.rightText]}>
                        {shared.helpers.format.formatMoney(tila.remainingDirectpayAmount)}
                      </Text>
                    </View>
                  </View>

                  <View style={themedStyle.itemizationRow}>
                    <View style={themedStyle.itemizationBox}>
                      <Text style={[themedStyle.small, themedStyle.leftText]}>
                        4. Amount financed
                      </Text>
                    </View>
                    <View style={themedStyle.itemizationBox}>
                      <Text style={[themedStyle.small, themedStyle.rightText]}>
                        {shared.helpers.format.formatMoney(tila.loanAmount)}
                      </Text>
                    </View>
                  </View>

                  <View style={themedStyle.itemizationRow}>
                    <View style={themedStyle.itemizationBox}>
                      <Text style={[themedStyle.small, themedStyle.leftText]}>
                        Prepaid Finance Charge (Origination Fee)
                      </Text>
                    </View>
                    <View style={[themedStyle.itemizationBox]}>
                      <Text style={[themedStyle.small, themedStyle.rightText]}>
                        {shared.helpers.format.formatMoney(tila.fee)}
                      </Text>
                    </View>
                  </View>
                  <Text style={[themedStyle.small, themedStyle.additionalInfo]}>
                    The amounts disclosed in this itemization are estimates. Your Final Amount
                    Financed will not exceed the Amount Financed disclosed above, but it may be
                    less.
                  </Text>
                </View>
              )}
            />
          </View>
        </>
      ) : null}
      <View style={themedStyle.plainTextBox}>
        <Text style={themedStyle.small}>
          Your lender is FinWise Bank (LendingPoint LLC if you are resident of Colorado, South
          Dakota, Utah or Georgia, for loan amounts greater tan $3,000)
        </Text>
      </View>

      {isLoading ? <Loading isLoading /> : null}
    </>
  );
};

export default withStyles(Tila, theme => ({
  bold: { fontWeight: 'bold' },
  uppercase: { textTransform: 'uppercase' },
  centerText: { textAlign: 'center' },
  leftText: { textAlign: 'left' },
  rightText: { textAlign: 'right' },
  contractInfoContainer: {
    marginTop: 15,
    padding: 0,
  },
  tilaInfoContainer: {
    flex: 1,
    marginBottom: 15,
    flexDirection: 'row',
    alignContent: 'stretch',
    flexWrap: 'wrap',
    justifyContent: 'center',
    borderColor: theme['color-basic-600'],
    borderWidth: 1,
    margin: 0,
    padding: 10,
  },
  disclosuresTitle: {
    marginVertical: 5,
    fontWeight: '500',
    fontSize: 13,
    color: theme['color-basic-1000'],
  },
  scheduleTitle: {
    marginVertical: 0,
    fontWeight: '500',
    fontSize: 13,
    color: theme['color-basic-1000'],
  },
  infoBox: {
    width: '45%',
    margin: 5,
  },
  infoBoxFull: {
    width: '95%',
    margin: 5,
  },
  plainTextBox: {
    width: '95%',
    margin: 5,
    borderColor: theme['color-basic-800'],
    borderWidth: 1,
    padding: 8,
    paddingRight: 5,
  },
  small: {
    fontSize: 12,
    lineHeight: 13,
    color: theme['color-basic-900'],
  },
  additionalInfo: {
    marginTop: 8,
    paddingHorizontal: 5,

    marginBottom: 16,
  },
  scheduleInfo: {
    paddingHorizontal: 20,
  },
  contractTermsDisclaimer: {
    fontSize: 14,
    lineHeight: 14,
    marginTop: 7,
    marginHorizontal: 5,
  },
  customBody: {
    flex: 1,
    width: '100%',
    paddingHorizontal: 5,
    paddingVertical: 8,
    flexDirection: 'row',
  },
  customChild: {
    flex: 0.4,
  },
  customSib: {
    flex: 0.6,
    alignSelf: 'flex-end',
  },
  itemizationContainer: {
    borderColor: theme['color-basic-800'],
    borderWidth: 1,
  },
  itemizationRow: {
    flex: 1,
    flexDirection: 'row',
  },
  itemizationBox: {
    flex: 0.5,
    borderColor: theme['color-basic-800'],
    borderRightWidth: 1,
    borderBottomWidth: 1,
    paddingVertical: 5,
    justifyContent: 'center',
    paddingHorizontal: 5,
  },
}));
