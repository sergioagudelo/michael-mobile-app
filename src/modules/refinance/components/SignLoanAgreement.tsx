import React from 'react';
import * as Progress from 'react-native-progress';
import { View } from 'react-native';
import { withStyles, ThemedComponentProps, Text, Button } from 'react-native-ui-kitten';
import shared from '../../shared';
import TilaSignature from './TilaSignature';
import { User } from '../../profile/utils';

const { Loading } = shared.components;

type SignLoanAgreementProps = {
  userInfo: User;
  unsignedContract: any;
  signAgreementIsLoading: boolean;
  signAgreementError: string;
  handleSubmit: any;
  electronicCommunicationsChecked: any;
  loanAgreementChecked: any;
  captureSignature: any;
  signatureShot: any;
  handleFile: (attachment: string) => Promise<any>;
} & ThemedComponentProps;

const SignLoanAgreement = ({
  theme,
  themedStyle,
  userInfo,
  unsignedContract,
  signAgreementIsLoading,
  signAgreementError,
  handleSubmit,
  electronicCommunicationsChecked,
  loanAgreementChecked,
  captureSignature,
  signatureShot,
  handleFile,
}: SignLoanAgreementProps) => {
  return (
    <>
      {userInfo ? (
        <TilaSignature
          captureSignature={captureSignature}
          signatureShot={signatureShot}
          name={`${userInfo.firstName} ${userInfo.lastName}`}
        />
      ) : null}
      <Text
        category="metatext"
        style={[
          themedStyle.uppercase,
          themedStyle.small,
          themedStyle.bold,
          themedStyle.centerText,
          themedStyle.lastDisclaimer,
        ]}
      >
        Please review your{' '}
        <Text
          style={[themedStyle.bold, themedStyle.small]}
          category="metatext"
          status="info"
          onPress={async () =>
            // eslint-disable-next-line no-return-await
            unsignedContract ? await handleFile(unsignedContract.fileContent) : null
          }
        >
          Loan agreement
        </Text>{' '}
        {!unsignedContract ? (
          <Progress.CircleSnail
            style={[themedStyle.miniLoader, themedStyle.miniLoaderSize]}
            size={11}
            thickness={1}
            color={[theme['color-warning-500'], theme['color-primary-500']]}
          />
        ) : ''}
        to read all of the loan terms and important disclosures about the loan including late
        charges, payment authorization, arbitration requirements and certain state disclosures.
      </Text>

      <View style={themedStyle.unsignedContractContainer}>
        {signAgreementError ? (
          <Text category="p1" status="danger" style={themedStyle.centerText}>
            {signAgreementError}
          </Text>
        ) : null}
        {signAgreementIsLoading ? (
          <Loading isLoading />
        ) : (
          <Button
            onPress={() => handleSubmit()}
            disabled={!loanAgreementChecked || !electronicCommunicationsChecked}
            textStyle={[themedStyle.uppercase, themedStyle.centerText]}
          >
            Sign loan agreement &amp; consent to electronic communications
          </Button>
        )}
      </View>
    </>
  );
};

export default withStyles(SignLoanAgreement, () => ({
  small: {
    fontSize: 11,
    lineHeight: 14,
  },
  bold: { fontWeight: 'bold' },
  uppercase: { textTransform: 'uppercase' },
  centerText: { textAlign: 'center' },
  container: {
    marginTop: 15,
    marginHorizontal: 50,
    margin: 0,
  },
  infoBox: {
    width: '45%',
    margin: 5,
  },

  unsignedContractContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  lastDisclaimer: {
    marginTop: 15,
    marginBottom: 25,
  },
  miniLoader: {
    paddingRight: 5,
  },
  miniLoaderSize: {
    width: 11,
    height: 11,
  }
}));
