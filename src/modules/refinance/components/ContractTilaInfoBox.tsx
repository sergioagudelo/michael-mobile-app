import React from 'react';
import { View } from 'react-native';
import { Text, withStyles, ThemedComponentProps } from 'react-native-ui-kitten';

type ContractTilaInfoBoxProps = {
  title: string;
  caption?: string;
  body?: string;
  customBody?: any;
  isDark?: boolean;
  darkBg?: true;
} & ThemedComponentProps;

const ContractTilaInfoBox = ({
  title,
  caption,
  body,
  customBody,
  isDark = false,
  darkBg,
  themedStyle,
  theme,
}: ContractTilaInfoBoxProps) => {
  return (
    <View
      style={[
        themedStyle.cardContainer,
        darkBg ? { borderWidth: 2, backgroundColor: theme['color-basic-300'] } : null,
      ]}
    >
      <View style={[themedStyle.titleContainer, isDark ? themedStyle.dark : themedStyle.light]}>
        <Text
          style={[
            themedStyle.small,
            themedStyle.uppercase,
            themedStyle.centerText,
            themedStyle.bold,
            isDark ? { color: theme['color-basic-200'] } : { color: theme['color-basic-800'] },
          ]}
          category="metatext"
        >
          {title}
        </Text>
      </View>

      {caption ? (
        <View style={[themedStyle.captionContainer]}>
          <Text style={[themedStyle.centerText, themedStyle.small]}>{caption}</Text>
        </View>
      ) : null}
      {body ? (
        <View style={[themedStyle.bodyContainer]}>
          <Text category="h5" style={[themedStyle.centerText, themedStyle.bodyContent]}>
            {body}
          </Text>
        </View>
      ) : null}
      {customBody ? customBody() : null}
    </View>
  );
};

export default withStyles(ContractTilaInfoBox, theme => ({
  small: { fontSize: 10, lineHeight: 10 },
  bold: { fontWeight: 'bold' },
  uppercase: { textTransform: 'uppercase' },
  centerText: { textAlign: 'center' },
  cardContainer: {
    borderColor: theme['color-basic-800'],
    borderWidth: 1,
    flex: 1,
  },
  titleContainer: {
    borderBottomColor: theme['color-basic-800'],
    borderBottomWidth: 1,
    padding: 5,
  },
  captionContainer: {
    padding: 5,
  },
  bodyContainer: {
    padding: 5,
    paddingTop: 0,
  },
  bodyContent: {
    marginVertical: 5,
    color: theme['color-basic-1000'],
  },
  light: {
    backgroundColor: theme['color-basic-300'],
  },
  dark: {
    backgroundColor: theme['color-basic-800'],
  },
}));
