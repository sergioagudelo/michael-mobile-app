import React from 'react';
import { View, Text } from 'react-native';
import ViewShot from 'react-native-view-shot';
import { withStyles, ThemedComponentProps } from 'react-native-ui-kitten';
import shared from '../../shared';

type TilaSignatureProps = {
  name: string;
  captureSignature: any;
  signatureShot: any;
} & ThemedComponentProps;
const { Dashed } = shared.components;
const TilaSignature = ({
  name,
  captureSignature,
  signatureShot,
  themedStyle,
  theme,
}: TilaSignatureProps) => {
  return (
    <ViewShot ref={signatureShot} options={{ format: 'png', result: 'base64' }}>
      <View style={themedStyle.signatureContainer}>
        <Text style={themedStyle.signature} onPress={captureSignature}>
          {name}
        </Text>
        <Dashed color={theme['color-info-600']} customStyle={themedStyle.separator} />
        <Text style={[themedStyle.small, themedStyle.centerText]}>
          Your above signature will be placed on the Consent to Electronic Communications and LOAN
          AGREEMENT and the documents will be emailed to you after you click the below "SIGN LOAN
          AGREEMENT &amp; CONSENT TO ELECTRONIC COMMUNICATIONS" button.
        </Text>
      </View>
    </ViewShot>
  );
};

export default withStyles(TilaSignature, theme => ({
  centerText: { textAlign: 'center' },
  small: {
    fontSize: 10,
    lineHeight: 12,
    color: theme['color-basic-1100'],
  },
  signatureContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  signature: {
    fontFamily: 'GreatVibes-Regular',
    fontSize: 35,
  },
  separator: {
    marginTop: 10,
    marginBottom: 15,
    elevation: 5,
  },
}));
