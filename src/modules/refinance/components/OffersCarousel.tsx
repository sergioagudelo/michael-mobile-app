import React, { useRef, useState } from 'react';
// import { View } from 'react-native';
import { Layout, Text, withStyles, ThemedComponentProps, Icon, Button } from 'react-native-ui-kitten';
// import { HTTPHooksResponse } from '../../../services/utils';
import Carousel from 'react-native-snap-carousel';
import { RefinanceOffer } from '../utils';
import OfferCarouselItem from './OfferCarouselItem';

// import shared from '../../shared';

type OffersCarouselProps = {
  offers: RefinanceOffer[];
  handleSelectOffer: (offerLoanAmount: any) => Promise<void>;
  isLoading?: boolean;
  errorMessage?: string | null;
} &ThemedComponentProps

const OffersCarousel = ({
  offers,
  handleSelectOffer,
  isLoading,
  errorMessage,
  themedStyle,

}: OffersCarouselProps) => {
  const carouselRef = useRef<any>('');
  const [activeIndex, setActiveIndex] = useState(1)
  return (
    <>
      <Layout>
        <Carousel
          ref={(c : any) => (carouselRef.current = c)}
          onBeforeSnapToItem={(i) => setActiveIndex(i+1)}
          scrollEnabled={!isLoading}
          data={offers}
          enableMomentum
          loop

          renderItem={({ item }) => (
            <OfferCarouselItem
              offer={item as RefinanceOffer}
              onSubmit={handleSelectOffer}
              isLoading={isLoading}
              errorMessage={errorMessage ?? ''}
            />
          )}

          containerCustomStyle={themedStyle.containerCustomStyle}
          sliderWidth={400}
          itemWidth={330}
          inactiveSlideScale={1}
        />
        <Layout style={themedStyle.buttonsContainer}>
          <Button
            size="tiny"
            icon={style => <Icon name="arrow-back" {...style} width={45} height={45} />}
            style={themedStyle.carouselButton}
            onPress={() => { return !isLoading ? carouselRef.current.snapToPrev() : null }}


          />
          <Text category="h2">{`${activeIndex} of ${offers ? offers.length : ''}`}</Text>
          <Button
            size="tiny"
            icon={style => <Icon name="arrow-forward" {...style} width={45} height={45} />}
            style={themedStyle.carouselButton}
            onPress={() => { return !isLoading ? carouselRef.current.snapToNext(): null }}
          />
        </Layout>
        <Text category="label" style={[themedStyle.centerText, themedStyle.disclaimer]}>
          Estimated deposit amount. Once approved, your new loan will be applied to your current payoff balance and the remaining amount will be deposited into your bank account.
        </Text>
      </Layout>
    </>
  );
};

export default withStyles(OffersCarousel, theme =>({
  centerText:{
    textAlign:'center'
  },
  containerCustomStyle: {
    flexGrow: 0,
  },
  buttonsContainer:{
    flexDirection:"row",
    justifyContent:'space-between',
    alignItems:'center',
    paddingHorizontal: 40
  },
  carouselButton:{
    borderRadius: 12,
    borderWidth:0,
    backgroundColor: theme['color-primary-700'],
    width:60,
    height:60
  },
  disclaimer:{
    marginTop: 30,
    paddingHorizontal:60

  },
}));
