import React from 'react';
import * as Animatable from 'react-native-animatable';

import { Image, View, TouchableOpacity, Dimensions} from 'react-native';
import { ThemedComponentProps, withStyles, Text, Button, Layout } from 'react-native-ui-kitten';

import { moderateScale } from 'react-native-size-matters';
import shared from '../../shared';
// Components
import { RefinanceOpportunity } from '../utils';

const {width, height} = Dimensions.get('window');
const { helpers: { format } } = shared

type RefinancingCardProps = {
  opportunity: RefinanceOpportunity;
  onPress: () => void;
} & ThemedComponentProps;

const RefinancingCard = ({ opportunity, onPress, themedStyle }: RefinancingCardProps) => {
  return (
    <View style={[themedStyle.loanContainer]}>
      <Animatable.View
        iterationCount={2}
        animation="pulse"
        duration={300}
        delay={100}
        useNativeDriver
      >
        <TouchableOpacity
          onPress={onPress}
          {...shared.helpers.setTestID(`LoanDetailsBtn_${opportunity.id}`)}
        >
          <Layout style={[themedStyle.loan, themedStyle.borderInfo]}>
            <View style={[themedStyle.row, themedStyle.cardBody]}>
              <View style={themedStyle.leftCol}>
                <Image
                  style={themedStyle.icon}
                  source={require('../../../img/icons/ico_refi.png')}
                />
              </View>
              <View style={[themedStyle.rightCol, themedStyle.centerText]}>
                <View style={themedStyle.textContainer}>
                  <Text status="primary" category="h2" style={themedStyle.header}>Good news!</Text>
                  <Text style={[themedStyle.uppercase, themedStyle.marginBottom]} category="metatext">Your on-time payments have earned you access to more money.</Text>
                </View>
                <Button status="info" size="medium" style={[themedStyle.uppercase, themedStyle.footer, themedStyle.centerText, themedStyle.refinanceButton]}
                    onPress={onPress}>
                    {`GET ${format.formatMoney(opportunity.disbursalAmount ?? 0)} MORE!`}
                  </Button>
              </View>
            </View>
          </Layout>
        </TouchableOpacity>
      </Animatable.View>
    </View>
  );
};

export default withStyles(RefinancingCard, theme => ({
  uppercase: {
    textTransform: 'uppercase',
  },
  centerText: {
    textAlign: 'center',
  },
  row: {
    flexDirection: 'row',
  },
  loanContainer: {
    marginHorizontal: 30,
    marginVertical: 20,
  },
  loan: {
    paddingHorizontal: 20,
    paddingVertical: 20,
    borderWidth: 2,
    minHeight: moderateScale(160),
    justifyContent: 'space-evenly',
  },
  borderInfo: {
    borderColor: theme['color-primary-200'],
  },
  backInfo: {
    backgroundColor: theme['text-info-color'],
  },
  cardBody: {
    justifyContent: 'space-between',
    marginTop: 5
  },
  header: {
    marginBottom: 5
  },
  marginBottom: {
    marginBottom: 15
  },
  rightCol: {
    alignSelf: 'flex-end',
    flex: .95,
    margin: 0
  },
  textContainer: {
    paddingLeft: 20
  },
  leftCol: {
    alignSelf: 'flex-start',
  },
  footer: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
  icon: {
    marginTop: 10,
    width: 80,
    height: 80,
    resizeMode: 'contain'
  },
  circleProgress: {
    width: 20,
    height: 20
  },
  refinanceButton: {
    borderRadius: 20,
    borderWidth: 0,
  }
}));



