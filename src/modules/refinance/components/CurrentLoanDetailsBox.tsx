import React from 'react';
import { View } from 'react-native';
import { Text, ThemedComponentProps, withStyles } from 'react-native-ui-kitten';

type CurrentLoanDetailsBox = {
  label: string;
  content: string;
  bottomLabel?: string;
} & ThemedComponentProps;

const CurrentLoanDetailsBox = ({
  label,
  content,
  bottomLabel,
  themedStyle,
}: CurrentLoanDetailsBox) => {
  return (
    <View style={themedStyle.container}>
      <Text style={[themedStyle.label, themedStyle.uppercase]}>{label}</Text>
      <Text status="primary" category="h4" style={themedStyle.content}>
        {content}
      </Text>
      {bottomLabel ? (
        <Text category="label" style={[themedStyle.uppercase, themedStyle.bottomLabel]}>
          {bottomLabel}
        </Text>
      ) : null}
    </View>
  );
};

export default withStyles(CurrentLoanDetailsBox, theme => ({
  uppercase: {
    textTransform: 'uppercase',
  },
  container: {
    alignItems: 'center',
    marginHorizontal: 5,
  },
  label: {
    fontSize: 13,
    fontWeight: '400',
    color: theme['color-basic-800'],
  },
  content: {
    fontWeight: '600',
    fontSize: 16,
  },
  bottomLabel: {
    color: theme['color-basic-800'],
    fontWeight: '400',
    fontSize: 10,
    lineHeight: 10,
  },
}));
