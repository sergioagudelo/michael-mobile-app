import React, { useState } from 'react'
import { View } from 'react-native'
import { withStyles, ThemedComponentProps, Text, Layout, Icon } from 'react-native-ui-kitten'
import Collapsible from 'react-native-collapsible';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { RefinanceOpportunity, RefinanceOffer, frequencyValueMap } from '../utils';
import { LoanDetailsType } from '../../loans/utils';

import shared from "../../shared";

import CurrentLoanDetailsBox from "./CurrentLoanDetailsBox";

const { Dashed } = shared.components;



type CurrentLoanDetailsComponentProps = { opportunity: RefinanceOpportunity, loanDetails: LoanDetailsType } & ThemedComponentProps;

const CurrentLoanDetails = ({ theme, themedStyle, opportunity, loanDetails }: CurrentLoanDetailsComponentProps) => {
  const [collapsed, setCollapsed] = useState(false)
  // setTimeout(() => !collapsed ? setCollapsed(true) : null, 3000)
  return (
    <>
      <View style={themedStyle.content}>

        <Text
          appearance="alternative"
          category="c1"
          style={[themedStyle.centerText, themedStyle.uppercase, themedStyle.subtitle]}
        >
          {` Your on-time payments have earned you access to more money. Get an extra ${shared.helpers.format.formatMoney(
            opportunity.disbursalAmount ?? 0
          )}.`}
        </Text>
      </View>
      <Layout style={[themedStyle.cardContainer, themedStyle.centerText]} >
        <TouchableOpacity onPress={() => setCollapsed(!collapsed)} style={[themedStyle.cardHeader, themedStyle.centerText]} >
          <Text category="label" status="primary" style={[themedStyle.uppercase, themedStyle.collapsibleHeader]}>
            Your current loan balance
          </Text>
          <Icon style={themedStyle.icon} name="chevron-up" width={40} height={40} fill={theme['color-basic-600']} />
        </TouchableOpacity>

        <Collapsible collapsed={collapsed}
          duration={30}
        >
          <Dashed color={theme['color-info-500']} customStyle={themedStyle.separator} />
          <View style={themedStyle.currentOfferContainer}>
            <CurrentLoanDetailsBox
              label="Amount"
              content={shared.helpers.format.formatMoney(loanDetails.loanPayOffAmountAsOfToday)}
              bottomLabel="Current Balance"
            />
            <CurrentLoanDetailsBox
              label="Payment"
              content={shared.helpers.format.formatMoney(loanDetails.paymentAmount)}
              bottomLabel="Monthly"
            />
            <CurrentLoanDetailsBox
              label="Term"
              content={loanDetails.remainingTerm.toString()}
              bottomLabel="Remaining"
            />
            <CurrentLoanDetailsBox
              label="APR"
              content={`${loanDetails.apr}%`}
            />
          </View>
        </Collapsible>
      </Layout>
    </>
  )
}

export default withStyles(CurrentLoanDetails, theme => ({
  centerText: {
    textAlign: 'center',
  },
  uppercase: {
    textTransform: 'uppercase',
  },
  content: {
    marginTop: 25,
  },

  subtitle: {
    paddingVertical: 20,
    paddingHorizontal: 20,
  },
  currentLoanHeader: {
    marginHorizontal: 30,
  },
  itemTextDescription: {
    fontWeight: "bold",
  },
  cardHeader: {
    flexDirection: 'row',
    alignItems: "center",
    alignContent: "space-between",
  },
  collapsibleHeader: {
    color: theme['color-primary-700'],
    fontWeight: '600'
  },
  icon: {
    margin: 0,
    alignSelf: 'flex-end'
  },
  cardContainer: {
    marginHorizontal: 30,
    paddingHorizontal: 0,
    paddingBottom: 10,
    paddingTop: 5,
    alignItems: 'center',
    borderColor: theme['color-basic-400'],
    borderWidth: 1,
    elevation: 2,
    ...shared.helpers.shadow(2),
  },
  currentOfferContainer: {
    flexDirection: 'row',
    alignContent: "space-between",
  },
  separator: {
    marginVertical: 5,
    elevation: 9,
  },
}))
