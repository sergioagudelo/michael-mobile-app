import React from 'react';
import * as Progress from 'react-native-progress';

import { View } from 'react-native';

import { Text, withStyles, ThemedComponentProps, CheckBox } from 'react-native-ui-kitten';

import shared from '../../shared';
import config from '../../../config';

const TilaUnsignedContract = ({
  unsignedContract,
  themedStyle,
  theme,
  electronicCommunicationsChecked,
  setElectronicCommunicationsChecked,
  setLoanAgreementChecked,
  loanAgreementChecked,
  handleFile,
}: {
  unsignedContract: any;
  electronicCommunicationsChecked: any;
  setElectronicCommunicationsChecked: any;
  setLoanAgreementChecked: any;
  loanAgreementChecked: any;
  handleFile: (attachment: string) => Promise<any>;
} & ThemedComponentProps) => {
  return (
    <View style={themedStyle.container}>
      <View style={themedStyle.toggleContainer}>
        <CheckBox
          onChange={checked => setElectronicCommunicationsChecked(checked)}
          appearance="default"
          status="info"
          checked={electronicCommunicationsChecked}
          style={themedStyle.checkBox}
        />
        <Text category="metatext" style={themedStyle.toggleLabel}>
          By clicking the box, you agree that you have reviewed and agree to the{' '}
          <Text
            style={[themedStyle.uppercase, themedStyle.link]}
            status="info"
            onPress={() => shared.helpers.openUrl(config.electronicCommunicationsUrl)}
          >
            Consent to electronic communications
          </Text>{' '}
          terms (click blue hyperlink to view), and agree to affix your electronic signature
          thereto, which provide among others things that communications, notices, documents, and
          signatures on documents may be done via electronic means and format. You further agree
          that your electronic signature shall bind you in the same manner and to same extent as a
          physical signature would do, in accordance with the Electronic Signatura in Global and
          National Commerce Act (ESIGN) to the extent applicable.
        </Text>
      </View>
      <View style={themedStyle.toggleContainer}>
        <CheckBox
          onChange={checked => setLoanAgreementChecked(checked)}
          appearance="default"
          status="info"
          checked={loanAgreementChecked}
          style={themedStyle.checkBox}
        />
        <Text category="metatext" style={themedStyle.toggleLabel}>
          By clicking the box, you agree (1) that you have reviewed and agree to the terms set forth
          in the{' '}
          <Text
            style={[themedStyle.uppercase, themedStyle.link]}
            status="info"
            onPress={async () =>
              // eslint-disable-next-line no-return-await
              unsignedContract ? await handleFile(unsignedContract.fileContent) : null
            }
          >
            Loan agreement
          </Text>{' '}
          {!unsignedContract ? (
            <Progress.CircleSnail
              style={[themedStyle.miniLoader, themedStyle.miniLoaderSize]}
              size={15}
              thickness={1}
              color={[theme['color-primary-500'], theme['color-warning-500']]}
            />
          ) : ''}
          (click blue hyperlink to view), (2) that signing below will affix your electronic
          signature to the LOAN AGREEMENT, (3) to a credit check inquiry that may affect your credit
          score, and (4) that the LOAN AGREEMENT is conditional upon final approval by the lender.
        </Text>
      </View>
    </View>
  );
};

export default withStyles(TilaUnsignedContract, () => ({
  uppercase: { textTransform: 'uppercase' },
  container: {
    paddingTop: 20,
    paddingRight: 25,
  },
  toggleContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    paddingHorizontal: 25,
  },
  toggleLabel: {
    marginLeft: 10,
  },
  checkBox: {
    margin: 0,
  },
  link: {
    fontSize: 14,
  },
  miniLoaderSize: {
    width: 15,
    height: 15,
  }
}));
