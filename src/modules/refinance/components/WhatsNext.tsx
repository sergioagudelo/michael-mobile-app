import React from 'react';
import { Image } from 'react-native';
import { withStyles, Text, Layout, ThemedComponentProps, Button } from 'react-native-ui-kitten';

import { withNavigation } from 'react-navigation';
import { NavigationStackScreenProps } from 'react-navigation-stack';
import shared from '../../shared';
import { formatMoney } from '../../shared/helpers/format';

const { Dashed } = shared.components;

type WhatsNextProps = {
  paymentAmount: string;
  dueDate: string;
  extraMoneyAmount: string;
  yearlyPaymentSavings: string | boolean;
} & ThemedComponentProps &
  NavigationStackScreenProps;

const WhatsNext = ({
  paymentAmount,
  dueDate,
  extraMoneyAmount,
  yearlyPaymentSavings,
  themedStyle,
  theme,
  navigation,
}: WhatsNextProps) => {
  const handleGoHome = () => navigation.navigate('Dashboard');
  return (
    <Layout style={themedStyle.container}>
      <Text style={[themedStyle.centerText]}>
        Thank you for being the best part of Lendingpoint. You'll soon receive an email with your
        new loan details for your records.
      </Text>
      <Text
        style={[
          themedStyle.bold,
          themedStyle.uppercase,
          themedStyle.centerText,
          themedStyle.nextPaymentLabel,
        ]}
      >
        your next payment of{' '}
        <Text
          status="primary"
          style={[themedStyle.bold, themedStyle.uppercase, themedStyle.paymentAmount]}
        >
          {paymentAmount}
        </Text>
      </Text>
      <Dashed color={theme['color-primary-600']} customStyle={themedStyle.separator} />
      <Text
        status="primary"
        style={[themedStyle.bold, themedStyle.uppercase, themedStyle.centerText]}
      >
        Will be due on {dueDate}
      </Text>
      <Text
        appearance="hint"
        style={[themedStyle.uppercase, themedStyle.centerText, themedStyle.dueDateLabel]}
      >
        We'll send you a reminder via email 10 days prior to your due date.
      </Text>
      <Image style={themedStyle.icon} source={require('../../../img/icons/ico_refi.png')} />
      <Text
        style={[
          themedStyle.bold,
          themedStyle.uppercase,
          themedStyle.centerText,
          themedStyle.nextPaymentLabel,
        ]}
      >
        with this new loan you get
      </Text>
      <Dashed color={theme['color-primary-600']} customStyle={themedStyle.separator} />
      <Text
        status="primary"
        style={[themedStyle.bold, themedStyle.uppercase, themedStyle.centerText]}
      >
        {extraMoneyAmount} more
      </Text>
      <Dashed color={theme['color-basic-600']} customStyle={themedStyle.separator} />
      {
        yearlyPaymentSavings ? 
        <Text
          status="primary"
          style={[themedStyle.bold, themedStyle.uppercase, themedStyle.centerText]}
        >
          Yearly payment savings: {`${formatMoney(yearlyPaymentSavings ? Number(yearlyPaymentSavings.toString()) : 0)}`}
        </Text> : null
      }

      {
        yearlyPaymentSavings ?
        <Dashed color={theme['color-basic-600']} customStyle={themedStyle.separator} /> : null
      }
      
      
      <Button style={themedStyle.cta} textStyle={themedStyle.uppercase} onPress={handleGoHome}>
        Go home
      </Button>
    </Layout>
  );
};

export default withStyles(withNavigation(WhatsNext), theme => ({
  uppercase: {
    textTransform: 'uppercase',
  },
  centerText: {
    textAlign: 'center',
  },
  bold: {
    fontWeight: '600',
  },
  container: {
    paddingVertical: 30,
    paddingHorizontal: 50,
  },
  nextPaymentLabel: {
    marginTop: 15,
    color: theme['color-basic-900'],
    fontSize: 15,
  },
  paymentAmount: {
    fontSize: 17,
  },
  separator: {
    marginVertical: 10,
  },
  dueDateLabel: {
    paddingVertical: 5,
    fontSize: 13,
    lineHeight: 15,
  },
  icon: {
    alignSelf: 'center',
    marginTop: 10,
    width: 120,
    height: 120,
    resizeMode: 'contain',
  },
  cta: {
    marginTop: 15,
  },
})) as React.ElementType;
