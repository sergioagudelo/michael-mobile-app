import React from 'react';
import { View, StyleProp, TextStyle } from 'react-native';
import { Text, withStyles, ThemedComponentProps } from 'react-native-ui-kitten';

type DetailItemComponentProps = ThemedComponentProps & {
  label: string;
  content: string;
  labelStyle?: StyleProp<TextStyle>;
  contentStyle?: StyleProp<TextStyle>;
};

const DetailItemComponent = ({
  label,
  content,
  labelStyle,
  contentStyle,
  themedStyle,
}: DetailItemComponentProps) => (
  <View style={themedStyle.container}>
    <View style={themedStyle.labelContainer}>
      <Text category="metatext" style={[themedStyle.uppercase, labelStyle]}>
        {label}
      </Text>
    </View>
    <View style={themedStyle.separator} />
    <View style={themedStyle.contentContainer}>
      <Text category="s1" status="accent" style={[contentStyle]}>
        {content}
      </Text>
    </View>
  </View>
);

const CardRowComponent = withStyles(DetailItemComponent, () => ({
  uppercase: {
    textTransform: 'uppercase',
  },
  container: {
    paddingVertical: 0,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  labelContainer: {
    flex: 1.5,
    alignItems: 'flex-end',
  },
  separator: {
    width: 10,
  },
  contentContainer: {
    flex: 1,
    alignItems: 'flex-start',
    marginRight: 30,
  },
}));

export default CardRowComponent;
