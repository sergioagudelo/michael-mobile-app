import React from 'react';
import { View } from 'react-native';
import { Layout, Button, withStyles, ThemedComponentProps, Text } from 'react-native-ui-kitten';

import CardRowComponent from './CardRowComponent';
import { RefinanceOffer } from '../utils';
import shared from '../../shared';

import { Loading } from '../../shared/components';

const {
  format: { formatMoney, formatPercent },
} = shared.helpers;
const { Dashed } = shared.components;

type OfferCarouselItemProps = {
  offer: RefinanceOffer;
  onSubmit: (offerLoanAmount: number) => Promise<void>;
  isLoading?: boolean;
  errorMessage?: string;
} & ThemedComponentProps;

const OfferCarouselItem = ({
  offer,
  onSubmit,
  isLoading,
  errorMessage,
  themedStyle,
  theme,
}: OfferCarouselItemProps) => {
  return (
    <>
      <View style={themedStyle.item}>
        <Layout style={themedStyle.container}>
          <Text
            category="label"
            appearance="hint"
            style={[themedStyle.centerText, themedStyle.uppercase, themedStyle.bold]}
          >
            new loan amount
          </Text>
          <Text category="h1" status="primary" style={[themedStyle.centerText, themedStyle.bold]}>
            {formatMoney(offer.loanAmount)}
          </Text>

          <Dashed color={theme['color-basic-600']} customStyle={themedStyle.separator} />

          {
            offer.yearlySavings ?
            <View style={themedStyle.yearlySavingsTag}>
              <Text style={[themedStyle.uppercase, themedStyle.yearlySavingsText, themedStyle.bold]}>Save</Text>
              <Text style={[themedStyle.uppercase, themedStyle.yearlySavingsAmount, themedStyle.bold]}>{`${formatMoney(offer.yearlySavings)}`}</Text>
              <Text style={[themedStyle.uppercase, themedStyle.yearlySavingsText, themedStyle.bold]}>Yearly</Text>
            </View> : null
          }

          <View style={themedStyle.content}>
            <CardRowComponent
              label="PAYMENT"
              content={formatMoney(offer.paymentAmount, {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2,
              })}
            />
            <CardRowComponent label="NEW LOAN TERM" content={offer.term.toString()} />
            <CardRowComponent label="FREQUENCY" content="Monthly" />
            <CardRowComponent label="NEW APR" content={`${formatPercent(offer.aprPercent, {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2,
              })}`} />
            <CardRowComponent
              label="ORIGINATION FEE"
              content={formatMoney(offer.fee, {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2,
              })}
            />
          </View>

          <Dashed color={theme['color-basic-600']} customStyle={themedStyle.separator} />
          <Text
            category="label"
            appearance="hint"
            style={[themedStyle.centerText, themedStyle.uppercase, themedStyle.bold]}
          >
            Estimated deposit amount
          </Text>
          <Text category="h1" status="primary" style={[themedStyle.centerText, themedStyle.bold]}>
            {formatMoney(offer.disbursalAmount)}
          </Text>
        </Layout>
        {isLoading ? (
          <Loading isLoading />
        ) : (
          <Button style={themedStyle.cta} onPress={() => onSubmit(offer.loanAmount)}>
            SELECT OFFER
          </Button>
        )}
        {errorMessage ? (
          <Text category="p1" status="danger" style={themedStyle.centerText}>
            {errorMessage}
          </Text>
        ) : null}
      </View>
    </>
  );
};

export default withStyles(OfferCarouselItem, theme => ({
  item: {
    paddingVertical: 15,
    marginHorizontal: 10,
  },
  lastPaymentButton: {
    alignSelf: 'center',
  },
  cta: {
    alignSelf: 'center',
    marginTop: -25,
    minWidth: 200,
    elevation: 2,
    marginBottom: 0,
  },
  bold: {
    fontWeight: 'bold',
  },
  uppercase: {
    textTransform: 'uppercase',
  },
  centerText: {
    textAlign: 'center',
  },
  container: {
    paddingVertical: 30,
    paddingHorizontal: 20,
    paddingBottom: 35,
    borderColor: theme['color-primary-500'],
    borderWidth: 2,
    elevation: 2,
    ...shared.helpers.shadow(2),
  },
  content: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  separator: {
    marginVertical: 15,
  },
  yearlySavingsTag: {
    width: 80,
    backgroundColor: theme['color-info-500'],
    alignItems: 'center',
    position: 'absolute',
    top: 60,
    left: 240,
    paddingTop: 8,
    paddingBottom: 8,
    
  },
  yearlySavingsText: {
    fontSize: 10,
    color: 'white',
    lineHeight: 15
  },
  yearlySavingsAmount: {
    color: 'white'
  }
}));
