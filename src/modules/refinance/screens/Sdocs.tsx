import React from 'react';
// import { ContractAgreement as ContractAgreementContainer } from '../containers';
import { View } from 'react-native';
import { ThemedComponentProps, withStyles } from 'react-native-ui-kitten';

import { ScrollView } from 'react-native-gesture-handler';
import { Tila, SignLoanAgreement } from '../containers';

import shared from '../../shared';
import { useHTTP } from '../../../services/hooks';
import { createApiManagerToken } from '../services';
import { Loading } from '../../shared/components';

const { HeaderView } = shared.components;

const Sdocs = ({ themedStyle }: ThemedComponentProps) => {
  const { isLoading, payload: success } = useHTTP({
    httpFunction: createApiManagerToken,
    callOnComponentMount: true,
  });

  return (
    <ScrollView>
      <View style={themedStyle.container}>
        <HeaderView title="Sign Agreement" subTitle="Review and sign your contract" />
        {isLoading ? <Loading isLoading /> : null}
        {success ? (
          <>
            <Tila />
            <SignLoanAgreement />
          </>
        ) : null}
      </View>
    </ScrollView>
  );
};

export default withStyles(Sdocs, () => ({
  container: {
    marginTop: 15,
    marginHorizontal: 40,
    margin: 0,
  },
}));
