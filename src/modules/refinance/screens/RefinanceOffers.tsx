import React from 'react';
import { SafeAreaView, ScrollView, View } from 'react-native';
import { withStyles, ThemedComponentProps } from 'react-native-ui-kitten';
import { withNavigation } from 'react-navigation';
import { NavigationStackScreenProps } from 'react-navigation-stack';

// Modules
import shared from '../../shared';

import { Offers, Congrats, CurrentLoanDetails } from '../containers';

// Components
const { Background } = shared.components;

type ConsolidateOffersComponentProps = NavigationStackScreenProps & ThemedComponentProps;

const RefinanceOffers = ({ themedStyle, theme }: ConsolidateOffersComponentProps) => {
  return (
    <SafeAreaView
      style={themedStyle.screen}
      {...shared.helpers.setTestID('ConsolidateOffersScreen')}
    >
      <ScrollView contentContainerStyle={themedStyle.body}>
        <Congrats />
        <View>
          <Background
            colors={[theme['color-info-900'], theme['color-info-500'], theme['color-info-900']]}
            wrapperStyle={themedStyle.wrapper}
            minHeight={142}
          />
        </View>
        <CurrentLoanDetails />
        <Offers />
      </ScrollView>
    </SafeAreaView>
  );
};

export default withStyles(withNavigation(RefinanceOffers), theme => ({
  screen: {
    flex: 1,
  },
  body: {
    flexGrow: 1,
  },
  centerText: {
    textAlign: 'center',
  },
  uppercase: {
    textTransform: 'uppercase',
  },
  wrapper: {
    marginTop: 25,
  },
  content: {
    marginTop: 25,
  },
  consolidateBanner: {
    minHeight: 200,
    justifyContent: 'center',
    paddingHorizontal: 30,
  },
  subtitle: {
    paddingVertical: 20,
    paddingHorizontal: 20,
  },
  item: {
    paddingVertical: 15,
  },
  itemText: {
    paddingVertical: 5,
    paddingHorizontal: 30,
  },
  itemTextDescription: {
    paddingBottom: 20,
  },
  currentLoanHeader: {
    marginHorizontal: 30,
    borderRadius: 25,
  },
  currentOfferContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginVertical: 20,
    marginHorizontal: 30,
  },
  refiOffersContainer: {
    marginVertical: 20,
    marginHorizontal: 30,
  },
  disclaimer: {
    marginVertical: 20,
    marginHorizontal: 70,
  },
}));
