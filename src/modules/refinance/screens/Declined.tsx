import React from 'react';
import { SafeAreaView, Image, ScrollView } from 'react-native';
import { ThemedComponentProps, withStyles, Text, Layout, Button } from 'react-native-ui-kitten';
import { parsePhoneNumberFromString } from 'libphonenumber-js';

import { withNavigation } from 'react-navigation';
import { NavigationStackScreenProps } from 'react-navigation-stack';
import config from '../../../config';

import shared from '../../shared';

const { helpers } = shared;

type DeclinedProps = ThemedComponentProps & NavigationStackScreenProps;

const Declined = ({ navigation, themedStyle }: DeclinedProps) => {
  const contactPhone = parsePhoneNumberFromString(config.contactPhone, 'US');
  let formattedContactPhone = '';
  if (contactPhone) {
    formattedContactPhone = contactPhone.formatNational();
  }
  return (
    <SafeAreaView style={themedStyle.screen}>
      <ScrollView
        contentContainerStyle={{
          flexGrow: 1,
        }}
      >
        <Layout style={themedStyle.screenContainer}>
          <Layout style={themedStyle.logoContainer}>
            <Image source={{ uri: 'lp_logo_hz' }} resizeMode="contain" style={themedStyle.logo} />
          </Layout>

          <Layout style={themedStyle.titleContainer}>
            <Text category="metatext" style={themedStyle.uppercase}>
              {/* Something is not right */}
            </Text>
          </Layout>

          <Layout style={themedStyle.imageContainer}>
            <Image source={require('../../../img/icons/ico_alert_error.png')} />
          </Layout>

          <Layout style={themedStyle.sorryContainer}>
            <Layout style={themedStyle.sorry}>
              <Text style={{ textAlign: 'center' }} category="h1" status="warning">
                At this moment, we have no offers available.
              </Text>
            </Layout>
          </Layout>

          <Layout style={themedStyle.somethingContainer}>
            <Layout style={themedStyle.something}>
              <Text style={{ textAlign: 'center' }} category="h2">
                Thank you for your interest in Lendingpoint
              </Text>
            </Layout>
          </Layout>
          <Button
            status="primary"
            style={themedStyle.button}
            onPress={() => navigation.navigate('Dashboard')}
          >
            GO HOME
          </Button>
          <Layout style={themedStyle.callUsContainer}>
            <Text style={themedStyle.callUsText}>
              {`Have a question? \nWe're always here to help.\nCall us at `}
              <Text
                onPress={() => {
                  helpers.callNumber(config.contactPhone);
                }}
                status="primary"
              >
                {formattedContactPhone}
              </Text>
            </Text>
          </Layout>
        </Layout>
      </ScrollView>
    </SafeAreaView>
  );
};

export default withStyles(withNavigation(Declined), theme => ({
  screen: {
    flex: 1,
  },
  screenContainer: {
    flexGrow: 1,
    paddingHorizontal: 30,
    paddingBottom: 50,
  },
  logoContainer: {
    borderBottomColor: theme['color-basic-500'],
    borderBottomWidth: 2,
  },
  logo: {
    alignSelf: 'center',
    width: '80%',
    minHeight: 80,
  },
  imageContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 30,
    marginTop: 50,
    elevation: 2,
  },
  titleContainer: {
    paddingTop: 15,
    alignSelf: 'center',
  },
  uppercase: {
    textTransform: 'uppercase',
  },
  sorryContainer: {
    paddingBottom: 8,
  },
  sorry: {
    alignSelf: 'center',
    paddingTop: 25,
  },
  somethingContainer: {
    paddingBottom: 20,
    marginHorizontal: 30,
  },
  something: {
    alignSelf: 'center',
    paddingTop: 25,
  },
  callUsContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 50,
    marginTop: 35,
  },
  callUsText: {
    textAlign: 'center',
  },
  button: {
    marginTop: 25,
    marginHorizontal: 70,
  },
}));
