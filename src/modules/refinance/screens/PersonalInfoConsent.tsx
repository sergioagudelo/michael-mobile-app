import React from 'react';
import { ScrollView } from 'react-native-gesture-handler';
import { PersonalInfoConsent as PersonalInfoConsentContainer } from '../containers';
import shared from '../../shared';

const { HeaderView } = shared.components;

const PersonalInfoConsent = () => {
  return (
    <ScrollView>
      <HeaderView title="Confirm &amp; Continue" subTitle="Confirm your personal information" />
      <PersonalInfoConsentContainer />
    </ScrollView>
  );
};

export default PersonalInfoConsent;
