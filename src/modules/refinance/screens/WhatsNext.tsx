import React from 'react';

import { ScrollView } from 'react-native-gesture-handler';

import { WhatsNext as WhatsNextContainer } from '../containers';

import shared from '../../shared';

const { HeaderView } = shared.components;

const WhatsNext = () => {
  return (
    <ScrollView>
      <HeaderView title="Your're All Set!" />
      <WhatsNextContainer />
    </ScrollView>
  );
};

export default WhatsNext;
