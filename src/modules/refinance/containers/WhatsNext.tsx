import React, { useState } from 'react';
import { View } from 'react-native';
import { RootState } from 'typesafe-actions';
import { connect } from 'react-redux';

import { bindActionCreators, Dispatch } from 'redux';
import { WhatsNext as WhatsNextComponent } from '../components';

import * as store from '../store';

import shared from '../../shared';
import { frequencyValueMap } from '../utils';

const mapStateToProps = (state: RootState) => ({
  tila: store.selectors.getTila(state),
  details: state.loans.details,
  frequency: state.loans.details && frequencyValueMap[(state.loans.details.frequency as string).toUpperCase()],
  selectedOffer: state.refinance.selectedOffer,
});

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(
    {
      getOpportunity: store.actions.thunks.getOpportunity,
    },
    dispatch,
  );

type WhatsNextProps = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps> & {dispatch: Dispatch};

const WhatsNext = ({ tila, getOpportunity, details, frequency, selectedOffer }: WhatsNextProps) => {
  const [cleared, setCleared] = useState(false);
  const clearOpp = async () => {
    await getOpportunity();
    setCleared(true);
  };
  if (!cleared) {
    clearOpp();
  }

  return tila ? (
    <View>
      <WhatsNextComponent
        paymentAmount={shared.helpers.format.formatMoney(tila.paymentAmount)}
        dueDate={shared.helpers.format.formatDate(tila.firstPaymentDate)}
        extraMoneyAmount={shared.helpers.format.formatMoney(tila.remainingRefinanceAmount)}
        yearlyPaymentSavings={details && selectedOffer && details.paymentAmount * frequency > selectedOffer.paymentAmount * 12 && details.remainingTerm > 12 && details.paymentAmount * frequency - selectedOffer.paymentAmount * 12}
      />
    </View>
  ) : null;
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(WhatsNext) as React.ElementType;
