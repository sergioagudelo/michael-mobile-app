import React, { useEffect } from 'react';
import { compose, Dispatch, bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withNavigation } from 'react-navigation';

// Types
import { RootState } from 'typesafe-actions';

import { Text } from 'react-native-ui-kitten';
import shared from '../../shared';
import * as loansStore from '../../loans/store';
// store
import * as store from '../store';

// Components
import { CurrentLoanDetails as CurrentLoanDetailsComponent } from '../components';
import { useHTTP } from '../../../services/hooks';

const { Loading } = shared.components;

const mapStateToProps = (state: RootState) => ({
  loanDetails: loansStore.selectors.getLoanDetails(state),
  loans: loansStore.selectors.getLoans(state),
  opportunity: store.selectors.getOpportunity(state),
});

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(
    {
      getLoanDetails: loansStore.actions.thunks.getDetails,
    },
    dispatch,
  );

type CurrentLoanDetailsConnectProps = ReturnType<typeof mapStateToProps> &
  ReturnType<typeof mapDispatchToProps>;
const CurrentLoanDetails = ({
  getLoanDetails,
  loanDetails,
  opportunity,
}: CurrentLoanDetailsConnectProps) => {
  const { isLoading, errorMessage, handleSubmit: getDetails } = useHTTP({
    httpFunction: getLoanDetails,
    functionPayload: opportunity ? opportunity.loanId : '',
  });
  useEffect(() => {
    if (!loanDetails && !isLoading && !errorMessage && getDetails) {
      getDetails();
    }
  }, [isLoading, opportunity, getLoanDetails, loanDetails, getDetails, errorMessage]);

  return (
    <>
      {isLoading ? <Loading isLoading /> : null}
      {errorMessage ? <Text status="danger">errorMessage</Text> : null}
      {loanDetails && opportunity ? (
        <CurrentLoanDetailsComponent opportunity={opportunity} loanDetails={loanDetails} />
      ) : null}
    </>
  );
};

export default compose(
  withNavigation,
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(CurrentLoanDetails) as React.ComponentType;
