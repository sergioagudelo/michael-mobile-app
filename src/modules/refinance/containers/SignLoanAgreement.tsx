import React, { useState, useRef } from 'react';
import { Alert, Platform } from 'react-native';
import { RootState } from 'typesafe-actions';
import { connect } from 'react-redux';
import ViewShot from 'react-native-view-shot';
import RNFetchBlob from 'rn-fetch-blob';

import { withNavigation } from 'react-navigation';
import { NavigationStackScreenProps } from 'react-navigation-stack';
import {
  UnsignedLoanAgreement,
  SignLoanAgreement as SignLoanAgreementComponent
} from '../components';

import * as store from '../store';
import { getContract, signAgreement } from '../services';
import { useHTTP } from '../../../services/hooks';

import profile from "../../profile";
import shared from '../../shared';

const mapStateToProps = (state: RootState) => ({
  opportunity: store.selectors.getOpportunity(state),
  user: profile.store.selectors.getUserInfo(state)
});

type SignLoanAgreementProps = NavigationStackScreenProps & ReturnType<typeof mapStateToProps>;

const SignLoanAgreement = ({ opportunity, user, navigation }: SignLoanAgreementProps) => {
  const opportunityId = opportunity ? opportunity.id : ''

  const { isLoading: contractIsLoading, errorMessage: contractError, payload: unsignedContract } = useHTTP({
    httpFunction: getContract,
    functionPayload: opportunityId,
    callOnComponentMount: true
  });

  const { isLoading: signAgreementIsLoading, errorMessage: signAgreementError, handleSubmit } = useHTTP({ httpFunction: signAgreement });
  const [electronicCommunicationsChecked, setElectronicCommunicationsChecked] = useState(false)
  const [loanAgreementChecked, setLoanAgreementChecked] = useState(false)

  const signatureShot = useRef<ViewShot>(null);

  const captureSignature = async () => {
    if (signatureShot.current && signatureShot.current.capture) {
      try {
        const signatureFileContent = RNFetchBlob.base64.encode(
          RNFetchBlob.base64.decode(await signatureShot.current.capture()),
        );
        const signatureFileName = `${user.firstName}-${user.lastName}`;
        return { signatureFileContent, signatureFileName };
      } catch (e) {
        Alert.alert('Oops', `There was an error saving the receipt:\n${e.message}`);
      }
    }
  };

  const handleFile = async (attachment: string) => {
    try {
      if (attachment) {
        const filePath = await shared.helpers.saveFile(
          `documents/${`${user.firstName}-${user.lastName}-UnsignedContract-${new Date().toDateString().replace(/ /g, "-")}`}.pdf`,
          attachment,
        );
        if (Platform.OS === 'android') {
          RNFetchBlob.android.actionViewIntent(filePath, 'application/pdf');
        } else {
          RNFetchBlob.ios.previewDocument(filePath);
        }
        return;
      }
      Alert.alert('Oops', 'Document cannot be downloaded');
    } catch (e) {
      Alert.alert('Oops', e.message);
    }
  };

  // FIXME TYPE THIS
  const handleConsent = async () => {
    if (opportunity) {
      const signature = await captureSignature()
      if (signature) {
        const { signatureFileContent, signatureFileName } = signature
        const { success } = await handleSubmit({
          opportunityId: opportunity.id,
          signatureFileName,
          signatureFileContent,
        });
        if (success) {
          navigation.navigate('WhatsNext')
        }
      }
    }
  };


  return (
    <>
      <UnsignedLoanAgreement
        unsignedContract={unsignedContract}
        contractIsLoading={contractIsLoading}
        contractError={contractError ?? ''}
        electronicCommunicationsChecked={electronicCommunicationsChecked}
        setElectronicCommunicationsChecked={setElectronicCommunicationsChecked}
        loanAgreementChecked={loanAgreementChecked}
        setLoanAgreementChecked={setLoanAgreementChecked}
        handleFile={handleFile}
      />
      <SignLoanAgreementComponent
        userInfo={user}
        unsignedContract={unsignedContract}
        signAgreementIsLoading={signAgreementIsLoading}
        signAgreementError={signAgreementError ?? ''}
        handleSubmit={handleConsent}
        electronicCommunicationsChecked={electronicCommunicationsChecked}
        loanAgreementChecked={loanAgreementChecked}
        captureSignature={captureSignature}
        signatureShot={signatureShot}
        handleFile={handleFile}

      />
    </>
  );
};

export default connect(mapStateToProps)(withNavigation(SignLoanAgreement)) as React.ElementType;
