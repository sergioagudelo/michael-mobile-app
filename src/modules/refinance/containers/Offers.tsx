import React from 'react';
import { Text, Icon, withStyles, ThemedComponentProps } from 'react-native-ui-kitten';
import { View } from 'react-native';

import { connect } from 'react-redux';
import {Dispatch} from 'redux';
import { RootState } from 'typesafe-actions';
import { withNavigation } from 'react-navigation';
import { NavigationStackScreenProps } from 'react-navigation-stack';
import * as store from '../store';

import { OffersCarousel } from '../components';
import { useHTTP } from '../../../services/hooks';
import { recalculateAndSelectOffer } from '../services';
import { frequencyValueMap, RefinanceOffer } from '../utils';
import { LoanDetailsType } from '../../loans/utils';
import { setSelectedOffer } from '../store/actions/actions';

const mapStateToProps = (state: RootState) => ({
  opportunity: store.selectors.getOpportunity(state),
  offers: store.selectors.getOffers(state)?.sort((a, b) => {
    if (a.loanAmount > b.loanAmount) {
      return -1;
    } else {
      return a.loanAmount < b.loanAmount ? 1 : 0;
    }
  }).map((offer) => {
    const details = state.loans.details as LoanDetailsType;
    const frequency = details && frequencyValueMap[details.frequency.toUpperCase()];

    if (details && details.paymentAmount * frequency > offer.paymentAmount * 12 && details.remainingTerm > 12) {
      return {
        ...offer,
        yearlySavings:
        details.paymentAmount * frequency - offer.paymentAmount * 12
      };
    } else {
      return {...offer};
    }
  }),
});

type OffersProps = ThemedComponentProps &
  ReturnType<typeof mapStateToProps> &
  NavigationStackScreenProps & {dispatch: Dispatch};

const Offers = ({ navigation, themedStyle, offers, opportunity, dispatch }: OffersProps) => {
  const { isLoading, errorMessage, handleSubmit } = useHTTP({
    httpFunction: recalculateAndSelectOffer,
  });
  const handleSelectOffer = async (loanAmount: number) => {
    const res = await handleSubmit({
      loanAmount,
      opportunityId: opportunity ? opportunity.id : '',
    });

    dispatch(setSelectedOffer(res.payload as RefinanceOffer));

    if (res.success) {
      navigation.navigate('PersonalInfoConsent');
    }
  };
  return (
    <View style={themedStyle.container}>
      <Text category="h2" status="info" style={themedStyle.centerText}>
        Get ready for a better loan
      </Text>

      <View style={themedStyle.carouselContainer}>
        {/* {errorMessage ? <Text>errorMessage</Text> : null}
        {isLoading ? <Loading isLoading /> : null} */}
        {offers ? (
          <OffersCarousel
            offers={offers}
            handleSelectOffer={handleSelectOffer}
            isLoading={isLoading}
            errorMessage={errorMessage}
          />
        ) : null}
      </View>
    </View>
  );
};

export default connect(mapStateToProps)(
  withStyles(withNavigation(Offers), () => ({
    container: {
      marginVertical: 20,
      marginHorizontal: 30,
      alignItems: 'center',
    },
    carouselContainer: {
      alignItems: 'center',
    },
    centerText: {
      textAlign: 'center',
    },
    uppercase: {
      textTransform: 'uppercase',
    },
    disclaimer: {
      marginVertical: 20,
      marginHorizontal: 70,
    },
  })),
);
