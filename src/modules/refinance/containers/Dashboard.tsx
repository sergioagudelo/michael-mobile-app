import React, { useEffect } from 'react';

import { withNavigation } from 'react-navigation';

// redux
import { connect, useSelector } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';

import { NavigationStackScreenProps } from 'react-navigation-stack';
import * as store from '../store';
import {getUserInfo} from '../../auth/store/selectors';

import { DashboardCard } from '../components';
import { useHTTP } from '../../../services/hooks';
import { NO_AVAILABLE_OPPORTUNITIES_ERROR_MESSAGE } from '../store/actions/thunks';

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(
    {
      getOpportunity: store.actions.thunks.getOpportunity,
    },
    dispatch,
  );

type RefinanceDashboardProps = NavigationStackScreenProps & ReturnType<typeof mapDispatchToProps>;

const RefinanceDashboard = ({ navigation, getOpportunity }: RefinanceDashboardProps) => {
  const opportunity = useSelector(store.selectors.getOpportunity);
  const opportunityError = useSelector(store.selectors.getOpportunityError);
  const userInfo = useSelector(getUserInfo);

  const { isLoading, handleSubmit: getOpp } = useHTTP({
    httpFunction: getOpportunity,
  });

  useEffect(() => {
    let executeRequest = async () => {
      await getOpp();
    };

    if (
      opportunityError !== NO_AVAILABLE_OPPORTUNITIES_ERROR_MESSAGE &&
      !isLoading &&
      getOpp &&
      !opportunity &&
      executeRequest &&
      userInfo
    ) {
      executeRequest();
    }
  }, [getOpp, getOpportunity, isLoading, opportunity, opportunityError, navigation]);

  return (
    <>
      {opportunity && opportunity.disbursalAmount ? (
        <DashboardCard
          opportunity={opportunity}
          onPress={() => navigation.navigate('RefinanceOffers')}
        />
      ) : null}
    </>
  );
};

export default connect(
  null,
  mapDispatchToProps,
)(withNavigation(RefinanceDashboard));
