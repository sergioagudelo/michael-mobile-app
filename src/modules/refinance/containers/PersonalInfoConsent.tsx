import React from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import { RootState } from 'typesafe-actions';
import { withNavigation } from 'react-navigation';
import { NavigationStackScreenProps } from 'react-navigation-stack';
import { PersonalInfoConsent as PersonalInfoConsentComponent } from '../components';
import { consent } from '../services';
import { useHTTP } from '../../../services/hooks';
import * as store from '../store';
import profile from '../../profile';
import * as loansStore from '../../loans/store';

const mapStateToProps = (state: RootState) => ({
  loanDetails: loansStore.selectors.getLoanDetails (state),
  userInfo: profile.store.selectors.getUserInfo(state),
  opportunity: store.selectors.getOpportunity(state),
});

type PersonalInfoConsentContainerProps = NavigationStackScreenProps &
  ReturnType<typeof mapStateToProps>;

const PersonalInfoConsentContainer = ({
  userInfo,
  opportunity,
  loanDetails,
  navigation,
}: PersonalInfoConsentContainerProps) => {
  const { isLoading, errorMessage, handleSubmit } = useHTTP({ httpFunction: consent });
  const handleConsent = async () => {
    if (opportunity) {
      const { success } = await handleSubmit(opportunity.id);
      if (success) {
        navigation.navigate('Sdocs');
      }
    }
  };
  return (
    <View>
      {userInfo ? (
        <PersonalInfoConsentComponent
          isLoading={isLoading}
          errorMessage={errorMessage ?? ' '}
          handlePress={handleConsent}
          userInfo={userInfo}
          loanDetails={loanDetails}
        />
      ) : null}
    </View>
  );
};

export default connect(mapStateToProps)(withNavigation(PersonalInfoConsentContainer)) as React.ElementType;
