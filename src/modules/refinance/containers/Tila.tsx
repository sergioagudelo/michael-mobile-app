import React from 'react';

import { RootState } from 'typesafe-actions';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';

import {  NavigationStackScreenProps } from 'react-navigation-stack';
import { Tila as TilaComponent } from '../components';

import * as store from '../store';
import { useHTTP } from '../../../services/hooks';
import { Tila as TilaType} from '../utils';

const mapStateToProps = (state: RootState) => ({
  opportunity: store.selectors.getOpportunity(state),
  tilaPayload: store.selectors.getTila(state),
});

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(
    {
      getTila: store.actions.thunks.getTila,
    },
    dispatch,
  );


type ContractAgreementProps = NavigationStackScreenProps & ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>;

const Tila = ({ opportunity, getTila, tilaPayload }: ContractAgreementProps) => {
  const opportunityId = opportunity ? opportunity.id : ''
  const { isLoading, errorMessage } = useHTTP({
    httpFunction: getTila,
    functionPayload: opportunityId,
    callOnComponentMount: true,
  })

  return (
    <>
      <TilaComponent
        opportunityId={opportunityId}
        isLoading={isLoading}
        errorMessage={errorMessage?? ''}
        tila={tilaPayload as TilaType}
      />
    </>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(Tila) as React.ElementType;
