import Congrats from './Congrats';
import Offers from './Offers';
import Dashboard from './Dashboard';
import CurrentLoanDetails from './CurrentLoanDetails';
import PersonalInfoConsent from './PersonalInfoConsent';
import Tila from './Tila';
import SignLoanAgreement from './SignLoanAgreement';
import WhatsNext from './WhatsNext';

export {
  Congrats,
  Offers,
  Dashboard,
  CurrentLoanDetails,
  PersonalInfoConsent,
  Tila,
  SignLoanAgreement,
  WhatsNext,
};
