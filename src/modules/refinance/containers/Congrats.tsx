import React from 'react';
import { Text } from 'react-native-ui-kitten';

import { connect } from 'react-redux';
import { RootState } from 'typesafe-actions';

import profile from '../../profile';
import shared from '../../shared';

const { HeaderView } = shared.components;

const mapStateToProps = (state: RootState) => ({
  userInfo: profile.store.selectors.getUserInfo(state),
});

type CongratsProps = ReturnType<typeof mapStateToProps>;

const Congrats = ({ userInfo }: CongratsProps) => (
  <>
    <HeaderView
      title={`Good news, ${userInfo ? userInfo.firstName : ''}!`}
      subTitle="Paying on time has paid off."
    />
  </>
);

export default connect(mapStateToProps)(Congrats);
