import { NAME } from '../../constants';

import shared from '../../../shared';

export default shared.helpers.createActionTypes(NAME, [
  'REFINANCE_OPPORTUNITY.ATTEMPT',
  'REFINANCE_OPPORTUNITY.SUCCESS',
  'REFINANCE_OPPORTUNITY.FAILURE',
  'REFINANCE_OFFERS.ATTEMPT',
  'REFINANCE_OFFERS.SUCCESS',
  'REFINANCE_OFFERS.FAILURE',
  'REFINANCE_TILA.ATTEMPT',
  'REFINANCE_TILA.SUCCESS',
  'REFINANCE_TILA.FAILURE',
  'REFINANCE_SELECTED_OFFER.SET_SELECTED_OFFER',
]);
