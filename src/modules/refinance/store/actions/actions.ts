import { createAsyncAction, createAction } from 'typesafe-actions';

// Types

import types from './types';
import { RefinanceOpportunity, RefinanceOffer, Tila } from '../../utils';

export const opportunity = createAsyncAction(
  types.REFINANCE_OPPORTUNITY.ATTEMPT,
  types.REFINANCE_OPPORTUNITY.SUCCESS,
  types.REFINANCE_OPPORTUNITY.FAILURE,
)<undefined, RefinanceOpportunity | undefined, string | undefined>();

export const offers = createAsyncAction(
  types.REFINANCE_OFFERS.ATTEMPT,
  types.REFINANCE_OFFERS.SUCCESS,
  types.REFINANCE_OFFERS.FAILURE,
)<undefined, RefinanceOffer[] | undefined, string | undefined>();

export const tila = createAsyncAction(
  types.REFINANCE_TILA.ATTEMPT,
  types.REFINANCE_TILA.SUCCESS,
  types.REFINANCE_TILA.FAILURE,
)<undefined, Tila | undefined, string | undefined>();

export const setSelectedOffer = createAction(
  types.REFINANCE_SELECTED_OFFER.SET_SELECTED_OFFER,
  action => (selectedOffer: RefinanceOffer) => action(selectedOffer)
);
