import { Dispatch } from 'redux';
import { opportunity as opportunityActions, offers, tila } from './actions';
import { getLoanOpportunities, createApiManagerToken, offersCatalog, getTila as getTilaService } from '../../services';
import { RefinanceOpportunity, RefinanceOffer } from '../../utils';
import { HTTPServicesResponse } from '../../../../services/utils';

export const NO_AVAILABLE_OPPORTUNITIES_ERROR_MESSAGE = 'No available opportunities';

export const getOpportunity = () => async (dispatch: Dispatch): Promise<HTTPServicesResponse> => {
  dispatch(opportunityActions.request())
  const { success, payload, errorMessage } = await getLoanOpportunities();
  let _opportunity: RefinanceOpportunity | null = null;

  if (success && payload) {
    _opportunity = payload.opportunities.find(opportunity => (
      !!opportunity.loanId &&
      opportunity.type === "Refinance" &&
      !opportunity.isContractSigned &&
      opportunity.isRefiE2Eeligible &&
      opportunity.isAutopay &&
      opportunity.hasOffers)
    ) ?? null;

    if (!_opportunity) {
      const errMsg = NO_AVAILABLE_OPPORTUNITIES_ERROR_MESSAGE;
      dispatch(opportunityActions.failure(errMsg));
      return { success: false, errorMessage: errMsg };
    }

    dispatch(opportunityActions.success(_opportunity as RefinanceOpportunity | undefined));
    const { success: managerTokenSuccess } = await createApiManagerToken();
    if (managerTokenSuccess) {
      const { payload: catalog } = await offersCatalog(_opportunity ? _opportunity.id : '');
      if (catalog) {
        // TODO Properly sort
        const _offers = catalog.sort((b, a) => b.loanAmount - a.loanAmount);
        dispatch(offers.success(_offers));
        const { disbursalAmount } = catalog.sort((a, b) => b.loanAmount - a.loanAmount)[0];
        if (_opportunity) {
          _opportunity.disbursalAmount = disbursalAmount;
          dispatch(opportunityActions.success(_opportunity));
        }
      }
    }

    return { success: true, payload: _opportunity }
  }

  dispatch(opportunityActions.failure(errorMessage ?? 'Thunk error'))
  return { success: false, errorMessage: '' }

};

export const getTila = (opportunityId: string) => async (dispatch: Dispatch): Promise<HTTPServicesResponse> => {

  const { success, payload, errorMessage } = await getTilaService(opportunityId);
  if (success && payload) {
    dispatch(tila.success(payload))
    return { success: true, payload: 'success' }
  }
  dispatch(tila.failure(errorMessage ?? 'Thunk error'))
  return { success: false, errorMessage: (errorMessage ?? 'Thunk error') }
}
