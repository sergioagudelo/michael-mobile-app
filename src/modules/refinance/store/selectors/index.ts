import { createSelector } from 'reselect';
import { RootState } from 'typesafe-actions';
import { NAME } from '../../constants';
import { RefinanceOpportunity, RefinanceOffer, Tila } from '../../utils';

// type

const opportunitySelector = (state: RootState): RefinanceOpportunity | undefined | null =>
  state[NAME].opportunity;

export const getOpportunity = createSelector(
  opportunitySelector,
  (opportunity: RefinanceOpportunity | undefined | null) => opportunity,
);

const opportunityErrorSelector = (state: RootState): string => state[NAME].opportunityError;

export const getOpportunityError = createSelector(opportunityErrorSelector, (opportunityError: string) => opportunityError);

const offersSelector = (state: RootState): RefinanceOffer[] | null => state[NAME].offers;

export const getOffers = createSelector(
  offersSelector,
  (offers: RefinanceOffer[] | null) => offers,
);

const tilaSelector = (state: RootState): Tila | null => state[NAME].tila;

export const getTila = createSelector(
  tilaSelector,
  (tila: Tila | null) => tila,
);
