import { combineReducers } from 'redux';
import { createReducer, PayloadAction } from 'typesafe-actions';

import { actions } from '../actions';
import { RefinanceOpportunity, RefinanceOffer, Tila } from '../../utils';

const opportunity = createReducer(null as RefinanceOpportunity | null).handleAction(
  actions.opportunity.success,
  (state, action: PayloadAction<'', RefinanceOpportunity>) =>
    action.payload as RefinanceOpportunity | null,
)

const opportunityError = createReducer('')
  .handleAction(actions.opportunity.failure, 
    (state, action: PayloadAction<string, string>) => action.payload as string)

const offers = createReducer(null as RefinanceOffer[] | null).handleAction(
  actions.offers.success,
  (state, action: PayloadAction<'', RefinanceOffer[]>) => action.payload as RefinanceOffer[] | null,
);
const tila = createReducer(null as Tila | null).handleAction(
  actions.tila.success,
  (state, action: PayloadAction<'', Tila>) => action.payload as Tila | null,
);
const selectedOffer = createReducer(null as RefinanceOffer | null).handleAction(
  actions.setSelectedOffer,
  (state, action: PayloadAction<string, RefinanceOffer>) => action.payload as RefinanceOffer | null,
);

const RefinanceReducer = combineReducers({
  opportunity,
  opportunityError,
  offers,
  tila,
  selectedOffer
});

export default RefinanceReducer;
export type LoansState = ReturnType<typeof RefinanceReducer>;
