export type RefinanceOffer = {
  aprPercent: number;
  paymentAmount: number;
  apr: number;
  fee: number;
  loanAmount: number;
  term: number;
  disbursalAmount: number;
  id: number;
  yearlySavings?: number;
};
export type RefinanceOpportunity = {
  id: string;
  type: string;
  isDTC: boolean;
  loanId: string;
  isAutopay: boolean;
  hasOffers: boolean;
  isContractSigned: boolean;
  isRefiE2Eeligible: boolean;
  disbursalAmount?: number;
  yearlySavings?: number;
};
export type LoanApplicationsResponse = {
  opportunities: RefinanceOpportunity[];
};

export type OfferFromRecalculate = {
  repaymentFrequency: string;
  paymentAmount: number;
  offerId: string;
  offerExpiryDate: number;
  loanAmount: number;
  installments: number;
  effectiveAPR: number;
  aPRPercent: number;
  fee: number;
  term: number;
  criteria: string;
  offerType: string;
  offerCode: string;
  offerUrl: string;
  selected: boolean;
};

export type RecalculateResponse = {
  personalizedURL: string;
  offers: Array<OfferFromRecalculate>;
  oldOpportunityStatus: string;
  opportunityStatus: string;
};
export type Tila = {
  accountNumber: string;
  loanAmount: number;
  fee: number;
  totalLoanAmount: number;
  paymentAmount: number;
  term: number;
  paymentFrequency: string;
  apr: number;
  interestRate: number;
  firstPaymentDate: string;
  installment: number;
  offerType: string;
  financeCharged: number;
  remainingRefinanceAmount: number;
};

export const frequencyValueMap: {[key: string]: number;} = {
  'MONTHLY': 12,
  'SEMI-MONTHLY': 24,
  'BI-WEEKLY': 26,
  '28 DAYS': 13
};
