import { AxiosResponse } from 'axios';
import { lendingpoint } from '../../../services';

import { HTTPServicesResponse } from '../../../services/utils';
import { RecalculateResponse, LoanApplicationsResponse, Tila } from '../utils';
// import { LoansResponse, LoanDetailsType, PaymentHistoryResponse } from '../utils';
import config from '../../../config';

const {
  api: { consumer, manager },
  constants: { paths },
  helpers: { handleSubModuleError, addAuthTokens },
} = lendingpoint;

export const getLoanOpportunities = async (): Promise<
  HTTPServicesResponse<LoanApplicationsResponse>
> => {
  try {
    const { data } = await consumer.get(paths.loanApplications);
    return { success: true, payload: data };
  } catch (err) {
    return handleSubModuleError(err);
  }
};

export const createApiManagerToken = async (): Promise<HTTPServicesResponse<any>> => {
  const {
    apiManager: { managerAuth },
  } = config.api;
  try {
    const payload = {
      ...managerAuth,
      grantType: 'password',
    };
    const { data } = await manager.post(paths.apiManagerLogin, payload);
    addAuthTokens({ managerAuthorization: data.accessToken });
    return { success: true, payload: 'No content' };
  } catch (err) {
    return handleSubModuleError(err);
  }
};

export const offersCatalog = async (
  opportunityId: string,
): Promise<HTTPServicesResponse<any[]>> => {
  try {
    const { data } = await consumer({
      method: 'POST',
      baseURL: `${config.api.host}/consumer/v2`,
      url: paths.applications + paths.offersCatalog,
      data: {
        opportunityId,
        systemSource: 'Mobile.Refinance',
      },
    });
    return { success: true, payload: data.offers };
  } catch (err) {
    return handleSubModuleError(err);
  }
};
export const recalculate = async ({
  loanAmount,
  opportunityId,
}: {
  loanAmount: number;
  opportunityId: string;
}): Promise<HTTPServicesResponse<any[]>> => {
  try {
    const { data } = await consumer.post(`${paths.loanApplications}/${opportunityId}/offers`, {
      loanAmount,
    });
    return { success: true, payload: data.offers };
  } catch (err) {
    return handleSubModuleError(err);
  }
};
export const selectOffer = async (
  opportunityId: string,
  offerId: number,
): Promise<HTTPServicesResponse<any[]>> => {
  try {
    const { data } = await consumer.patch(
      `${paths.loanApplications}/${opportunityId}/offers/${offerId}`,
      {
        selected: true,
      },
    );
    return { success: true, payload: data };
  } catch (err) {
    return handleSubModuleError(err);
  }
};

export const recalculateAndSelectOffer = async ({
  loanAmount,
  opportunityId,
}: {
  loanAmount: number;
  opportunityId: string;
}): Promise<HTTPServicesResponse<any[]>> => {
  try {
    const {
      data: { offers },
    }: AxiosResponse<{ offers: Array<any> }> = await consumer.post(
      `${paths.loanApplications}/${opportunityId}/offers`,
      {
        loanAmount,
      },
    );
    const selectedOffer = offers.sort((a, b) => {
      const m1 = a.loanAmount;
      const m2 = b.loanAmount;
      const n1 = a.term;
      const n2 = b.term;
      return m1 > m2 ? -1 : m1 < m2 ? 1 : n1 > n2 ? -1 : n1 > n2 ? 1 : 0;
    })[0];
    const { data } = await consumer.patch(
      `${paths.loanApplications}/${opportunityId}/offers/${selectedOffer.id}`,
      { selected: true, source: 'RefinanceMobileApp', offerSelectSrc: 'RefinanceMobileApp' },
    );
    return { success: true, payload: selectedOffer };
  } catch (err) {
    return handleSubModuleError(err);
  }
};

export const consent = async (opportunityId: string): Promise<HTTPServicesResponse<any[]>> => {
  try {
    const { data } = await consumer.patch(`${paths.loanApplications}/${opportunityId}`, {
      isSameIncome: true,
      isSameEmployment: true,
      isSameBankInfo: true,
    });
    return { success: true, payload: data };
  } catch (err) {
    return handleSubModuleError(err);
  }
};

export const getTila = async (opportunityId: string): Promise<HTTPServicesResponse<Tila>> => {
  try {
    const { data } = await consumer.get(`${paths.applications}/${opportunityId}`);
    return { success: true, payload: data };
  } catch (err) {
    return handleSubModuleError(err);
  }
};

export const getContract = async (opportunityId: string): Promise<HTTPServicesResponse<any[]>> => {
  try {
    const { data } = await consumer.get(`${paths.applications}/${opportunityId}/loan-agreement`);
    return { success: true, payload: data };
  } catch (err) {
    return handleSubModuleError(err);
  }
};
// FIXME TYPE THIS
export const signAgreement = async ({
  opportunityId,
  signatureFileName,
  signatureFileContent,
}: any): Promise<HTTPServicesResponse<any>> => {
  try {
    const { data } = await consumer.patch(`${paths.applications}/${opportunityId}`, {
      signatureFileName,
      signatureFileContent,
    });
    return { success: true, payload: data };
  } catch (err) {
    return handleSubModuleError(err);
  }
};

// export const get = async (): Promise<HTTPServicesResponse<RecalculateResponse>> => {
//   try {
//     const { data } = await consumer.get(paths.loanApplications);
//     return { success: true, payload: data };
//   } catch (err) {
//     return handleSubModuleError(err);
//   }
// };
// export const post = async (payload: any): Promise<HTTPServicesResponse<RecalculateResponse>> => {
//   try {
//     const { data } = await consumer.post(`${paths.loanApplications}/aaa/offers`, payload);
//     return { success: true, payload: data };
//   } catch (err) {
//     return handleSubModuleError(err);
//   }
// };
