# Refi flow

1. Loan Applications (Opportunities)

    GET https://iodev.lendingpoint.com/consumer/v1/me/loan-applications

        !!opportunity.loanId &&
        opportunity.type === "Refinance" &&
        !opportunity.isContractSigned &&
        opportunity.isRefiE2Eeligible &&
        opportunity.isAutopay &&
        opportunity.hasOffers

2. API MANGER AUTH

    POST https://iodev.lendingpoint.com/api-manager/oauth/token



3. Offers Catalog (displayed offers)
  POST https://iodev.lendingpoint.com/consumer/v1/applications/offers/catalog

        Como calcular deposit amount?

4. Loan Details

   GET https://iodev.lendingpoint.com/consumer/v1/me/loans/4f7f28c4-9419-4e1e-92d2-649b04f50460


5. Recalculate

   POST https://iodev.lendingpoint.com/consumer/v1/me/loan-applications/646eb876-3223-4e5e-bc3c-c7e9ab623614/offers

6. Select Offer

   PATCH https://iodev.lendingpoint.com/consumer/v1/me/loan-applications/646eb876-3223-4e5e-bc3c-c7e9ab623614/offers/0708f2be-ed85-4aca-aeae-e3ff9419294f


7. Personal info (called at login)

    GET https://iodev.lendingpoint.com/consumer/v1/me

        How to calculate bank accounts?

8. Personal info consent
9. TILA sdocs

    GET https://iodev.lendingpoint.com/consumer/v1/applications/646eb876-3223-4e5e-bc3c-c7e9ab623614

10.  Get unsigned loan agreement sdocs

        GET https://iodev.lendingpoint.com/consumer/v1/applications/646eb876-3223-4e5e-bc3c-c7e9ab623614/loan-agreement

12. Sign Agreement sdocs

    PATCH https://iodev.lendingpoint.com/consumer/v1/applications/646eb876-3223-4e5e-bc3c-c7e9ab623614

