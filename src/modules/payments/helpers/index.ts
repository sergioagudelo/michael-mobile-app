import { PaymentMethodType } from '../utils';
import { PaymentMethodTypeKeys, PaymentMethodNameKeys } from '../constants';
import * as sharedHelpers from '../../shared/helpers';
import * as sharedUtils from '../../shared/utils';

export const truncateDecimals = (value: number, decimals = 2): number =>
  parseFloat((Math.floor(value * 10 ** decimals) / 10 ** decimals).toFixed(decimals));

export const getDecimalsCount = (num: number) => {
  if (Math.floor(num.valueOf()) === num.valueOf()) return 0;
  return num.toString().split('.')[1].length || 0;
};

export const decimalInputHandler = (val: string, decimalSize = 2) => {
  let parsed = parseFloat(val);
  // eslint-disable-next-line no-restricted-globals
  if (isNaN(parsed) || parseFloat(val) >= 1000000) {
    return '';
  }
  if (getDecimalsCount(parsed) > 2) {
    parsed = truncateDecimals(parsed, decimalSize);
  }
  return val.endsWith('.') ? val : parsed;
};

// todo truncate
export const getPercentAsFloat = (amount: number, percent: number): number =>
  amount * (percent / 100);

export const paymentMethodsParser = (methods: PaymentMethodType[]) =>
  methods.map(entry => ({
    ...entry,
    isACH: !!entry.bankId,
    paymentMethodId: entry.bankId ? entry.bankId : entry.accountId,
    mode: entry.bankId ? PaymentMethodTypeKeys.BANK_ACCOUNT : PaymentMethodTypeKeys.DEBIT_CARD,
    methodName: entry.bankId
      ? PaymentMethodNameKeys.BANK_ACCOUNT
      : PaymentMethodNameKeys.DEBIT_CARD,
  }));

export const getPaymentDate = (isAch = false): Date => {
  const date = new Date();
  if (isAch) {
    date.setDate(new Date().getDate() + 1);
  }
  return date;
};

export const getConsentText = (
  userFullName: string,
  paymentMethod: PaymentMethodType | undefined,
  dueDate: string,
) => {
  if (!userFullName || !paymentMethod) {
    return '';
  }
  const { isACH } = paymentMethod;
  const methodNumber = sharedHelpers.format.formatPaymentMethodNumber(paymentMethod);
  return !isACH
    ? `You authorize LendingPoint to process your debit card for the amounts owing on each payment date scheduled in the Loan Agreement or as changed thereafter ${
        dueDate
          ? sharedHelpers.format.formatDate(dueDate, sharedUtils.formatDateOptions[1])
          : 'due date'
      } in accordance with the Debit Consent Terms. You understand the amount of each payment will show up on your bank statement for the purposes of payment and amount verification. `
    : `By clicking below to make your scheduled payments, you, ${userFullName}, authorize LendingPoint to electronically debit your above bank account ending with ${methodNumber} in accordance with the ACH Consent Terms for amounts owing on each payment date scheduled in your Loan Agreement or as changed thereafter ${
        dueDate
          ? sharedHelpers.format.formatDate(dueDate, sharedUtils.formatDateOptions[1])
          : 'due date'
      }.`;
};
