import React, { useState } from 'react';
import {
  withStyles,
  Text,
  Layout,
  Button,
  ThemedComponentProps,
  ListItem,
} from 'react-native-ui-kitten';

import { NavigationStackScreenProps } from 'react-navigation-stack';

import { connect } from 'react-redux';

import { RootState } from 'typesafe-actions';
import { withNavigation } from 'react-navigation';
import { compose } from 'redux';
import * as loansStore from '../../loans/store';
import * as store from '../store'; // TODO refactor selector for debit and bank

import shared from '../../shared';
import profile from '../../profile';

import LoanBalanceHeader from '../../loans/components/LoanBalanceHeader';
import { PaymentType, PaymentMethodModeType, MakePaymentResponse } from '../utils';
import * as helpers from '../helpers';

import { ChoosePaymentModal, PaymentMethodSelector } from '../components';
import * as api from '../services/payments';
import { ApiResponse } from '../../../services/utils';

const { Dashed, Loading } = shared.components;

type LoanDetailsMapStateProps = {
  loanId: string;
};

const mapStateToProps = (state: RootState, { loanId }: LoanDetailsMapStateProps) => ({
  email: profile.store.selectors.getUserEmail(state),
  loan: loansStore.selectors.getLoanById(state, loanId),
  paymentMethods: store.selectors.getPaymentMethods(state),
  _defaultMethod: store.selectors.getDefaultPaymentMethod(state),
});

type ExtraPaymentProps = {
  loanId: string;
  amount: number;
} & ThemedComponentProps &
  NavigationStackScreenProps &
  ReturnType<typeof mapStateToProps>;

const ExtraPayment = ({
  amount,
  loan,
  theme,
  navigation,
  themedStyle,
  paymentMethods,
  _defaultMethod,
  email,

}: ExtraPaymentProps) => {
  const [isModalVisible, setIsModalVisible] = useState(false);

  const [defaultMethod, setDefaultMethod] = useState(_defaultMethod);
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [error, setError] = useState<string>();




  const payment: PaymentType = {
    amount,
    contractId: loan.loanId,
    total: amount,
    date: shared.helpers.format.formatDate(
      helpers.getPaymentDate(defaultMethod ? defaultMethod.isACH : false),
      'YYYY-MM-DD',
    ),
    isAdditionalPayment: true,
    transactionId: '',
    mode: '',
    email,

  };


  const handleSubmitPayment = async () => {
    setIsSubmitting(true);
    setError(undefined);

    const res = await api.makePayment(defaultMethod.paymentMethodId, payment, defaultMethod.mode);
    setIsSubmitting(false);
    if (res ) {
      if (res.success && res.response) {

        payment.transactionId = res.response.loanTransactionId;

        navigation.navigate('PaymentReceipt', {
          payment: { ...payment, paymentMethod: defaultMethod },
        });
        return;
      }
      setError(res && res.details);

    }


  };
  if (paymentMethods.length == 0 || !loan) {
    navigation.goBack();
    return <Loading spinnerColors={['#fff']} color="#fff" isLoading />;
  }

  return (
    <>
      <ChoosePaymentModal
        title="How would you like to pay?"
        buttonTitle="Continue"
        visible={isModalVisible}
        selectedOption={defaultMethod}
        options={paymentMethods}
        onSubmit={setDefaultMethod}
        onDismiss={() => setIsModalVisible(false)}
        onNewPaymentMethod={() => navigation.navigate('PaymentMethods')}
      />

      <LoanBalanceHeader lai={loan.lai} balance={loan.balance} />

      <Layout style={themedStyle.paymentContainer}>
        <Text
          category="h2"
          status="accent"
          style={[themedStyle.paymentTitle, themedStyle.centerText]}
        >
          Payment Options:
        </Text>
        <ListItem
          title="Extra payment Amount"
          titleStyle={[themedStyle.uppercase, themedStyle.titleStyle]}
          description={shared.helpers.format.formatMoney(payment.amount, {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2,
          })}
          descriptionStyle={themedStyle.descriptionStyle}
          disabled
        />
        <Dashed color={theme['color-basic-600']} customStyle={themedStyle.itemSeparator} />

        <PaymentMethodSelector
          disabled={isSubmitting}
          theme={theme}
          isLoading={paymentMethods === null}
          defaultMethod={defaultMethod}
          setIsModalVisible={setIsModalVisible}
          handleNoPaymentMethods={() => navigation.navigate('PaymentMethods')}
        />

        <Dashed color={theme['color-basic-600']} customStyle={themedStyle.itemSeparator} />
        <ListItem
          title="PAYMENT DATE"
          titleStyle={[themedStyle.uppercase, themedStyle.titleStyle]}
          description={shared.helpers.format.formatDate(
            payment.date ?? '',
            shared.utils.formatDateOptions[1],
          )}
          descriptionStyle={themedStyle.descriptionStyle}
          accessory={() => (
            <Button
              appearance="ghost"
              size="tiny"
              status="info"
              textStyle={themedStyle.uppercase}
            />
          )}
          disabled
        />
        <Dashed color={theme['color-primary-700']} customStyle={themedStyle.itemSeparator} />
        <Layout style={themedStyle.paymentTotal}>
          <Text category="p1" status="accent" style={themedStyle.uppercase}>
            PAYMENT AMOUNT
          </Text>
          <Text category="number" status="accent">
            {shared.helpers.format.formatMoney(payment.total ?? 0, {
              minimumFractionDigits: 2,
              maximumFractionDigits: 2,
            })}
          </Text>
        </Layout>

        {error && (
          <Layout style={themedStyle.paymentError}>
            <Text category="p1" status="danger" style={themedStyle.centerText}>
              {error}
            </Text>
          </Layout>
        )}

        {isSubmitting ? (
          <Loading isLoading={isSubmitting} />
        ) : (
            <Button
              size="large"
              disabled={!paymentMethods || !defaultMethod}
              textStyle={themedStyle.uppercase}
              onPress={handleSubmitPayment}
            >
              SUBMIT PAYMENT
          </Button>
          )}
      </Layout>
    </>
  );
};

const ExtraPaymentCheckout = withStyles(ExtraPayment, theme => ({
  itemSeparator: {
    marginVertical: 10,
  },
  centerText: {
    textAlign: 'center',
  },
  uppercase: {
    textTransform: 'uppercase',
  },
  subtitle: {
    paddingTop: 10,
  },
  wrapper: {
    marginTop: 20,
  },
  content: {
    paddingTop: 40,
    paddingHorizontal: 30,
  },
  loanHeader: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  paymentItem: {
    borderBottomWidth: 2,
    borderBottomColor: '#DEE2E8',
  },
  paymentContainer: {
    marginTop: 20,
    padding: 25,
    borderColor: theme['color-basic-400'],
    borderWidth: 1,
    elevation: 2,
    ...shared.helpers.shadow(2),
  },
  paymentTitle: {
    paddingBottom: 10,
  },
  paymentIconContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 8,
    paddingHorizontal: 6,
    paddingVertical: 6,
  },
  paymentIcon: {
    width: 40,
    height: 40,
    tintColor: theme['color-success-500'],
  },
  paymentTotal: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingBottom: 10,
  },
  paymentError: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 15,
    paddingBottom: 10,
  },
  paymentNotice: {
    paddingVertical: 20,
  },
  paymentTC: {
    paddingVertical: 15,
  },
  loanBalance: {
    paddingLeft: 20,
  },
  lastPaymentInfo: {
    paddingVertical: 15,
  },
  item: {
    paddingVertical: 15,
  },
  titleStyle: {
    color: theme['color-basic-800'],
  },
  descriptionStyle: {
    color: theme['color-primary-700'],
  },
}));


export default compose(
    withNavigation,
    connect(mapStateToProps)
)(ExtraPaymentCheckout)

