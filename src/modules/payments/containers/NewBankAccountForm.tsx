import { compose, bindActionCreators, Dispatch } from 'redux';
import { withFormik } from 'formik';
import { withNavigation } from 'react-navigation';
import { NavigationStackScreenProps } from 'react-navigation-stack';

// Types
import { connect } from 'react-redux';
import { Keyboard } from 'react-native';
import { BankAccountFormValues } from '../utils';

// Constants
import { forms, PaymentMethodTypeKeys, formKeys } from '../constants';

// Component
import BankAccountForm from '../components/BankAccountForm';

import * as store from '../store';

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(
    {
      addPaymentMethod: store.actions.thunks.addPaymentMethod,
    },
    dispatch,
  );

type BankAccountFormProps = NavigationStackScreenProps & {
  initialValues?: BankAccountFormValues;
} & ReturnType<typeof mapDispatchToProps>;

export default compose(
  withNavigation,
  connect(
    null,
    mapDispatchToProps,
  ),
  withFormik<BankAccountFormProps, BankAccountFormValues>({
    mapPropsToValues: () => ({
      [formKeys.bankAccount.bankName]: forms.bankAccount.initialValues.bankName,
      [formKeys.bankAccount.routingNumber]: forms.bankAccount.initialValues.routingNumber,
      [formKeys.bankAccount.accountNumber]: forms.bankAccount.initialValues.accountNumber,
      [formKeys.bankAccount.confirmAccountNumber]:
        forms.bankAccount.initialValues.confirmAccountNumber,
      [formKeys.bankAccount.accountType]: forms.bankAccount.initialValues.accountType,
      [formKeys.bankAccount.setDefault]:
        forms.bankAccount.initialValues[formKeys.bankAccount.setDefault],
    }),
    isInitialValid: props => forms.bankAccount.schema.isValidSync(props.initialValues),
    validationSchema: forms.bankAccount.schema,
    handleSubmit: async (values, { setSubmitting, setStatus, props }) => {
      setStatus({});
      Keyboard.dismiss();
      const result = await props.addPaymentMethod(values, PaymentMethodTypeKeys.BANK_ACCOUNT);
      setSubmitting(false);
      if (result.success) {
        props.navigation.goBack();
        return;
      }
      setStatus({
        success: false,
        // TODO: Handle API error
        error: result.error ? result.details : 'Error adding bank account, please try again',
      });
    },
  }),
)(BankAccountForm);
