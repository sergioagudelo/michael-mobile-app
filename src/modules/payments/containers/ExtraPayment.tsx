import React, { useState } from 'react';
import { withNavigation } from 'react-navigation';
import { NavigationStackScreenProps } from 'react-navigation-stack';
import { Formik } from 'formik';
import * as Yup from 'yup';

import { getPercentAsFloat, truncateDecimals } from '../helpers';
import { ExtraPaymentComponent } from '../components';

// Types
import { Loan } from '../../loans/utils';

import { forms, formKeys } from '../constants';
import { ExtraPaymentFormValues } from '../utils';
import shared from '../../shared';

type ExtraPaymentContainerProps = {
  onPress: () => void;
  loan: Loan;
} & NavigationStackScreenProps;
//  & ReturnType<typeof mapStateToProps>;
// const ExtraPaymentContainer = compose(
//   connect(mapStateToProps),

const ExtraPaymentContainer = ({ navigation, loan }: ExtraPaymentContainerProps) => {
  const [isEnabled, setIsEnabled] = useState(false);

  const { loanId } = loan;

  const extraPaymentMaxAmount = truncateDecimals(getPercentAsFloat(loan.balance, 20), 2);

  // TODO find a better solution for dynamic less than validation
  const schema = Yup.object({
    amount: Yup.number()
      .required('Please provide an amount for your extra payment')
      .max(
        extraPaymentMaxAmount,
        `The amount for your extra payment can't be more than ${shared.helpers.format.formatMoney(
          extraPaymentMaxAmount,
          {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2,
          },
        )}`,
      ),
  });

  const handleOnPress = (amount: number) => navigation.navigate('ExtraPayment', { loanId, amount });

  const handleToggle = (checked: boolean) => setIsEnabled(checked);

  return (
    <Formik
      initialValues={{} as ExtraPaymentFormValues}
      enableReinitialize
      isInitialValid={() =>
        forms.extraPayment.schema.isValidSync({
          [formKeys.extraPayment.amount]: extraPaymentMaxAmount,
        })
      }
      validationSchema={schema}
      onSubmit={({ amount }) => {
        handleOnPress(amount);
      }}
    >
      {props => (
        <ExtraPaymentComponent
          {...props}
          extraPaymentMaxAmount={extraPaymentMaxAmount}
          isEnabled={isEnabled}
          onToggle={handleToggle}
        />
      )}
    </Formik>
  );
};

export default withNavigation(ExtraPaymentContainer);
