import NewBankAccountForm from './NewBankAccountForm';
import NewDebitCardForm from './NewDebitCardForm';
import PaymentMethodsList from './PaymentMethodsList';
import MakePayment from './MakePayment';
import PaymentReceipt from './PaymentReceipt';
import ExtraPaymentContainer from './ExtraPayment';
import ExtraPaymentCheckout from './ExtraPaymentCheckout';
import AutopaySetup from './AutopaySetup';

export {
  PaymentReceipt,
  MakePayment,
  NewBankAccountForm,
  NewDebitCardForm,
  PaymentMethodsList,
  ExtraPaymentContainer,
  ExtraPaymentCheckout,
  AutopaySetup,
};
