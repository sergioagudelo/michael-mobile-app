import React, { useState } from 'react';
import { compose, Dispatch, bindActionCreators } from 'redux';
import { withNavigation } from 'react-navigation';
import { NavigationStackProp } from 'react-navigation-stack';
import { FormikValues } from 'formik';

// Types
import { connect } from 'react-redux';
import { RootState } from 'typesafe-actions';
import * as store from '../store';
import * as profileStore from '../../profile/store';
// Constants
import { forms, PaymentMethodTypeKeys } from '../constants';

// Component
import DebitCardForm from '../components/DebitCardForm';
import { User } from '../../profile/utils';

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(
    {
      addPaymentMethod: store.actions.thunks.addPaymentMethod,
    },
    dispatch,
  );

const mapStateToProps = (state: RootState) => ({
  userInfo: profileStore.selectors.getUserInfo(state),
});

type DebitCardFormContainerProps = {
  navigation: NavigationStackProp;
} & ReturnType<typeof mapDispatchToProps> &
  ReturnType<typeof mapStateToProps>;

type FormDataType = User & typeof forms.debitCard.initialValues;

const DebitCardFormContainer = ({
  userInfo,
  addPaymentMethod,
  navigation,
}: DebitCardFormContainerProps) => {
  const [modalData, setModalData] = useState();

  /**
   * save debit card
   * @param values DebitCardFormValues
   */
  const onSubmit = async (values: FormikValues, setSubmitting: (isSubmitting: boolean) => void) => {
    // TODO where to put these validations?
    values.expiryDate = values.expDate.split('/')[1] + values.expDate.split('/')[0];
    values.securityCode = values.cvc;
    values.line1Address = values.addressLine1;
    values.line2Address = values.addressLine2;
    values.cardNumber = values.cardNumber.replace(/\s/g, '');

    setModalData(undefined);

    const res = await addPaymentMethod(values, PaymentMethodTypeKeys.DEBIT_CARD);
    if (res.success) {
      navigation.goBack();
    } else {
      setSubmitting(false);

      setModalData({
        title: "Can't save the information",
        options: [res.details],
        buttonTitle: 'Close',
        visible: true,
      });
    }
  };

  return (
    <DebitCardForm
      userInfo={userInfo}
      debitCardValues={forms.debitCard.initialValues}
      onSubmit={onSubmit}
      modalData={modalData}
    />
  );
};

export default compose(
  withNavigation,
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(DebitCardFormContainer);
