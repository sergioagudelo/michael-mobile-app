/* eslint-disable global-require */
import React, { useState } from 'react';
import { withStyles, Text, Layout, Button, ThemedComponentProps } from 'react-native-ui-kitten';

import { NavigationStackScreenProps } from 'react-navigation-stack';

import { connect } from 'react-redux';
import { RootState } from 'typesafe-actions';
import { withNavigation } from 'react-navigation';
import { Dispatch, bindActionCreators } from 'redux';
import * as store from '../store';

// FIXME loan details circular dependencies
import * as loanStore from '../../loans/store';
import LoanBalanceHeader from '../../loans/components/LoanBalanceHeader';

// modules
import profile from '../../profile';
import shared from '../../shared';

import { AutopaySetup as AutopaySetupComponent } from '../components';
import { PaymentMethodType } from '../utils';
import { ApiResponse } from '../../../utils/services';
import * as loanService from '../../loans/services/loans';

const { store: profileStore } = profile;

type LoanDetailsMapStateProps = {
  loanId: string;
};

const mapStateToProps = (state: RootState, { loanId }: LoanDetailsMapStateProps) => ({
  userInfo: profileStore.selectors.getUserInfo(state),
  paymentMethods: store.selectors.getPaymentMethods(state),
  defaultPaymentMethod: store.selectors.getDefaultPaymentMethod(state),
  loan: loanStore.selectors.getLoanById(state, loanId),
  loanDetails: loanStore.selectors.getLoanDetails(state),
});

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(
    {
      getLoanDetails: loanStore.actions.thunks.getDetails,
    },
    dispatch,
  );

type MakePaymentProps = LoanDetailsMapStateProps &
  ThemedComponentProps &
  NavigationStackScreenProps &
  ReturnType<typeof mapStateToProps> &
  ReturnType<typeof mapDispatchToProps>;

const AutopaySetup = ({
  navigation,
  paymentMethods,
  defaultPaymentMethod,
  loan,
  userInfo,
  getLoanDetails,
}: MakePaymentProps) => {
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [error, setError] = useState<string | undefined>();

  const handleNewPaymentMethod = () => {
    navigation.navigate('PaymentMethods');
  };
  const handleSetAsAutopay = async (method: PaymentMethodType): Promise<ApiResponse> => {
    const loanId = loan ? loan.loanId : ' ';
    setIsSubmitting(true);
    setError(undefined);
    const result = await loanService.setAutoPayMethod(loanId, method);
    if (result.success) {
      await getLoanDetails(loanId);
      setIsSubmitting(false);
      navigation.goBack();
    }
    setIsSubmitting(false);
    setError(result.details);
    return result;
  };
  return (
    <>
      {loan ? <LoanBalanceHeader lai={loan.lai} balance={loan.balance} /> : null}
      <AutopaySetupComponent
        userFullName={userInfo ? `${userInfo.firstName} ${userInfo.lastName}` : ''}
        defaultPaymentMethod={defaultPaymentMethod}
        paymentMethods={paymentMethods}
        isLoading={isSubmitting}
        errorMsg={error}
        setAsAutopay={handleSetAsAutopay}
        onNewPaymentMethod={handleNewPaymentMethod}
      />
    </>
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(
  withStyles(withNavigation(AutopaySetup), theme => ({
    itemSeparator: {
      marginVertical: 10,
    },
    centerText: {
      textAlign: 'center',
    },
    uppercase: {
      textTransform: 'uppercase',
    },
    subtitle: {
      paddingTop: 10,
    },
    wrapper: {
      marginTop: 20,
    },
    content: {
      paddingTop: 40,
      paddingHorizontal: 30,
    },
    loanHeader: {
      flexDirection: 'row',
      justifyContent: 'center',
    },
    paymentItem: {
      borderBottomWidth: 2,
      borderBottomColor: '#DEE2E8',
    },
    paymentContainer: {
      marginTop: 20,
      padding: 25,
      borderColor: theme['color-basic-400'],
      borderWidth: 1,
      elevation: 2,
      ...shared.helpers.shadow(2),
    },
    paymentTitle: {
      paddingBottom: 10,
    },
    paymentIconContainer: {
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      marginHorizontal: 8,
      paddingHorizontal: 6,
      paddingVertical: 6,
    },
    paymentIcon: {
      width: 40,
      height: 40,
      tintColor: theme['color-success-500'],
    },
    paymentTotal: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      paddingBottom: 10,
    },

    paymentError: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      marginVertical: 15,
      paddingBottom: 10,
    },
    paymentNotice: {
      paddingVertical: 20,
    },
    paymentTC: {
      paddingVertical: 15,
    },
    loanBalance: {
      paddingLeft: 20,
    },
    lastPaymentInfo: {
      paddingVertical: 15,
    },
    item: {
      paddingVertical: 15,
    },
    titleStyle: {
      color: theme['color-basic-800'],
    },
    descriptionStyle: {
      color: theme['color-primary-700'],
    },
  })),
);
