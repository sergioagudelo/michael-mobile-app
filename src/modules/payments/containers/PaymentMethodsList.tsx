import React, { useState, useCallback } from 'react';
import { useFocusEffect } from 'react-navigation-hooks';

// Types
import { Dispatch, bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { RootState } from 'typesafe-actions';
import * as store from '../store';

// Components
import PaymentMethodsListComponent from '../components/PaymentMethodsList';
import { PaymentMethodType } from '../utils';

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(
    {
      getPaymentMethods: store.actions.thunks.getPaymentMethods,
      setPaymentsDefault: store.actions.thunks.setPaymentsDefault,
      deletePayment: store.actions.thunks.deleteSelectedPayment,
      refreshPaymentMethods: store.actions.thunks.refreshPaymentMethods,
      updateDefaultPaymentMethod: store.actions.thunks.updateDefaultPaymentMethod,
    },
    dispatch,
  );

const mapStateToProps = (state: RootState) => ({
  paymentMethodOptions: store.selectors.getPaymentMethods(state),
});

type PaymentMethodsListProps = ReturnType<typeof mapDispatchToProps> &
  ReturnType<typeof mapStateToProps>;

const PaymentMethodsList = ({
  getPaymentMethods,
  setPaymentsDefault,
  deletePayment,
  paymentMethodOptions,
  refreshPaymentMethods,
  updateDefaultPaymentMethod,
}: PaymentMethodsListProps) => {
  const [isLoading, setIsLoading] = useState(false);
  const [deleteId, setDeleteId] = useState('');
  const [loadingDefaultMethod, setLoadingDefaultMethod] = useState('');

  /**
   * useFocusEffect
   * Permit to execute an effect when the screen takes focus, and cleanup the effect when the screen loses focus.
   * @see https://github.com/react-navigation/hooks#usefocuseffectcallback
   */
  useFocusEffect(
    useCallback(() => {
      const getData = async () => {
        setIsLoading(true);
        await getPaymentMethods();
        setIsLoading(false);
      };
      if (!paymentMethodOptions) {
        getData();
      }
    }, [getPaymentMethods, paymentMethodOptions]),
  );

  const setMethodAsDefault = async (paymentMethod: PaymentMethodType) => {
    // Show loader on some paymentMethodCard
    setLoadingDefaultMethod(paymentMethod.paymentMethodId);
    // remove default from all cards
    updateDefaultPaymentMethod(paymentMethodOptions, '');
    const result = await setPaymentsDefault(paymentMethod.paymentMethodId, paymentMethod.mode);
    // Hide loader on all paymentMethodCards
    setLoadingDefaultMethod('');
    if (result && result.success) {
      updateDefaultPaymentMethod(paymentMethodOptions, paymentMethod.paymentMethodId);
    }
    // TODO add selector for default method
  };

  const onDelete = async (paymentMethod: PaymentMethodType, index: number) => {
    // TODO handle error and result
    await deletePayment(
      paymentMethod.paymentMethodId,
      setDeleteId,
      paymentMethodOptions,
      index,
      paymentMethod.mode,
    );
    refreshPaymentMethods(paymentMethodOptions);
  };

  return (
    <PaymentMethodsListComponent
      paymentMethods={paymentMethodOptions}
      isListLoading={isLoading}
      loadingDefaultMethod={loadingDefaultMethod}
      deleteId={deleteId}
      onPressSetAsDefault={setMethodAsDefault}
      onDelete={onDelete}
    />
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PaymentMethodsList);
