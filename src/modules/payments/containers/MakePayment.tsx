/* eslint-disable global-require */
import React, { useState } from 'react';
import {
  withStyles,
  Text,
  Layout,
  Button,
  ThemedComponentProps,
  ListItem,
} from 'react-native-ui-kitten';

import { NavigationStackScreenProps } from 'react-navigation-stack';

import { connect } from 'react-redux';

import { RootState } from 'typesafe-actions';
import profile from '../../profile';
import * as loanStore from '../../loans/store';

import * as store from '../store'; // TODO refactor selector for debit and bank

import shared from '../../shared';
import LoanBalanceHeader from '../../loans/components/LoanBalanceHeader';
import { PaymentType } from '../utils';
import * as helpers from '../helpers';

import { ChoosePaymentModal, PaymentMethodSelector } from '../components';
import * as api from '../services/payments';

const { Dashed, Loading } = shared.components;

type LoanDetailsMapStateProps = {
  loanId: string;
};

const mapStateToProps = (state: RootState, { loanId }: LoanDetailsMapStateProps) => ({
  email: profile.store.selectors.getUserEmail(state),
  loan: loanStore.selectors.getLoanById(state, loanId),
  paymentMethods: store.selectors.getPaymentMethods(state),
  _defaultMethod: store.selectors.getDefaultPaymentMethod(state),
});

type MakePaymentProps = {
  loanId: string;
} & ThemedComponentProps &
  NavigationStackScreenProps &
  ReturnType<typeof mapStateToProps>;

const MakePayment = ({
  loan,
  theme,
  navigation,
  themedStyle,
  paymentMethods,
  _defaultMethod,
  email,
}: MakePaymentProps) => {
  const [isModalVisible, setIsModalVisible] = useState(false);

  const [defaultMethod, setDefaultMethod] = useState(_defaultMethod);
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [error, setError] = useState<string>();

  const payment: PaymentType = {
    contractId: loan.loanId,
    amount: loan.payment,
    fee: 0, // todo use config
    total: loan.payment,
    date: shared.helpers.format.formatDate(
      helpers.getPaymentDate(defaultMethod ? defaultMethod.isACH : false),
      'YYYY-MM-DD',
    ),
    isAdditionalPayment: false,
    transactionId: '',
    mode: '',
    email,
  };

  const handleSubmitPayment = async () => {
    setIsSubmitting(true);
    setError(undefined);
    const res = await api.makePayment(defaultMethod.paymentMethodId, payment, defaultMethod.mode);
    setIsSubmitting(false);
    if (res && res.success && res.response) {
      payment.transactionId = res.response.loanTransactionId;

      navigation.navigate('PaymentReceipt', {
        payment: { ...payment, paymentMethod: defaultMethod },
      });
      return;
    }
    setError(res && res.details);
    // navigation.navigate('TryAgain', { isLogged: true });
  };
  if (!defaultMethod || !loan) {
    navigation.goBack();
    return <Loading spinnerColors={['#fff']} color="#fff" isLoading />;
  }
  return (
    <>
      <ChoosePaymentModal
        title="How would you like to pay?"
        buttonTitle="Continue"
        visible={isModalVisible}
        selectedOption={defaultMethod}
        options={paymentMethods}
        onSubmit={setDefaultMethod}
        onDismiss={() => setIsModalVisible(false)}
        onNewPaymentMethod={() => navigation.navigate('PaymentMethods')}
      />

      <LoanBalanceHeader lai={loan.lai} balance={loan.balance} />

      <Layout style={themedStyle.paymentContainer}>
        <Text
          category="h2"
          status="accent"
          style={[themedStyle.paymentTitle, themedStyle.centerText]}
        >
          Payment Options:
        </Text>
        <ListItem
          title="minimum amount due"
          titleStyle={[themedStyle.uppercase, themedStyle.titleStyle]}
          description={shared.helpers.format.formatMoney(payment.amount, {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2,
          })}
          descriptionStyle={themedStyle.descriptionStyle}
          disabled
        />
        <Dashed color={theme['color-basic-600']} customStyle={themedStyle.itemSeparator} />

        <PaymentMethodSelector
          disabled={isSubmitting}
          theme={theme}
          isLoading={paymentMethods === null}
          defaultMethod={defaultMethod}
          setIsModalVisible={setIsModalVisible}
          handleNoPaymentMethods={() => navigation.navigate('PaymentMethods')}
        />

        <Dashed color={theme['color-basic-600']} customStyle={themedStyle.itemSeparator} />
        <ListItem
          title="PAYMENT DATE"
          titleStyle={[themedStyle.uppercase, themedStyle.titleStyle]}
          description={shared.helpers.format.formatDate(
            payment.date,
            shared.utils.formatDateOptions[1],
          )}
          descriptionStyle={themedStyle.descriptionStyle}
          accessory={() => (
            <Button
              appearance="ghost"
              size="tiny"
              status="info"
              textStyle={themedStyle.uppercase}
            />
          )}
          disabled
        />
        <Dashed color={theme['color-primary-700']} customStyle={themedStyle.itemSeparator} />
        <Layout style={themedStyle.paymentTotal}>
          <Text category="p1" status="accent" style={themedStyle.uppercase}>
            PAYMENT AMOUNT
          </Text>
          <Text category="number" status="accent">
            {shared.helpers.format.formatMoney(payment.total as number, {
              minimumFractionDigits: 2,
              maximumFractionDigits: 2,
            })}
          </Text>
        </Layout>

        {error && (
          <Layout style={themedStyle.paymentError}>
            <Text category="p1" status="danger" style={themedStyle.centerText}>
              {error}
            </Text>
          </Layout>
        )}

        {isSubmitting ? (
          <Loading isLoading={isSubmitting} />
        ) : (
          <Button
            size="large"
            disabled={!paymentMethods || !defaultMethod}
            textStyle={themedStyle.uppercase}
            onPress={handleSubmitPayment}
          >
            SUBMIT PAYMENT
          </Button>
        )}
      </Layout>
    </>
  );
};

export default connect(mapStateToProps)(
  withStyles(MakePayment, theme => ({
    itemSeparator: {
      marginVertical: 10,
    },
    centerText: {
      textAlign: 'center',
    },
    uppercase: {
      textTransform: 'uppercase',
    },
    subtitle: {
      paddingTop: 10,
    },
    wrapper: {
      marginTop: 20,
    },
    content: {
      paddingTop: 40,
      paddingHorizontal: 30,
    },
    loanHeader: {
      flexDirection: 'row',
      justifyContent: 'center',
    },
    paymentItem: {
      borderBottomWidth: 2,
      borderBottomColor: '#DEE2E8',
    },
    paymentContainer: {
      marginTop: 20,
      padding: 25,
      borderColor: theme['color-basic-400'],
      borderWidth: 1,
      elevation: 2,
      ...shared.helpers.shadow(2),
    },
    paymentTitle: {
      paddingBottom: 10,
    },
    paymentIconContainer: {
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      marginHorizontal: 8,
      paddingHorizontal: 6,
      paddingVertical: 6,
    },
    paymentIcon: {
      width: 40,
      height: 40,
      tintColor: theme['color-success-500'],
    },
    paymentTotal: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      paddingBottom: 10,
    },

    paymentError: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      marginVertical: 15,
      paddingBottom: 10,
    },
    paymentNotice: {
      paddingVertical: 20,
    },
    paymentTC: {
      paddingVertical: 15,
    },
    loanBalance: {
      paddingLeft: 20,
    },
    lastPaymentInfo: {
      paddingVertical: 15,
    },
    item: {
      paddingVertical: 15,
    },
    titleStyle: {
      color: theme['color-basic-800'],
    },
    descriptionStyle: {
      color: theme['color-primary-700'],
    },
  })),
);
