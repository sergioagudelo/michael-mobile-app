/* eslint-disable global-require */
import React, { useRef, useState } from 'react';
import { View, Image, Platform, Alert } from 'react-native';
import {
  withStyles,
  Text,
  ThemedComponentProps,
  ListItem,
  Icon,
  Button,
  Layout,
} from 'react-native-ui-kitten';
import ViewShot from 'react-native-view-shot';
import RNFetchBlob from 'rn-fetch-blob';

import { connect } from 'react-redux';
import { RootState } from 'typesafe-actions';

import * as store from '../../profile/store';

import * as api from '../services/payments';

// Types
import { PaymentType } from '../utils';

// Modules
import shared from '../../shared';
import { PaymentMethodTypeKeys } from '../constants';
import { Modal } from '../../shared/components';

// Components
const { Dashed, Loading } = shared.components;

const mapStateToProps = (state: RootState) => ({
  userInfo: store.selectors.getUserInfo(state),
});

type PaymentReceiptComponentProps = {
  payment: PaymentType;
} & ThemedComponentProps &
  ReturnType<typeof mapStateToProps>;

const PaymentReceiptComponent = ({
  payment,
  theme,
  themedStyle,
  userInfo,
}: PaymentReceiptComponentProps) => {
  const paymentMethodUsedIsAch = payment.mode === PaymentMethodTypeKeys.BANK_ACCOUNT || false;
  payment.date = payment.date || ('' as Date);
  const receiptShot = useRef<ViewShot>(null);

  const [isLoading, setIsLoading] = useState(false);
  const [modalData, setModalData] = useState();

  const sendReceipt = async () => {
    setIsLoading(true);
    const result = await api.sendReceipt(payment);
    setIsLoading(false);
    if (result.success) {
      setModalData({
        title: 'Payment Receipt succesfully sent!',
        options: [`An email was sent to ${payment.email}`],
        buttonTitle: 'Ok',
        visible: true,
      });
      return;
    }

    setModalData({
      title: "Can't send the email",
      options: [result.error],
      buttonTitle: 'Close',
      visible: true,
    });
  };

  const printReceipt = async () => {
    if (receiptShot.current && receiptShot.current.capture) {
      try {
        const capture = RNFetchBlob.base64.encode(
          RNFetchBlob.base64.decode(await receiptShot.current.capture()),
        );
        const captureName = `receipts/${`${payment.transactionId}_`??''}${
          payment.date ? payment.date.toString() : Date.now()
        }.jpg`;
        const captureMime = 'image/jpeg';
        const captureUri = await shared.helpers.saveFile(captureName, capture);

        if (Platform.OS === 'android') {
          RNFetchBlob.android.actionViewIntent(captureUri, captureMime);
        } else {
          RNFetchBlob.ios.previewDocument(captureUri);
        }
      } catch (e) {
        Alert.alert('Oops', `There was an error saving the receipt:\n${e.message}`);
      }
    }
  };

  return (
    <>
      {modalData && <Modal {...modalData} />}
      <View style={themedStyle.receiptMessageContainer}>
        <Image
          source={require('../../../img/icons/ico_ok.png')}
          style={themedStyle.receiptMessageIcon}
        />
        <Text appearance="alternative" category="h4" style={themedStyle.receiptMessage}>
          {`Your payment has been ${
            paymentMethodUsedIsAch ? 'submitted to your bank for processing' : 'received'
          }.\nThank you, ${userInfo.firstName}`}
        </Text>
      </View>

      <ViewShot ref={receiptShot} options={{ format: 'jpg', result: 'base64' }}>
        <Layout>
          <Text
            category="h2"
            status="accent"
            style={[themedStyle.centerText, themedStyle.receiptTitle]}
          >
            Payment Receipt
          </Text>

          <View style={themedStyle.receiptContainer}>
            <ListItem
              icon={() => (
                <Image
                  source={require('../../../img/icons/ico_ok.png')}
                  style={themedStyle.itemIcon}
                />
              )}
              title="PAYMENT AMOUNT DUE"
              description={shared.helpers.format.formatMoney(payment.amount, {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2,
              })}
              style={themedStyle.receiptItem}
              titleStyle={themedStyle.uppercase}
              disabled
            />
            <Dashed color={theme['color-basic-600']} customStyle={themedStyle.itemSeparator} />

            <ListItem
              icon={() => (
                <Image
                  source={require('../../../img/icons/ico_ok.png')}
                  style={themedStyle.itemIcon}
                />
              )}
              title="PAYMENT METHOD"
              description={shared.helpers.format.formatPaymentMethodNumber(payment.paymentMethod)}
              style={themedStyle.receiptItem}
              titleStyle={themedStyle.uppercase}
              disabled
            />
            <Dashed color={theme['color-basic-600']} customStyle={themedStyle.itemSeparator} />

            <ListItem
              icon={() => (
                <Image
                  source={require('../../../img/icons/ico_ok.png')}
                  style={themedStyle.itemIcon}
                />
              )}
              title="PAYMENT DATE"
              description={shared.helpers.format.formatDate(
                payment.date.toString(),
                'dddd MMM Do, YYYY',
              )}
              style={themedStyle.receiptItem}
              titleStyle={themedStyle.uppercase}
              disabled
            />
            <Dashed color={theme['color-basic-600']} customStyle={themedStyle.itemSeparator} />

            <ListItem
              icon={() => (
                <Image
                  source={require('../../../img/icons/ico_ok.png')}
                  style={themedStyle.itemIcon}
                />
              )}
              title="TOTAL PAID"
              description={shared.helpers.format.formatMoney(payment.total || 0, {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2,
              })}
              style={themedStyle.receiptItem}
              titleStyle={themedStyle.uppercase}
              disabled
            />
          </View>
        </Layout>
      </ViewShot>

      {/* TODO button loader */}
      {isLoading ? (
        <Loading isLoading={isLoading} />
      ) : (
        <Layout style={[themedStyle.item, themedStyle.receiptActions]}>
          <Button
            appearance="outline"
            status="basic"
            size="small"
            icon={style => <Icon name="email-outline" {...style} />}
            textStyle={themedStyle.uppercase}
            disabled={paymentMethodUsedIsAch}
            {...shared.helpers.setTestID('EmailReceiptButton')}
            onPress={sendReceipt}
          >
            Send to email
          </Button>
          <Button
            appearance="outline"
            status="basic"
            size="small"
            icon={style => <Icon name="printer-outline" {...style} />}
            onPress={printReceipt}
            textStyle={themedStyle.uppercase}
            {...shared.helpers.setTestID('PrintReceiptButton')}
          >
            Print receipt
          </Button>
        </Layout>
      )}
    </>
  );
};

export default connect(mapStateToProps)(
  withStyles(PaymentReceiptComponent, theme => ({
    screen: {
      flex: 1,
    },
    body: {
      flexGrow: 1,
    },
    centerText: {
      textAlign: 'center',
    },
    uppercase: {
      textTransform: 'uppercase',
    },
    subtitle: {
      paddingTop: 10,
    },
    content: {
      paddingVertical: 15,
    },
    item: {
      marginBottom: 20,
      marginHorizontal: 30,
    },
    receiptMessageContainer: {
      padding: 20,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: theme['color-success-600'],
    },
    receiptMessageIcon: {
      tintColor: theme['color-success-100'],
    },
    receiptMessage: {
      textAlign: 'center',
      paddingTop: 10,
      justifyContent: 'center',
    },
    receiptTitle: {
      paddingVertical: 20,
    },
    receiptContainer: {
      paddingVertical: 10,
      marginBottom: 20,
      marginHorizontal: 30,
      borderWidth: 2,
      borderColor: theme['color-basic-600'],
    },
    receiptItem: {
      paddingVertical: 10,
    },
    receiptActions: {
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    homeButton: {
      alignSelf: 'center',
    },
    itemIcon: {
      width: 40,
      height: 40,
      tintColor: theme['color-basic-600'],
    },
  })),
);
