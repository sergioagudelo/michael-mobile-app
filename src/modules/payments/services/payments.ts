import { lendingpoint } from '../../../services';
import { ApiResponse } from '../../../services/utils';

import {
  ResponsePayments,
  MakePaymentResponse,
  PaymentType,
  PaymentMethodModeType,
} from '../utils';
import { PaymentMethodTypeKeys } from '../constants';
import { handleError } from '../../../utils/services';

const {
  api: { consumer },
  constants: { paths },
  helpers: { handleSubModuleError },
} = lendingpoint;

export const getPaymentMethods = async (): Promise<ApiResponse<ResponsePayments>> => {
  try {
    const response = await consumer.get(paths.paymentMethods);
    return { success: true, response: response.data, status: response.status };
  } catch (err) {
    return handleSubModuleError(err);
  }
};

export const addPaymentMethod = async (
  payload: any, // TODO fix form types
  mode: PaymentMethodModeType,
): Promise<ApiResponse<ResponsePayments>> => {
  try {
    const response = await consumer.post(`${paths.paymentMethods}`, {
      ...payload,
      paymentMethod: mode,
    });
    return { success: true, response: response.data };
  } catch (err) {
    return handleSubModuleError(err);
  }
};

export const savePaymentsDefault = async (
  accountId: string,
  mode: string,
): Promise<ApiResponse<ResponsePayments>> => {
  try {
    const response = await consumer.patch(`${paths.paymentMethods}/${accountId}`, {
      paymentMethod: mode,
    });
    return { success: true, response: response.data };
  } catch (err) {
    return handleSubModuleError(err);
  }
};

export const deletePayment = async (
  paymentMethodId: string,
  mode: string,
): Promise<ApiResponse<ResponsePayments>> => {
  try {
    const response = await consumer.delete(
      `${paths.paymentMethods}/${paymentMethodId}?paymentMethod=${mode}`,
      {},
    );
    return { success: true, response: response.data, status: response.status };
  } catch (err) {
    return handleSubModuleError(err);
  }
};

export const makePayment = async (
  paymentMethodId: string,
  payment: PaymentType,
  mode: PaymentMethodModeType,
): Promise<ApiResponse<MakePaymentResponse>> => {
  const { contractId, amount, isAdditionalPayment, date, email } = payment;
  try {
    const response = await consumer.post(
      `${paths.loans}/${contractId}/payments`,
      mode === PaymentMethodTypeKeys.BANK_ACCOUNT
        ? {
            bankId: paymentMethodId,
            paymentDate: date ?? '',
            email: email ?? '',
            paymentMethod: mode,
            amount,
            isAdditionalPayment,
          }
        : {
            accountId: paymentMethodId,
            paymentMethod: mode,
            amount,
            isAdditionalPayment,},
    );
    return { success: true, response: response.data, status: response.status };
  } catch (err) {
    return handleSubModuleError(err);
  }
};

export const _sendReceipt = async (payment: PaymentType) => {
  const { contractId, transactionId } = payment;
  try {
    const response = await consumer.post(
      `${paths.loans}/${contractId}/payments/${transactionId}?action=sendMail`,
    );
    return { success: true, response: response.data, status: response.status };
  } catch (err) {
    return handleSubModuleError(err);
  }
};

export const sendReceipt = async (payment: PaymentType) => {
  const { contractId, transactionId, paymentMethod:{ mode } } = payment;
  try {
    const response = await api.post(
      `${paths.loans}/${contractId}/payment-receipt`,
      {
        paymentMethod:mode,
        transactionId
      }
    );
    return { success: true, response: response.data, status: response.status };
  } catch (err) {
    return handleError(err);
  }
};


