/* eslint-disable global-require */
import React from 'react';
import { SafeAreaView, ScrollView, View } from 'react-native';
import { withStyles, ThemedComponentProps, Text } from 'react-native-ui-kitten';
import { NavigationStackScreenProps } from 'react-navigation-stack';

// Config
// import config from '../../../config';

// Modules
import shared from '../../shared';
// import { MakePayment } from '../containers';
import { MakePaymentHeader } from '../components';
import { AutopaySetup } from '../containers';

const { Background } = shared.components;

type AutopaySetupLayoutProps = NavigationStackScreenProps & ThemedComponentProps;

const AutopaySetupLayout = ({ navigation, theme, themedStyle }: AutopaySetupLayoutProps) => {
  const loanId = navigation.getParam('loanId');

  return (
    <>
      <SafeAreaView style={themedStyle.screen} {...shared.helpers.setTestID('AutopaySetupScreen')}>
        <ScrollView contentContainerStyle={themedStyle.body}>
          <Text category="h1" status="accent" style={themedStyle.centerText}>
            Loan Autopayments
          </Text>
          <Text
            category="metatext"
            style={[themedStyle.centerText, themedStyle.uppercase, themedStyle.subtitle]}
          >
            Loan autopayment setup
          </Text>
          <View>
            <Background
              colors={[
                theme['color-primary-600'],
                theme['color-primary-700'],
                theme['color-primary-900'],
              ]}
              wrapperStyle={themedStyle.wrapper}
            />
          </View>

          <View style={[themedStyle.content]}>
            <AutopaySetup loanId={loanId} />
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const MakePaymentScreen = withStyles(AutopaySetupLayout, theme => ({
  screen: {
    flex: 1,
  },
  body: {
    flexGrow: 1,
  },
  itemSeparator: {
    marginVertical: 10,
  },
  centerText: {
    textAlign: 'center',
  },
  uppercase: {
    textTransform: 'uppercase',
  },
  subtitle: {
    paddingTop: 10,
    color: theme['color-basic-800'],
  },
  wrapper: {
    marginTop: 20,
  },
  content: {
    paddingTop: 40,
    paddingHorizontal: 30,
  },
  loanHeader: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  paymentItem: {
    borderBottomWidth: 2,
    borderBottomColor: '#DEE2E8',
  },
  paymentContainer: {
    marginTop: 20,
    padding: 25,
    borderColor: theme['color-basic-400'],
    borderWidth: 1,
    elevation: 2,
    ...shared.helpers.shadow(2),
  },
  paymentTitle: {
    paddingBottom: 10,
  },
  paymentIconContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 8,
    paddingHorizontal: 6,
    paddingVertical: 6,
  },
  paymentIcon: {
    width: 40,
    height: 40,
    tintColor: theme['color-success-500'],
  },
  paymentTotal: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingBottom: 10,
  },
  paymentNotice: {
    paddingVertical: 20,
  },
  paymentTC: {
    paddingVertical: 25,
  },
  loanBalance: {
    paddingLeft: 20,
  },
  lastPaymentInfo: {
    paddingVertical: 15,
  },
  item: {
    paddingVertical: 15,
  },
  titleStyle: {
    color: theme['color-basic-800'],
  },
  descriptionStyle: {
    color: theme['color-primary-700'],
  },
  disclaimer: {
    textAlign: 'center',
    paddingHorizontal: 20,
  },
}));

export default MakePaymentScreen;
