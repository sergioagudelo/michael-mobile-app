import MakePayment from './MakePayment';
import PaymentReceipt from './PaymentReceipt';
import PaymentMethods from './PaymentMethods';
import NewBankAccount from './NewBankAccount';
import NewDebitCard from './NewDebitCard';
import ExtraPaymentCheckout from './ExtraPaymentCheckout';
import AutopaySetup from './AutopaySetup';

export {
  MakePayment,
  PaymentReceipt,
  PaymentMethods,
  NewBankAccount,
  NewDebitCard,
  ExtraPaymentCheckout,
  AutopaySetup,
};
