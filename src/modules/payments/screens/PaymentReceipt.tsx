import React from 'react';
import { SafeAreaView, ScrollView, View } from 'react-native';
import { withStyles, ThemedComponentProps, Button, Layout } from 'react-native-ui-kitten';
import { NavigationStackScreenProps } from 'react-navigation-stack';

// Types
import { PaymentType } from '../utils';

// Modules
import shared from '../../shared';
import loans from '../../loans';

// Components
import { PaymentReceipt as PaymentReceiptContainer } from '../containers';
import { MakePaymentHeader } from '../components';

const { RefinancingCard } = loans.components;
const { VirtualCard } = shared.components;

type PaymentReceiptComponentProps = NavigationStackScreenProps<{
  payment: PaymentType;
}> &
  ThemedComponentProps;

const PaymentReceiptComponent = ({ navigation, themedStyle }: PaymentReceiptComponentProps) => {
  const payment = navigation.getParam('payment');

  return (
    <SafeAreaView style={themedStyle.screen} {...shared.helpers.setTestID('PaymentReceiptScreen')}>
      <ScrollView contentContainerStyle={themedStyle.body}>
        <MakePaymentHeader />

        <View style={[themedStyle.content]}>
          <PaymentReceiptContainer payment={payment} />

          <Layout style={[themedStyle.item, themedStyle.homeButton]}>
            <Button
              appearance="link"
              onPress={() => navigation.popToTop()}
              textStyle={themedStyle.uppercase}
              {...shared.helpers.setTestID('BackToLoginFromPaymentReceipt')}
            >
              Go Home
            </Button>
          </Layout>

          {/* <RefinancingCard data={loans.utils.testData.refinanceOption} />
          <VirtualCard dismissible={false} /> */}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const PaymentReceipt = withStyles(PaymentReceiptComponent, theme => ({
  screen: {
    flex: 1,
  },
  body: {
    flexGrow: 1,
  },
  centerText: {
    textAlign: 'center',
  },
  uppercase: {
    textTransform: 'uppercase',
  },
  subtitle: {
    paddingTop: 10,
  },
  content: {
    paddingVertical: 15,
  },
  item: {
    marginBottom: 20,
    marginHorizontal: 30,
  },
  receiptMessageContainer: {
    padding: 20,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme['color-success-600'],
  },
  receiptMessageIcon: {
    tintColor: theme['color-success-100'],
  },
  receiptMessage: {
    paddingTop: 10,
  },
  receiptTitle: {
    paddingVertical: 20,
  },
  receiptContainer: {
    paddingVertical: 10,
    marginBottom: 20,
    marginHorizontal: 30,
    borderWidth: 2,
    borderColor: theme['color-basic-600'],
  },
  receiptItem: {
    paddingVertical: 10,
  },
  receiptActions: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  homeButton: {
    alignSelf: 'center',
  },
  itemIcon: {
    width: 40,
    height: 40,
    tintColor: theme['color-basic-600'],
  },
}));

export default PaymentReceipt;
