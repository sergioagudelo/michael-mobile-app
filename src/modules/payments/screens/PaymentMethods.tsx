import React from 'react';
import { SafeAreaView, ScrollView, View } from 'react-native';
import { withStyles, ThemedComponentProps, Text } from 'react-native-ui-kitten';
import { scale, verticalScale } from 'react-native-size-matters';
import { NavigationStackScreenProps } from 'react-navigation-stack';
import * as Animatable from 'react-native-animatable';

// Modules
import shared from '../../shared';
import payments from '..';

// Components
const { VerticalCardButton } = shared.components;
const { PaymentMethodsList } = payments.containers;

type PaymentMethodsComponentProps = NavigationStackScreenProps & ThemedComponentProps;

const PaymentMethodsComponent = ({ navigation, themedStyle }: PaymentMethodsComponentProps) => (
  <SafeAreaView style={themedStyle.screen} {...shared.helpers.setTestID('PaymentMethodsScreen')}>
    <ScrollView contentContainerStyle={themedStyle.body}>
      <Text category="h1" status="accent" style={themedStyle.centerText}>
        Payment Methods
      </Text>

      <PaymentMethodsList />

      <Text
        category="h2"
        status="accent"
        style={[themedStyle.centerText, { paddingHorizontal: scale(60) }]}
      >
        Add new payment method:
      </Text>

      <View style={themedStyle.actionsContainer}>
        <Animatable.View
          style={themedStyle.flexColumn}
          animation="fadeIn"
          duration={800}
          useNativeDriver
        >
          <VerticalCardButton
            icon={require('../../../img/icons/ico_bank.png')}
            onPress={() => navigation.navigate('NewBankAccount')}
            containerStyle={themedStyle.action}
            captionContainerStyle={themedStyle.actionCaption}
          >
            New Bank Account
          </VerticalCardButton>
        </Animatable.View>
        <Animatable.View
          style={themedStyle.flexColumn}
          animation="fadeIn"
          duration={800}
          delay={200}
          useNativeDriver
        >
          <VerticalCardButton
            icon={require('../../../img/icons/ico_debit_card.png')}
            onPress={() => navigation.navigate('NewDebitCard')}
            containerStyle={themedStyle.action}
            captionContainerStyle={themedStyle.actionCaption}
          >
            New Debit Card
          </VerticalCardButton>
        </Animatable.View>
      </View>
    </ScrollView>
  </SafeAreaView>
);

const PaymentMethods = withStyles(PaymentMethodsComponent, () => ({
  screen: {
    flex: 1,
  },
  body: {
    flexGrow: 1,
  },
  centerText: {
    textAlign: 'center',
  },
  uppercase: {
    textTransform: 'uppercase',
  },
  subtitle: {
    paddingTop: 10,
  },
  actionsContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    paddingVertical: verticalScale(30),
  },
  action: {
    width: 130,
    marginHorizontal: verticalScale(10),
  },
  actionCaption: {
    width: '80%',
  },
}));

export default PaymentMethods;
