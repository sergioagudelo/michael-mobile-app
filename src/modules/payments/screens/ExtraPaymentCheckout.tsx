/* eslint-disable global-require */
import React from 'react';
import { SafeAreaView, ScrollView, View, TouchableOpacity } from 'react-native';
import { withStyles, Text, ThemedComponentProps } from 'react-native-ui-kitten';
import { NavigationStackScreenProps } from 'react-navigation-stack';

// Config
import config from '../../../config';

// Modules
import shared from '../../shared';
import { ExtraPaymentCheckout } from '../containers';
import { MakePaymentHeader } from '../components';

const { Background } = shared.components;

type MakePaymentLayoutProps = NavigationStackScreenProps<{ loanId: string }> & ThemedComponentProps;

const ExtraPayment = ({ navigation, themedStyle }: MakePaymentLayoutProps) => {
  const loanId = navigation.getParam('loanId');
  const amount = navigation.getParam('amount');

  return (
    <>
      <SafeAreaView style={themedStyle.screen} {...shared.helpers.setTestID('MakePaymentScreen')}>
        <ScrollView contentContainerStyle={themedStyle.body}>
          <MakePaymentHeader title="Extra Payment" />
          <View>
            <Background
              colors={['#004B8D', '#003267', '#0072BB']}
              wrapperStyle={themedStyle.wrapper}
            />
          </View>

          <View style={[themedStyle.content]}>
            <ExtraPaymentCheckout amount={amount} loanId={loanId} />

            {/* terms and conditions */}
            <TouchableOpacity
              style={themedStyle.paymentTC}
              onPress={() => shared.helpers.openUrl(config.tcUrl)}
            >
              <Text style={themedStyle.disclaimer}>
                By submitting payment, you agree to the
                <Text status="warning">&nbsp;Terms &amp; Conditions</Text>
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const ExtraPaymentScreen = withStyles(ExtraPayment, theme => ({
  screen: {
    flex: 1,
  },
  body: {
    flexGrow: 1,
  },
  itemSeparator: {
    marginVertical: 10,
  },
  centerText: {
    textAlign: 'center',
  },
  uppercase: {
    textTransform: 'uppercase',
  },
  subtitle: {
    paddingTop: 10,
  },
  wrapper: {
    marginTop: 20,
  },
  content: {
    paddingTop: 40,
    paddingHorizontal: 30,
  },
  loanHeader: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  paymentItem: {
    borderBottomWidth: 2,
    borderBottomColor: '#DEE2E8',
  },
  paymentContainer: {
    marginTop: 20,
    padding: 25,
    borderColor: theme['color-basic-400'],
    borderWidth: 1,
    elevation: 2,
    ...shared.helpers.shadow(2),
  },
  paymentTitle: {
    paddingBottom: 10,
  },
  paymentIconContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 8,
    paddingHorizontal: 6,
    paddingVertical: 6,
  },
  paymentIcon: {
    width: 40,
    height: 40,
    tintColor: theme['color-success-500'],
  },
  paymentTotal: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingBottom: 10,
  },
  paymentNotice: {
    paddingVertical: 20,
  },
  paymentTC: {
    paddingVertical: 25,
  },
  loanBalance: {
    paddingLeft: 20,
  },
  lastPaymentInfo: {
    paddingVertical: 15,
  },
  item: {
    paddingVertical: 15,
  },
  titleStyle: {
    color: theme['color-basic-800'],
  },
  descriptionStyle: {
    color: theme['color-primary-700'],
  },
  disclaimer: {
    textAlign: 'center',
    paddingHorizontal: 20,
  },
}));

export default ExtraPaymentScreen;
