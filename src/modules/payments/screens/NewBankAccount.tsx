import React from 'react';
import { SafeAreaView, Platform } from 'react-native';
import { Text, withStyles, ThemedComponentProps } from 'react-native-ui-kitten';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

// Modules
import shared from '../../shared';
import payments from '..';

// Components
const { NewBankAccountForm } = payments.containers;

type NewBankAccountComponentProps = ThemedComponentProps;

const NewBankAccountComponent = ({ themedStyle }: NewBankAccountComponentProps) => {
  return (
    <SafeAreaView style={themedStyle.screen} {...shared.helpers.setTestID('NewBankAccountScreen')}>
      <KeyboardAwareScrollView
        enableOnAndroid
        contentContainerStyle={themedStyle.body}
        keyboardShouldPersistTaps={Platform.select({
          ios: 'never',
          android: 'handled',
        })}
      >
        <Text category="h1" status="accent" style={themedStyle.centerText}>
          Payment Methods
        </Text>
        <Text
          category="metatext"
          style={[themedStyle.centerText, themedStyle.uppercase, themedStyle.subtitle]}
        >
          Add Bank Account
        </Text>
        <NewBankAccountForm />
      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
};

const NewBankAccount = withStyles(NewBankAccountComponent, () => ({
  screen: {
    flex: 1,
  },
  body: {
    flexGrow: 1,
  },
  centerText: {
    textAlign: 'center',
  },
  uppercase: {
    textTransform: 'uppercase',
  },
  subtitle: {
    paddingTop: 10,
  },
}));

export default NewBankAccount;
