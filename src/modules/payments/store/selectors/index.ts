import { RootState } from 'typesafe-actions';

import { NAME } from '../../constants';
import { PaymentMethodType } from '../../utils';

export const getPaymentMethods = (state: RootState): PaymentMethodType[] => state[NAME].methods;

export const getDefaultPaymentMethod = (state: RootState): PaymentMethodType =>
  state[NAME].methods && state[NAME].methods.find(entry => entry.default === true);
