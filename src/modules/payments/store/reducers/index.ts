import { combineReducers } from 'redux';
import { createReducer } from 'typesafe-actions';

import { PaymentMethodType } from '../../utils';

import { actions } from '../actions';

const methods = createReducer(null as PaymentMethodType[] | null)
  .handleAction(actions.fetchPaymentMethods.success, (state, action) => action.payload)
  .handleAction(actions.updatePaymentMethods, (state, action) => action.payload);

const methodsReducer = combineReducers({
  methods,
});

export default methodsReducer;
export type PaymentsState = ReturnType<typeof methodsReducer>;
