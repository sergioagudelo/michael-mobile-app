// Types
import { Dispatch } from 'redux';
import * as api from '../../services/payments';
import { fetchPaymentMethods, updatePaymentMethods } from './actions';
import { PaymentMethodType } from '../../utils';
import { paymentMethodsParser } from '../../helpers';

export const getPaymentMethods = () => async (dispatch: Dispatch) => {
  dispatch(fetchPaymentMethods.request());
  const { success, response, error } = await api.getPaymentMethods();
  if (success && response) {
    const payments = [...response.debitCards, ...response.bankAccounts];
    dispatch(fetchPaymentMethods.success(paymentMethodsParser(payments)));
    return;
  }
  dispatch(fetchPaymentMethods.failure(error));
};

// TODO type payment methods properly
export const addPaymentMethod = (payload: any, mode: string) => async (dispatch: Dispatch) => {
  const res = await api.addPaymentMethod(payload, mode);
  if (res.success) {
    dispatch(fetchPaymentMethods.success(null));
    return res;
  }
  dispatch(fetchPaymentMethods.failure(res.error));
  return res;
};

/**
 * setup all methods.default = false
 */
export const updateDefaultPaymentMethod = (
  methods: PaymentMethodType[],
  defaultAccountId: string,
) => (dispatch: Dispatch) => {
  dispatch(
    updatePaymentMethods(
      methods.map(entry => {
        entry.default = entry.paymentMethodId === defaultAccountId;
        return entry;
      }),
    ),
  );
};

export const setPaymentsDefault = (paymentMethodId: string, mode: string) => async (
  dispatch: Dispatch,
) => {
  dispatch(fetchPaymentMethods.request());
  const result = await api.savePaymentsDefault(paymentMethodId, mode);
  if (result.success) {
    return result;
  }
  dispatch(fetchPaymentMethods.failure(result.error));
};

/**
 * refresh methods, force render
 */
export const refreshPaymentMethods = (methods: PaymentMethodType[]) => (dispatch: Dispatch) => {
  dispatch(fetchPaymentMethods.success(null));
  dispatch(fetchPaymentMethods.success(methods));
};

export const deleteSelectedPayment = (
  paymentMethodId: string,
  setDeleteId: React.Dispatch<React.SetStateAction<string>>,
  methods: PaymentMethodType[],
  index: number,
  mode: string,
) => async (dispatch: Dispatch) => {
  // TODO is it ok to call component's local state as callback?
  setDeleteId(paymentMethodId);
  const res = await api.deletePayment(paymentMethodId, mode);
  if (res.success) {
    dispatch(fetchPaymentMethods.success(methods.splice(index - 1, 1)));
  }
  setDeleteId('');
};
