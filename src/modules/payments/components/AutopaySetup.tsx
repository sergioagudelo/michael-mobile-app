import React, { useState } from 'react';

import {
  ThemedComponentProps,
  withStyles,
  Text,
  Layout,
  Icon,
  ListItem,
  CheckBox,
  Button,
} from 'react-native-ui-kitten';

import { ApiResponse } from '../../../utils/services';

import { PaymentMethodType } from '../utils';
import * as helpers from '../helpers';
import { autopayConsentTerms, PaymentMethodTypeKeys, PaymentMethodNameKeys } from '../constants';
import AutopayConsentTermsModal from './AutopayConsentTermsModal';
import AutopayLabelConsent from './AutopayLabelConsent';

import shared from '../../shared';

const { Dashed, Loading } = shared.components;

type AutopaySetupProps = {
  userFullName: string;
  dueDate: string;
  defaultPaymentMethod?: PaymentMethodType;
  paymentMethods: PaymentMethodType[];
  isLoading: boolean;
  errorMsg?: string;
  setAsAutopay: (item: PaymentMethodType) => Promise<ApiResponse>;
  onNewPaymentMethod: () => void;
} & ThemedComponentProps;

const AutopaySetup = ({
  paymentMethods,
  isLoading,
  userFullName,
  dueDate,
  setAsAutopay,
  errorMsg,
  theme,
  themedStyle,
}: AutopaySetupProps) => {
  const [selectedPaymentMethod, setSelectedPaymentMethod] = useState<
    PaymentMethodType | undefined
  >();
  const [isConsentAgreed, setIsConsentAgreed] = useState(false);
  const [isModalVisible, setIsModalVisible] = useState(false);

  const handleSetAsAutopay = async () => {
    if (selectedPaymentMethod) {
      await setAsAutopay(selectedPaymentMethod);
    }
  };

  const handleOnDismiss = () => setIsModalVisible(false);

  return (
    <>
      {isModalVisible ? (
        <AutopayConsentTermsModal
          visible={isModalVisible}
          selectedPaymentMethod={selectedPaymentMethod}
          onDismiss={handleOnDismiss}
          consentText={
            autopayConsentTerms[
              selectedPaymentMethod
                ? selectedPaymentMethod.mode
                : PaymentMethodTypeKeys.BANK_ACCOUNT
            ]
          }
        />
      ) : null}
      <Layout style={themedStyle.autopaySetupContainer}>
        <Text style={[themedStyle.centerText, themedStyle.title]}>
          Choose the payment method you want to setup as autopay
        </Text>
        <Dashed color={theme['color-basic-600']} customStyle={themedStyle.itemSeparator} />
        {paymentMethods.map((option, i) => (
          <React.Fragment key={option.paymentMethodId}>
            <ListItem
              themedStyle={themedStyle.listItem}
              title={option.methodName}
              titleStyle={themedStyle.uppercase}
              description={shared.helpers.format.formatPaymentMethodNumber(option)}
              onPress={() => {
                if (isLoading) return;

                setSelectedPaymentMethod(option);
              }}
              icon={() =>
                selectedPaymentMethod && selectedPaymentMethod.isACH ? (
                  <Icon
                    name={
                      selectedPaymentMethod.bankId === option.bankId
                        ? 'checkmark-circle-2'
                        : 'radio-button-off'
                    }
                    fill={
                      selectedPaymentMethod && selectedPaymentMethod.bankId === option.bankId
                        ? theme['color-primary-700']
                        : theme['color-basic-500']
                    }
                  />
                ) : (
                  <Icon
                    name={
                      selectedPaymentMethod &&
                      selectedPaymentMethod.paymentMethodId === option.paymentMethodId
                        ? 'checkmark-circle-2'
                        : 'radio-button-off'
                    }
                    fill={
                      selectedPaymentMethod &&
                      selectedPaymentMethod.paymentMethodId === option.paymentMethodId
                        ? theme['color-primary-700']
                        : theme['color-basic-500']
                    }
                  />
                )
              }
            />
            {i !== paymentMethods.length - 1 && (
              <Dashed color={theme['color-basic-600']} customStyle={themedStyle.itemSeparator} />
            )}
          </React.Fragment>
        ))}
      </Layout>

      <Layout style={themedStyle.consentContainer}>
        <AutopayLabelConsent
          dueDate={dueDate}
          userFullName={userFullName}
          paymentMethod={selectedPaymentMethod}
          handlePress={() => setIsModalVisible(true)}
        />
        {selectedPaymentMethod ? (
          <Layout style={themedStyle.toggleContainer}>
            <CheckBox
              checked={isConsentAgreed}
              onChange={checkBoxStatus => setIsConsentAgreed(checkBoxStatus)}
              style={themedStyle.checkBox}
            />
            <Text status="primary" category="metatext" style={themedStyle.toggleLabel}>
              Yes, I have read the terms and conditions specified, and I agree to authorize this
              recurring transaction.
            </Text>
          </Layout>
        ) : null}
        {errorMsg ? (
          <Layout style={themedStyle.paymentError}>
            <Text category="p1" status="danger" style={themedStyle.centerText}>
              {errorMsg}
            </Text>
          </Layout>
        ) : null}
        {isLoading ? (
          <Loading isLoading />
        ) : (
          <Button
            size="large"
            disabled={!paymentMethods || !selectedPaymentMethod || !isConsentAgreed}
            textStyle={themedStyle.uppercase}
            onPress={handleSetAsAutopay}
            style={themedStyle.cta}
          >
            Enroll in autopay
          </Button>
        )}
      </Layout>
    </>
  );
};

export default withStyles(AutopaySetup, theme => ({
  centerText: {
    textAlign: 'center',
  },
  uppercase: {
    textTransform: 'uppercase',
  },
  autopaySetupContainer: {
    margin: 10,
    padding: 20,
    borderColor: theme['color-basic-400'],
    borderWidth: 1,
    elevation: 2,
    ...shared.helpers.shadow(2),
  },
  title: {
    padding: 15,
    paddingTop: 5,
    color: theme['color-basic-1000'],
  },
  listItem: {
    padding: 0,
  },
  itemSeparator: {
    marginVertical: 10,
  },
  consentContainer: {
    marginHorizontal: 20,
    marginVertical: 15,
  },
  labelConsentText: {
    marginBottom: 20,
  },
  toggleContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'flex-start',
    paddingHorizontal: 5,
    paddingLeft: 12,
    marginBottom: 15,
  },
  toggleLabel: {
    paddingHorizontal: 5,
    color: theme['color-basic-1000'],
  },
  checkBox: {
    marginRight: 30,
  },
  errorLabel: {
    marginBottom: 15,
  },
  link: {
    fontWeight: '500',
  },
  cta: {
    marginTop: 15,
  },
}));
