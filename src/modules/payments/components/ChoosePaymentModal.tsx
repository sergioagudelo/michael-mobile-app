import React, { useState, useEffect, useCallback, useRef } from 'react';
import { View, ListRenderItemInfo, BackHandler } from 'react-native';
import {
  withStyles,
  Text,
  Layout,
  ListItem,
  Button,
  Icon,
  ThemedComponentProps,
  ModalService,
} from 'react-native-ui-kitten';
import color from 'color';

// Types
import { useFocusEffect } from 'react-navigation-hooks';
import { PaymentMethodType } from '../utils';
import { PaymentMethodNameKeys } from '../constants';

// Modules
import shared from '../../shared';

// Components
const { Dashed } = shared.components;

type ChoosePaymentModalProps = ThemedComponentProps & {
  visible: boolean;
  selectedOption: PaymentMethodType;
  options: PaymentMethodType[];
  onDismiss: () => void;
  onNewPaymentMethod: () => void;
  onSubmit: (option: PaymentMethodType) => void;
  title: string;
  buttonTitle: string;
};

const ChoosePaymentModal = ({
  options: paymentMethodOptions,
  selectedOption: selectedPaymentMethodOption,
  onSubmit: setPaymentMethod,
  onNewPaymentMethod,
  buttonTitle,
  title,
  visible,
  onDismiss,
  theme,
  themedStyle,
}: ChoosePaymentModalProps) => {
  const { height, width } = shared.helpers.hooks.useScreenDimensions();
  const [value, setValue] = useState(selectedPaymentMethodOption);
  const [prevValue, setPrevValue] = useState(value);

  const modalId = useRef('');
  const checkIsDefaultPaymentMethod = (
    _value: PaymentMethodType,
    defaultMethod: PaymentMethodType,
  ) => {
    if (defaultMethod.isACH) {
      return _value.bankId === defaultMethod.bankId;
    }
    return _value.paymentMethodId === defaultMethod.paymentMethodId;
  };
  const hideModal = useCallback(() => {
    setValue(selectedPaymentMethodOption);
    modalId.current = ModalService.hide(modalId.current);
    if (onDismiss) {
      onDismiss();
    }
  }, [onDismiss, selectedPaymentMethodOption]);

  useFocusEffect(
    useCallback(() => {
      const onBackPress = () => {
        if (visible) {
          hideModal();
          return true;
        }
        return false;
      };

      BackHandler.addEventListener('hardwareBackPress', onBackPress);

      return () => BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    }, [visible, hideModal]),
  );

  // TODO CARD NUMBER PARSER SHARED
  const renderModal = useCallback(() => {
    const modalRenderItem = ({ item: option, index: i }: ListRenderItemInfo<PaymentMethodType>) => (
      <React.Fragment key={option.paymentMethodId}>
        <ListItem
          title={
            option.isACH ? PaymentMethodNameKeys.BANK_ACCOUNT : PaymentMethodNameKeys.DEBIT_CARD
          }
          titleStyle={themedStyle.uppercase}
          description={shared.helpers.format.formatPaymentMethodNumber(option)}
          onPress={() => {
            setValue(prev => {
              setPrevValue(prev);
              return option;
            });
          }}
          icon={() => (
            <Icon
              name={
                value && value.paymentMethodId === option.paymentMethodId
                  ? 'checkmark-circle-2'
                  : 'radio-button-off'
              }
              fill={
                value && value.paymentMethodId === option.paymentMethodId
                  ? theme['color-primary-700']
                  : theme['color-basic-500']
              }
            />
          )}
        />
        {i !== paymentMethodOptions.length - 1 && (
          <Dashed color={theme['color-basic-600']} customStyle={themedStyle.itemSeparator} />
        )}
      </React.Fragment>
    );
    return (
      <Layout style={themedStyle.choosePaymentModal}>
        <Text
          category="h3"
          status="accent"
          style={[themedStyle.centerText, themedStyle.modalTitle]}
        >
          {title}
        </Text>

        <Dashed color={theme['color-primary-700']} customStyle={themedStyle.itemSeparator} />

        {paymentMethodOptions &&
          paymentMethodOptions.map((option: PaymentMethodType, i: number) => (
            <React.Fragment key={option.paymentMethodId}>
              <ListItem
                title={`${
                  option.isACH
                    ? PaymentMethodNameKeys.BANK_ACCOUNT
                    : PaymentMethodNameKeys.DEBIT_CARD
                }${option.default ? ' - DEFAULT' : ''}`}
                titleStyle={themedStyle.uppercase}
                description={shared.helpers.format.formatPaymentMethodNumber(option)}
                onPress={() => {
                  setValue(prev => {
                    setPrevValue(prev);
                    return option;
                  });
                }}
                icon={() => (
                  <Icon
                    name={
                      value && value.paymentMethodId === option.paymentMethodId
                        ? 'checkmark-circle-2'
                        : 'radio-button-off'
                    }
                    fill={
                      value && value.paymentMethodId === option.paymentMethodId
                        ? theme['color-primary-700']
                        : theme['color-basic-500']
                    }
                  />
                )}
              />
              {i !== paymentMethodOptions.length - 1 && (
                <Dashed color={theme['color-basic-600']} customStyle={themedStyle.itemSeparator} />
              )}
            </React.Fragment>
          ))}

        <View style={themedStyle.modalCloseButton}>
          <Button
            // disabled={checkIsDefaultPaymentMethod(value, selectedPaymentMethodOption)}
            appearance="outline"
            status="success"
            onPress={() => {
              setPaymentMethod(value);
              hideModal();
            }}
          >
            {buttonTitle}
          </Button>
          <Button
            style={themedStyle.addNewButton}
            appearance="ghost"
            status="info"
            size="small"
            onPress={() => {
              onNewPaymentMethod();
              hideModal();
            }}
          >
            Add new payment method
          </Button>
        </View>
      </Layout>
    );
  }, [
    buttonTitle,
    hideModal,
    onNewPaymentMethod,
    paymentMethodOptions,
    setPaymentMethod,
    theme,
    themedStyle.addNewButton,
    themedStyle.centerText,
    themedStyle.choosePaymentModal,
    themedStyle.itemSeparator,
    themedStyle.modalCloseButton,
    themedStyle.modalTitle,
    themedStyle.uppercase,
    title,
    value,
  ]);

  const showModal = useCallback(() => {
    const ModalContent = renderModal();

    if (modalId.current) {
      ModalService.update(modalId.current, ModalContent);
    } else {
      const Modal = (
        <Layout
          style={[
            themedStyle.backdrop,
            {
              width,
              height,
            },
          ]}
          pointerEvents="box-none"
        >
          {ModalContent}
        </Layout>
      );
      modalId.current = ModalService.show(Modal, {
        allowBackdrop: true,
        onBackdropPress: () => hideModal(),
      });
    }
  }, [renderModal, themedStyle.backdrop, width, height, hideModal]);

  useEffect(() => {
    if (visible) {
      showModal();
    } else {
      hideModal();
    }
  }, [hideModal, showModal, visible]);

  return null;
};

export default withStyles(ChoosePaymentModal, theme => ({
  backdrop: {
    position: 'absolute',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: color(theme['color-basic-900'])
      .alpha(0.7)
      .rgb()
      .string(),
  },
  choosePaymentModal: {
    margin: 10,
    padding: 30,
    borderRadius: 10,
  },
  modalTitle: {
    paddingBottom: 10,
  },
  modalCloseButton: {
    paddingTop: 25,
    alignSelf: 'center',
  },
  itemSeparator: {
    marginVertical: 10,
  },
  centerText: {
    textAlign: 'center',
  },
  uppercase: {
    textTransform: 'uppercase',
  },
  addNewButton: {
    marginTop: 15,
  },
}));
