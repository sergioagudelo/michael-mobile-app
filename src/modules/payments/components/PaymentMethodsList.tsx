import React from 'react';
import {
  Layout,
  ModalService,
  withStyles,
  ThemedComponentProps,
  List,
  Text,
} from 'react-native-ui-kitten';
import * as Animatable from 'react-native-animatable';
// Types
import { ListRenderItemInfo } from 'react-native';
import { PaymentMethodType } from '../utils';

// Components
import PaymentMethodsItem from './PaymentMethodsItem';
import DeleteConfirmationModal from './DeleteConfirmationModal';
import { Loading } from '../../shared/components';

type PaymentMethodsListComponentProps = {
  paymentMethods: PaymentMethodType[] | null;
  isListLoading: boolean;
  loadingDefaultMethod: string;
  deleteId: string;
  onDelete: (paymentMethod: PaymentMethodType, index: number) => void;
  onPressSetAsDefault: (paymentMethod: PaymentMethodType) => void;
} & ThemedComponentProps;

const PaymentMethodsListComponent = ({
  paymentMethods,
  isListLoading,
  onDelete,
  onPressSetAsDefault,
  themedStyle,
  deleteId,
  loadingDefaultMethod,
}: PaymentMethodsListComponentProps) => {
  let modalId = '';
  const dismissDelete = () => {
    modalId = ModalService.hide(modalId);
  };

  const openConfirmationDelete = async (item: PaymentMethodType, index: number) => {
    const modal = (
      <DeleteConfirmationModal
        item={item}
        onDismiss={dismissDelete}
        onConfirm={() => {
          dismissDelete();
          onDelete(item, index);
        }}
      />
    );

    if (modalId) {
      ModalService.update(modalId, modal);
    } else {
      modalId = ModalService.show(modal, {
        allowBackdrop: true,
        onBackdropPress: () => {},
      });
    }
  };

  const paymentMethodRenderItem = ({ index, item }: ListRenderItemInfo<PaymentMethodType>) => (
    <Animatable.View animation="fadeInRight" key={index} duration={300} delay={200} useNativeDriver>
      <PaymentMethodsItem
        key={item.paymentMethodId}
        onPress={() => {
          onPressSetAsDefault(item);
        }}
        isLoadingDefault={item.paymentMethodId === loadingDefaultMethod}
        isLoadingDelete={item.paymentMethodId === deleteId}
        paymentMethod={item}
        onDelete={() => {
          if (
            item.paymentMethodId === loadingDefaultMethod ||
            (paymentMethods && paymentMethods.length === 1)
          ) {
            return;
          }
          openConfirmationDelete(item, index);
        }}
      />
    </Animatable.View>
  );

  if (isListLoading) {
    return <Loading isLoading={isListLoading} />;
  }

  return (
    <Layout style={themedStyle.container}>
      <List
        data={paymentMethods}
        renderItem={paymentMethodRenderItem}
        showsVerticalScrollIndicator={false}
        ListEmptyComponent={() => (
          <Animatable.View animation="fadeInDown" duration={300} delay={150} useNativeDriver>
            <Text status="warning" style={themedStyle.emptyList}>
              You don't have any payment methods.
            </Text>
          </Animatable.View>
        )}
      />
    </Layout>
  );
};

const PaymentMethodsList = withStyles(PaymentMethodsListComponent, () => ({
  container: {
    paddingVertical: 20,
  },
  emptyList: { textAlign: 'center', marginVertical: 50 },
}));

export default PaymentMethodsList;
