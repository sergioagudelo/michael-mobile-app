import React from 'react';
import { withStyles, ListItem, Button, Icon, ThemedComponentProps } from 'react-native-ui-kitten';
import shared from '../../shared';
import { PaymentMethodType } from '../utils';

const { Loading } = shared.components;

type PaymentMethodSelectorProps = {
  disabled: boolean;
  isLoading: boolean;
  defaultMethod: PaymentMethodType;
  setIsModalVisible: (bool: boolean) => void;
  handleNoPaymentMethods: () => void;
} & ThemedComponentProps;

const PaymentMethodSelector = ({
  theme,
  isLoading,
  disabled,
  themedStyle,
  defaultMethod,
  setIsModalVisible,
  handleNoPaymentMethods,
}: PaymentMethodSelectorProps) => {
  if (isLoading) {
    return <Loading isLoading />;
  }

  if (!defaultMethod) {
    return (
      <ListItem
        titleStyle={[themedStyle.uppercase, themedStyle.titleStyle]}
        description="PLEASE SELECT A PAYMENT METHOD"
        descriptionStyle={themedStyle.descriptionStyle}
        accessory={() => (
          <Button
            appearance="ghost"
            size="tiny"
            status="info"
            textStyle={themedStyle.uppercase}
            onPress={() => handleNoPaymentMethods()}
            icon={style => (
              <Icon
                name="edit-2-outline"
                {...style}
                width={32}
                height={32}
                fill={theme['color-primary-600']}
              />
            )}
          />
        )}
        disabled
      />
    );
  }

  return (
    <ListItem
      title="PAYMENT METHOD"
      titleStyle={[themedStyle.uppercase, themedStyle.titleStyle]}
      description={`${defaultMethod.methodName} ${shared.helpers.format.formatPaymentMethodNumber(
        defaultMethod,
      )}`}
      descriptionStyle={themedStyle.descriptionStyle}
      accessory={() => (
        <Button
          disabled={disabled}
          appearance="ghost"
          size="tiny"
          status="info"
          textStyle={themedStyle.uppercase}
          onPress={() => setIsModalVisible(true)}
          icon={style => (
            <Icon
              name="edit-2-outline"
              {...style}
              width={32}
              height={32}
              fill={theme['color-basic-600']}
            />
          )}
        />
      )}
      disabled
    />
  );
};

export default withStyles(PaymentMethodSelector, theme => ({
  uppercase: {
    textTransform: 'uppercase',
  },
  titleStyle: {
    color: theme['color-basic-800'],
  },
  descriptionStyle: {
    color: theme['color-primary-700'],
  },
}));
