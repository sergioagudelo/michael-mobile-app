import React from 'react';
import { Text, withStyles, ThemedComponentProps } from 'react-native-ui-kitten';
import { PaymentMethodType } from '../utils';

import shared from '../../shared';

const { format } = shared.helpers;

type AutopayLabelConsentProps = {
  userFullName: string;
  paymentMethod?: PaymentMethodType;
  handlePress: any;
} & ThemedComponentProps;

const AutopayLabelConsent = ({
  handlePress,
  userFullName,
  paymentMethod,
  themedStyle,
}: AutopayLabelConsentProps) => {
  if (!userFullName || !paymentMethod) {
    return null;
  }
  const { isACH } = paymentMethod;
  const methodNumber = format.formatPaymentMethodNumber(paymentMethod);

  return isACH ? (
    <Text status="primary" category="metatext" style={themedStyle.labelConsentText}>
      {`By clicking below to make your scheduled payments, you, ${userFullName}, authorize LendingPoint to electronically debit your above bank account ending with ${methodNumber} and routing number ending with ${format.formatBankAccountNumber(
        paymentMethod.routingNumber,
      )} in accordance with the `}
      <Text status="primary" category="metatext" style={themedStyle.link} onPress={handlePress}>
        ACH Consent Terms{' '}
      </Text>
      for amounts owing on each payment date scheduled in your Loan Agreement or as changed
      thereafter changed thereafter (“Due Date”).
    </Text>
  ) : (
    <Text status="primary" category="metatext" style={themedStyle.labelConsentText}>
      You authorize LendingPoint to process your debit card for the amounts owing on each payment
      date scheduled in the Loan Agreement or as changed thereafter (“Due Date”) in accordance with
      the
      <Text status="primary" category="metatext" style={themedStyle.link} onPress={handlePress}>
        {' '}
        Debit Consent Terms{' '}
      </Text>
      You understand the amount of each payment will show up on your bank statement for the purposes
      of payment and amount verification.
    </Text>
  );
};

export default withStyles(AutopayLabelConsent, theme => ({
  labelConsentText: {
    marginBottom: 20,
    color: theme['color-basic-1000'],
  },
  link: {
    fontWeight: '500',
  },
}));
