/* eslint-disable global-require */
import React from 'react';
import { View, Image, TouchableHighlight } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { verticalScale } from 'react-native-size-matters';
import * as Progress from 'react-native-progress';

import {
  Layout,
  Button,
  Icon,
  Text,
  withStyles,
  ThemedComponentProps,
} from 'react-native-ui-kitten';

import { PaymentMethodType } from '../utils';
import shared from '../../shared';

type PaymentMethodsItemProps = {
  paymentMethod: PaymentMethodType;
  onPress: () => void;
  onDelete: () => void;
  isLoadingDefault: boolean;
  isLoadingDelete: boolean;
} & ThemedComponentProps;

// TODO WIP BANK CARD

const PaymentMethodsItem = ({
  paymentMethod,
  onPress,
  onDelete,
  theme,
  themedStyle,
  isLoadingDefault,
  isLoadingDelete,
}: PaymentMethodsItemProps) => (
  <Layout style={themedStyle.container}>
    <View style={themedStyle.actionsContainer}>
      {(isLoadingDefault && (
        <Progress.CircleSnail
          style={themedStyle.formLoading}
          color={[theme['color-primary-500'], theme['color-warning-500'], theme['color-info-500']]}
        />
      )) || (
        <TouchableHighlight
          style={themedStyle.select}
          underlayColor={theme['color-basic-transparent-200']}
          onPress={onPress}
        >
          <Image
            source={
              paymentMethod.default
                ? require('../../../img/icons/ico_check.png')
                : require('../../../img/icons/ico_radio.png')
            }
            style={themedStyle.buttonIcon}
          />
        </TouchableHighlight>
      )}
      <Text
        category="label"
        status="accent"
        style={[themedStyle.centerText, themedStyle.uppercase, themedStyle.selectLabel]}
      >
        {paymentMethod.default ? 'Preferred' : 'Set as preferred'}
      </Text>
      <View style={themedStyle.deleteButtonContainer}>
        {(isLoadingDelete && (
          <Progress.CircleSnail
            style={themedStyle.formLoading}
            color={[
              theme['color-primary-500'],
              theme['color-warning-500'],
              theme['color-info-500'],
            ]}
          />
        )) ||
          (isLoadingDefault ||
            (!paymentMethod.default && (
              <Button
                appearance="ghost"
                status="warning"
                size="tiny"
                icon={style => <Icon name="trash-outline" {...style} width={35} height={35} />}
                onPress={onDelete}
              />
            )))}
      </View>
    </View>
    <LinearGradient
      useAngle
      angle={-225}
      angleCenter={{ x: 0.5, y: 0.5 }}
      colors={
        paymentMethod.default
          ? ['#004B8D', '#003267', '#0072BB']
          : ['#DEE2E8', '#EEF0F3', '#FBFDFF']
      }
      style={themedStyle.cardContainer}
    >
      <View style={themedStyle.cardTitle}>
        <Text
          appearance={paymentMethod.default ? 'alternative' : 'default'}
          category="h4"
          status={!paymentMethod.default ? 'accent' : undefined}
          style={themedStyle.uppercase}
        >
          {paymentMethod.methodName}
        </Text>
        {paymentMethod.isACH && (
          <Text
            category="label"
            appearance={paymentMethod.default ? 'alternative' : 'default'}
            style={[themedStyle.bankInfo, themedStyle.uppercase]}
          >
            {`${paymentMethod.bankName},\n${paymentMethod.accountType}`}
          </Text>
        )}
      </View>
      <View style={themedStyle.cardInfo}>
        <Text
          appearance={paymentMethod.default ? 'alternative' : 'default'}
          category="h5"
          style={[themedStyle.uppercase]}
        >
          {shared.helpers.format.formatPaymentMethodNumber(paymentMethod)}
        </Text>
      </View>
    </LinearGradient>
  </Layout>
);

export default withStyles(PaymentMethodsItem, () => ({
  centerText: {
    textAlign: 'center',
  },
  uppercase: {
    textTransform: 'uppercase',
  },
  container: {
    flexDirection: 'row',
    minHeight: verticalScale(180),
    marginVertical: 15,
  },
  actionsContainer: {
    flex: 0.5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonIcon: {
    width: 35,
    height: 35,
  },
  select: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  selectLabel: {
    marginTop: 8,
  },
  deleteButtonContainer: {
    paddingTop: verticalScale(20),
  },
  cardContainer: {
    flex: 0.5,
    justifyContent: 'space-between',
    padding: verticalScale(30),
    borderTopLeftRadius: verticalScale(10.5),
    borderBottomLeftRadius: verticalScale(10.5),
  },
  cardTitle: {
    alignItems: 'flex-start',
  },
  cardInfo: {
    alignItems: 'flex-end',
  },
  bankInfo: {
    marginVertical: 15,
  },
}));
