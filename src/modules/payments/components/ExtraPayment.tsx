import React from 'react';
import { View } from 'react-native';
import {
  withStyles,
  Layout,
  Text,
  Button,
  ThemedComponentProps,
  Input,
  Toggle,
} from 'react-native-ui-kitten';

import { FormikProps } from 'formik';
import { LoanDetailsBox } from '../../loans/components';
import { ExtraPaymentFormValues } from '../utils';
import { formKeys, forms } from '../constants';

import shared from '../../shared';
import { decimalInputHandler } from '../helpers';

type ExtraPaymentProps = {
  onToggle: (checked: boolean) => void;
  isEnabled: boolean;
  extraPaymentMaxAmount: number;
} & ThemedComponentProps &
  FormikProps<ExtraPaymentFormValues>;

const ExtraPayment = ({
  onToggle,
  isEnabled,
  extraPaymentMaxAmount,
  themedStyle,
  touched,
  errors,
  handleBlur,
  handleChange,
  handleSubmit,
  isValid,
  values,
  setFieldValue,
}: ExtraPaymentProps) => {
  const parseAndHandleChange = (value: string, formKey: string) => {
    setFieldValue(formKey, decimalInputHandler(value));
    handleChange(formKey);
  };

  return (
    <View style={themedStyle.parent}>
      <LoanDetailsBox title="Extra Payments">
        <View style={themedStyle.toggleContainer}>
          <Toggle
            onChange={checked => onToggle(checked)}
            appearance="default"
            status="basic"
            checked={isEnabled}
          />
          <Text category="metatext" style={themedStyle.toggleLabel}>
            I'd like to pay down my loan sooner by making an extra payment.
          </Text>
        </View>

        {isEnabled && (
          <View style={themedStyle.content}>
            <View style={themedStyle.paymentInfo}>
              <Text status="primary" category="h6" style={themedStyle.paymentAmountLabel}>
                Enter extra payment amount:
              </Text>
            </View>
            <Layout style={themedStyle.inputContainer}>
              <Input
                status={shared.helpers.getInputStatus<ExtraPaymentFormValues>(
                  formKeys.extraPayment.amount,
                  {
                    touched,
                    errors,
                  },
                )}
                onBlur={handleBlur(formKeys.extraPayment.amount)}
                value={
                  values[formKeys.extraPayment.amount]
                    ? values[formKeys.extraPayment.amount].toString()
                    : values[formKeys.extraPayment.amount]
                }
                onChangeText={value => parseAndHandleChange(value, formKeys.extraPayment.amount)}
                // onChangeText={handleChange(formKeys.extraPayment.amount)}
                placeholder={forms.extraPayment.placeholders[formKeys.extraPayment.amount](
                  extraPaymentMaxAmount,
                )}
                caption={
                  touched &&
                  touched[formKeys.extraPayment.amount] &&
                  errors[formKeys.extraPayment.amount]
                    ? errors[formKeys.extraPayment.amount]
                    : ''
                }
                autoCapitalize="none"
                keyboardType="numeric"
                returnKeyType="done"
              />
            </Layout>
          </View>
        )}
      </LoanDetailsBox>
      {isEnabled && (
        <View style={themedStyle.cta}>
          <Button
            status="primary"
            size="large"
            disabled={!isValid}
            textStyle={themedStyle.button}
            onPress={handleSubmit}
          >
            Continue
          </Button>
        </View>
      )}
    </View>
  );
};

const ExtraPaymentComponent = withStyles(ExtraPayment, theme => ({
  parent: {
    marginBottom: 15,
  },
  container: {
    borderColor: theme['color-basic-400'],
    borderWidth: 1,
    elevation: 2,
    ...shared.helpers.shadow(2),
  },
  headerContainer: {
    backgroundColor: undefined,
    paddingTop: 8,
  },
  header: {
    fontWeight: '100',
    paddingVertical: 10,
    alignSelf: 'center',
  },
  body: {
    padding: 20,
  },
  content: {
    flexDirection: 'column',
    justifyContent: 'center',
  },
  toggleContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingHorizontal: 25,
    marginBottom: 25,
  },
  toggleLabel: {
    marginLeft: 40,
  },
  uppercase: {
    textTransform: 'uppercase',
  },
  paymentAmountLabel: {
    textAlign: 'center',
    alignSelf: 'center',
    fontWeight: '400',
    paddingBottom: 15,
  },
  inputContainer: {
    paddingHorizontal: 55,
    paddingBottom: 5,
  },
  cta: {
    backgroundColor: null,
    alignSelf: 'center',
    marginTop: -35,
    minWidth: 200,
    elevation: 2,
    marginBottom: 0,
  },
  button: {
    textTransform: 'uppercase',
    margin: 0,
  },
  svgContainer: {},
}));

export default ExtraPaymentComponent;
