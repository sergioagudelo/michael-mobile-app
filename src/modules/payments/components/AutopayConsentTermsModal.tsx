import React from 'react';

import Modal from 'react-native-modal';
import { ScrollView, View } from 'react-native';
import { withStyles, Text, Layout, ThemedComponentProps } from 'react-native-ui-kitten';
import color from 'color';

import { TouchableOpacity } from 'react-native-gesture-handler';
import { PaymentMethodType } from '../utils';

import shared from '../../shared';

const { Dashed } = shared.components;

const { Ico } = shared.helpers;

type AutopayConsentTermsModalProps = ThemedComponentProps & {
  visible: boolean;
  selectedPaymentMethod?: PaymentMethodType;
  onDismiss: () => void;
  consentText: string;
};

const AutopayConsentTermsModal = ({
  visible,
  onDismiss,
  consentText,
  theme,
  themedStyle,
}: AutopayConsentTermsModalProps) => {
  const { height, width } = shared.helpers.hooks.useScreenDimensions();

  return (
    <Modal
      isVisible={visible}
      coverScreen
      hasBackdrop
      onBackdropPress={onDismiss}
      onBackButtonPress={onDismiss}
      customBackdrop={
        <Layout
          style={[
            themedStyle.backdrop,
            {
              width,
              height,
            },
          ]}
          pointerEvents="box-none"
        />
      }
      deviceWidth={width}
      deviceHeight={height}
    >
      <View style={[themedStyle.close]}>
        <TouchableOpacity onPress={() => onDismiss()}>
          <Ico name="close" width={50} height={50} />
        </TouchableOpacity>
      </View>
      <Layout style={themedStyle.modalContainer}>
        <Text
          category="h3"
          status="accent"
          style={[themedStyle.centerText, themedStyle.modalTitle]}
        >
          Autopay consent terms
        </Text>
        <Dashed color={theme['color-primary-700']} customStyle={themedStyle.itemSeparator} />
        <ScrollView>
          <Text style={[themedStyle.textBody]}>{consentText}</Text>
        </ScrollView>
      </Layout>
    </Modal>
  );
};

export default withStyles(AutopayConsentTermsModal, theme => ({
  backdrop: {
    backgroundColor: color(theme['color-basic-900'])
      .alpha(0.7)
      .rgb()
      .string(),
  },
  modalContainer: {
    margin: 15,
    padding: 30,
    maxHeight: '85%',
  },
  modalTitle: {
    paddingBottom: 10,
  },
  modalCloseButton: {
    paddingTop: 25,
    alignSelf: 'center',
  },
  itemSeparator: {
    marginVertical: 10,
  },
  centerText: {
    textAlign: 'center',
  },
  uppercase: {
    textTransform: 'uppercase',
  },
  consentContainer: {
    marginHorizontal: 10,
    marginVertical: 15,
  },
  textBody: {
    marginTop: 15,
    fontSize: 12,
    lineHeight: 14,
  },
  close: {
    width: '100%',
    flex: 0.2,
    justifyContent: 'center',
    alignItems: 'flex-end',
    marginRight: 20,
  },
}));
