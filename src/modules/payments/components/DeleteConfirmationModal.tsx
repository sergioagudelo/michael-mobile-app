import React from 'react';
import { withStyles, ThemedComponentProps, Text, Layout, Button } from 'react-native-ui-kitten';
import color from 'color';

// Types
import { PaymentMethodType } from '../utils';

// Modules
import shared from '../../shared';

type DeleteConfirmationModalProps = {
  onDismiss: () => void;
  onConfirm: () => void;
  item: PaymentMethodType;
} & ThemedComponentProps;

const DeleteConfirmationModal = ({
  onDismiss,
  onConfirm,
  item,
  themedStyle,
}: DeleteConfirmationModalProps) => {
  const { height, width } = shared.helpers.hooks.useScreenDimensions();
  return (
    <Layout
      style={[
        themedStyle.backdrop,
        {
          width,
          height,
        },
      ]}
    >
      <Layout style={themedStyle.confirmationModalContainer}>
        <Text>Are you sure you want to delete?</Text>
        <Layout style={themedStyle.modalActions}>
          <Button appearance="ghost" onPress={onDismiss}>
            Cancel
          </Button>
          <Button appearance="ghost" onPress={onConfirm}>
            Ok
          </Button>
        </Layout>
      </Layout>
    </Layout>
  );
};

export default withStyles(DeleteConfirmationModal, theme => ({
  backdrop: {
    position: 'absolute',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: color(theme['color-basic-900'])
      .alpha(0.7)
      .rgb()
      .string(),
  },
  confirmationModalContainer: {
    padding: 30,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalActions: {
    flexDirection: 'row',
    marginTop: 10,
  },
}));
