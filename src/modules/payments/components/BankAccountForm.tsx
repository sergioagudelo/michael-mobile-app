import React from 'react';
import { View } from 'react-native';
import {
  withStyles,
  Layout,
  Input,
  Button,
  ThemedComponentProps,
  Text,
  Select,
  CheckBox,
} from 'react-native-ui-kitten';
import * as Progress from 'react-native-progress';
import { FormikProps } from 'formik';
import { InputComponent } from 'react-native-ui-kitten/ui/input/input.component';
import { Modal } from '../../shared/components';

// Types
import { BankAccountFormValues } from '../utils';

// Modules
import shared from '../../shared';
import config from '../../../config';

// Constants
import { forms, accountTypeValues, formKeys } from '../constants';

// Components

type BankAccountFormComponentProps = ThemedComponentProps & FormikProps<BankAccountFormValues>;

const BankAccountFormComponent = ({
  theme,
  themedStyle,
  values,
  touched,
  errors,
  handleBlur,
  handleChange,
  isValid,
  isSubmitting,
  handleSubmit,
  status,
  setStatus,
}: BankAccountFormComponentProps) => {
  const selectedAccountType = values.accountType !== '' ? { text: values.accountType } : undefined;

  const routingNumberInputRef = React.useRef<InputComponent>(null);
  const accountNumberInputRef = React.useRef<InputComponent>(null);
  const confirmAccountNumberInputRef = React.useRef<InputComponent>(null);

  return (
    <Layout style={themedStyle.formContainer}>
      <Modal
        {...{
          title: "Can't save the information",
          options: [status && status.error],
          buttonTitle: 'Close',
          visible: !!(status && status.error),
          onDismiss: () => setStatus({}),
        }}
      />
      <Layout style={themedStyle.input}>
        <Input
          status={shared.helpers.getInputStatus<BankAccountFormValues>(
            formKeys.bankAccount.bankName,
            {
              touched,
              errors,
            },
          )}
          value={values[formKeys.bankAccount.bankName]}
          onBlur={handleBlur(formKeys.bankAccount.bankName)}
          onChangeText={handleChange(formKeys.bankAccount.bankName)}
          label={forms.bankAccount.labels.bankName}
          placeholder={forms.bankAccount.placeholders.bankName}
          caption={touched.bankName && errors.bankName ? errors.bankName : ''}
          onSubmitEditing={() => {
            if (routingNumberInputRef && routingNumberInputRef.current) {
              routingNumberInputRef.current.focus();
            }
          }}
          returnKeyType="next"
        />
      </Layout>

      <Layout style={themedStyle.input}>
        <Input
          ref={routingNumberInputRef}
          status={shared.helpers.getInputStatus<BankAccountFormValues>(
            formKeys.bankAccount.routingNumber,
            {
              touched,
              errors,
            },
          )}
          value={values[formKeys.bankAccount.routingNumber]}
          onBlur={handleBlur(formKeys.bankAccount.routingNumber)}
          onChangeText={handleChange(formKeys.bankAccount.routingNumber)}
          label={forms.bankAccount.labels.routingNumber}
          placeholder={forms.bankAccount.placeholders.routingNumber}
          caption={touched.routingNumber && errors.routingNumber ? errors.routingNumber : ''}
          onSubmitEditing={() => {
            if (accountNumberInputRef && accountNumberInputRef.current) {
              accountNumberInputRef.current.focus();
            }
          }}
          keyboardType="numeric"
          returnKeyType="next"
        />
      </Layout>

      <Layout style={themedStyle.input}>
        <Input
          ref={accountNumberInputRef}
          status={shared.helpers.getInputStatus<BankAccountFormValues>(
            formKeys.bankAccount.accountNumber,
            {
              touched,
              errors,
            },
          )}
          value={values[formKeys.bankAccount.accountNumber]}
          onBlur={handleBlur(formKeys.bankAccount.accountNumber)}
          onChangeText={handleChange(formKeys.bankAccount.accountNumber)}
          label={forms.bankAccount.labels.accountNumber}
          placeholder={forms.bankAccount.placeholders.accountNumber}
          caption={touched.accountNumber && errors.accountNumber ? errors.accountNumber : ''}
          onSubmitEditing={() => {
            if (confirmAccountNumberInputRef && confirmAccountNumberInputRef.current) {
              confirmAccountNumberInputRef.current.focus();
            }
          }}
          keyboardType="numeric"
          returnKeyType="next"
        />
      </Layout>

      <Layout style={themedStyle.input}>
        <Input
          ref={confirmAccountNumberInputRef}
          status={shared.helpers.getInputStatus<BankAccountFormValues>(
            formKeys.bankAccount.confirmAccountNumber,
            {
              touched,
              errors,
            },
          )}
          value={values[formKeys.bankAccount.confirmAccountNumber]}
          onBlur={handleBlur(formKeys.bankAccount.confirmAccountNumber)}
          onChangeText={handleChange(formKeys.bankAccount.confirmAccountNumber)}
          label={forms.bankAccount.labels.confirmAccountNumber}
          placeholder={forms.bankAccount.placeholders.confirmAccountNumber}
          caption={
            touched.confirmAccountNumber && errors.confirmAccountNumber
              ? errors.confirmAccountNumber
              : ''
          }
          keyboardType="numeric"
          returnKeyType="next"
        />
      </Layout>

      <Layout style={themedStyle.input}>
        <Select
          status={shared.helpers.getInputStatus<BankAccountFormValues>(
            formKeys.bankAccount.accountType,
            {
              touched,
              errors,
            },
          )}
          data={accountTypeValues.map(accountType => ({ text: accountType }))}
          selectedOption={selectedAccountType}
          label={forms.bankAccount.labels.accountType}
          placeholder="Select..."
          onSelect={option => handleChange(formKeys.bankAccount.accountType)(option.text)}
          onPress={() => handleBlur(formKeys.bankAccount.accountType)}
        />
        {touched.accountType && errors.accountType && (
          <View style={themedStyle.errorMsg}>
            <Text category="c2" status="danger">
              errors.accountType
            </Text>
          </View>
        )}
      </Layout>

      <Layout style={themedStyle.input}>
        <CheckBox
          checked={values[formKeys.bankAccount.setDefault]}
          text={forms.bankAccount.labels[formKeys.bankAccount.setDefault]}
          onChange={handleChange(formKeys.bankAccount.setDefault)}
        />
      </Layout>
      <Layout style={themedStyle.input}>
        <Text category="p2">
          {`You are not enrolling in Autopay. Are you ready to enjoy the convenience of our set it and forget it Autopay program?\nWe can help you enroll at `}
          <Text
            status="info"
            onPress={() => {
              shared.helpers.callNumber(config.contactPhone);
            }}
          >
            {shared.helpers.format.formatPhone(config.contactPhone.slice(2) /* remove +1 */)}
          </Text>
        </Text>
      </Layout>

      {!isSubmitting ? (
        <Button
          status="primary"
          size="giant"
          disabled={!isValid || isSubmitting}
          onPress={handleSubmit}
          textStyle={themedStyle.uppercase}
          {...shared.helpers.setTestID('BankAccountFormSubmitBtn')}
        >
          Add bank account
        </Button>
      ) : (
        <View style={themedStyle.formLoadingContainer}>
          <Progress.CircleSnail
            style={themedStyle.formLoading}
            color={[
              theme['color-primary-500'],
              theme['color-warning-500'],
              theme['color-info-500'],
            ]}
          />
        </View>
      )}
    </Layout>
  );
};

const BankAccountForm = withStyles(BankAccountFormComponent, () => ({
  uppercase: {
    textTransform: 'uppercase',
  },
  input: {
    paddingBottom: 20,
  },
  formContainer: {
    paddingHorizontal: 30,
    paddingVertical: 15,
  },
  formTitle: {
    paddingVertical: 10,
  },
  label: {
    textAlign: 'center',
  },
  errorMsg: {
    marginTop: 4,
    flexDirection: 'row',
    alignItems: 'center',
  },
  formLoadingContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  formLoading: {
    marginVertical: 14,
  },
}));

export default BankAccountForm;
