import React from 'react';
import { View } from 'react-native';
import {
  withStyles,
  Layout,
  Input,
  Button,
  ThemedComponentProps,
  Text,
  CheckBox,
  Select,
} from 'react-native-ui-kitten';
import * as Progress from 'react-native-progress';
import { FormikProps, Formik, FormikValues, FormikActions } from 'formik';
import { InputComponent } from 'react-native-ui-kitten/ui/input/input.component';
import { TextInputMask } from 'react-native-masked-text';
import * as Animatable from 'react-native-animatable';

// Types
import { DebitCardFormValues } from '../utils';
import { User } from '../../profile/utils';
import { ModalProps } from '../../shared/components/Modal';

// Modules
import shared from '../../shared';
import config from '../../../config';

// Constants
import { forms, formKeys } from '../constants';
import { Modal } from '../../shared/components';

const { debitCard: debitCardFormKeys } = formKeys;

type DebitCardFormComponentProps = ThemedComponentProps &
  FormikProps<DebitCardFormValues> & {
    userInfo: User;
    debitCardValues: typeof forms.debitCard.initialValues;
    onSubmit: (values: FormikValues, setSubmitting: (isSubmitting: boolean) => void) => void;
    modalData: ModalProps;
  };

const DebitCardFormComponent = ({
  theme,
  themedStyle,
  onSubmit,
  userInfo,
  debitCardValues,
  modalData,
}: DebitCardFormComponentProps) => {
  const expDateInputRef = React.useRef<InputComponent>(null);
  const cvcInputRef = React.useRef<InputComponent>(null);
  const firstNameInputRef = React.useRef<InputComponent>(null);
  const lastNameInputRef = React.useRef<InputComponent>(null);
  const phoneInputRef = React.useRef<InputComponent>(null);
  const addressLine1InputRef = React.useRef<InputComponent>(null);
  const addressLine2InputRef = React.useRef<InputComponent>(null);
  const cityInputRef = React.useRef<InputComponent>(null);
  const stateInputRef = React.useRef<InputComponent>(null);
  const zipCodeInputRef = React.useRef<InputComponent>(null);

  return (
    <Animatable.View animation="fadeIn" duration={300} delay={200} useNativeDriver>
      <Formik
        initialValues={{
          ...debitCardValues,
          ...userInfo,
          [debitCardFormKeys.addressLine1]: userInfo.street,
          [debitCardFormKeys.zipCode]: userInfo.postalCode,
        }}
        enableReinitialize
        isInitialValid={() =>
          forms.debitCard.schema.isValidSync({ ...userInfo, ...debitCardValues })
        }
        validationSchema={forms.debitCard.schema}
        onSubmit={(values, { setSubmitting }: FormikActions<FormikValues>) => {
          onSubmit(values, setSubmitting);
        }}
      >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          isSubmitting,
          isValid,
          handleSubmit,
        }) => (
          <Layout style={themedStyle.formContainer}>
            <Modal {...modalData} />
            <Layout style={themedStyle.input}>
              <TextInputMask
                type="credit-card"
                customTextInput={Input}
                customTextInputProps={{
                  status: shared.helpers.getInputStatus<DebitCardFormValues>(
                    debitCardFormKeys.cardNumber,
                    {
                      errors,
                      touched,
                    },
                  ),
                  label: forms.debitCard.labels.cardNumber,
                  placeholder: forms.debitCard.placeholders.cardNumber,
                  caption:
                    touched && touched.cardNumber && errors.cardNumber ? errors.cardNumber : '',
                }}
                value={values.cardNumber}
                onBlur={handleBlur(debitCardFormKeys.cardNumber)}
                onChangeText={handleChange(debitCardFormKeys.cardNumber)}
                onSubmitEditing={() => {
                  if (expDateInputRef && expDateInputRef.current) {
                    expDateInputRef.current.focus();
                  }
                }}
                // textContentType="creditCardNumber"
                autoCompleteType="cc-number"
                keyboardType="numeric"
                returnKeyType="next"
              />
            </Layout>

            <Layout style={themedStyle.input}>
              <TextInputMask
                type="datetime"
                options={{
                  format: 'MM/YYYY',
                }}
                customTextInput={Input}
                customTextInputProps={{
                  ref: expDateInputRef,
                  status: shared.helpers.getInputStatus<DebitCardFormValues>(
                    debitCardFormKeys.expDate,
                    {
                      errors,
                      touched,
                    },
                  ),
                  label: forms.debitCard.labels.expDate,
                  placeholder: forms.debitCard.placeholders.expDate,
                  caption: touched && touched.expDate && errors.expDate ? errors.expDate : '',
                }}
                value={values.expDate}
                onBlur={handleBlur(debitCardFormKeys.expDate)}
                onChangeText={handleChange(debitCardFormKeys.expDate)}
                onSubmitEditing={() => {
                  if (cvcInputRef && cvcInputRef.current) {
                    cvcInputRef.current.focus();
                  }
                }}
                textContentType="none"
                autoCompleteType="cc-exp"
                returnKeyType="done"
              />
            </Layout>

            <Layout style={themedStyle.input}>
              <TextInputMask
                type="only-numbers"
                customTextInput={Input}
                customTextInputProps={{
                  ref: cvcInputRef,
                  status: shared.helpers.getInputStatus<DebitCardFormValues>(
                    debitCardFormKeys.cvc,
                    {
                      errors,
                      touched,
                    },
                  ),
                  label: forms.debitCard.labels.cvc,
                  placeholder: forms.debitCard.placeholders.cvc,
                  caption: touched && touched.cvc && errors.cvc ? errors.cvc : '',
                }}
                value={values.cvc}
                onBlur={handleBlur(debitCardFormKeys.cvc)}
                onChangeText={handleChange(debitCardFormKeys.cvc)}
                onSubmitEditing={() => {
                  if (firstNameInputRef && firstNameInputRef.current) {
                    firstNameInputRef.current.focus();
                  }
                }}
                textContentType="none"
                autoCompleteType="cc-csc"
                returnKeyType="done"
              />
            </Layout>

            <Text category="h2" status="accent" style={themedStyle.formTitle}>
              Billing information
            </Text>

            <Layout style={themedStyle.input}>
              <Input
                ref={firstNameInputRef}
                status={shared.helpers.getInputStatus<DebitCardFormValues>(
                  debitCardFormKeys.firstName,
                  {
                    errors,
                    touched,
                  },
                )}
                label={forms.debitCard.labels.firstName}
                placeholder={forms.debitCard.placeholders.firstName}
                caption={
                  touched && touched.firstName && errors && errors.firstName
                    ? errors.firstName.toString()
                    : ''
                }
                value={values.firstName}
                onBlur={handleBlur(debitCardFormKeys.firstName)}
                onChangeText={handleChange(debitCardFormKeys.firstName)}
                onSubmitEditing={() => {
                  if (lastNameInputRef && lastNameInputRef.current) {
                    lastNameInputRef.current.focus();
                  }
                }}
                autoCapitalize="words"
                textContentType="givenName"
                autoCompleteType="name"
                returnKeyType="next"
              />
            </Layout>

            <Layout style={themedStyle.input}>
              <Input
                ref={lastNameInputRef}
                status={shared.helpers.getInputStatus<DebitCardFormValues>(
                  debitCardFormKeys.lastName,
                  {
                    errors,
                    touched,
                  },
                )}
                label={forms.debitCard.labels.lastName}
                placeholder={forms.debitCard.placeholders.lastName}
                caption={
                  touched && touched.lastName && errors && errors.lastName ? errors.lastName : ''
                }
                value={values.lastName}
                onBlur={handleBlur(debitCardFormKeys.lastName)}
                onChangeText={handleChange(debitCardFormKeys.lastName)}
                onSubmitEditing={() => {
                  if (phoneInputRef && phoneInputRef.current) {
                    phoneInputRef.current.focus();
                  }
                }}
                autoCapitalize="words"
                textContentType="familyName"
                autoCompleteType="name"
                returnKeyType="next"
              />
            </Layout>

            <Layout style={themedStyle.input}>
              <TextInputMask
                type="custom"
                options={{
                  mask: '(999) 999-9999',
                }}
                customTextInput={Input}
                customTextInputProps={{
                  ref: phoneInputRef,
                  status: shared.helpers.getInputStatus<DebitCardFormValues>(
                    debitCardFormKeys.phoneNumber,
                    {
                      errors,
                      touched,
                    },
                  ),
                  label: forms.debitCard.labels.phoneNumber,
                  placeholder: forms.debitCard.placeholders.phoneNumber,
                  caption:
                    touched && touched.phoneNumber && errors.phoneNumber ? errors.phoneNumber : '',
                }}
                value={values.phoneNumber}
                onBlur={handleBlur(debitCardFormKeys.phoneNumber)}
                onChangeText={handleChange(debitCardFormKeys.phoneNumber)}
                onSubmitEditing={() => {
                  if (firstNameInputRef && firstNameInputRef.current) {
                    // TODO este puede ser  el error
                    firstNameInputRef.current.focus();
                  }
                }}
                textContentType="telephoneNumber"
                autoCompleteType="tel"
                returnKeyType="done"
                dataDetectorTypes="phoneNumber"
                keyboardType="phone-pad"
              />
            </Layout>

            <Layout style={themedStyle.input}>
              <Input
                ref={addressLine1InputRef}
                status={shared.helpers.getInputStatus<DebitCardFormValues>(
                  debitCardFormKeys.addressLine1,
                  {
                    errors,
                    touched,
                  },
                )}
                label={forms.debitCard.labels.addressLine1}
                placeholder={forms.debitCard.placeholders.addressLine1}
                caption={
                  touched && touched.addressLine1 && errors && errors.addressLine1
                    ? errors.addressLine1.toString()
                    : ''
                }
                value={values.addressLine1}
                onBlur={handleBlur(debitCardFormKeys.addressLine1)}
                onChangeText={handleChange(debitCardFormKeys.addressLine1)}
                onSubmitEditing={() => {
                  if (addressLine2InputRef && addressLine2InputRef.current) {
                    addressLine2InputRef.current.focus();
                  }
                }}
                textContentType="streetAddressLine1"
                autoCompleteType="street-address"
                returnKeyType="next"
              />
            </Layout>

            <Layout style={themedStyle.input}>
              <Input
                ref={addressLine2InputRef}
                status={shared.helpers.getInputStatus<DebitCardFormValues>(
                  debitCardFormKeys.addressLine2,
                  {
                    errors,
                    touched,
                  },
                )}
                label={forms.debitCard.labels.addressLine2}
                placeholder={forms.debitCard.placeholders.addressLine2}
                caption={
                  touched && touched.addressLine2 && errors && errors.addressLine2
                    ? errors.addressLine2.toString()
                    : ''
                }
                value={values.addressLine2}
                onBlur={handleBlur(debitCardFormKeys.addressLine2)}
                onChangeText={handleChange(debitCardFormKeys.addressLine2)}
                onSubmitEditing={() => {
                  if (cityInputRef && cityInputRef.current) {
                    cityInputRef.current.focus();
                  }
                }}
                textContentType="streetAddressLine2"
                autoCompleteType="street-address"
                returnKeyType="next"
              />
            </Layout>

            <Layout style={themedStyle.input}>
              <Input
                ref={cityInputRef}
                status={shared.helpers.getInputStatus<DebitCardFormValues>(debitCardFormKeys.city, {
                  errors,
                  touched,
                })}
                label={forms.debitCard.labels.city}
                placeholder={forms.debitCard.placeholders.city}
                caption={
                  touched && touched.city && errors && errors.city ? errors.city.toString() : ''
                }
                value={values.city}
                onBlur={handleBlur(debitCardFormKeys.city)}
                onChangeText={handleChange(debitCardFormKeys.city)}
                onSubmitEditing={() => {
                  if (stateInputRef && stateInputRef.current) {
                    stateInputRef.current.focus();
                  }
                }}
                textContentType="addressCity"
                autoCompleteType="street-address"
                returnKeyType="next"
              />
            </Layout>

            <Layout style={themedStyle.input}>
              <Select
                status={shared.helpers.getInputStatus<DebitCardFormValues>(
                  debitCardFormKeys.state,
                  {
                    errors,
                    touched,
                  },
                )}
                data={shared.constants.stateValues.map(state => ({ text: state }))}
                selectedOption={{ text: values.state }}
                label={forms.debitCard.labels.state}
                placeholder="Select..."
                onSelect={option => handleChange(debitCardFormKeys.state)(option.text)}
                onPress={() => handleBlur(debitCardFormKeys.state)}
              />
              {touched.state && errors && errors.state && (
                <View style={themedStyle.errorMsg}>
                  <Text category="c2" status="danger">
                    errors.state
                  </Text>
                </View>
              )}
            </Layout>

            <Layout style={themedStyle.input}>
              <TextInputMask
                type="zip-code"
                customTextInput={Input}
                customTextInputProps={{
                  ref: zipCodeInputRef,
                  status: shared.helpers.getInputStatus<DebitCardFormValues>(
                    debitCardFormKeys.zipCode,
                    {
                      errors,
                      touched,
                    },
                  ),
                  label: forms.debitCard.labels.zipCode,
                  placeholder: forms.debitCard.placeholders.zipCode,
                  caption:
                    touched && touched.zipCode && errors && errors.zipCode ? errors.zipCode : '',
                }}
                value={values.zipCode}
                onBlur={handleBlur(debitCardFormKeys.zipCode)}
                onChangeText={handleChange(debitCardFormKeys.zipCode)}
                textContentType="postalCode"
                autoCompleteType="postal-code"
                returnKeyType="done"
              />
            </Layout>

            <Layout style={themedStyle.input}>
              <CheckBox
                checked={values[formKeys.debitCard.setDefault]}
                text={forms.debitCard.labels[formKeys.debitCard.setDefault]}
                onChange={handleChange(formKeys.debitCard.setDefault)}
              />
            </Layout>

            <Layout style={themedStyle.input}>
              <Text category="p2">
                By clicking “Add Debit Card” below, you authorize LendingPoint, LLC to charge this
                debit card above for agreed upon payments, and understand that this information will
                be saved to be used for future transactions on your account.
              </Text>
            </Layout>
            <Layout style={themedStyle.input}>
              <Text category="p2">
                {`You are not enrolling in Autopay. Are you ready to enjoy the convenience of our set it and forget it Autopay program?\nWe can help you enroll at `}
                <Text
                  status="info"
                  onPress={() => {
                    shared.helpers.callNumber(config.contactPhone);
                  }}
                >
                  {shared.helpers.format.formatPhone(config.contactPhone.slice(2) /* remove +1 */)}
                </Text>
              </Text>
            </Layout>
            {!isSubmitting ? (
              <Button
                status="primary"
                size="giant"
                disabled={!isValid || isSubmitting}
                onPress={handleSubmit}
                textStyle={themedStyle.uppercase}
                {...shared.helpers.setTestID('DebitCardFormSubmitBtn')}
              >
                Add debit card
              </Button>
            ) : (
              <View style={themedStyle.formLoadingContainer}>
                <Progress.CircleSnail
                  style={themedStyle.formLoading}
                  color={[
                    theme['color-primary-500'],
                    theme['color-warning-500'],
                    theme['color-info-500'],
                  ]}
                />
              </View>
            )}
          </Layout>
        )}
      </Formik>
    </Animatable.View>
  );
};

export default withStyles(DebitCardFormComponent, () => ({
  uppercase: {
    textTransform: 'uppercase',
  },
  input: {
    paddingBottom: 20,
  },
  formContainer: {
    paddingHorizontal: 30,
    paddingVertical: 15,
  },
  formTitle: {
    paddingVertical: 10,
  },
  label: {
    textAlign: 'center',
  },
  formLoadingContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  formLoading: {
    marginVertical: 14,
  },
}));
