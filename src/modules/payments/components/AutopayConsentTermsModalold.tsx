import React, { useEffect, useCallback, useRef } from 'react';
import { BackHandler, ScrollView, View } from 'react-native';
import { Modalize } from 'react-native-modalize';

import {
  withStyles,
  Text,
  Layout,
  ThemedComponentProps,
  ModalService,
} from 'react-native-ui-kitten';
import color from 'color';

import { useFocusEffect } from 'react-navigation-hooks';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { PaymentMethodType } from '../utils';

import shared from '../../shared';

const { Dashed } = shared.components;

const { Ico } = shared.helpers;

type AutopayConsentTermsModalProps = ThemedComponentProps & {
  visible: boolean;
  selectedPaymentMethod?: PaymentMethodType;
  onDismiss: () => void;
  consentText: string;
};

const AutopayConsentTermsModal = ({
  visible,
  selectedPaymentMethod,
  onDismiss,
  consentText,
  theme,
  themedStyle,
}: AutopayConsentTermsModalProps) => {
  const { height, width } = shared.helpers.hooks.useScreenDimensions();
  const modalId = useRef('');

  const hideModal = useCallback(() => {
    modalId.current = ModalService.hide(modalId.current);
    if (onDismiss) {
      onDismiss();
    }
  }, [onDismiss]);

  useFocusEffect(
    useCallback(() => {
      const onBackPress = () => {
        if (visible) {
          hideModal();
          return true;
        }
        return false;
      };

      BackHandler.addEventListener('hardwareBackPress', onBackPress);

      return () => BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    }, [visible, hideModal]),
  );

  const renderModal = useCallback(
    () => (
      <>
        <View style={[themedStyle.close]}>
          <TouchableOpacity onPress={() => hideModal()}>
            <Ico name="close" width={50} height={50} />
          </TouchableOpacity>
        </View>
        <Layout style={themedStyle.modalContainer}>
          <Text
            category="h3"
            status="accent"
            style={[themedStyle.centerText, themedStyle.modalTitle]}
          >
            Autopay consent terms
          </Text>
          <Dashed color={theme['color-primary-700']} customStyle={themedStyle.itemSeparator} />
          <ScrollView>
            <Text style={[themedStyle.textBody]}>{consentText}</Text>
          </ScrollView>
        </Layout>
      </>
    ),
    [
      consentText,
      hideModal,
      theme,
      themedStyle.centerText,
      themedStyle.close,
      themedStyle.itemSeparator,
      themedStyle.modalContainer,
      themedStyle.modalTitle,
      themedStyle.textBody,
    ],
  );

  const showModal = useCallback(() => {
    const ModalContent = renderModal();

    if (modalId.current) {
      ModalService.update(modalId.current, ModalContent);
    } else {
      const Modal = (
        <Layout
          style={[
            themedStyle.backdrop,
            {
              width,
              height,
            },
          ]}
          pointerEvents="box-none"
        >
          {ModalContent}
        </Layout>
      );
      modalId.current = ModalService.show(Modal, {
        allowBackdrop: true,
        onBackdropPress: () => hideModal(),
      });
    }
  }, [renderModal, themedStyle.backdrop, width, height, hideModal]);

  useEffect(() => {
    if (visible) {
      showModal();
    } else {
      hideModal();
    }
  }, [hideModal, showModal, visible]);

  return null;
};

export default withStyles(AutopayConsentTermsModal, theme => ({
  backdrop: {
    position: 'absolute',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: color(theme['color-basic-900'])
      .alpha(0.7)
      .rgb()
      .string(),
  },
  modalContainer: {
    margin: 20,
    padding: 30,
    borderRadius: 10,
    maxHeight: '80%',
  },
  modalTitle: {
    paddingBottom: 10,
  },
  modalCloseButton: {
    paddingTop: 25,
    alignSelf: 'center',
  },
  itemSeparator: {
    marginVertical: 10,
  },
  centerText: {
    textAlign: 'center',
  },
  uppercase: {
    textTransform: 'uppercase',
  },
  consentContainer: { marginHorizontal: 20, marginVertical: 15 },
  consentText: { marginBottom: 20 },
  toggleContainer: {
    // flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingHorizontal: 25,
  },
  toggleLabel: {
    marginLeft: 40,
  },
  errorLabel: {
    marginBottom: 15,
  },
  addNewButton: {
    marginTop: 15,
  },
  textBody: {
    textAlign: 'justify',
    paddingVertical: 15,
  },
  close: {
    width: '100%',
    flex: 0.2,
    justifyContent: 'center',
    alignItems: 'flex-end',
    marginRight: 20,
  },
}));
