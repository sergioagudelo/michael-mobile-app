import React from 'react';
import { withStyles, Text, ThemedComponentProps } from 'react-native-ui-kitten';

const MakePaymentHeader = ({
  themedStyle,
  title = ' Make a Payment',
}: { title?: string } & ThemedComponentProps) => (
  <>
    <Text category="h1" status="accent" style={themedStyle.centerText}>
      {title}
    </Text>
  </>
);

export default withStyles(MakePaymentHeader, () => ({
  centerText: {
    textAlign: 'center',
    marginTop: 15,
  },
  uppercase: {
    textTransform: 'uppercase',
  },
  subtitle: {
    paddingTop: 10,
  },
}));
