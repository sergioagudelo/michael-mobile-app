export const paymentMethods = {
  '1': {
    id: '1',
    type: 'card',
    title: 'DEBIT CARD',
    description: '•••• 3487',
  },
  '2': {
    id: '2',
    type: 'bank',
    title: 'BANK OF AMERICA, CHECKING',
    description: '•••• 8888',
  },
  '3': {
    id: '3',
    type: 'card',
    title: 'DEBIT CARD',
    description: '•••• 9981',
  },
};
