import * as Yup from 'yup';

import { forms } from '../constants';

import * as testData from './testData';

export type DebitCardType = {
  accountId: string;
  cardNumber: string;
  expiryDate: string;
  createdDate: string;
  selectedForAutoPay: boolean;
  cardType: string;
  active: boolean;
  default: boolean;
};

export type BankAccountType = {
  bankId: string;
  bankName: string;
  routingNumber: string;
  accountingNumber: string;
  accountType: string;
  default: boolean;
};

export type PaymentMethodType = {
  email?: string;
  methodName: string;
  isACH: boolean;
  methodType: string;
  mode: string;
  paymentMethodId: string; // This is done to avoid multi type id
} & DebitCardType &
  BankAccountType;

export type BankAccountFormValues = Yup.InferType<typeof forms.bankAccount.schema>;
export type DebitCardFormValues = Yup.InferType<typeof forms.debitCard.schema>;

export { testData };

export type ResponsePayments = {
  debitCards: PaymentMethodType[];
  bankAccounts: PaymentMethodType[];
};

export type MakePaymentResponse = {
  loanTransactionId: string;
  transactionID: string;
  network: string;
  networkRC: string;
  status: string;
  approvalCode: string;
};

// TODO fix -argument of type string is not assignable to parameter of type PaymentMethodModeType -
export type PaymentMethodModeType = 'debitCard' | 'bankAccount' | string;

export type PaymentType = {
  transactionId?: string;
  contractId: string;
  mode?: string;
  isAdditionalPayment: boolean;
  amount: number;
  fee?: number;
  total?: number;
  date?: Date;
  email?: string;
  paymentMethod?: PaymentMethodType;
};

export type ExtraPaymentFormValues = {
  [amount: string]: number;
};
