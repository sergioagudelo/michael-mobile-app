import * as Yup from 'yup';

import { stateValues } from '../../shared/constants';

/**
 * redux key name
 */
export const NAME = 'payments';

export const accountTypeValues = ['Savings', 'Checking'];

export const PaymentMethodTypeKeys = {
  DEBIT_CARD: 'debitCard',
  BANK_ACCOUNT: 'bankAccount',
};

export const PaymentMethodNameKeys = {
  DEBIT_CARD: 'DEBIT CARD',
  BANK_ACCOUNT: 'BANK ACCOUNT',
};

export const formKeys = {
  debitCard: {
    cardNumber: 'cardNumber',
    expDate: 'expDate',
    expiryDate: 'expiryDate',
    cvc: 'cvc',
    securityCode: 'securityCode',
    firstName: 'firstName',
    lastName: 'lastName',
    phoneNumber: 'phoneNumber',
    addressLine1: 'addressLine1',
    line1Address: 'line1Address',
    line2Address: 'line2Address',
    addressLine2: 'addressLine2',
    city: 'city',
    state: 'state',
    zipCode: 'zipCode',
    setDefault: 'default',
  },
  bankAccount: {
    bankName: 'bankName',
    routingNumber: 'routingNumber',
    accountNumber: 'accountNumber',
    confirmAccountNumber: 'confirmAccountNumber',
    accountType: 'accountType',
    setDefault: 'default',
  },
  extraPayment: {
    amount: 'amount',
  },
};

// TODO more custom errors // is there a best approach? DRY
export const customValidationMessages = {
  debitCard: {
    [formKeys.debitCard.cardNumber]: { required: 'Card number is required' },
    [formKeys.debitCard.expDate]: {
      required: 'Expiration date is required',
      format: 'Please use valid format MM/YYYY',
    },
    [formKeys.debitCard.cvc]: {
      required: 'Security code is required',
      min: 'Security code should be 3 character length at least',
      max: 'Security code should be 4 character length maximum',
    },
    [formKeys.debitCard.firstName]: { required: 'First name is required' },
    [formKeys.debitCard.lastName]: { required: 'Last name is required' },
    [formKeys.debitCard.phoneNumber]: { required: 'Phone number is required' },
    [formKeys.debitCard.addressLine1]: { required: 'Address is required' },
    [formKeys.debitCard.city]: { required: 'City is required' },
    [formKeys.debitCard.state]: { required: 'State is required' },
    [formKeys.debitCard.zipCode]: { required: 'Zip code is required' },
  },
  bankAccount: {
    [formKeys.bankAccount.bankName]: { required: 'Bank name is required' },
    [formKeys.bankAccount.routingNumber]: {
      required: 'Routing number is required',
      onlyNumbers: 'Please use only numbers',
    },
    [formKeys.bankAccount.accountNumber]: { required: 'Account number is required' },
    [formKeys.bankAccount.confirmAccountNumber]: {
      required: 'Is required to confirm account number',
    },
    [formKeys.bankAccount.accountType]: { required: 'Account type is required' },
  },
  extraPayment: {
    [formKeys.extraPayment.amount]: { required: 'Extra payment amount is required' },
  },
};

export const forms = {
  bankAccount: {
    initialValues: {
      [formKeys.bankAccount.bankName]: __DEV__ ? 'Bank of US test ' : '',
      [formKeys.bankAccount.routingNumber]: __DEV__ ? '122105278' : '',
      [formKeys.bankAccount.accountNumber]: __DEV__ ? '0000000019' : '',
      [formKeys.bankAccount.confirmAccountNumber]: __DEV__ ? '0000000019' : '',
      [formKeys.bankAccount.accountType]: __DEV__ ? 'Checking' : 'Savings',
      [formKeys.bankAccount.setDefault]: false,
    },
    labels: {
      [formKeys.bankAccount.bankName]: 'Bank Name',
      [formKeys.bankAccount.routingNumber]: 'Routing Number',
      [formKeys.bankAccount.accountNumber]: 'Account Number',
      [formKeys.bankAccount.confirmAccountNumber]: 'Confirm account number',
      [formKeys.bankAccount.accountType]: 'Account Type',
      [formKeys.bankAccount.setDefault]: 'Use this bank account to make my payments.',
    },
    placeholders: {
      [formKeys.bankAccount.bankName]: '',
      [formKeys.bankAccount.routingNumber]: '',
      [formKeys.bankAccount.accountNumber]: '',
      [formKeys.bankAccount.confirmAccountNumber]: '',
      [formKeys.bankAccount.accountType]: '',
      [formKeys.bankAccount.setDefault]: '',
    },
    schema: Yup.object({
      [formKeys.bankAccount.bankName]: Yup.string()
        .required(customValidationMessages.bankAccount.bankName.required)
        .ensure()
        .trim()
        .lowercase(),
      [formKeys.bankAccount.routingNumber]: Yup.string()
        .required(customValidationMessages.bankAccount.routingNumber.required)
        .ensure()
        .trim()
        .lowercase()
        // TODO: Numeric only validation not working
        .matches(
          /[0-9]/,
          customValidationMessages.bankAccount[formKeys.bankAccount.routingNumber].onlyNumbers,
        ),
      [formKeys.bankAccount.accountNumber]: Yup.string()
        .required(customValidationMessages.bankAccount.accountNumber.required)
        .ensure()
        .trim()
        .lowercase()
        // TODO: Numeric only validation not working
        .matches(
          /[0-9]+/,
          customValidationMessages.bankAccount[formKeys.bankAccount.accountNumber].onlyNumbers,
        ),
      [formKeys.bankAccount.confirmAccountNumber]: Yup.string()
        .required(customValidationMessages.bankAccount.confirmAccountNumber.required)
        .ensure()
        .trim()
        .lowercase()
        // TODO: Numeric only validation not working
        .matches(
          /[0-9]+/,
          customValidationMessages.bankAccount[formKeys.bankAccount.accountNumber].onlyNumbers,
        )
        .test('account-numbers-match', "Account numbers doesn't match", function(value) {
          return this.parent.accountNumber === value;
        }),
      [formKeys.bankAccount.accountType]: Yup.string()
        .required(customValidationMessages.bankAccount.accountType.required)
        .oneOf(accountTypeValues),
      [formKeys.bankAccount.setDefault]: Yup.boolean(),
    }),
  },
  debitCard: {
    initialValues: {
      [formKeys.debitCard.cardNumber]: __DEV__ ? '9010113999999998' : '',
      [formKeys.debitCard.expDate]: __DEV__ ? '04/2020' : '',
      [formKeys.debitCard.cvc]: __DEV__ ? '412' : '',
      [formKeys.debitCard.firstName]: __DEV__ ? 'John' : '',
      [formKeys.debitCard.lastName]: __DEV__ ? 'Smith' : '',
      [formKeys.debitCard.phoneNumber]: __DEV__ ? '2901709325' : '',
      [formKeys.debitCard.addressLine1]: __DEV__ ? '220 LOCUS AVE' : '',
      [formKeys.debitCard.addressLine2]: __DEV__ ? '' : '',
      [formKeys.debitCard.city]: __DEV__ ? 'Atlanta' : '',
      [formKeys.debitCard.state]: __DEV__ ? 'GA' : '',
      [formKeys.debitCard.zipCode]: __DEV__ ? '30318' : '',
      [formKeys.debitCard.setDefault]: false,
    },
    labels: {
      [formKeys.debitCard.cardNumber]: 'Card Number',
      [formKeys.debitCard.expDate]: 'Expiration Date',
      [formKeys.debitCard.cvc]: 'Security Code',
      [formKeys.debitCard.firstName]: 'First Name',
      [formKeys.debitCard.lastName]: 'Last Name',
      [formKeys.debitCard.phoneNumber]: 'Phone Number',
      [formKeys.debitCard.addressLine1]: 'Address',
      [formKeys.debitCard.addressLine2]: 'Address 2',
      [formKeys.debitCard.city]: 'City',
      [formKeys.debitCard.state]: 'State',
      [formKeys.debitCard.zipCode]: 'ZIP Code',
      [formKeys.debitCard.setDefault]: 'Use this debit card to make my payments.',
    },
    placeholders: {
      [formKeys.debitCard.cardNumber]: '',
      [formKeys.debitCard.expDate]: 'MM/YYYY',
      [formKeys.debitCard.cvc]: '',
      [formKeys.debitCard.firstName]: '',
      [formKeys.debitCard.lastName]: '',
      [formKeys.debitCard.phoneNumber]: '',
      [formKeys.debitCard.addressLine1]: '',
      [formKeys.debitCard.addressLine2]: '',
      [formKeys.debitCard.city]: '',
      [formKeys.debitCard.state]: '',
      [formKeys.debitCard.zipCode]: '',
      [formKeys.debitCard.setDefault]: '',
    },
    schema: Yup.object({
      [formKeys.debitCard.cardNumber]: Yup.string()
        .required(customValidationMessages.debitCard[formKeys.debitCard.cardNumber].required)
        .ensure()
        .trim()
        .lowercase(),
      [formKeys.debitCard.expiryDate]: Yup.string(),
      [formKeys.debitCard.expDate]: Yup.string()
        .required(customValidationMessages.debitCard[formKeys.debitCard.expDate].required)
        .ensure()
        .trim()
        .lowercase()
        .matches(
          /^(0[1-9]|1[012])[-/](?:20)[0-9]{2}/,
          customValidationMessages.debitCard[formKeys.debitCard.expDate].format,
        ),
      [formKeys.debitCard.cvc]: Yup.string()
        .required(customValidationMessages.debitCard[formKeys.debitCard.cvc].required)
        .ensure()
        .trim()
        .lowercase()
        .min(3, customValidationMessages.debitCard[formKeys.debitCard.cvc].min)
        .max(4, customValidationMessages.debitCard[formKeys.debitCard.cvc].max),
      [formKeys.debitCard.securityCode]: Yup.string(),
      [formKeys.debitCard.firstName]: Yup.string()
        .required(customValidationMessages.debitCard[formKeys.debitCard.firstName].required)
        .ensure()
        .trim()
        .lowercase(),
      [formKeys.debitCard.lastName]: Yup.string()
        .required(customValidationMessages.debitCard[formKeys.debitCard.lastName].required)
        .ensure()
        .trim()
        .lowercase(),
      [formKeys.debitCard.phoneNumber]: Yup.string()
        .required(customValidationMessages.debitCard[formKeys.debitCard.phoneNumber].required)
        .ensure()
        .trim()
        .lowercase(),
      [formKeys.debitCard.line1Address]: Yup.string(),
      [formKeys.debitCard.addressLine1]: Yup.string()
        .required(customValidationMessages.debitCard[formKeys.debitCard.addressLine1].required)
        .ensure()
        .trim()
        .lowercase(),
      [formKeys.debitCard.line2Address]: Yup.string(),
      [formKeys.debitCard.addressLine2]: Yup.string()
        .notRequired()
        .ensure()
        .trim()
        .lowercase(),
      [formKeys.debitCard.city]: Yup.string()
        .required(customValidationMessages.debitCard[formKeys.debitCard.city].required)
        .ensure()
        .trim()
        .lowercase(),
      // TODO: validate validation logic
      [formKeys.debitCard.state]: Yup.string()
        .required(customValidationMessages.debitCard[formKeys.debitCard.state].required)
        .ensure()
        .trim()
        .length(2)
        .uppercase()
        .oneOf(stateValues),
      [formKeys.debitCard.zipCode]: Yup.string()
        .required(customValidationMessages.debitCard[formKeys.debitCard.zipCode].required)
        .ensure()
        .trim()
        .lowercase(),
      [formKeys.debitCard.setDefault]: Yup.boolean(),
    }),
  },
  extraPayment: {
    initialValues: {
      [formKeys.extraPayment.amount]: __DEV__ ? 10000 : 0,
    },
    schema: Yup.object({
      [formKeys.extraPayment.amount]: Yup.number().required(
        customValidationMessages.extraPayment[formKeys.extraPayment.amount].required,
      ),
    }),
    placeholders: {
      [formKeys.extraPayment.amount]: (maxAmount: number) => `$1.00 to $${maxAmount}`,
    },
  },
};

export const autopayConsentTerms = {
  [PaymentMethodTypeKeys.DEBIT_CARD]: `By selecting the Debit Card option, you authorize us, our agents, and our successors and assigns to initiate debits to the Debit Card you provided to us (“Card”) to process your scheduled payments under the terms of this Agreement.  If necessary, you also authorize initiating debits and credits to correct any erroneous payment.  The debit will appear on your deposit account statement.
We will initiate each debit on or after each applicable Due Date set forth in the Payment Schedule (“Due Date”).  If the Due Date occurs on a weekend or holiday, the debit will occur on the next business day.  The amount of any scheduled debit may be increased by any additional fees and/or charges you have incurred under this Agreement, including but not limited to any Late Charge and/or Returned Payment Fee.  If you make any partial prepayments, then you authorize us to vary the amount of the scheduled debit as needed to account for those partial prepayments   We will make no more than two (2) attempts to debit any single scheduled payment to your Card.  This Debit Card authorization will remain in full force and effect until we have received written notification from you of its termination in such time and in such manner as to afford us and the depository institution a reasonable opportunity to act on it.  A termination request will not apply to a scheduled debit unless we receive the request at least three (3) business days before the scheduled debit. You may send written notification by e-mail to customerservice@lendingpoint.com or by mail to LendingPoint, Attn: Customer Service, 1201 Roberts Blvd, Suite 200, Kennesaw, GA 30144.
`,
  [PaymentMethodTypeKeys.BANK_ACCOUNT]: `You represent and warrant that you are named as an account holder on Your Bank Account and that you have the right to withdraw funds from Your Bank Account.
You authorize us to re-initiate such ACH debit a total of two additional times (if necessary) for the same amount if the ACH debit is dishonored. You acknowledge that the origination of an electronic debit to Your Bank Account must comply with the provisions of U.S. law.  You agree that you are responsible for maintaining funds in Your Bank Account sufficient to satisfy each scheduled payment when it becomes due, and you waive any claim you may have against us arising from your failure to do so.
Your Bank Account Information. If there is any missing or erroneous information regarding Your Bank Account then you authorize us to verify and correct the information. You acknowledge, warrant, and represent that Your Bank Account is a legitimate, open, and active account into which your regular employment, benefit, and/or other income is currently deposited and that you have the right to initiate (and to authorize us to initiate) electronic debits from Your Bank Account.
Dates. You acknowledge that this authorization is an authorization to initiate an ACH debit to debit Your Bank Account on or after each Due Date set forth in the Loan Agreement or as changed thereafter.  If the above noted Due Date falls on a weekend or holiday, you authorize us to execute the payment on the next business day.
Range of Debits and Notice of Variation. Our debits to Your Bank Account will be in an amount between the amount of the scheduled payment and the total amount of any scheduled payments due and past due at the time we initiate the debit, plus any additional charges and/or fees incurred under the Loan Agreement, or a lesser amount owing based on your payment history. We will not debit your account for more than the total amount of scheduled payments due and past due at the time we initiate a debit (plus additional charges and/or fees, as applicable). If a debit amount will fall outside of the specified range, then we will email you notification of the amount of the debit and the date on or after which we will debit such amount, at least ten calendar days before the scheduled debit.
Partial Prepayments. If you make any partial prepayments, then you authorize us to vary the amount of the electronic debit as needed to reflect those partial prepayments.
Additional Amounts. You further authorize us to initiate separate electronic debits to Your Bank Account for any applicable Late Charge(s), Returned Payment Fee(s), and/or other fees or costs provided in this Loan Agreement.
Depository Institution Charges. You agree that you are solely responsible for any and all charges imposed on you by your depository institution in connection with payments processed and/or attempted under this authorization, including but not limited to charges related to insufficient funds in Your Bank Account.
Termination. You understand and acknowledge that you may terminate this authorization by notifying us in writing.  This authorization will remain in full force and effect until we have received written notification from you of its termination at least fifteen (15) business days prior to the next billing date. You may send written notification by e-mail to customerservice@lendingpoint.com or by mail to LendingPoint, Attn: Collections, 1201 Roberts Blvd, Suite 200, Kennesaw, GA 30144.
Error Correction. In the event we make an error in processing any payment, you authorize us to initiate a payment to or from Your Bank Account to correct the error.
PLEASE NOTE THAT YOU ARE NOT REQUIRED TO SELECT ACH DEBIT AUTHORIZATION. ACH DEBIT AUTHORIZATION IS FOR YOUR CONVENIENCE IN PAYING. BY SELECTING THE ACH DEBIT OPTION, YOU ACKNOWLEDGE THAT YOU HAVE BEEN PROVIDED AN ALTERNATIVE PAYMENT METHOD AND THAT YOU HAVE VOLUNTARILY CHOSEN TO PAY ELECTRONICALLY.
`,
};
