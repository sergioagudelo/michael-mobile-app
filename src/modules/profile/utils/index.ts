import { ImageSourcePropType } from 'react-native';
import { NavigationTabProp } from 'react-navigation-tabs';
import * as Yup from 'yup';

import { forms } from '../constants';

import * as testData from './testData';

export type ChartItemType = {
  label: string;
  value: number;
};

export type ChartChildrenProps<T> = {
  x: (value: number) => number;
  y: (value: number) => number;
  data: T[];
};

/**
 * Profile Options
 */
export type ProfileSettingProps = {
  title: string;
  disabled: boolean;
  icon: ImageSourcePropType;
  onPress?: (navigation: NavigationTabProp) => void;
};

export type DocumentType = {
  id: string;
  date: string;
  name: string;
  contentType: string | null;
  data: string | null;
};

export type ProfileInfoFormValues = Yup.InferType<typeof forms.profileInfo.schema>;

export { testData };

export type User = {
  id?: string;
  firstName: string;
  lastName: string;
  email: string;
  phoneNumber: string;
  street: string;
  city: string;
  state: string;
  postalCode: string;
  employmentType: string;
  employerName: string;
  annualIncome: number;

  country?: null;
  unit?: string;
  userName?: string;
  refinanceEligible?: boolean;
  contractIsACH?: boolean;
};
