import React from 'react';
import { ThemedComponentProps, withStyles } from 'react-native-ui-kitten';
import * as Animatable from 'react-native-animatable';
import { NavigationTabScreenProps } from 'react-navigation-tabs';

// store
import { connect } from 'react-redux';
import { RootState } from 'typesafe-actions';
import * as store from '../store';

// Components
import { CardUser } from '../components';

const mapStateToProps = (state: RootState) => ({
  userInfo: store.selectors.getUserInfo(state),
});

type ProfileProps = NavigationTabScreenProps &
  ThemedComponentProps &
  ReturnType<typeof mapStateToProps>;

const Profile = ({ navigation, userInfo }: ProfileProps) => {
  return (
    <>
      <Animatable.View animation="zoomIn" duration={800} useNativeDriver>
        <CardUser userInfo={userInfo} onEditPress={() => navigation.navigate('ProfileInfo')} />
      </Animatable.View>

      {
        // TODO: complete this container: buttons.
      }
    </>
  );
};

export default connect(mapStateToProps)(
  withStyles(Profile, () => ({
    centerText: {
      textAlign: 'center',
    },
    uppercase: {
      textTransform: 'uppercase',
    },
    screen: {
      flex: 1,
    },
    title: {
      marginVertical: 30,
    },
    profileOptions: {
      flexDirection: 'row',
      flexWrap: 'wrap',
      alignItems: 'center',
      justifyContent: 'center',
      alignContent: 'center',
    },
    optionContainer: {
      margin: 5,
      width: 95,
    },
    notificationsButtonContainer: {
      marginBottom: 30,
      marginHorizontal: 30,
    },
    notificationsButton: {
      borderRadius: 30,
      flexDirection: 'row-reverse',
    },
  })),
);
