import DocumentsInfo from './DocumentsInfo';
import ProfileInfoForm from './ProfileInfoForm';
import ProfileSecurityInfo from './ProfileSecurityInfo';
import Profile from './Profile';

export { DocumentsInfo, ProfileInfoForm, ProfileSecurityInfo, Profile };
