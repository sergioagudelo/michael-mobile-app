import React, { useState } from 'react';
import { connect } from 'react-redux';

// Types
import { Dispatch, bindActionCreators } from 'redux';
import { RootState } from 'typesafe-actions';
import { withNavigation } from 'react-navigation';
import shared from '../../shared';

// Components
import ProfileSecurity from '../components/ProfileSecurity';

const { store: sharedStore } = shared;

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(
    {
      setBiometricAuthPermission: sharedStore.actions.actions.setBiometricAuthPermission,
    },
    dispatch,
  );

const mapStateToProps = (state: RootState) => ({
  biometricAuthPermission: sharedStore.selectors.getBiometricAuthPermission(state),
  isBiometricAuthSupported: sharedStore.selectors.getIsBiometricAuthSupported(state),
});

type ProfileSecurityInfoProps = ReturnType<typeof mapDispatchToProps> &
  ReturnType<typeof mapStateToProps>;

const ProfileSecurityInfo = ({
  biometricAuthPermission: faceId,
  setBiometricAuthPermission: setFaceId,
  isBiometricAuthSupported,
  navigation,
}: ProfileSecurityInfoProps) => {
  /**
   * states needed by the component
   */
  // const [loading, setLoading] = useState(true);
  const [device, setDevice] = useState(false);

  const [notifications, setNotifications] = useState(false);

  /**
   * save callback for latter use when toggle is touched
   */
  const states = {
    device: setDevice,
    faceId: setFaceId,
    notifications: setNotifications,
  };

  /**
   * one of the toggles was touched
   */
  const onChangeToggle = (checked: boolean, type: 'device' | 'faceId' | 'notifications') => {
    if (type === 'faceId' && !isBiometricAuthSupported) {
      return;
    }
    if (type === 'notifications') {
      navigation.navigate('ForgotPassword');
      return;
    }
    states[type](checked);
  };

  /**
   * simulate network request
   */

  return (
    <ProfileSecurity
      isBiometricAuthSupported={isBiometricAuthSupported}
      device={device}
      faceId={faceId}
      notifications={notifications}
      isLoading={false}
      onChangeToggle={onChangeToggle}
    />
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withNavigation(ProfileSecurityInfo));
