import { compose } from 'redux';
import { withFormik } from 'formik';
import { withNavigation } from 'react-navigation';
import { NavigationStackScreenProps } from 'react-navigation-stack';

// store
import { connect } from 'react-redux';
import { RootState } from 'typesafe-actions';
import * as store from '../store';

// Types
import { ProfileInfoFormValues } from '../utils';

// Constants
import { forms } from '../constants';

// Component
import ProfileInfoForm from '../components/ProfileInfoTEXT';

const mapStateToProps = (state: RootState) => ({
  userInfo: store.selectors.getUserInfo(state),
});

type ProfileInfoFormProps = ReturnType<typeof mapStateToProps> &
  NavigationStackScreenProps & { initialValues?: ProfileInfoFormValues };

export default compose(
  withNavigation,
  connect(mapStateToProps),
  withFormik<ProfileInfoFormProps, ProfileInfoFormValues>({
    mapPropsToValues: props => ({
      firstName: props.userInfo.firstName,
      lastName: props.userInfo.lastName,
      email: props.userInfo.email,
      phoneNumber: props.userInfo.phoneNumber,
      street: props.userInfo.street,
      city: props.userInfo.city,
      state: props.userInfo.state,
      zipCode: props.userInfo.postalCode, // TODO: YONVISIONS please use double typing
      employmentType: props.userInfo.employmentType,
      employerName: props.userInfo.employerName,
      annualIncome: props.userInfo.annualIncome,
    }),
    isInitialValid: props => forms.profileInfo.schema.isValidSync(props.initialValues),
    validationSchema: forms.profileInfo.schema,
    handleSubmit: (values, { setSubmitting, setStatus, props }) => {
      setTimeout(() => {
        setSubmitting(false);
        // TODO: Check if call failed
        const failed = false;
        if (!failed) {
          // TODO: Handle success case, e.g. navigate to another screen, show modal with success message, etc.
          props.navigation.goBack();
        } else {
          // ? Show the errors for the requests that failed
          setStatus({
            success: false,
            // TODO: Show appropriate error message
            error: 'Error calling API',
          });
        }
      }, 2000);
    },
  }),
)(ProfileInfoForm);
