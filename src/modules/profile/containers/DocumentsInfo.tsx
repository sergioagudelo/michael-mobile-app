import React, { useState, useEffect } from 'react';

// Types
import { DocumentType } from '../utils';

// Components
import Documents from '../components/Documents';
import * as profileService from '../services/profile';

const DocumentsInfo = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [documents, setDocuments] = useState<DocumentType[]>();
  const [failed, setFailed] = useState(false);

  useEffect(() => {
    const getDocuments = async () => {
      // TODO REDUX
      setIsLoading(true);
      const result = await profileService.getAllUserDocuments();
      if (result.success && result.response) {
        setDocuments(result.response);
        setIsLoading(false);
        return;
      }
      setFailed(true);
      setIsLoading(false);
    };
    if (!isLoading && !documents && !failed) {
      getDocuments();
    }
  });

  const getDocumentData = async (_document: DocumentType): Promise<DocumentType> => {
    try {
      const document = { ..._document };
      const res = await profileService.getSingleUserDocument(document.id);

      if (res.success && res.response) {
        document.data = res.response.fileContent;
        document.contentType = res.response.fileType;
      }
      return document;
    } catch (e) {
      return _document;

      // TODO HANDLE ERR
    }
  };

  return (
    <Documents getDocumentData={getDocumentData} documents={documents} isLoading={isLoading} />
  );
};

export default DocumentsInfo;
