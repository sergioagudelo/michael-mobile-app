import Styles from './Styles';

import { MAX_CREDIT_SCORE } from '../constants';
import { DocumentType } from '../utils';
import { AllUserDocumentsResponseType } from '../services/utils';

export const calcScorePosition = (score: number, creditBarWidth: number) =>
  ((score * creditBarWidth) / MAX_CREDIT_SCORE / creditBarWidth) * 100;

export const AllDocumentsResponseParser = (
  response: AllUserDocumentsResponseType,
): DocumentType[] =>
  response.documents.map(({ id, name, date }) => ({
    id,
    date,
    name,
    contentType: null,
    data: null,
  }));

export { Styles };
