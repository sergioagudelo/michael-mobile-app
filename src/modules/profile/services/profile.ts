import { lendingpoint } from '../../../services';
import { ApiResponse } from '../../../services/utils';
import { SingleUserDocumentsResponseType } from './utils';
import { AllDocumentsResponseParser } from '../helpers';
import { DocumentType } from '../utils';
// import { ResponseUserInfo } from '../utils';
const {
  api: { consumer },
  constants: { paths },
  helpers: { handleSubModuleError },
} = lendingpoint;
// TODO HTTPServicesResponse
export const getAllUserDocuments = async (): Promise<ApiResponse<DocumentType[]>> => {
  try {
    const response = await consumer.get(paths.documents);
    return { success: true, response: AllDocumentsResponseParser(response.data) };
  } catch (err) {
    return handleSubModuleError(err);
  }
};

export const getSingleUserDocument = async (
  documentId: string,
): Promise<ApiResponse<SingleUserDocumentsResponseType>> => {
  try {
    const response = await consumer.get(`${paths.documents}/${documentId}`);
    return { success: true, response: response.data };
  } catch (err) {
    return handleSubModuleError(err);
  }
};

// TODO type user res
export const getUserInfo = async (): Promise<ApiResponse<any>> => {
  try {
    const response = await consumer.get(paths.user);
    return { success: true, response: response.data };
  } catch (err) {
    return handleSubModuleError(err);
  }
};
