export type AllUserDocumentsResponseType = {
  documents: [
    {
      id: string;
      date: string;
      name: string;
    },
  ];
};
export type SingleUserDocumentsResponseType = {
  fileName: string;
  fileType: string | null;
  size: number;
  fileContent: string;
};
