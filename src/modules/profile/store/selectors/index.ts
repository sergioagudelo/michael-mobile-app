import { RootState } from 'typesafe-actions';
import { createSelector } from 'reselect';

import { NAME } from '../../constants';
import { User } from '../../utils';

const userInfoSelector = (state: RootState) => state[NAME].userInfo;

export const getUserInfo = createSelector(
  userInfoSelector,
  (item: User) => item,
);

export const getUserEmail = createSelector(
  userInfoSelector,
  (item: User) => item.email,
);
