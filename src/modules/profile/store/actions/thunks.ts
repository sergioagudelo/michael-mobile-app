// Types
import { Dispatch } from 'redux';
import { UserInfo } from './actions';

import { getUserInfo } from '../../services/profile';

export const userInfo = () => async (dispatch: Dispatch) => {
  dispatch(UserInfo.request());
  const res = await getUserInfo();
  if (res.success) {
    dispatch(UserInfo.success({ ...res.response, employmentType: res.response.employementType }));
    return;
  }
  dispatch(UserInfo.failure(res.error));
};
