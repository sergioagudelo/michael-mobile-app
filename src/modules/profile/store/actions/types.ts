import { NAME } from '../../constants';

import shared from '../../../shared';

export default shared.helpers.createActionTypes(NAME, [
  'USER.ATTEMPT',
  'USER.SUCCESS',
  'USER.FAILURE',
]);
