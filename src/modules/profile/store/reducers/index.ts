import { combineReducers } from 'redux';
import { createReducer } from 'typesafe-actions';

import { User } from '../../utils';

import { actions } from '../actions';

const userInfo = createReducer(null as User | null).handleAction(
  actions.UserInfo.success,
  (state, action) => action.payload,
);

const userReducer = combineReducers({
  userInfo,
});

export default userReducer;
export type UserState = ReturnType<typeof userReducer>;
