/* eslint-disable global-require */
import * as Yup from 'yup';
import { NavigationTabProp } from 'react-navigation-tabs';
import { parsePhoneNumber } from 'libphonenumber-js/mobile';

import shared from '../../shared';

/**
 * redux key name
 */
export const NAME = 'profile';

/**
 * profile options screen
 */
export const profileButtons = [
  {
    title: 'Profile\n',
    icon: require('../../../img/icons/ico_personal_info.png'),
    onPress: (navigation: NavigationTabProp) => navigation.navigate('ProfileInfo'),
    disabled: false,
  },
  {
    title: 'Security\n',
    icon: require('../../../img/icons/ico_security.png'),
    onPress: (navigation: NavigationTabProp) => navigation.navigate('ProfileSecurityDetails'),
    disabled: false,
  },
  {
    title: 'Documents\n',
    icon: require('../../../img/icons/ico_documents.png'),
    onPress: (navigation: NavigationTabProp) => navigation.navigate('ProfileDocumentsDetails'),
    disabled: false,
  },
  {
    title: 'Payment\nMethods',
    icon: require('../../../img/icons/ico_payment_methods.png'),
    onPress: (navigation: NavigationTabProp) => navigation.navigate('PaymentMethods'),
    disabled: false,
  },
  // {
  //   title: 'My Credit\nScore',
  //   icon: require('../../../img/icons/ico_score.png'),
  //   onPress: (navigation: NavigationTabProp) => navigation.navigate('CreditScore'),
  //   disabled: false,
  // },
  // {
  //   title: 'My Virtual\nCard',
  //   icon: require('../../../img/icons/ico_card.png'),
  //   onPress: (navigation: NavigationTabProp) => navigation.navigate('VirtualCard'),
  //   disabled: true,
  // },
  {
    title: 'Help Center',
    icon: require('../../../img/icons/ico_help.png'),
    onPress: (navigation: NavigationTabProp) => navigation.navigate('HelpCenter'),
    disabled: false,
  },
  {
    title: 'Sign Out\n ',
    icon: require('../../../img/icons/ico_exit.png'),
    onPress: (navigation: NavigationTabProp) => {
      navigation.navigate('AuthLoading');
    },
    disabled: false,
  },
];

export const MAX_CREDIT_SCORE = 850;

export const forms = {
  profileInfo: {
    initialValues: {
      firstName: __DEV__ ? 'John' : 'John',
      lastName: __DEV__ ? 'Smith' : 'Smith',
      email: __DEV__ ? 'test@mail.com' : 'test@mail.com',
      phoneNumber: __DEV__ ? '3216549870' : '3216549870',
      street: __DEV__ ? '220 LOCUS AVE' : '220 LOCUS AVE',
      city: __DEV__ ? 'Atlanta' : 'Atlanta',
      state: __DEV__ ? 'GA' : 'GA',
      zipCode: __DEV__ ? '30318' : '30318',
      employmentType: __DEV__ ? 'Employee' : 'Employee',
      employerName: __DEV__ ? '' : '',
      annualIncome: __DEV__ ? 25000.0 : 25000.0,
    },
    labels: {
      firstName: 'First Name',
      lastName: 'Last Name',
      email: 'Email',
      phoneNumber: 'Phone Number',
      street: 'Address',
      city: 'City',
      state: 'State',
      zipCode: 'ZIP Code',
      employmentType: 'Employment Type',
      employerName: "Employer's Name",
      annualIncome: 'Annual Income',
    },
    placeholders: {
      firstName: 'John',
      lastName: 'Smith',
      email: 'myemail@email.com',
      phoneNumber: '(999) 999-9999',
      street: '220 LOCUS AVE',
      city: 'Atlanta',
      state: 'Select...',
      zipCode: '30318',
      employmentType: '',
      employerName: '',
      annualIncome: '',
    },
    schema: Yup.object({
      firstName: Yup.string()
        .required()
        .ensure()
        .trim()
        .lowercase(),
      lastName: Yup.string()
        .required()
        .ensure()
        .trim()
        .lowercase(),
      email: Yup.string()
        .required()
        .ensure()
        .trim()
        .lowercase()
        .email(),
      phoneNumber: Yup.string()
        .required()
        .ensure()
        .trim()
        .lowercase()
        .test('phoneNumber', 'Phone number is invalid', function(val) {
          try {
            const number = parsePhoneNumber(val, 'US');
            return number.isValid();
          } catch (e) {
            return false;
          }
        }),
      street: Yup.string()
        .required()
        .ensure()
        .trim()
        .lowercase(),
      city: Yup.string()
        .required()
        .ensure()
        .trim()
        .lowercase(),
      // TODO: validate validation logic
      state: Yup.string()
        .required()
        .ensure()
        .trim()
        .length(2)
        .uppercase()
        .oneOf(shared.constants.stateValues),
      zipCode: Yup.string()
        .required()
        .ensure()
        .trim()
        .lowercase(),
      // TODO: validate validation logic and field type
      employmentType: Yup.string()
        .ensure()
        .trim()
        .lowercase(),
      // TODO: validate validation logic
      employerName: Yup.string()
        .ensure()
        .trim()
        .lowercase(),
      // TODO: validate validation logic
      annualIncome: Yup.number().positive(),
    }),
  },
};
