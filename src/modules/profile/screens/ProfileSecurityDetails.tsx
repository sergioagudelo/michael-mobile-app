import React from 'react';
import { SafeAreaView, ScrollView } from 'react-native';
import { ThemedComponentProps, withStyles, Button } from 'react-native-ui-kitten';
import { withNavigation } from 'react-navigation';
import * as Animatable from 'react-native-animatable';
import { NavigationStackScreenProps } from 'react-navigation-stack';

// Modules
import shared from '../../shared';

// Helpers
import { Styles } from '../helpers';

// Components
import { ProfileSecurityInfo } from '../containers';

const { HeaderView } = shared.components;

type ProfileSecurityDetailsProps = ThemedComponentProps & NavigationStackScreenProps;

/**
 * Render Profile Element
 */
const ProfileSecurityDetailsElement = ({
  themedStyle,
  navigation,
}: ProfileSecurityDetailsProps) => {
  return (
    <SafeAreaView style={themedStyle.flex}>
      <ScrollView>
        <HeaderView title="Account Security" subTitle="personal settings" />
        <ProfileSecurityInfo />
        {/*
        <Animatable.View animation="zoomIn" delay={400} duration={800} useNativeDriver>
          <VirtualCard dismissible={false} />
        </Animatable.View> */}
      </ScrollView>
    </SafeAreaView>
  );
};

/**
 * apply theme styles to Profile
 * @see https://akveo.github.io/react-native-ui-kitten/docs/components/withstyles/overview#withstyles
 */
const ProfileSecurityDetails = withStyles(withNavigation(ProfileSecurityDetailsElement), () => ({
  ...Styles,
  button: {
    width: '60%',
    alignSelf: 'center',
    marginTop: 40,
    marginBottom: 25,
  },
}));

export default ProfileSecurityDetails;
