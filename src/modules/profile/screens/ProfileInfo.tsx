import React from 'react';
import { SafeAreaView, Platform } from 'react-native';
import { Text, withStyles, ThemedComponentProps } from 'react-native-ui-kitten';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

// Modules
import shared from '../../shared';

// Components
import { ProfileInfoForm } from '../containers';

type ProfileInfoComponentProps = ThemedComponentProps;

const ProfileInfoComponent = ({ themedStyle }: ProfileInfoComponentProps) => {
  return (
    <SafeAreaView style={themedStyle.screen} {...shared.helpers.setTestID('ProfileInfoScreen')}>
      <KeyboardAwareScrollView
        enableOnAndroid
        contentContainerStyle={themedStyle.body}
        keyboardShouldPersistTaps={Platform.select({
          ios: 'never',
          android: 'handled',
        })}
      >
        <Text category="h1" status="accent" style={themedStyle.centerText}>
          Personal Information
        </Text>
        <Text
          category="metatext"
          style={[themedStyle.centerText, themedStyle.uppercase, themedStyle.subtitle]}
        >
          your lendingpoint profile
        </Text>
        <ProfileInfoForm />
      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
};

const ProfileInfo = withStyles(ProfileInfoComponent, () => ({
  screen: {
    flex: 1,
  },
  body: {
    flexGrow: 1,
  },
  centerText: {
    textAlign: 'center',
  },
  uppercase: {
    textTransform: 'uppercase',
  },
  subtitle: {
    paddingTop: 10,
  },
}));

export default ProfileInfo;
