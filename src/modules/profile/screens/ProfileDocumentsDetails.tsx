import React from 'react';
import { SafeAreaView, ScrollView } from 'react-native';
import { ThemedComponentProps, withStyles } from 'react-native-ui-kitten';
// import * as Animatable from 'react-native-animatable';

// Modules
import shared from '../../shared';
// import loans from '../../loans';

// Components
import { DocumentsInfo } from '../containers';

// const { VirtualCard } = shared.containers;

type ProfileDocumentsDetailsProps = ThemedComponentProps;

/**
 * Render Profile Element
 */
const ProfileDocumentsDetails = ({ themedStyle }: ProfileDocumentsDetailsProps) => {
  return (
    <SafeAreaView style={themedStyle.screen} {...shared.helpers.setTestID('ProfileInfoScreen')}>
      <ScrollView contentContainerStyle={themedStyle.body} showsVerticalScrollIndicator={false}>
        <DocumentsInfo />

        {/* <Animatable.View animation="zoomIn" delay={400} duration={800} useNativeDriver>
          <VirtualCard dismissible={false} />
        </Animatable.View> */}
      </ScrollView>
    </SafeAreaView>
  );
};

export default withStyles(ProfileDocumentsDetails, () => ({
  screen: {
    flex: 1,
  },
  body: {
    flexGrow: 1,
  },
}));
