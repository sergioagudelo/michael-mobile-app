import React from 'react';
import { SafeAreaView, ScrollView } from 'react-native';
import { ThemedComponentProps, withStyles, Layout } from 'react-native-ui-kitten';
// import * as Animatable from 'react-native-animatable';
import { NavigationTabScreenProps } from 'react-navigation-tabs';

// store
import { connect } from 'react-redux';
import { actions as authActions } from '../../auth/store/actions';
// Types

import { ProfileSettingProps } from '../utils';

// Modules
import shared from '../../shared';

// Constants
import { profileButtons } from '../constants';

// Components
import { Profile } from '../containers';

const mapStateToProps = (state: RootState) => ({
  isBiometricAuthSupported: shared.store.selectors.getIsBiometricAuthSupported(state),
});

const { HeaderView, VerticalCardButton, AppVersionLabel } = shared.components;

type ProfileProps = NavigationTabScreenProps & ThemedComponentProps;

const ProfileScreen = ({
  navigation,
  themedStyle,
  dispatch,
  isBiometricAuthSupported,
}: ProfileProps) => {
  return (
    <SafeAreaView style={themedStyle.screen} {...shared.helpers.setTestID('ProfileScreen')}>
      <ScrollView>
        <HeaderView title="Your Account" subTitle="Information &amp; settings" />

        <Profile navigation={navigation} themedStyle={themedStyle} />

        <Layout style={themedStyle.profileOptions}>
          {(profileButtons as ProfileSettingProps[]).map((option, index) => {
            return (
              <VerticalCardButton
                disabled={option.disabled}
                key={index}
                icon={option.icon}
                onPress={() => {
                  if (option.title.includes('Sign Out')) {
                    dispatch(authActions.logout());
                  }
                  if (option.onPress) {
                    option.onPress(navigation);
                  }
                }}
                containerStyle={themedStyle.optionContainer}
              >
                {option.title}
              </VerticalCardButton>
            );
          })}
        </Layout>
        {/* <Text category="h2" status="primary" style={[themedStyle.centerText, themedStyle.title]}>
          Recent Notifications
        </Text> */}
        <Layout style={themedStyle.notificationsButtonContainer}>
          {/* <Button
            status="info"
            style={themedStyle.notificationsButton}
            textStyle={[themedStyle.uppercase, themedStyle.centerText]}
            icon={style => <Icon name="arrow-circle-right-outline" {...style} />}
            onPress={() => navigation.navigate('Notifications')}
          >
             {`You Have ${
              testData.testNotifications.filter(notification => !notification.read).length
            } Unread Notifications!`}
            Check your notifications
          </Button> */}
          <AppVersionLabel />
        </Layout>
      </ScrollView>
    </SafeAreaView>
  );
};

export default connect(mapStateToProps)(
  withStyles(ProfileScreen, () => ({
    centerText: {
      textAlign: 'center',
    },
    uppercase: {
      textTransform: 'uppercase',
    },
    screen: {
      flex: 1,
    },
    title: {
      marginVertical: 30,
    },
    profileOptions: {
      marginTop: 30,
      flexDirection: 'row',
      flexWrap: 'wrap',
      alignItems: 'center',
      justifyContent: 'center',
      alignContent: 'center',
    },
    optionContainer: {
      margin: 5,
      width: 95,
    },
    notificationsButtonContainer: {
      marginBottom: 30,
      marginHorizontal: 30,
    },
    notificationsButton: {
      borderRadius: 30,
      flexDirection: 'row-reverse',
    },
  })),
);
