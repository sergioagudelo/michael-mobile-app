import Profile from './Profile';
import ProfileInfo from './ProfileInfo';
import ProfileDocumentsDetails from './ProfileDocumentsDetails';
import ProfileSecurityDetails from './ProfileSecurityDetails';

export { Profile, ProfileInfo, ProfileDocumentsDetails, ProfileSecurityDetails };
