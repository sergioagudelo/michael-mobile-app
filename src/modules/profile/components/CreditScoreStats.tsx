import React from 'react';
import { Image, View, Text as NativeText } from 'react-native';
import { withStyles, ThemedComponentProps, Text } from 'react-native-ui-kitten';
import * as Animatable from 'react-native-animatable';

// Modules
import shared from '../../shared';

// Helpers
import * as helpers from '../helpers';

// Components
const { Dashed } = shared.components;

type CreditScoreStatsComponentProps = { score: number } & ThemedComponentProps;

const CreditScoreStatsComponent = ({ score, themedStyle }: CreditScoreStatsComponentProps) => {
  const [creditBarWidth, setCreditBarWidth] = React.useState(0);

  return (
    <>
      <View style={themedStyle.stats}>
        <View style={themedStyle.statItem}>
          <View style={themedStyle.statValueContainer}>
            <NativeText style={themedStyle.scoreValue}>{score}</NativeText>
          </View>
          <Text status="primary" style={[themedStyle.uppercase, themedStyle.centerText]}>
            EXCELLENT
          </Text>
        </View>

        <Dashed direction="column" customStyle={themedStyle.statSeparator} />

        <View style={themedStyle.statItem}>
          <View style={themedStyle.statValueContainer}>
            <Image
              source={require('../../../img/icons/ico_arrow_up.png')}
              style={themedStyle.statIcon}
            />
            <NativeText style={[themedStyle.uppercase, themedStyle.scoreMetadata]}>
              5<NativeText style={themedStyle.scoreMetadataSuffix}>&nbsp;pts</NativeText>
            </NativeText>
          </View>
          <Text
            appearance="hint"
            category="mini"
            style={[themedStyle.uppercase, themedStyle.centerText]}
          >
            PAST 7 DAYS
          </Text>
        </View>

        <Dashed direction="column" customStyle={themedStyle.statSeparator} />

        <View style={themedStyle.statItem}>
          <View style={themedStyle.statValueContainer}>
            <Image
              source={require('../../../img/icons/ico_arrow_up.png')}
              style={themedStyle.statIcon}
            />
            <NativeText style={[themedStyle.uppercase, themedStyle.scoreMetadata]}>
              10
              <NativeText style={themedStyle.scoreMetadataSuffix}>&nbsp;pts</NativeText>
            </NativeText>
          </View>
          <Text
            appearance="hint"
            category="mini"
            style={[themedStyle.uppercase, themedStyle.centerText]}
          >
            PAST 30 DAYS
          </Text>
        </View>
      </View>

      <View
        style={themedStyle.scoreBar}
        onLayout={({
          nativeEvent: {
            layout: { width },
          },
        }) => setCreditBarWidth(width)}
      >
        <Animatable.View
          animation="slideInLeft"
          delay={100}
          useNativeDriver
          style={[
            themedStyle.scoreBarIndicator,
            {
              left: `${helpers.calcScorePosition(score, creditBarWidth)}%`,
              borderLeftWidth: 16 / 2,
              borderRightWidth: 16 / 2,
            },
          ]}
        />
        <View style={themedStyle.badScoreContainer}>
          <View style={[themedStyle.scoreBarItem, themedStyle.badScore]} />
          <Text category="metatext" style={themedStyle.badScoreLabelContainer}>
            300
          </Text>
        </View>

        <View style={themedStyle.regularScoreContainer}>
          <View style={[themedStyle.scoreBarItem, themedStyle.regularScore]} />
          <Text category="metatext" style={themedStyle.regularScoreLabelContainer}>
            630
          </Text>
        </View>

        <View style={themedStyle.goodScoreContainer}>
          <View style={[themedStyle.scoreBarItem, themedStyle.goodScore]} />
          <Text category="metatext" style={themedStyle.goodScoreLabelContainer}>
            690
          </Text>
        </View>

        <View style={themedStyle.excellentScoreContainer}>
          <View style={[themedStyle.scoreBarItem, themedStyle.excellentScore]} />
          <Text category="metatext" style={themedStyle.excellentScoreLabelContainer}>
            720
          </Text>
        </View>
      </View>

      <Text
        category="mini"
        appearance="hint"
        style={[themedStyle.uppercase, themedStyle.statTimeAgo]}
      >
        UPDATED 5 DAYS AGO
      </Text>
    </>
  );
};

const CreditScoreStats = withStyles(CreditScoreStatsComponent, theme => ({
  centerText: {
    textAlign: 'center',
  },
  uppercase: {
    textTransform: 'uppercase',
  },
  stats: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  statItem: {
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  statSeparator: {
    marginHorizontal: 15,
  },
  statValueContainer: {
    flex: 1,
    justifyContent: 'space-around',
  },
  statIcon: {
    position: 'absolute',
    marginLeft: -15,
    marginTop: -5,
  },
  scoreValue: {
    fontFamily: 'FiraSans-Regular',
    fontSize: 60,
    color: theme['color-primary-700'],
  },
  scoreMetadata: {
    fontFamily: 'FiraSans-Regular',
    fontSize: 38,
    color: theme['color-basic-1100'],
  },
  scoreMetadataSuffix: {
    fontFamily: 'FiraSans-Medium',
    fontSize: 12,
  },
  scoreBar: {
    flexDirection: 'row',
    marginTop: 14,
    paddingTop: 14,
  },
  scoreBarIndicator: {
    position: 'absolute',
    borderTopWidth: 8,
    borderTopColor: theme['color-primary-700'],
    borderLeftColor: 'transparent',
    borderRightColor: 'transparent',
  },
  scoreBarItem: {
    height: 10,
  },
  badScoreContainer: {
    flex: 0.5,
  },
  badScore: {
    backgroundColor: theme['color-danger-500'],
    borderTopLeftRadius: 7.5,
    borderBottomLeftRadius: 7.5,
  },
  badScoreLabelContainer: {
    marginTop: 6,
  },
  regularScoreContainer: {
    flex: 0.18,
  },
  regularScore: {
    backgroundColor: '#EBC127',
  },
  regularScoreLabelContainer: {
    marginTop: 6,
    marginLeft: -11,
  },
  goodScoreContainer: {
    flex: 0.18,
  },
  goodScore: {
    backgroundColor: theme['color-primary-300'],
  },
  goodScoreLabelContainer: {
    marginTop: 6,
    marginLeft: -11,
  },
  excellentScoreContainer: {
    flex: 0.14,
  },
  excellentScore: {
    backgroundColor: theme['color-success-500'],
    borderTopRightRadius: 7.5,
    borderBottomRightRadius: 7.5,
  },
  excellentScoreLabelContainer: {
    marginTop: 6,
    marginLeft: -11,
  },
  statTimeAgo: {
    marginTop: 6,
  },
}));

export default CreditScoreStats;
