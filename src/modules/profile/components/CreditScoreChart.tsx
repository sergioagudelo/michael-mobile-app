import React from 'react';
import { View } from 'react-native';
import { ThemedComponentProps, withStyles } from 'react-native-ui-kitten';
import { LineChart, XAxis, YAxis } from 'react-native-svg-charts';
import { Circle, Line } from 'react-native-svg';

// Types
import { ChartChildrenProps, ChartItemType } from '../utils';

const X_AXIS_HEIGHT = 30;

type CreditScoreChartProps = { chart: ChartItemType[] } & ThemedComponentProps;

const CreditScoreChart = ({ chart, theme, themedStyle }: CreditScoreChartProps) => {
  const verticalContentInset = { top: 10, bottom: 10, left: 10, right: 10 };

  const Decorator = ({ x, y, data }: ChartChildrenProps<ChartItemType>) => {
    return data.map((value, index) => (
      <Circle
        key={index}
        cx={x(index)}
        cy={y(value.value)}
        r={4}
        stroke={theme['color-primary-900']}
        fill="white"
      />
    ));
  };

  const HorizontalTopLine = ({
    y,
    data: { length, [length - 1]: last },
  }: ChartChildrenProps<ChartItemType>) => {
    return (
      <Line
        key="zero-axis"
        x1="0%"
        x2="100%"
        y1={y(last.value)}
        y2={y(last.value)}
        stroke={theme['color-primary-900']}
        strokeDasharray={[2, 10]}
        strokeWidth={1}
      />
    );
  };

  const HorizontalBottomLine = ({ y, data }: ChartChildrenProps<ChartItemType>) => {
    const minY = Math.min(...data.map(item => item.value));
    return (
      <Line
        key="zero-axis"
        x1="0%"
        x2="100%"
        y1={y(minY)}
        y2={y(minY)}
        stroke={theme['color-primary-900']}
        strokeDasharray={[2, 10]}
        strokeWidth={1}
      />
    );
  };

  return (
    <>
      <YAxis
        data={chart}
        yAccessor={({ item }) => item.value}
        style={{ marginBottom: X_AXIS_HEIGHT }}
        contentInset={verticalContentInset}
        svg={themedStyle.yAxeSvg}
        numberOfTicks={3}
      />
      <View style={themedStyle.chartContainer}>
        <LineChart
          style={themedStyle.chart}
          data={chart}
          contentInset={verticalContentInset}
          yAccessor={({ item }) => item.value}
          svg={themedStyle.chartLine}
          numberOfTicks={3}
        >
          <HorizontalTopLine />
          <Decorator />
          <HorizontalBottomLine />
        </LineChart>
        <XAxis
          style={themedStyle.chartXAxis}
          data={chart}
          xAccessor={({ index }) => index}
          formatLabel={(_, index) => chart[index].label}
          contentInset={{ left: 18, right: 18 }}
          svg={themedStyle.xAxeSvg}
        />
      </View>
    </>
  );
};

export default withStyles(CreditScoreChart, theme => ({
  chartContainer: {
    flex: 1,
    marginLeft: 10,
  },
  chart: {
    flex: 1,
  },
  chartXAxis: {
    marginHorizontal: -10,
    height: X_AXIS_HEIGHT,
  },
  chartLine: {
    stroke: theme['color-primary-900'],
  },
  yAxeSvg: {
    fontSize: 10,
    fill: 'grey',
  },
  xAxeSvg: {
    fontSize: 10,
    fill: 'grey',
  },
}));
