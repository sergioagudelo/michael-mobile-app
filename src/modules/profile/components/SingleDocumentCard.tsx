import React from 'react'
const SingleDocumentCard = = ({ item, index }: ListRenderItemInfo<DocumentType>) => {
  const [isLoading, setIsLoading] = useState(false);
  return (
    <Animatable.View
      animation="fadeInDown"
      duration={300}
      delay={(index && index * 150) || 150}
      useNativeDriver
    >
      {isLoading ? (
        <Loading isLoading />
      ) : (
          <ListItem
            title={item.name}
            description={`${format.formatDate(item.date)}`}
            accessory={renderButton}
            icon={style => (
              <Icon
                name="attach"
                {...style}
                fill={theme['color-primary-600']}
                transform={[
                  {
                    rotate: '-45deg',
                  },
                ]}
              />
            )}
            onPress={async () => {
              let _item = item;
              if (item.data === null) {
                setIsLoading(true);
                _item = await getDocumentData(item);
                setIsLoading(false);
              }
              handleFile(_item);
            }}
          />
        )}
    </Animatable.View>
  );
};