/* eslint-disable global-require */
import React from 'react';
import { View, Image } from 'react-native';
import { ThemedComponentProps, withStyles, Text, Button, Layout } from 'react-native-ui-kitten';

// Types
import { User } from '../utils';

// Modules
import shared from '../../shared';
import { Styles } from '../helpers';

/**
 * component type
 */
type CardUserProps = ThemedComponentProps & {
  userInfo: User;
  onEditPress: () => void;
};

/**
 * render userInfo with edit button
 */
const CardUserElement = ({
  themedStyle,
  userInfo,
  onEditPress,
}: CardUserProps): React.ReactElement | null => {
  if (!userInfo) {
    return null;
  }
  /**
   * return React.ReactElement
   */
  return (
    <Layout style={[themedStyle.flex, themedStyle.card]}>
      <View style={[themedStyle.flexRow]}>
        <View style={[themedStyle.flex, themedStyle.ico]}>
          <Image style={themedStyle.img} source={require('../../../img/profile.png')} />
        </View>
        <View style={[themedStyle.flex, themedStyle.info]}>
          <View style={[themedStyle.flex]}>
            <Text style={themedStyle.pad} category="label" status="primary">
              {userInfo.firstName} {userInfo.lastName}
            </Text>
            <Text style={themedStyle.pad} category="metatext">
              {shared.helpers.format.formatPhone(userInfo.phoneNumber)}
            </Text>
            <Text style={themedStyle.pad} category="metatext">
              {userInfo.email}
            </Text>
          </View>
        </View>
        {/*
        <View style={[themedStyle.flex, themedStyle.button]}>
           <Button appearance="link" size="tiny" onPress={onEditPress}>
            EDIT
          </Button>
        </View>
          */}
      </View>
    </Layout>
  );
};

/**
 * create styles using theme
 * @see https://akveo.github.io/react-native-ui-kitten/docs/components/withstyles/overview#withstyles
 */
const CardUser = withStyles(CardUserElement, theme => ({
  ...Styles,
  card: {
    width: '80%',
    marginTop: 20,
    paddingHorizontal: 10,
    paddingVertical: 19,
    borderColor: theme['color-basic-400'],
    borderWidth: 1,
    alignSelf: 'center',
    elevation: 2,
    ...shared.helpers.shadow(2),
  },
  img: {
    width: 62,
    height: 62,
    marginLeft: 5,
  },
  pad: {
    paddingVertical: 4,
    flexWrap: 'nowrap',
  },
  ico: {
    flex: 0.3,
    alignItems: 'center',
  },
  info: {
    flex: 1,
    justifyContent: 'center',
    marginLeft: 10,
  },
  button: {
    flex: 0.3,
    alignItems: 'flex-start',
  },
}));

export default CardUser;
