import React from 'react';
import { View } from 'react-native';
import { withStyles, Layout, ThemedComponentProps, Text } from 'react-native-ui-kitten';

import { FormikProps } from 'formik';

import config from '../../../config';

// Types
import { ProfileInfoFormValues } from '../utils';

// Modules
import shared from '../../shared';

// Constants
import { forms } from '../constants';

const { formatPhone, formatMoney } = shared.helpers.format;

type ProfileInfoFormComponentProps = ThemedComponentProps & FormikProps<ProfileInfoFormValues>;

const ProfileInfoFormComponent = ({ themedStyle, values }: ProfileInfoFormComponentProps) => {
  return (
    <Layout style={themedStyle.formContainer}>
      <Layout style={themedStyle.input}>
        <Text status="warning" style={{ textAlign: 'center', marginVertical: 25 }}>
          {`If this information is not accurate,\nplease call us at ${formatPhone(
            config.contactPhone.slice(2) /* remove +1 */,
          )}.`}
        </Text>
        <Text status="info">{forms.profileInfo.labels.firstName}</Text>
        <Text category="h4" style={{ marginVertical: 15 }}>
          {values.firstName}
        </Text>
      </Layout>

      <Layout style={themedStyle.input}>
        <Text status="info">{forms.profileInfo.labels.lastName}</Text>
        <Text category="h4" style={{ marginVertical: 15 }}>
          {values.lastName}
        </Text>
      </Layout>

      <Layout style={themedStyle.input}>
        <Text status="info">{forms.profileInfo.labels.email}</Text>
        <Text category="h4" style={{ marginVertical: 15 }}>
          {values.email}
        </Text>
      </Layout>

      <Layout style={themedStyle.input}>
        <Text status="info">{forms.profileInfo.labels.phoneNumber}</Text>
        <Text category="h4" style={{ marginVertical: 15 }}>
          {formatPhone(values.phoneNumber)}
        </Text>
      </Layout>

      <Layout style={themedStyle.input}>
        <Text status="info">{forms.profileInfo.labels.street}</Text>
        <Text category="h4" style={{ marginVertical: 15 }}>
          {values.street}
        </Text>
      </Layout>

      <Layout style={themedStyle.input}>
        <Text status="info">{forms.profileInfo.labels.city}</Text>
        <Text category="h4" style={{ marginVertical: 15 }}>
          {values.city}
        </Text>
      </Layout>

      <Layout style={themedStyle.input}>
        <Text status="info">{forms.profileInfo.labels.state}</Text>
        <Text category="h4" style={{ marginVertical: 15 }}>
          {values.state}
        </Text>
      </Layout>

      <Layout style={themedStyle.input}>
        <Text status="info">{forms.profileInfo.labels.zipCode}</Text>
        <Text category="h4" style={{ marginVertical: 15 }}>
          {values.zipCode}
        </Text>
      </Layout>

      {/*
      // TODO: Validate field type
      */}
      <Layout style={themedStyle.input}>
        <Text status="info">{forms.profileInfo.labels.employmentType}</Text>
        <Text category="h4" style={{ marginVertical: 15 }}>
          {values.employmentType}
        </Text>
      </Layout>

      <Layout style={themedStyle.input}>
        <Text status="info">{forms.profileInfo.labels.employerName}</Text>
        <Text category="h4" style={{ marginVertical: 15 }}>
          {values.employerName}
        </Text>
      </Layout>

      <Layout style={themedStyle.input}>
        <Text status="info">{forms.profileInfo.labels.annualIncome}</Text>
        <Text category="h4" style={{ marginVertical: 15 }}>
          {formatMoney(values.annualIncome)}
        </Text>
      </Layout>

      {/* {!isSubmitting ? (
        <Button
          status="primary"
          size="giant"
          disabled={!isValid || isSubmitting}
          onPress={handleSubmit}
          textStyle={themedStyle.uppercase}
          {...shared.helpers.setTestID('ProfileInfoFormSubmitBtn')}
        >
          Update Information
        </Button>
      ) : (
        <View style={themedStyle.formLoadingContainer}>
          <Progress.CircleSnail
            style={themedStyle.formLoading}
            color={[
              theme['color-primary-500'],
              theme['color-warning-500'],
              theme['color-info-500'],
            ]}
          />
        </View>
      )} */}
    </Layout>
  );
};

const ProfileInfoForm = withStyles(ProfileInfoFormComponent, () => ({
  uppercase: {
    textTransform: 'uppercase',
  },
  input: {
    paddingBottom: 20,
  },
  formContainer: {
    paddingHorizontal: 30,
    paddingVertical: 15,
  },
  formTitle: {
    paddingVertical: 10,
  },
  label: {
    textAlign: 'center',
  },
  errorMsg: {
    marginTop: 4,
    flexDirection: 'row',
    alignItems: 'center',
  },
  formLoadingContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  formLoading: {
    marginVertical: 14,
  },
}));

export default ProfileInfoForm;
