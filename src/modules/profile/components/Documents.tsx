import React, { useState, useEffect } from 'react';
import { Image, ListRenderItemInfo, Platform, Alert } from 'react-native';
import * as Progress from 'react-native-progress';
import * as mime from 'react-native-mime-types';

import {
  ThemedComponentProps,
  withStyles,
  Text,
  List,
  ListItem,
  Icon,
  Layout,
} from 'react-native-ui-kitten';
import * as Animatable from 'react-native-animatable';
import RNFetchBlob from 'rn-fetch-blob';

// Types
import { DocumentType } from '../utils';

// Modules
import shared from '../../shared';

// Components
const { Dashed, Loading, HeaderView } = shared.components;

// Helpers
const { format } = shared.helpers;

/**
 * component type
 */
type DocumentProps = ThemedComponentProps & {
  documents: DocumentType[] | undefined;
  isLoading: boolean;
  getDocumentData: (document: DocumentType) => Promise<DocumentType>;
};

/**
 * render card status/valid/limit
 */
const DocumentsElement = ({
  themedStyle,
  theme,
  documents: _documents,
  isLoading,
  getDocumentData,
}: DocumentProps) => {
  const [isLoadingSingleDoc, setIsLoadingSingleDoc] = useState<number[]>([]);
  const [documents, setDocuments] = useState(_documents);
  /**
   * render button with image
   */
  useEffect(() => {
    const setNewDocs = (docs: DocumentType[]) => setDocuments(docs);
    if (_documents) {
      setNewDocs(_documents);
    }
  }, [_documents]);
  const renderButton = () => <Image source={{ uri: 'ico_plus' }} style={themedStyle.plus} />;
  const renderLoader = () => (
    <Progress.CircleSnail
      color={[theme['color-primary-500'], theme['color-warning-500'], theme['color-info-500']]}
    />
  );

  const handleFile = async (attachment: DocumentType) => {
    try {
      if (!attachment.data || !attachment.contentType) {
        Alert.alert('Oops', 'Document cannot be downloaded');
      }

      if (attachment.data && attachment.contentType) {
        const filePath = await shared.helpers.saveFile(
          `documents/${attachment.name}`,
          attachment.data,
        );

        if (Platform.OS === 'android') {
          const mimeType = mime.lookup(filePath) || attachment.contentType;
          RNFetchBlob.android.actionViewIntent(filePath, mimeType);
        } else {
          RNFetchBlob.ios.previewDocument(filePath);
        }
      }
    } catch (e) {
      Alert.alert('Oops', e.message);
    }
  };

  /**
   * render item
   */
  const renderItem = ({ item: _item, index }: ListRenderItemInfo<DocumentType>) => {
    const item = _item;
    // TODO make child and loader
    const handlePress = async () => {
      if (documents && item.data === null) {
        setIsLoadingSingleDoc([...isLoadingSingleDoc, index]);
        const newItem = await getDocumentData(item);
        setIsLoadingSingleDoc(isLoadingSingleDoc.filter(entry => entry !== index));
        setDocuments(documents.map(entry => (entry.id === newItem.id ? newItem : entry)));
        await handleFile(newItem);
        return;
      }
      await handleFile(item);
    };
    return (
      <Animatable.View
        animation="fadeInDown"
        duration={300}
        delay={(index && index * 150) || 150}
        useNativeDriver
      >
        <ListItem
          title={item.name}
          description={`${format.formatDate(item.date)}`}
          accessory={isLoadingSingleDoc.includes(index) ? renderLoader : renderButton}
          icon={style => (
            <Icon
              name="attach"
              {...style}
              fill={theme['color-primary-600']}
              transform={[
                {
                  rotate: '-45deg',
                },
              ]}
            />
          )}
          onPress={handlePress}
        />
      </Animatable.View>
    );
  };

  return (
    <Layout>
      <HeaderView title="Account Documents" />

      {isLoading ? (
        <Loading isLoading={isLoading} />
      ) : (
        <List
          scrollEnabled={false}
          showsVerticalScrollIndicator={false}
          // TODO fix types
          data={documents}
          renderItem={renderItem}
          ListHeaderComponent={() => (
            <>
              <Dashed
                color={theme['color-primary-700']}
                customStyle={themedStyle.headerSeparator}
              />
            </>
          )}
          ListEmptyComponent={
            <Layout style={themedStyle.emptyListContainer}>
              <Text style={{ color: theme['color-primary-600'] }}>
                You don't have any document.
              </Text>
            </Layout>
          }
          ItemSeparatorComponent={() => <Dashed />}
          style={themedStyle.container}
        />
      )}
    </Layout>
  );
};

const ProfileSecurity = withStyles(DocumentsElement, () => ({
  container: {
    marginVertical: 20,
    marginHorizontal: 30,
  },
  headerContainer: {
    alignItems: 'center',
  },
  headerSeparator: {
    marginTop: 5,
  },
  plus: {
    width: 44,
    height: 44,
  },
  emptyListContainer: { flex: 1, alignItems: 'center', marginVertical: 50 },
}));

export default ProfileSecurity;
