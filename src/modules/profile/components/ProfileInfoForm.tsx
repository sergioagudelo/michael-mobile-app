import React from 'react';
import { View } from 'react-native';
import {
  withStyles,
  Layout,
  Input,
  Button,
  ThemedComponentProps,
  Icon,
  StyleType,
  ThemeType,
  Select,
  Text,
} from 'react-native-ui-kitten';
import * as Progress from 'react-native-progress';
import { TextInputMask } from 'react-native-masked-text';
import { FormikProps } from 'formik';
import { InputComponent } from 'react-native-ui-kitten/ui/input/input.component';

import config from '../../../config';

// Types
import { ProfileInfoFormValues } from '../utils';

// Modules
import shared from '../../shared';

// Constants
import { forms } from '../constants';

const { formatPhone } = shared.helpers.format;

// Components

const renderEditIcon = (theme: ThemeType) => (style?: StyleType) => (
  <Icon name="edit-2-outline" fill={theme['color-primary-500']} {...style} />
);

const renderCancelIcon = (theme: ThemeType) => (style?: StyleType) => (
  <Icon name="close-circle-outline" fill={theme['color-secondary-500']} {...style} />
);

type ProfileInfoFormComponentProps = ThemedComponentProps & FormikProps<ProfileInfoFormValues>;

const ProfileInfoFormComponent = ({
  theme,
  themedStyle,
  values,
  touched,
  errors,
  handleBlur,
  handleChange,
  isValid,
  isSubmitting,
  handleSubmit,
}: ProfileInfoFormComponentProps) => {
  const lastNameInputRef = React.useRef<InputComponent>(null);
  const emailInputRef = React.useRef<InputComponent>(null);
  const phoneNumberInputRef = React.useRef<InputComponent>(null);
  const streetInputRef = React.useRef<InputComponent>(null);
  const cityInputRef = React.useRef<InputComponent>(null);
  const stateInputRef = React.useRef<InputComponent>(null);
  const zipCodeInputRef = React.useRef<InputComponent>(null);
  const employmentTypeInputRef = React.useRef<InputComponent>(null);
  const employerNameInputRef = React.useRef<InputComponent>(null);
  const annualIncomeInputRef = React.useRef<InputComponent>(null);

  const [editEmail, setEditEmail] = React.useState(false);
  const [editPhoneNumber, setEditPhoneNumber] = React.useState(false);

  return (
    <Layout style={themedStyle.formContainer}>
      <Layout style={themedStyle.input}>
        <Text status="warning" style={{ textAlign: 'center', marginVertical: 25 }}>
          {`If this information is not accurate,\nplease call us at ${formatPhone(
            config.contactPhone.slice(2) /* remove +1 */,
          )}.`}
        </Text>
        <Input
          status={shared.helpers.getInputStatus<ProfileInfoFormValues>('firstName', {
            touched,
            errors,
          })}
          label={forms.profileInfo.labels.firstName}
          placeholder={forms.profileInfo.placeholders.firstName}
          caption={touched.firstName && errors.firstName ? errors.firstName : ''}
          disabled
          editable={false}
          value={values.firstName}
          onBlur={handleBlur('firstName')}
          onChangeText={handleChange('firstName')}
          onSubmitEditing={() => {
            if (lastNameInputRef && lastNameInputRef.current) {
              lastNameInputRef.current.focus();
            }
          }}
          autoCapitalize="words"
          textContentType="givenName"
          autoCompleteType="name"
          returnKeyType="next"
        />
      </Layout>

      <Layout style={themedStyle.input}>
        <Input
          ref={lastNameInputRef}
          status={shared.helpers.getInputStatus<ProfileInfoFormValues>('lastName', {
            touched,
            errors,
          })}
          label={forms.profileInfo.labels.lastName}
          placeholder={forms.profileInfo.placeholders.lastName}
          caption={touched.lastName && errors.lastName ? errors.lastName : ''}
          disabled
          editable={false}
          value={values.lastName}
          onBlur={handleBlur('lastName')}
          onChangeText={handleChange('lastName')}
          onSubmitEditing={() => {
            if (emailInputRef && emailInputRef.current) {
              emailInputRef.current.focus();
            }
          }}
          autoCapitalize="words"
          textContentType="familyName"
          autoCompleteType="name"
          returnKeyType="next"
        />
      </Layout>

      <Layout style={themedStyle.input}>
        <Input
          ref={emailInputRef}
          status={shared.helpers.getInputStatus<ProfileInfoFormValues>('email', {
            touched,
            errors,
          })}
          label={forms.profileInfo.labels.email}
          placeholder={forms.profileInfo.placeholders.email}
          caption={touched.email && errors.email ? errors.email : ''}
          // icon={!editEmail ? renderEditIcon(theme) : renderCancelIcon(theme)}
          // onIconPress={() => setEditEmail(!editEmail)}
          disabled={!editEmail}
          editable={false}
          value={values.email}
          onBlur={handleBlur('email')}
          onChangeText={handleChange('email')}
          onSubmitEditing={() => {
            if (phoneNumberInputRef && phoneNumberInputRef.current) {
              phoneNumberInputRef.current.focus();
            }
          }}
          autoCapitalize="none"
          keyboardType="email-address"
          textContentType="username"
          autoCompleteType="username"
          returnKeyType="next"
        />
      </Layout>

      <Layout style={themedStyle.input}>
        <TextInputMask
          type="custom"
          options={{
            mask: '(999) 999-9999',
          }}
          customTextInput={Input}
          customTextInputProps={{
            ref: phoneNumberInputRef,
            status: shared.helpers.getInputStatus<ProfileInfoFormValues>('phoneNumber', {
              touched,
              errors,
            }),
            disabled: !editPhoneNumber,
            editable: editPhoneNumber,
            label: forms.profileInfo.labels.phoneNumber,
            placeholder: forms.profileInfo.placeholders.phoneNumber,
            caption: touched.phoneNumber && errors.phoneNumber ? errors.phoneNumber : '',
            // icon: !editPhoneNumber ? renderEditIcon(theme) : renderCancelIcon(theme),
            // onIconPress: () => setEditPhoneNumber(!editPhoneNumber),
          }}
          value={values.phoneNumber}
          onBlur={handleBlur('phoneNumber')}
          onChangeText={handleChange('phoneNumber')}
          onSubmitEditing={() => {
            if (streetInputRef && streetInputRef.current) {
              streetInputRef.current.focus();
            }
          }}
          textContentType="telephoneNumber"
          autoCompleteType="tel"
          returnKeyType="done"
          keyboardType="phone-pad"
        />
      </Layout>

      <Layout style={themedStyle.input}>
        <Input
          ref={streetInputRef}
          status={shared.helpers.getInputStatus<ProfileInfoFormValues>('street', {
            errors,
            touched,
          })}
          label={forms.profileInfo.labels.street}
          placeholder={forms.profileInfo.placeholders.street}
          caption={touched.street && errors.street ? errors.street : ''}
          disabled
          editable={false}
          value={values.street}
          onBlur={handleBlur('street')}
          onChangeText={handleChange('street')}
          onSubmitEditing={() => {
            if (cityInputRef && cityInputRef.current) {
              cityInputRef.current.focus();
            }
          }}
          textContentType="streetstreet"
          autoCompleteType="street-address"
          returnKeyType="next"
        />
      </Layout>

      <Layout style={themedStyle.input}>
        <Input
          ref={cityInputRef}
          status={shared.helpers.getInputStatus<ProfileInfoFormValues>('city', {
            errors,
            touched,
          })}
          label={forms.profileInfo.labels.city}
          placeholder={forms.profileInfo.placeholders.city}
          caption={touched.city && errors.city ? errors.city : ''}
          disabled
          editable={false}
          value={values.city}
          onBlur={handleBlur('city')}
          onChangeText={handleChange('city')}
          onSubmitEditing={() => {
            if (stateInputRef && stateInputRef.current) {
              stateInputRef.current.focus();
            }
          }}
          textContentType="addressCity"
          autoCompleteType="street-address"
          returnKeyType="next"
        />
      </Layout>

      <Layout style={themedStyle.input}>
        <Select
          disabled
          status={shared.helpers.getInputStatus<ProfileInfoFormValues>('state', {
            errors,
            touched,
          })}
          data={shared.constants.stateValues.map(state => ({ text: state }))}
          selectedOption={{ text: values.state }}
          label={forms.profileInfo.labels.state}
          placeholder={forms.profileInfo.placeholders.state}
          onSelect={option => handleChange('state')(option.text)}
          onPress={() => handleBlur('state')}
        />
        {touched.state && errors.state && (
          <View style={themedStyle.errorMsg}>
            <Text category="c2" status="danger">
              errors.state
            </Text>
          </View>
        )}
      </Layout>

      <Layout style={themedStyle.input}>
        <TextInputMask
          type="zip-code"
          customTextInput={Input}
          customTextInputProps={{
            ref: zipCodeInputRef,
            status: shared.helpers.getInputStatus<ProfileInfoFormValues>('zipCode', {
              errors,
              touched,
            }),
            label: forms.profileInfo.labels.zipCode,
            placeholder: forms.profileInfo.placeholders.zipCode,
            caption: touched.zipCode && errors.zipCode ? errors.zipCode : '',
            disabled: true,
            editable: false,
          }}
          value={values.zipCode}
          onBlur={handleBlur('zipCode')}
          onChangeText={handleChange('zipCode')}
          onSubmitEditing={() => {
            if (employmentTypeInputRef && employmentTypeInputRef.current) {
              employmentTypeInputRef.current.focus();
            }
          }}
          textContentType="postalCode"
          autoCompleteType="postal-code"
          returnKeyType="done"
        />
      </Layout>

      {/*
      // TODO: Validate field type
      */}
      <Layout style={themedStyle.input}>
        <Input
          ref={employmentTypeInputRef}
          status={shared.helpers.getInputStatus<ProfileInfoFormValues>('employmentType', {
            errors,
            touched,
          })}
          label={forms.profileInfo.labels.employmentType}
          placeholder={forms.profileInfo.placeholders.employmentType}
          caption={touched.employmentType && errors.employmentType ? errors.employmentType : ''}
          disabled
          editable={false}
          value={values.employmentType}
          onBlur={handleBlur('employmentType')}
          onChangeText={handleChange('employmentType')}
          onSubmitEditing={() => {
            if (zipCodeInputRef && zipCodeInputRef.current) {
              zipCodeInputRef.current.focus();
            }
          }}
          textContentType="none"
          returnKeyType="next"
        />
      </Layout>

      <Layout style={themedStyle.input}>
        <Input
          ref={employerNameInputRef}
          status={shared.helpers.getInputStatus<ProfileInfoFormValues>('employerName', {
            errors,
            touched,
          })}
          label={forms.profileInfo.labels.employerName}
          placeholder={forms.profileInfo.placeholders.employerName}
          caption={touched.employerName && errors.employerName ? errors.employerName : ''}
          disabled
          editable={false}
          value={values.employerName}
          onBlur={handleBlur('employerName')}
          onChangeText={handleChange('employerName')}
          onSubmitEditing={() => {
            if (zipCodeInputRef && zipCodeInputRef.current) {
              zipCodeInputRef.current.focus();
            }
          }}
          textContentType="organizationName"
          returnKeyType="next"
        />
      </Layout>

      <Layout style={themedStyle.input}>
        <TextInputMask
          type="money"
          options={{
            precision: 0,
            separator: '.',
            delimiter: ',',
            unit: '$',
          }}
          includeRawValueInChangeText
          customTextInput={Input}
          customTextInputProps={{
            ref: annualIncomeInputRef,
            status: shared.helpers.getInputStatus<ProfileInfoFormValues>('annualIncome', {
              errors,
              touched,
            }),
            label: forms.profileInfo.labels.annualIncome,
            placeholder: forms.profileInfo.placeholders.annualIncome,
            caption: touched.annualIncome && errors.annualIncome ? errors.annualIncome : '',
            disabled: true,
            editable: false,
          }}
          value={`${values.annualIncome}`}
          onBlur={handleBlur('annualIncome')}
          onChangeText={(_, val) => handleChange('annualIncome')(val)}
          textContentType="none"
          returnKeyType="done"
        />
      </Layout>

      {/* {!isSubmitting ? (
        <Button
          status="primary"
          size="giant"
          disabled={!isValid || isSubmitting}
          onPress={handleSubmit}
          textStyle={themedStyle.uppercase}
          {...shared.helpers.setTestID('ProfileInfoFormSubmitBtn')}
        >
          Update Information
        </Button>
      ) : (
        <View style={themedStyle.formLoadingContainer}>
          <Progress.CircleSnail
            style={themedStyle.formLoading}
            color={[
              theme['color-primary-500'],
              theme['color-warning-500'],
              theme['color-info-500'],
            ]}
          />
        </View>
      )} */}
    </Layout>
  );
};

const ProfileInfoForm = withStyles(ProfileInfoFormComponent, () => ({
  uppercase: {
    textTransform: 'uppercase',
  },
  input: {
    paddingBottom: 20,
  },
  formContainer: {
    paddingHorizontal: 30,
    paddingVertical: 15,
  },
  formTitle: {
    paddingVertical: 10,
  },
  label: {
    textAlign: 'center',
  },
  errorMsg: {
    marginTop: 4,
    flexDirection: 'row',
    alignItems: 'center',
  },
  formLoadingContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  formLoading: {
    marginVertical: 14,
  },
}));

export default ProfileInfoForm;
