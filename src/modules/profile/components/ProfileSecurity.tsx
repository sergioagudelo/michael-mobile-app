import React, { useState, useEffect } from 'react';
import { View } from 'react-native';
import {
  ThemedComponentProps,
  withStyles,
  Text,
  Toggle,
  Layout,
  Button,
  Icon,
} from 'react-native-ui-kitten';
import * as Animatable from 'react-native-animatable';

// Modules
import { BIOMETRY_TYPE } from 'react-native-keychain';
import shared from '../../shared';
import auth from '../../auth';

// Helpers
import { Styles } from '../helpers';

// Components
const { keychain } = auth.services;
const { Dashed, Loading } = shared.components;

/**
 * component type
 */
type ProfileSecurityElementProps = ThemedComponentProps & {
  isBiometricAuthSupported: boolean;
  isLoading: boolean;
  device: boolean;
  faceId: boolean;
  notifications: boolean;
  onChangeToggle: (checked: boolean, type: 'device' | 'faceId' | 'notifications') => void;
};

/**
 * render card status/valid/limit
 */
const ProfileSecurityElement = ({
  theme,
  themedStyle,
  isLoading,
  onChangeToggle,
  faceId,
  notifications,
  isBiometricAuthSupported,
}: ProfileSecurityElementProps): React.ReactElement => {
  const [biometryType, setBiometryType] = useState<BIOMETRY_TYPE | null>();

  useEffect(() => {
    const getBiometryType = async () => {
      if (!biometryType) {
        const type = await keychain.getSupportedBiometryType();
        setBiometryType(type);
      }
    };
    if (isBiometricAuthSupported && !biometryType) {
      getBiometryType();
    }
  }, [biometryType, isBiometricAuthSupported]);
  /**
   * render text ACTIVE|DISABLED and style
   * @param isActive boolean
   */
  const renderTextBehavior = (isActive: boolean) => {
    const behave = {
      category: isActive ? 'p2' : 'label',
      attr: isActive ? { status: 'info' } : { appearance: 'hint' },
      txt: isActive ? 'ACTIVE' : 'DISABLED',
    };
    return (
      <Text category={behave.category} {...behave.attr}>
        {behave.txt}
      </Text>
    );
  };

  /**
   * return React.ReactElement
   */
  if (isLoading) {
    return (
      <Layout style={[themedStyle.flex, themedStyle.card, themedStyle.center]}>
        <Loading isLoading={isLoading} />
      </Layout>
    );
  }
  return (
    <Animatable.View animation="fadeIn" duration={800} useNativeDriver>
      <Layout style={[themedStyle.flex, themedStyle.card]}>
        {/* <View style={[themedStyle.flex, themedStyle.pad]}>
          <View style={[themedStyle.flexRow, themedStyle.row]}>
            <View style={[themedStyle.flex, themedStyle.label]}>
              <Text category="label" appearance="dark">
                ACCOUNT LINKED DEVICE
              </Text>
              {renderTextBehavior(device)}
            </View>
            <View style={[themedStyle.flex]}>
              <Toggle
                onChange={checked => onChangeToggle(checked, 'device')}
                appearance="default"
                status="basic"
                checked={device}
              />
            </View>
          </View>
          <Dashed />
        </View> */}
        {isBiometricAuthSupported && (
          <View style={[themedStyle.flex, themedStyle.pad]}>
            <>
              <View style={[themedStyle.flexRow, themedStyle.row]}>
                <View style={[themedStyle.flex, themedStyle.label]}>
                  <Text category="label" appearance="dark">
                    {`ENABLE ${biometryType &&
                      keychain.handleBiometryType[biometryType]().toUpperCase()}`}
                  </Text>
                  {renderTextBehavior(faceId)}
                </View>
                <View style={[themedStyle.flex]}>
                  <Toggle
                    onChange={checked => onChangeToggle(checked, 'faceId')}
                    appearance="default"
                    status="basic"
                    checked={faceId}
                  />
                </View>
              </View>
            </>
          </View>
        )}
        <View style={[themedStyle.flex, themedStyle.pad]}>
          <View style={[themedStyle.flexRow]}>
            <Dashed />

            <View style={[themedStyle.flex]}>
              <Button
                appearance="outline"
                status="basic"
                textStyle={themedStyle.uppercase}
                onPress={() => onChangeToggle(false, 'notifications')}
              >
                CHANGE PASSWORD
              </Button>
            </View>
          </View>
        </View>
      </Layout>
    </Animatable.View>
  );
};

/**
 * create styles using theme
 * @see https://akveo.github.io/react-native-ui-kitten/docs/components/withstyles/overview#withstyles
 */
const ProfileSecurity = withStyles(ProfileSecurityElement, theme => ({
  ...Styles,
  card: {
    width: '80%',
    marginTop: 20,
    minHeight: 250,
    paddingHorizontal: 10,
    paddingVertical: 19,
    borderColor: theme['color-basic-400'],
    borderWidth: 1,
    alignSelf: 'center',
    elevation: 2,
    ...shared.helpers.shadow(2),
  },
  row: {
    alignItems: 'center',
    marginBottom: 20,
  },
  label: {
    flex: 2,
  },
  pad: {
    padding: 10,
  },
}));

export default ProfileSecurity;
