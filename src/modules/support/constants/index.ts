import * as Yup from 'yup';

export const NAME = 'support';

export const forms = {
  emailComposer: {
    initialValues: {
      subject: '',
      loan: '',
      message: '',
    },
    labels: {
      subject: '',
      loan: '',
      message: '',
    },
    placeholders: {
      subject: 'Subject',
      loan: 'Select loan',
      message: 'Write your message',
    },
    schema: (ctx: { loansIds: string[] }) =>
      Yup.object({
        subject: Yup.string()
          .required()
          .ensure()
          .trim()
          .lowercase(),
        loan: Yup.string()
          // .oneOf(ctx.loansIds)
          .required(),
        message: Yup.string()
          .required()
          .ensure()
          .trim(),
      }),
  },
};
