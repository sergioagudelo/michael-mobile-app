import { withFormik } from 'formik';

// Types
import { EmailComposerFormValues } from '../utils';
import { Loan } from '../../loans/utils';

// Constants
import { forms } from '../constants';

// Component
import EmailComposerForm from '../components/EmailComposerForm';
import { NotificationType } from '../../notifications/utils';

type EmailComposerFormContainerProps = {
  notification: NotificationType;
  loans: Loan['lai'][];
  onSubmit: (emailFormValues: EmailComposerFormValues) => boolean;
  onCancel: () => void;
  initialEmail?: {
    subject?: string;
    loan?: Loan['lai'];
  };
};

export default withFormik<EmailComposerFormContainerProps, EmailComposerFormValues>({
  mapPropsToValues: (props: EmailComposerFormContainerProps) => {
    const { initialEmail } = props;
    return {
      subject:
        initialEmail && initialEmail.subject
          ? initialEmail.subject
          : forms.emailComposer.initialValues.subject,
      loan:
        initialEmail && initialEmail.loan
          ? initialEmail.loan
          : forms.emailComposer.initialValues.loan,
      message: forms.emailComposer.initialValues.message,
    };
  },
  isInitialValid: props =>
    forms.emailComposer.schema({ loansIds: props.loans }).isValidSync(props.initialValues),
  validationSchema: (props: EmailComposerFormContainerProps) =>
    forms.emailComposer.schema({ loansIds: props.loans }),
  handleSubmit: async (values, { setSubmitting, setStatus, props }) => {
    setSubmitting(true);
    const success = await props.onSubmit(values);
    setSubmitting(false);
    if (success) {
      return success;
    }
    setStatus({
      success: false,
      error: 'An error ocurred. Please try again',
    });
  },
})(EmailComposerForm);
