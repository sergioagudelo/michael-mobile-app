import React, { useEffect, useState } from 'react';

import { getFaqs } from '../services/helpCenter';

import { FAQ } from '../components';

import shared from '../../shared';

const { Loading } = shared.components;

const FaqContainer = () => {
  const [data, setData] = useState<any | undefined>();
  const [isLoading, setIsLoading] = useState(false);
  const [failed, setFailed] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);
      const result = await getFaqs(); // service
      setIsLoading(false);
      if (result.success && result.response) {
        const { data: _data } = result.response;
        const parsed = _data.map(entry => ({
          ...entry,
          content: {
            rendered: entry.content.rendered.toString().replace(/\\n/g, '\n'),
          },
        }));
        setData(parsed);
        return;
      }
      setFailed(true);
    };
    if (data === undefined && !isLoading && !failed) {
      fetchData();
    }
  }, [data, isLoading, failed]);

  if (failed) {
    // navigation.navigate // TODO: General error
  }

  if (data) {
    return (
      <>
        <FAQ faq={data} />
      </>
    );
  }

  return <Loading isLoading={isLoading} />;
};

export default FaqContainer;
