import React from 'react';
import { View, Keyboard } from 'react-native';
import {
  withStyles,
  Layout,
  Input,
  Button,
  ThemedComponentProps,
  Icon,
  Select,
  Text,
} from 'react-native-ui-kitten';
import * as Progress from 'react-native-progress';
import { FormikProps } from 'formik';
import color from 'color';
import { scale } from 'react-native-size-matters';

// Types
import { EmailComposerFormValues } from '../utils';
import { Loan } from '../../loans/utils';

// Modules
import shared from '../../shared';

// Constants
import { forms } from '../constants';

export type EmailComposerFormProps = {
  initialEmail?: {
    subject?: string;
    loan?: Loan['lai'];
  };
  loans: Loan['lai'][];
  onSubmit: () => void;
  onCancel: () => void;
} & FormikProps<EmailComposerFormValues> &
  ThemedComponentProps;

const EmailComposerForm = ({
  initialEmail,
  values,
  touched,
  errors,
  handleBlur,
  handleChange,
  handleSubmit,
  isValid,
  isSubmitting,
  loans,
  onCancel,
  theme,
  themedStyle,
}: EmailComposerFormProps) => {
  const selectedLoan =
    initialEmail && initialEmail.loan !== '' ? { text: initialEmail.loan } : undefined;
  return (
    <>
      <Layout style={themedStyle.input}>
        <Input
          status={shared.helpers.getInputStatus<EmailComposerFormValues>('subject', {
            touched,
            errors,
          })}
          value={values.subject}
          onBlur={handleBlur('subject')}
          onChangeText={handleChange('subject')}
          label={forms.emailComposer.labels.subject}
          placeholder={forms.emailComposer.placeholders.subject}
          caption={touched.subject && errors.subject ? errors.subject : ''}
          disabled={!!initialEmail?.subject}
          returnKeyType="next"
          enablesReturnKeyAutomatically
        />
      </Layout>

      <Layout style={themedStyle.input}>
        <Select
          status={shared.helpers.getInputStatus<EmailComposerFormValues>('loan', {
            errors,
            touched,
          })}
          data={loans.map(loan => ({ text: loan }))}
          selectedOption={selectedLoan}
          label={forms.emailComposer.labels.loan}
          placeholder={forms.emailComposer.placeholders.loan}
          onSelect={option => handleChange('loan')(option.text)}
          disabled={!!values.loan}
          onPress={() => {
            Keyboard.dismiss();
            handleBlur('loan');
          }}
        />
        {touched.loan && errors.loan && (
          <View style={themedStyle.errorMsg}>
            <Text category="c2" status="danger">
              {errors.loan}
            </Text>
          </View>
        )}
      </Layout>

      <Layout style={themedStyle.input}>
        <Input
          status={shared.helpers.getInputStatus<EmailComposerFormValues>('message', {
            touched,
            errors,
          })}
          value={values.message}
          onBlur={handleBlur('message')}
          onChangeText={handleChange('message')}
          multiline
          numberOfLines={4}
          label={forms.emailComposer.labels.message}
          placeholder={forms.emailComposer.placeholders.message}
          caption={touched.message && errors.message ? errors.message : ''}
          style={themedStyle.textAreaInputContainer}
          textStyle={themedStyle.textAreaInput}
          size="large"
          returnKeyType="done"
          enablesReturnKeyAutomatically
          onSubmitEditing={() => Keyboard.dismiss()}
        />
      </Layout>

      <Layout style={themedStyle.formActions}>
        <Button
          status="basic"
          onPress={onCancel}
          textStyle={themedStyle.uppercase}
          size="small"
          {...shared.helpers.setTestID('EmailComposerFormCancelBtn')}
        >
          Cancel
        </Button>
        {!isSubmitting ? (
          <Button
            icon={style => <Icon name="paper-plane" {...style} />}
            disabled={isSubmitting}
            onPress={handleSubmit}
            textStyle={themedStyle.uppercase}
            size="small"
            {...shared.helpers.setTestID('EmailComposerFormSubmitBtn')}
            style={themedStyle.submitButton}
          >
            Send
          </Button>
        ) : (
          <View style={themedStyle.formLoadingContainer}>
            <Progress.CircleSnail
              style={themedStyle.formLoading}
              color={[
                theme['color-primary-500'],
                theme['color-warning-500'],
                theme['color-info-500'],
              ]}
            />
          </View>
        )}
      </Layout>
    </>
  );
};

export default withStyles(EmailComposerForm, theme => ({
  uppercase: {
    textTransform: 'uppercase',
  },
  input: {
    paddingBottom: 20,
  },
  backdrop: {
    position: 'absolute',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: color(theme['color-basic-900'])
      .alpha(0.7)
      .rgb()
      .string(),
  },
  title: {
    paddingVertical: 15,
  },
  center: {
    textAlign: 'center',
  },
  body: {
    paddingVertical: 15,
    paddingBottom: 30,
  },
  textAreaInputContainer: {
    maxHeight: 200,
    minWidth: scale(200),
  },
  textAreaInput: {
    alignSelf: 'flex-start',
  },
  formActions: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  submitButton: {
    flexDirection: 'row-reverse',
  },
  formLoadingContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  formLoading: {
    marginVertical: 8,
  },
}));
