import React, { useState } from 'react';
import { Platform, TouchableOpacity, Image } from 'react-native';
import { Text, withStyles, ThemedComponentProps, Layout, Modal } from 'react-native-ui-kitten';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Color from 'color';

// store
import { connect } from 'react-redux';
import { RootState } from 'typesafe-actions';
import * as store from '../../loans/store';

// Types
import { Loan } from '../../loans/utils';

// Modules
import shared from '../../shared';

// Components
import EmailComposerForm from '../containers/EmailComposerForm';
import { EmailComposerFormValues } from '../utils';
import { NotificationType } from '../../notifications/utils';

const { Dashed } = shared.components;

const mapStateToProps = (state: RootState) => ({
  loansList: store.selectors.getLoans(state),
});

type EmailUsProps = ThemedComponentProps & {
  loansList: Loan[];
  notification: NotificationType | null;
  visible: boolean;
  onDismiss: () => void;
  onSubmit: (
    notificationIdOrOverdueDays: number,
    EmailFormValues: EmailComposerFormValues,
    contractId: string,
  ) => Promise<boolean>;
} & ReturnType<typeof mapStateToProps>;

const EmailUs = ({
  visible,
  onDismiss,
  onSubmit,
  notification,
  theme,
  themedStyle,
  loansList,
}: EmailUsProps) => {
  const [showConfirmationModal, setShowConfirmationModal] = useState(false);
  const loanLai = notification ? notification.subject.match(/(LAI-).[0-9]*/) : null;
  const initialEmail = notification
    ? {
        subject: `RE: ${notification.subject}`,
        loan: loanLai ? loanLai[0] : '',
      }
    : { subject: '', loan: loansList && loansList.length === 1 ? loansList[0].lai : '' };

  const dismissConfirmation = () => setShowConfirmationModal(false);
  const getOverDueDays = (lai: string) => {
    const loan = loansList.find(entry => entry.lai === lai);
    return loan ? loan.overDueDays : 0;
  };
  const handleSubmit = async (EmailFormValues: EmailComposerFormValues) => {
    const targetLoan = loansList.find(entry => entry.lai === EmailFormValues.loan);
    const success = await onSubmit(
      notification ? notification.id : getOverDueDays(EmailFormValues.loan),
      EmailFormValues,
      targetLoan ? targetLoan.loanId : '',
    );
    if (success) {
      setShowConfirmationModal(true);
    }
    return success;
    // TODO navigate to error
  };
  return (
    <>
      <Modal
        allowBackdrop
        onBackdropPress={dismissConfirmation}
        backdropStyle={themedStyle.backdrop}
        visible={showConfirmationModal}
      >
        <Layout style={themedStyle.modalContainer}>
          <Layout style={themedStyle.confirmationTitle}>
            <Text category="p1" style={themedStyle.centerText}>
              We have received your message. We will be in touch soon.
            </Text>
          </Layout>
          <TouchableOpacity
            onPress={dismissConfirmation}
            style={themedStyle.confirmationButton}
            {...shared.helpers.setTestID('ForgotPasswordConfirmBtn')}
          >
            <Image source={{ uri: 'ico_check' }} style={themedStyle.check} />
          </TouchableOpacity>
        </Layout>
      </Modal>
      <Modal
        allowBackdrop
        onBackdropPress={() => {}}
        backdropStyle={themedStyle.backdrop}
        visible={visible}
      >
        <Layout style={themedStyle.modalContainer}>
          <KeyboardAwareScrollView
            enableOnAndroid
            keyboardShouldPersistTaps={Platform.select({
              ios: 'never',
              android: 'handled',
            })}
          >
            <Text category="h3" status="accent" style={themedStyle.centerText}>
              New message to LendingPoint
            </Text>

            <Dashed color={theme['color-primary-600']} customStyle={themedStyle.separator} />

            {loansList && (
              <EmailComposerForm
                onCancel={onDismiss}
                initialEmail={initialEmail}
                notification={notification}
                loans={loansList.map((loan: Loan) => loan.lai)}
                onSubmit={handleSubmit}
              />
            )}
          </KeyboardAwareScrollView>
        </Layout>
      </Modal>
    </>
  );
};

export default connect(mapStateToProps)(
  withStyles(EmailUs, theme => ({
    centerText: {
      textAlign: 'center',
    },
    uppercase: {
      textTransform: 'uppercase',
    },
    backdrop: {
      position: 'absolute',
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: Color(theme['color-basic-900'])
        .alpha(0.7)
        .rgb()
        .string(),
    },
    modalContainer: {
      margin: 30,
      padding: 30,
      borderRadius: 10,
    },
    confirmationTitle: {
      paddingVertical: 15,
    },
    confirmationButton: {
      alignSelf: 'center',
    },
    check: {
      width: 50,
      height: 50,
      tintColor: theme['color-success-600'],
    },
    separator: {
      marginVertical: 20,
    },
  })),
);
