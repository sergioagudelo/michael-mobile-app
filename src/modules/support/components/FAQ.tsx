import React, { useState } from 'react';
import { Image, View, TouchableOpacity } from 'react-native';
import { Layout, Text, withStyles, ThemedComponentProps, ListItem } from 'react-native-ui-kitten';
import Accordion from 'react-native-collapsible/Accordion';
import HTMLView from 'react-native-htmlview';

// Types
import { FAQType } from '../utils';

// Modules
import shared from '../../shared';

// Components
const { Dashed } = shared.components;

type FAQProps = { faq: FAQType[] } & ThemedComponentProps;

const FAQ = ({ faq, theme, themedStyle }: FAQProps) => {
  const [activeSections, setActiveSections] = useState<number[]>([]);

  const renderHeader = (section: FAQType, _: number, isActive: boolean) => {
    return (
      <>
        <ListItem
          title={section.title.rendered}
          icon={() => (
            <View>
              <Image source={require('../../../img/icons/ico-faq.png')} />
            </View>
          )}
          accessory={style => (
            <View style={style}>
              <Image
                source={
                  isActive
                    ? require('../../../img/icons/ico-chevron-up.png')
                    : require('../../../img/icons/ico-chevron-down.png')
                }
              />
            </View>
          )}
          disabled
          titleStyle={{
            color: theme['color-basic-1000'],
          }}
        />
        <Dashed />
      </>
    );
  };

  const renderContent = (section: FAQType) => {
    return (
      <HTMLView
        addLineBreaks
        value={section.content.rendered}
        stylesheet={themedStyle.faqContent}
        TextComponent={Text}
      />
    );
  };

  return (
    <Layout>
      <Text category="h2" status="accent" style={[themedStyle.centerText, themedStyle.faqTitle]}>
        Frequently Asked Questions
      </Text>

      <Text category="p2" style={[themedStyle.centerText, themedStyle.faqSubtitle]}>
        Important LendingPoint customer questions and information
      </Text>

      <Dashed color={theme['color-primary-600']} customStyle={themedStyle.separator} />

      <Accordion
        sections={faq}
        activeSections={activeSections}
        renderHeader={renderHeader}
        renderContent={renderContent}
        onChange={setActiveSections}
        touchableComponent={TouchableOpacity}
        touchableProps={{ activeOpacity: 1.0 }}
      />
    </Layout>
  );
};

export default withStyles(FAQ, theme => ({
  centerText: {
    textAlign: 'center',
  },
  uppercase: {
    textTransform: 'uppercase',
  },
  separator: {
    marginVertical: 10,
  },
  faqTitle: {
    marginBottom: 10,
  },
  faqSubtitle: {
    marginVertical: 10,
  },
  faqContent: {
    div: {
      paddingTop: 10,
      fontFamily: 'FiraSans-Regular',
      fontSize: 13,
      lineHeight: 15,
      fontWeight: '400',
      letterSpacing: 0.3,
      color: theme['color-primary-900'],
    },
    p: {
      fontFamily: 'FiraSans-Regular',
      fontSize: 13,
      lineHeight: 15,
      fontWeight: '400',
      letterSpacing: 0.3,
      color: theme['color-primary-900'],
    },
  },
}));
