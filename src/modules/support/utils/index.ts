export type EmailComposerFormValues = {
  subject: string;
  loan: string;
  message: string;
};

export type FAQType = {
  id: number;
  date: string;
  title: {
    rendered: string;
  };
  excerpt: {
    rendered: string;
  };
  content: {
    rendered: string;
  };
};
