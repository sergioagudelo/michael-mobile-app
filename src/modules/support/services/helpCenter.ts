import { wordpress } from '../../../services';
import { ApiResponse } from '../../../services/utils';
import { handleError } from '../../../utils/services';

const {
  api,
  constants: { paths },
} = wordpress;

export const getFaqs = async (): Promise<ApiResponse<{ data: any[] }>> => {
  try {
    const response = await api.get(paths.helpCenter);
    return {
      success: true,
      response: {
        data: response.data,
      },
    };
  } catch (err) {
    return handleError(err);
  }
};
