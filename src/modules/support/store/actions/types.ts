import { NAME } from '../../constants';

import shared from '../../../shared';

export default shared.helpers.createActionTypes(NAME, ['SOME_TYPE']);
