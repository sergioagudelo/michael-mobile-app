import React from 'react';
import { SafeAreaView, ScrollView } from 'react-native';
import { Text, withStyles, ThemedComponentProps, Layout } from 'react-native-ui-kitten';

// Modules
import shared from '../../shared';
import profile from '../../profile';

// Components
const { VirtualCard } = shared.components;
const { CreditScoreStats, CreditScoreChart } = profile.components;

type CreditScoreComponentProps = ThemedComponentProps;

const CreditScoreComponent = ({ themedStyle }: CreditScoreComponentProps) => {
  const [creditScore] = React.useState(720);

  return (
    <SafeAreaView style={themedStyle.screen} {...shared.helpers.setTestID('CreditScoreScreen')}>
      <ScrollView contentContainerStyle={themedStyle.body}>
        <Text category="h1" status="accent" style={themedStyle.centerText}>
          Credit Score
        </Text>
        <Text
          category="metatext"
          style={[themedStyle.centerText, themedStyle.uppercase, themedStyle.subtitle]}
        >
          Your financial status
        </Text>

        <Layout style={[themedStyle.item, themedStyle.scoreInfo]}>
          <CreditScoreStats score={creditScore} />
        </Layout>

        <Text category="h2" status="accent" style={[themedStyle.centerText, themedStyle.item]}>
          Credit History
        </Text>

        <Layout style={[themedStyle.item, themedStyle.scoreChart]}>
          <CreditScoreChart chart={profile.utils.testData.chartData} />
        </Layout>

        <VirtualCard dismissible={false} />
      </ScrollView>
    </SafeAreaView>
  );
};

const CreditScore = withStyles(CreditScoreComponent, theme => ({
  screen: {
    flex: 1,
  },
  body: {
    flexGrow: 1,
  },
  centerText: {
    textAlign: 'center',
  },
  uppercase: {
    textTransform: 'uppercase',
  },
  subtitle: {
    paddingTop: 10,
  },
  item: {
    marginVertical: 14,
    marginHorizontal: 30,
  },
  scoreInfo: {
    marginTop: 28,
  },
  scoreChart: {
    alignSelf: 'center',
    padding: 13,
    minHeight: 200,
    flexDirection: 'row',
    borderColor: theme['color-basic-400'],
    borderWidth: 1,
    elevation: 2,
    ...shared.helpers.shadow(2),
  },
}));

export default CreditScore;
