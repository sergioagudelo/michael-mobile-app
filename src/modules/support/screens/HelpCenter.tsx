/* eslint-disable global-require */
import React, { useState } from 'react';
import { SafeAreaView, ScrollView } from 'react-native';
import { Text, withStyles, ThemedComponentProps, Layout } from 'react-native-ui-kitten';
import { NavigationStackScreenProps } from 'react-navigation-stack';

// Config
import config from '../../../config';

import * as mailService from '../../notifications/services/mail';

// Modules
import shared from '../../shared';

// TestData
import { EmailComposerFormValues } from '../utils';

import { FAQ } from '../containers';
// Components
import { EmailUs } from '../components';

const { VerticalCardButton } = shared.components;

type HelpCenterProps = NavigationStackScreenProps & ThemedComponentProps;

const HelpCenter = ({ themedStyle }: HelpCenterProps) => {
  const [showEmailModal, setShowEmailModal] = useState(false);

  const handleComposeEmail = async (
    overDueDays: number,
    emailFormValues: EmailComposerFormValues,
    contractId: string,
  ) => {
    const result = await mailService.composeMail(overDueDays, emailFormValues, contractId);
    return !!result;
  };

  return (
    <SafeAreaView style={themedStyle.screen} {...shared.helpers.setTestID('HelpCenterScreen')}>
      <EmailUs
        notification={null}
        visible={showEmailModal}
        onDismiss={() => setShowEmailModal(false)}
        onSubmit={async (
          days: number,
          emailFormValues: EmailComposerFormValues,
          contractId: string,
        ) => {
          const success = await handleComposeEmail(days, emailFormValues, contractId);
          if (success) {
            setShowEmailModal(false);
          }
          return success;
        }}
      />

      <ScrollView contentContainerStyle={themedStyle.body}>
        <Text category="h1" status="accent" style={themedStyle.centerText}>
          Help Center
        </Text>
        <Text
          category="metatext"
          style={[themedStyle.centerText, themedStyle.uppercase, themedStyle.subtitle]}
        >
          Need to get in touch?
        </Text>

        <Layout style={[themedStyle.item, themedStyle.contactActions]}>
          <VerticalCardButton
            icon={require('../../../img/icons/ico-call-us.png')}
            onPress={() => {
              shared.helpers.callNumber(config.contactPhone);
            }}
            containerStyle={themedStyle.action}
            captionContainerStyle={themedStyle.actionCaption}
          >
            Call Us
          </VerticalCardButton>
          <VerticalCardButton
            icon={require('../../../img/icons/ico-email-us.png')}
            onPress={() => setShowEmailModal(true)}
            containerStyle={themedStyle.action}
            captionContainerStyle={themedStyle.actionCaption}
          >
            Email Us
          </VerticalCardButton>
          {/* <VerticalCardButton
            icon={require('../../../img/icons/ico-chat.png')}
            onPress={() => navigation.navigate('ChatSupport')}
            containerStyle={themedStyle.action}
            captionContainerStyle={themedStyle.actionCaption}
            disabled
          >
            Chat Now
          </VerticalCardButton> */}
        </Layout>

        <Layout style={themedStyle.item}>
          <FAQ />
        </Layout>
      </ScrollView>
    </SafeAreaView>
  );
};

export default withStyles(HelpCenter, () => ({
  screen: {
    flex: 1,
  },
  body: {
    flexGrow: 1,
  },
  centerText: {
    textAlign: 'center',
  },
  uppercase: {
    textTransform: 'uppercase',
  },
  subtitle: {
    paddingTop: 10,
  },
  item: {
    marginVertical: 14,
    marginHorizontal: 30,
  },
  action: {
    width: 90,
    marginHorizontal: 5,
  },
  contactActions: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
}));
