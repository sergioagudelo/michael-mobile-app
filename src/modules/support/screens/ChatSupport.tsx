import React, { useState } from 'react';
import { SafeAreaView, ImageBackground, StatusBar, View, Platform } from 'react-native';
import { withStyles, ThemedComponentProps, Text, Button } from 'react-native-ui-kitten';
import {
  GiftedChat,
  IChatMessage,
  Bubble,
  BubbleProps,
  Message,
  MessageProps,
  DayProps,
  Day,
  TimeProps,
  Time,
  Avatar,
  AvatarProps,
  InputToolbarProps,
  InputToolbar,
} from 'react-native-gifted-chat';
import Color from 'color';
import { NavigationStackScreenProps } from 'react-navigation-stack';

// Types
import { testData } from '../../shared/utils';

// Modules
import shared from '../../shared';

// Components
const { Dashed } = shared.components;

type ChatSupportProps = {} & NavigationStackScreenProps & ThemedComponentProps;

const ChatSupport = ({ navigation, theme, themedStyle }: ChatSupportProps) => {
  const [messages, setMessages] = useState<IChatMessage[]>([
    {
      _id: 1,
      text: `Hello! I’m Alejandro.\nWhat can I do for you today ${testData.userInfo.firstName}?`,
      createdAt: new Date(),
      user: {
        _id: 2,
        name: 'Alejandro',
        avatar: 'lp_logotopbar',
      },
    },
  ]);

  const onSend = (newMessages: IChatMessage[] = []) => {
    setMessages(previousMessages => GiftedChat.append(previousMessages, newMessages));
  };

  const renderAvatar = (props: AvatarProps<IChatMessage>) => {
    const containerStyle: AvatarProps<IChatMessage>['containerStyle'] = {
      left: themedStyle.avatarLeftContainer,
      right: themedStyle.avatarRightContainer,
    };

    const imageStyle: AvatarProps<IChatMessage>['imageStyle'] = {
      left: themedStyle.avatarLeft,
      right: themedStyle.avatarRight,
    };

    return <Avatar {...props} containerStyle={containerStyle} imageStyle={imageStyle} />;
  };

  const renderMessage = (props: MessageProps<IChatMessage>) => {
    const containerStyle: MessageProps<IChatMessage>['containerStyle'] = {
      left: themedStyle.messageLeftContainer,
      right: themedStyle.messageRightContainer,
    };

    return <Message {...props} containerStyle={containerStyle} />;
  };

  const renderDay = (props: DayProps<IChatMessage>) => {
    const dateFormat = 'MMM D | h:mm A';
    const textStyle: DayProps<IChatMessage>['textStyle'] = themedStyle.chatDayIndicator;

    return <Day {...props} dateFormat={dateFormat} textStyle={textStyle} />;
  };

  const renderBubble = (props: BubbleProps<IChatMessage>) => {
    const wrapperStyle: BubbleProps<IChatMessage>['wrapperStyle'] = {
      left: themedStyle.bubbleLeftWrapper,
      right: themedStyle.bubbleRightWrapper,
    };

    const textStyle: BubbleProps<IChatMessage>['textStyle'] = {
      left: themedStyle.bubbleLeftText,
      right: themedStyle.bubbleRightText,
    };

    return <Bubble {...props} wrapperStyle={wrapperStyle} textStyle={textStyle} />;
  };

  const renderTime = (props: TimeProps<IChatMessage>) => {
    const timeTextStyle: TimeProps<IChatMessage>['timeTextStyle'] = {
      left: themedStyle.textTimeLeft,
      right: themedStyle.textTimeRight,
    };

    return <Time {...props} timeTextStyle={timeTextStyle} />;
  };

  const renderInputToolbar = (props: InputToolbarProps) => {
    const containerStyle: InputToolbarProps['containerStyle'] = themedStyle.inputToolbarContainer;

    const primaryStyle: InputToolbarProps['primaryStyle'] = themedStyle.inputToolbarPrimary;

    return <InputToolbar {...props} containerStyle={containerStyle} primaryStyle={primaryStyle} />;
  };

  const renderChatFooter = () => {
    return (
      <View style={themedStyle.exitButtonContainer}>
        <Button
          appearance="ghost"
          onPress={() => navigation.goBack()}
          size="small"
          textStyle={[themedStyle.uppercase, themedStyle.exitButton]}
        >
          Exit Chat
        </Button>
      </View>
    );
  };

  return (
    <SafeAreaView style={themedStyle.screen} {...shared.helpers.setTestID('ChatSupportScreen')}>
      <StatusBar backgroundColor={theme['color-primary-900']} barStyle="light-content" />
      <ImageBackground source={{ uri: 'splash_background' }} style={themedStyle.chatBackground}>
        <View style={themedStyle.chatHeader}>
          <Text appearance="alternative" category="h3">
            Welcome to LendingPoint’s chat
          </Text>
          <Dashed customStyle={themedStyle.separator} />
        </View>

        <GiftedChat
          messages={messages}
          onSend={onSend}
          user={{
            _id: testData.userInfo.id,
          }}
          placeholder="Ask me anything..."
          keyboardShouldPersistTaps={Platform.select({
            ios: 'never',
            android: 'handled',
          })}
          renderAvatarOnTop
          renderAvatar={renderAvatar}
          renderMessage={renderMessage}
          renderDay={renderDay}
          renderBubble={renderBubble}
          renderTime={renderTime}
          renderChatFooter={renderChatFooter}
          renderInputToolbar={renderInputToolbar}
        />
      </ImageBackground>
    </SafeAreaView>
  );
};

export default withStyles(ChatSupport, theme => ({
  uppercase: {
    textTransform: 'uppercase',
  },
  screen: {
    flex: 1,
    backgroundColor: theme['color-primary-900'],
  },
  chatBackground: {
    width: '100%',
    height: '100%',
    paddingVertical: 20,
  },
  chatHeader: {
    alignSelf: 'center',
  },
  separator: {
    marginVertical: 25,
  },
  avatarLeftContainer: {
    marginLeft: 8,
  },
  avatarRightContainer: {
    marginLeft: 8,
  },
  avatarLeft: {
    width: 50,
    height: 50,
    resizeMode: 'contain',
    borderRadius: 0,
    overflow: 'visible',
    tintColor: theme['color-basic-100'],
  },
  avatarRight: {
    width: 50,
    height: 50,
    resizeMode: 'contain',
    borderRadius: 0,
    overflow: 'visible',
  },
  messageLeftContainer: {
    marginVertical: 10,
  },
  messageRightContainer: {
    marginVertical: 10,
  },
  chatDayIndicator: {
    fontFamily: 'FiraSans-Medium',
    fontSize: 12,
    lineHeight: 15,
    fontWeight: '500',
    color: theme['color-basic-100'],
    textTransform: 'uppercase',
  },
  bubbleLeftWrapper: {
    backgroundColor: 'transparent',
    borderWidth: 2,
    borderColor: theme['color-basic-100'],
    borderRadius: 6,
    padding: 10,
  },
  bubbleRightWrapper: {
    borderRadius: 6,
    backgroundColor: Color(theme['color-basic-100'])
      .alpha(0.9)
      .rgb()
      .string(),
    padding: 10,
  },
  bubbleLeftText: {
    fontFamily: 'FiraSans-Regular',
    fontSize: 14,
    lineHeight: 17,
    fontWeight: '400',
    letterSpacing: 0.3,
    color: theme['color-basic-100'],
  },
  bubbleRightText: {
    fontFamily: 'FiraSans-Regular',
    fontSize: 14,
    lineHeight: 17,
    fontWeight: '400',
    letterSpacing: 0.3,
    color: theme['color-primary-700'],
  },
  textTimeLeft: {
    fontFamily: 'FiraSans-Medium',
    fontSize: 12,
    lineHeight: 15,
    fontWeight: '500',
    color: theme['color-basic-100'],
  },
  textTimeRight: {
    fontFamily: 'FiraSans-Medium',
    fontSize: 12,
    lineHeight: 15,
    fontWeight: '500',
    color: theme['color-basic-700'],
  },
  inputToolbarContainer: {
    backgroundColor: 'transparent',
    borderTopWidth: 0,
    borderTopColor: 'transparent',
    marginHorizontal: 30,
  },
  inputToolbarPrimary: {
    borderWidth: 2,
    borderColor: theme['color-primary-500'],
    backgroundColor: theme['color-basic-100'],
  },
  exitButtonContainer: {
    alignItems: 'flex-end',
    marginRight: 10,
  },
  exitButton: {
    color: theme['color-basic-100'],
  },
}));
