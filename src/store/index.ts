// ? Redux + Typescript @see https://joshuaavalon.io/create-type-safe-react-redux-store-with-typescript
import { applyMiddleware, compose, createStore } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';
import thunk from 'redux-thunk';

import Reactotron from '../config/ReactotronConfig';

import rootReducer from './rootReducer';

const store = createStore(
  persistReducer({key: 'root', storage: AsyncStorage}, rootReducer),
  compose(
    applyMiddleware(thunk),
    Reactotron.createEnhancer(),
  ),
);

const persistor = persistStore(store);

export { store, persistor };
