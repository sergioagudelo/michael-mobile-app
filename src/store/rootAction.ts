import shared from '../modules/shared';
import loans from '../modules/loans';
import profile from '../modules/profile';
import auth from '../modules/auth';
import notifications from '../modules/notifications';
import refinance from '../modules/refinance';

export default {
  shared: shared.store.actions.actions,
  loans: loans.store.actions.actions,
  profile: profile.store.actions.actions,
  auth: auth.store.actions.actions,
  notifications: notifications.store.actions.actions,
  refinance: refinance.store.actions.actions,
};
