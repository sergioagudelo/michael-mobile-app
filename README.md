# MobileApp

## Repository and Environments
### 1. dev
Only for each developer
Fastlane command only for running

### 2. QA
Stable work from developers
No build should be promoted from internal track up to staging track,
Fastlane command for building & uploading, using prod schema i.e. disabling warnings, console logs, etc.
staging branch will be linked into the **staging** android's closed track/testflight group.
This staging distribution stage will include whole team including client(for early demo)
Once we got client's approval we will fork a **release/[name]** branch from master and merge approved features into it. After merging into master, feature branch should be deleted.

### 3. UAT
UAT testing before release
No build should be promoted from staging track up to pre-prod track
Fastlane command for building & uploading using prod schema.
master branch will be linked into the **pre-prod** android's closed track/testflight group.
This pre-prod distribution stage will include UAT team only.
Once we got UAT approval we must PROMOTE ?this build into production.

### 4. production
User consumption
No build will be uploaded only promoted builds from pre-prod will be accepted
No Fastlane command  ?   will be used since

