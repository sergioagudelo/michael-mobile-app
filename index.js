import 'react-native-gesture-handler';
/**
 * @format
 */

// ? React + Typescript guide @see https://github.com/piotrwitek/react-redux-typescript-guide
import { AppRegistry, YellowBox } from 'react-native';
import messaging from '@react-native-firebase/messaging';
import App from './src/App';
import { name as appName } from './app.json';

// Register background handler
messaging().setBackgroundMessageHandler(async remoteMessage => {
  console.log('Message handled in the background!', remoteMessage);
});

YellowBox.ignoreWarnings([
  '`-[RCTRootView cancelTouches]` is deprecated and will be deleted soon.', // https://github.com/kmagiera/react-native-gesture-handler/issues/746
]);

if (__DEV__) {
  // import('./src/config/ReactotronConfig').then(() => console.log('Reactotron Configured'));
  import('./src/config/ReactotronConfig').then(() => null);
}

AppRegistry.registerComponent(appName, () => App);
